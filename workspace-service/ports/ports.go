package ports

import (
	"context"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/workspace-service/types"
	"sync"
)

// IncomingQueryPort is an interface for a query port.
type IncomingQueryPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingQueryHandlers)
}

// IncomingQueryHandlers represents the handlers for all query operation this service supports, this is implemented by object in `domain` package
type IncomingQueryHandlers interface {
	List(ctx context.Context, session cacao_common_service.Session) cacao_common_service.WorkspaceListModel
	Get(context.Context, cacao_common_service.WorkspaceModel) cacao_common_service.WorkspaceModel
}

// IncomingEventPort is an interface for an event port.
type IncomingEventPort interface {
	Start(ctx context.Context, wg *sync.WaitGroup) error
	SetHandlers(handlers IncomingEventHandlers)
}

// IncomingEventHandlers represents the handlers for all event operations this service supports, this is implemented by object in `domain` package
type IncomingEventHandlers interface {
	Create(context.Context, cacao_common_service.WorkspaceModel, OutgoingEventPort)
	Update(context.Context, cacao_common_service.WorkspaceModel, OutgoingEventPort)
	Delete(context.Context, cacao_common_service.WorkspaceModel, OutgoingEventPort)
}

// OutgoingEventPort represents all events this service will publish, implementation of IncomingEventPort will construct this and pass to methods of IncomingEventHandlers.
type OutgoingEventPort interface {
	WorkspaceCreatedEvent(cacao_common_service.WorkspaceModel)
	WorkspaceCreateFailedEvent(cacao_common_service.WorkspaceModel)
	WorkspaceUpdatedEvent(cacao_common_service.WorkspaceModel)
	WorkspaceUpdateFailedEvent(cacao_common_service.WorkspaceModel)
	WorkspaceDeletedEvent(cacao_common_service.WorkspaceModel)
	WorkspaceDeleteFailedEvent(cacao_common_service.WorkspaceModel)
}

// PersistentStoragePort is an interface for Persistent Storage.
type PersistentStoragePort interface {
	Init(config *types.Config) error
	List(ctx context.Context, user string) ([]types.Workspace, error)
	Get(ctx context.Context, user string, workspaceID cacao_common.ID) (types.Workspace, error)
	Create(ctx context.Context, workspace types.Workspace) error
	Update(ctx context.Context, workspace types.Workspace, updateFieldNames []string) error
	Delete(ctx context.Context, user string, workspaceID cacao_common.ID) error
}
