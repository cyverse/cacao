package types

import (
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
)

// Config ...
type Config struct {
	LogLevel   string
	OnlyAdmins bool // set to true to restrict crud events to admins
	// NATS
	Messaging messaging2.NatsStanMsgConfig

	// MongoDB
	MongoDBConfig                 db.MongoDBConfig
	ProviderMongoDBCollectionName string
}

// ProcessDefaults will take a Config object and process the config object further, including
// populating any null values
func (c *Config) ProcessDefaults() {
	if c.LogLevel == "" {
		c.LogLevel = "debug"
	}

	// NATS
	if c.Messaging.ClientID == "" {
		c.Messaging.ClientID = DefaultNatsClientID
	}

	// MongoDB
	if c.MongoDBConfig.URL == "" {
		c.MongoDBConfig.URL = DefaultMongoDBURL
	}

	if c.MongoDBConfig.DBName == "" {
		c.MongoDBConfig.DBName = DefaultMongoDBName
	}

	if c.ProviderMongoDBCollectionName == "" {
		c.ProviderMongoDBCollectionName = DefaultProviderMongoDBCollectionName
	}
}

// Override ...
func (c *Config) Override() {
	c.Messaging.WildcardSubject = DefaultNatsWildcardSubject
	c.Messaging.DurableName = DefaultNatsDurableName
	c.Messaging.QueueGroup = DefaultNatsQueueGroup
}
