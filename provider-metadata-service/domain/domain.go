package domain

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/provider-metadata-service/ports"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"sync"
	"time"
)

// Domain is the base struct for the domain service
type Domain struct {
	conf          *types.Config
	QueriesIn     ports.IncomingQueryPort
	EventsIn      ports.IncomingEventPort
	Storage       ports.ProviderStoragePort
	UserSvc       ports.UserService
	queryHandlers QueryHandlers
	eventHandlers EventHandlers
}

// Init ...
func (d *Domain) Init(conf *types.Config) error {
	d.conf = conf

	err := d.Storage.Init(conf)
	if err != nil {
		return err
	}
	err = d.UserSvc.Init(conf)
	if err != nil {
		return err
	}

	d.queryHandlers = QueryHandlers{
		userSvc: d.UserSvc,
		storage: d.Storage,
	}
	d.QueriesIn.SetHandlers(&d.queryHandlers)
	d.eventHandlers = EventHandlers{
		userSvc:     d.UserSvc,
		storage:     d.Storage,
		timeSrc:     time.Now,
		idGenerator: providerIDGenerator,
	}
	d.EventsIn.SetHandlers(&d.eventHandlers)
	return nil
}

// Start ...
func (d *Domain) Start(ctx context.Context) error {
	var wg sync.WaitGroup
	wg.Add(1)
	err := d.QueriesIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Error()
		return err
	}

	wg.Add(1)
	err = d.EventsIn.Start(ctx, &wg)
	if err != nil {
		log.WithError(err).Error()
		return err
	}

	wg.Wait()
	return nil
}
