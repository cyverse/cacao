package main

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/provider-metadata-service/adapters"
	"gitlab.com/cyverse/cacao/provider-metadata-service/domain"
	"gitlab.com/cyverse/cacao/provider-metadata-service/types"
	"io"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.WithError(err).Panic()
	}
	config.Override()
	config.ProcessDefaults()

	// set the log level
	lvl, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.WithError(err).Panic()
	}
	log.SetLevel(lvl)

	natsConn, err := config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&natsConn)
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(config.Messaging)
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&stanConn)

	eventAdapter := adapters.NewEventAdapter(&stanConn)
	storage := &adapters.MongoAdapter{}
	defer logCloserError(storage)

	var svc = domain.Domain{
		QueriesIn: adapters.NewQueryAdapter(&natsConn),
		EventsIn:  eventAdapter,
		Storage:   storage,
		UserSvc:   adapters.NewUserSvc(&natsConn),
	}
	err = svc.Init(&config)
	if err != nil {
		log.WithError(err).Panic("fail to init service domain")
		return
	}
	appCtx, cancelAppCtx := context.WithCancel(context.Background())
	defer cancelAppCtx()
	common.CancelWhenSignaled(cancelAppCtx)

	err = svc.Start(appCtx)
	if err != nil {
		log.WithError(err).Panic()
		return
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
