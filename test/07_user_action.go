package test

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

var userActionID cacao_common.ID

var tests07UserAction = []testMap{
	{"CreateUserAction": testCreateUserAction},
	{"ListUserActions": testListUserActions},
	{"GetUserAction": testGetUserAction},
	{"UpdateUserAction": testUpdateUserAction},
	{"PatchUserAction": testPatchUserAction},
	{"DeleteUserAction": testDeleteUserAction},
}

func testCreateUserAction(t *testing.T) {
	userActionID = cacao_common_service.NewUserActionID()

	data := fmt.Sprintf(`{"id":"%s","name":"test_user_action_name","description":"test_user_action","public":false,"type":"instance_execution","action":{"method":"script","source_type":"url","data_type":"text/x-shellscript","data":"https://www.shellscript.sh/eg/first.sh.txt"}}`, userActionID)

	req, err := newRequest("POST", "/useractions", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testGetUserAction(t *testing.T) {
	req, err := newRequest("GET", "/useractions/"+userActionID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.UserAction
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, userActionID, result.ID)
}

func testListUserActions(t *testing.T) {
	req, err := newRequest("GET", "/useractions", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := ioutil.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result []cacao_common_http.UserAction
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.GreaterOrEqual(t, len(result), 1)
}

func testUpdateUserAction(t *testing.T) {
	// update
	data := `{"name":"test_user_action_name","description":"test_user_action_updated","public":true,"type":"instance_execution","action":{"method":"script","source_type":"url","data_type":"text/x-shellscript","data":"https://www.shellscript.sh/eg/updated.sh.txt"}}`

	reqUpdate, err := newRequest("PUT", "/useractions/"+userActionID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/useractions/"+userActionID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.UserAction
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, userActionID, result.ID)
	assert.Equal(t, "test_user_action_updated", result.Description)
	assert.Equal(t, true, result.Public)
	assert.Equal(t, cacao_common_service.UserActionTypeInstanceExecution, result.Type)
	assert.Equal(t, true, result.Action.IsScriptAction())
	assert.Equal(t, "https://www.shellscript.sh/eg/updated.sh.txt", result.Action.GetScriptAction().Data)
}

func testPatchUserAction(t *testing.T) {
	// patch
	data := `{"description":"test_user_action_patched"}`

	reqUpdate, err := newRequest("PATCH", "/useractions/"+userActionID.String(), data, userToken)
	assert.NoError(t, err)
	respUpdate, err := (&http.Client{}).Do(reqUpdate)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, respUpdate.StatusCode)

	// get
	reqGet, err := newRequest("GET", "/useractions/"+userActionID.String(), "", userToken)
	assert.NoError(t, err)
	respGet, err := (&http.Client{}).Do(reqGet)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, respGet.StatusCode)
	respBody, err := ioutil.ReadAll(respGet.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)

	var result cacao_common_http.UserAction
	err = json.Unmarshal(respBody, &result)
	assert.NoError(t, err)
	assert.Equal(t, userActionID, result.ID)
	assert.Equal(t, "test_user_action_patched", result.Description)
}

func testDeleteUserAction(t *testing.T) {
	req, err := newRequest("DELETE", "/useractions/"+userActionID.String(), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}
