package test

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"io"
	"net/http"
	"testing"
	"time"
)

var tokenID cacao_common.ID

var tests08Token = []testMap{
	{"GetTokenTypes": testGetTokenTypes},
	{"CreateToken": testCreateToken},
	{"ListTokens": testListTokens},
	{"GetToken": testGetToken},
	{"UpdateToken": testUpdateToken},
	{"UpdateTokenFields": testUpdateTokenFields},
	{"RevokeToken": testRevokeToken},
	{"DeleteToken": testDeleteToken},
}

func testGetTokenTypes(t *testing.T) {
	req, err := newRequest("GET", "/tokens/types", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resp.StatusCode)
	respBody, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testCreateToken(t *testing.T) {
	expiration := time.Now().Add(time.Hour * 24)
	data := fmt.Sprintf(`{"name": "testtoken1", "type": "personal", "scopes": "api", "public": false, "expiration": "%s"}`, expiration.String())
	req, err := newRequest("POST", "tokens", data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	// get token from response
	var token service.TokenCreateModel
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(&token)
	assert.NoError(t, err)
	tokenID = token.ID
}

func testGetToken(t *testing.T) {
	req, err := newRequest("GET", fmt.Sprintf("/tokens/%s", tokenID.String()), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testListTokens(t *testing.T) {
	req, err := newRequest("GET", "/tokens", "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testUpdateToken(t *testing.T) {
	data := fmt.Sprintf(`{"name": "updatedname", "description": "this is a new description"}`)
	req, err := newRequest("PUT", fmt.Sprintf("/tokens/%s", tokenID.String()), data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testUpdateTokenFields(t *testing.T) {
	data := fmt.Sprintf(`{"name": "updatedname", "description": "this is a new description"}`)
	req, err := newRequest("PATCH", fmt.Sprintf("/tokens/%s", tokenID.String()), data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
	respBody, err := io.ReadAll(resp.Body)
	assert.NoError(t, err)
	assert.Greater(t, len(respBody), 0)
}

func testRevokeToken(t *testing.T) {
	data := fmt.Sprintf(`{"id": "%s"}`, tokenID.String())
	req, err := newRequest("POST", fmt.Sprintf("/tokens/%s/revoke", tokenID.String()), data, userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}

func testDeleteToken(t *testing.T) {
	req, err := newRequest("DELETE", fmt.Sprintf("/tokens/%s", tokenID.String()), "", userToken)
	assert.NoError(t, err)
	resp, err := (&http.Client{}).Do(req)
	assert.NoError(t, err)
	assert.Equal(t, http.StatusAccepted, resp.StatusCode)
}
