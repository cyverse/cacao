package domain

import (
	"context"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao/js2allocation-service/ports"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

// Domain is the base struct for the domain service
type Domain struct {
	config  *types.Config
	Storage ports.PersistentStoragePort
	QueryIn ports.IncomingQueryPort
	JS2API  ports.JS2APIPort
	TimeSrc func() time.Time
}

// Init initializes all the specified adapters
func (d *Domain) Init(config *types.Config) {
	d.config = config
}

// Start will start the domain object, and in turn start all the async adapters
func (d *Domain) Start(ctx context.Context) error {
	// using waitgroups to block termination gracefully
	var wg sync.WaitGroup

	wg.Add(1)
	err := d.QueryIn.Start(ctx, d, &wg)
	if err != nil {
		return err
	}

	wg.Wait()
	return nil
}
