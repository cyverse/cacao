# JS2Allocation Service

The JS2Allocation Service is responsible for retrieving [JetStream2](https://jetstream-cloud.org) Allocation information.

## Configuration

The service is configured through a set of environment variables.

| env name                                    | description                                                        | default                   |
|---------------------------------------------|--------------------------------------------------------------------|---------------------------|
| `UserCacheExpirationTime`                   | expiration duration of the cache for user records (legacy TAS api) |
| `ProjectCacheExpirationTime`                | expiration duration of the cache for project records               |
| `JS2AllocationMongoDBUserCollectionName`    | -                                                                  |
| `JS2AllocationMongoDBProjectCollectionName` | -                                                                  |
| `NATS_URL`                                  | -                                                                  | nats://nats:4222          |
| `NATS_QUEUE_GROUP`                          | -                                                                  |                           |
| `NATS_WILDCARD_SUBJECT`                     | -                                                                  | cyverse.>                 |
| `NATS_CLIENT_ID`                            | -                                                                  |                           |
| `NATS_MAX_RECONNECTS`                       | -                                                                  | -1                        |
| `NATS_RECONNECT_WAIT`                       | -                                                                  | -1                        |
| `NATS_REQUEST_TIMEOUT`                      | -                                                                  | -1                        |
| `MONGODB_URL`                               | -                                                                  | mongodb://localhost:27017 |
| `MONGODB_DB_NAME`                           | -                                                                  |                           |

## TAS API (Legacy)
Jetstream 2 has switched to its own allocation API, TAS adapter is kept for now. After we changed the schema(NATS level) of the allocation, we should remove it.
`TACC_API_USER` and `TACC_API_PASS` are used by TAS api adapter, but not the new adapter for JS2 allocation API.
