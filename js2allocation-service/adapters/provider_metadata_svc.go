package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
	"net/url"
)

// ProviderMetadataService provides access to provider metadata service
type ProviderMetadataService interface {
	FindJS2Provider(session types.Session) ([]service.ProviderModel, error)
}

// ProviderMetadataSvc is a client to access provider metadata service, it implements ProviderMetadataService
type ProviderMetadataSvc struct {
	svcClient service.ProviderClient
}

// NewProviderMetadataService ...
func NewProviderMetadataService(queryConn messaging2.QueryConnection) ProviderMetadataService {
	svcClient, err := service.NewNatsProviderClientFromConn(queryConn, nil)
	if err != nil {
		log.WithError(err).Panic()
		return nil
	}
	svc := ProviderMetadataSvc{
		svcClient: svcClient,
	}
	return svc
}

// FindJS2Provider finds the Jetstream 2 provider, there should just be at most one provider for Jetstream 2, but return an array just in case.
func (svc ProviderMetadataSvc) FindJS2Provider(session types.Session) ([]service.ProviderModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ProviderMetadataSvc.FindJS2Provider",
	})
	providerList, err := svc.listProviders(context.TODO(), session)
	if err != nil {
		logger.WithError(err).Error("fail to list providers")
		return nil, err
	}
	return svc.filterJS2Provider(logger, providerList), nil
}

func (svc ProviderMetadataSvc) filterJS2Provider(logger *log.Entry, providerList []service.ProviderModel) []service.ProviderModel {
	var result = make([]service.ProviderModel, 0)
	for _, prov := range providerList {
		if !svc.isOpenStackProvider(prov) {
			// skip non-openstack type
			continue
		}
		if svc.isJS2Provider(&prov) {
			result = append(result, prov)
		}
	}
	logger.WithFields(log.Fields{
		"before": len(providerList),
		"after":  len(result),
	}).Info("filter js2 provider")
	return result
}

// JS2 provider must have OS_AUTH_URL on hostname js2.jetstream-cloud.org
func (svc ProviderMetadataSvc) isJS2Provider(provider *service.ProviderModel) bool {
	metadata := provider.Metadata
	if metadata == nil {
		return false
	}
	authURLRaw, ok := metadata["OS_AUTH_URL"]
	if !ok {
		return false
	}
	authURL, ok := authURLRaw.(string)
	if !ok {
		return false
	}
	parsed, err := url.ParseRequestURI(authURL)
	if err != nil {
		return false
	}
	return parsed.Hostname() == "js2.jetstream-cloud.org"
}

func (svc ProviderMetadataSvc) isOpenStackProvider(provider service.ProviderModel) bool {
	return provider.Type == "openstack"
}

func (svc ProviderMetadataSvc) listProviders(ctx context.Context, session types.Session) ([]service.ProviderModel, error) {
	return svc.svcClient.List(ctx, session.ToServiceActor())
}

// MockProviderMetadataSvcClient is used to generate mocks for service.ProviderClient, this interface is needed to generate the mocks locally in this repo
type MockProviderMetadataSvcClient interface {
	service.ProviderClient
}
