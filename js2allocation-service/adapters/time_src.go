package adapters

import "time"

// TimeSource is a source of current time.
// This abstract away fetching the current time, makes it easier to write tests
type TimeSource interface {
	Now() time.Time
}

// UTCTime implements TimeSource
type UTCTime struct{}

// Now returns the current UTC time
func (utc UTCTime) Now() time.Time {
	return time.Now().UTC()
}

// MockTimeSrc is a mocked time source
type MockTimeSrc struct {
	MockTime time.Time
}

// Now return mocked value
func (t MockTimeSrc) Now() time.Time {
	return t.MockTime
}
