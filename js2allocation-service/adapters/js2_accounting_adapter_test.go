package adapters

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/js2allocation-service/adapters/mocks"
	"gitlab.com/cyverse/cacao/js2allocation-service/types"
)

func TestJS2AccountingAPI_ListProjects(t *testing.T) {
	type mockObjs struct {
		providerSvc       *mocks.ProviderMetadataService
		credSvc           *mocks.CredentialService
		openstackSvc      *mocks.ProviderOpenStackService
		timeSrc           MockTimeSrc
		accountingRESTAPI *mocks.AccountingRESTAPI
	}
	type args struct {
		user string
	}

	var testSession = types.Session{
		SessionActor:    "actor-123",
		SessionEmulator: "",
	}
	generateTestProjectID := func() common.ID {
		return common.ID("js2project-aaaaaaaaaaaaaaaaaaaa")
	}
	generateTestAllocationID := func() common.ID {
		return common.ID("js2alloc-aaaaaaaaaaaaaaaaaaaa")
	}

	tests := []struct {
		name     string
		mockObjs mockObjs
		args     args
		want     []types.JS2Project
		wantErr  bool
	}{
		{
			name: "one-js2-provider one-openstack-cred",
			mockObjs: mockObjs{
				providerSvc: func() *mocks.ProviderMetadataService {
					svc := &mocks.ProviderMetadataService{}
					svc.On("FindJS2Provider", testSession).Return([]service.ProviderModel{
						{
							ID:        "provider-d17e30598850n9abikl0",
							Name:      "",
							Type:      "openstack",
							URL:       "",
							CreatedAt: time.Time{},
							UpdatedAt: time.Time{},
							Metadata:  nil,
						},
					}, nil)
					return svc
				}(),
				credSvc: func() *mocks.CredentialService {
					svc := &mocks.CredentialService{}
					svc.On("ListOpenStackCredentials", testSession).Return([]service.CredentialModel{
						{
							Value: "{}",
							Type:  "openstack",
							ID:    "cred-id-1",
							Tags:  map[string]string{"provider-d17e30598850n9abikl0": ""},
						},
					}, nil)
					return svc
				}(),
				openstackSvc: func() *mocks.ProviderOpenStackService {
					svc := &mocks.ProviderOpenStackService{}
					svc.On("FindAccountCatalogEntry", testSession, common.ID("provider-d17e30598850n9abikl0"), "cred-id-1").Return(providers.CatalogEntry{
						Name: "accounting",
						Type: "accounting",
						Endpoints: []providers.CatalogEndpoint{
							{
								ID:        "",
								Interface: "public",
								RegionID:  "region-123",
								URL:       "https://js2.jetstream-cloud.org/foobar",
								Region:    "region-123",
							},
						},
					}, nil)
					svc.On("ObtainToken", testSession, common.ID("provider-d17e30598850n9abikl0"), "cred-id-1").Return(providers.Token{
						Expires:   "",
						ID:        "token-1",
						ProjectID: "project-1",
						UserID:    "user-1",
					}, nil)
					return svc
				}(),
				timeSrc: MockTimeSrc{MockTime: time.Date(2022, 4, 15, 0, 0, 0, 0, time.UTC)},
				accountingRESTAPI: func() *mocks.AccountingRESTAPI {
					api := &mocks.AccountingRESTAPI{}
					api.On("ListAllocation", "https://js2.jetstream-cloud.org/foobar", providers.Token{
						Expires:   "",
						ID:        "token-1",
						ProjectID: "project-1",
						UserID:    "user-1",
					}).Return([]types.NewJS2Allocation{
						{
							Title:                 "title-123",
							Description:           "description-123",
							Abstract:              "abstract-123",
							Resource:              "js2.jetstream-cloud.org",
							ServiceUnitsAllocated: 100,
							ServiceUnitsUsed:      10,
							StartDate:             time.Time{},
							EndDate:               time.Date(3033, 4, 15, 0, 0, 0, 0, time.UTC),
							Active:                1,
						},
						{
							Title:                 "title-123",
							Description:           "description-123",
							Abstract:              "abstract-123",
							Resource:              "js2-foo.jetstream-cloud.org",
							ServiceUnitsAllocated: 100,
							ServiceUnitsUsed:      10,
							StartDate:             time.Time{},
							EndDate:               time.Time{}, // expired
							Active:                1,
						},
						{
							Title:                 "title-123",
							Description:           "description-123",
							Abstract:              "abstract-123",
							Resource:              "js2-bar.jetstream-cloud.org",
							ServiceUnitsAllocated: 100,
							ServiceUnitsUsed:      10,
							StartDate:             time.Time{},
							EndDate:               time.Date(3033, 4, 15, 0, 0, 0, 0, time.UTC),
							Active:                0, // inactive
						},
						{
							Title:                 "title-123",
							Description:           "description-123",
							Abstract:              "abstract-123",
							Resource:              "storage-123.jetstream-cloud.org",
							ServiceUnitsAllocated: 100,
							ServiceUnitsUsed:      10,
							StartDate:             time.Time{},
							EndDate:               time.Date(3033, 4, 15, 0, 0, 0, 0, time.UTC),
							Active:                1,
						},
					}, nil)
					return api
				}(),
			},
			args: args{user: "actor-123"},
			want: []types.JS2Project{
				{
					ID:          "js2project-aaaaaaaaaaaaaaaaaaaa",
					Owner:       "actor-123",
					Title:       "title-123",
					Description: "description-123",
					PI:          types.JS2PI{},
					Allocations: []types.JS2Allocation{
						{
							ID:               "js2alloc-aaaaaaaaaaaaaaaaaaaa",
							Start:            time.Time{},
							End:              time.Date(3033, 4, 15, 0, 0, 0, 0, time.UTC),
							ProjectCode:      "title-123",
							Resource:         "js2.jetstream-cloud.org",
							ComputeAllocated: 100,
							StorageAllocated: 0,
							ComputeUsed:      10,
						},
						{
							ID:               "js2alloc-aaaaaaaaaaaaaaaaaaaa",
							Start:            time.Time{},
							End:              time.Date(3033, 4, 15, 0, 0, 0, 0, time.UTC),
							ProjectCode:      "title-123",
							Resource:         "storage-123.jetstream-cloud.org",
							ComputeAllocated: 0,
							StorageAllocated: 100,
							ComputeUsed:      0,
						},
					},
					RetrivedAt: time.Date(2022, 4, 15, 0, 0, 0, 0, time.UTC),
				},
			},
			wantErr: false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			api := JS2AccountingAPI{
				providerSvc:           tt.mockObjs.providerSvc,
				openstackSvc:          tt.mockObjs.openstackSvc,
				credSvc:               tt.mockObjs.credSvc,
				timeSrc:               tt.mockObjs.timeSrc,
				accountingRESTAPI:     tt.mockObjs.accountingRESTAPI,
				projectIDGenerator:    generateTestProjectID,
				allocationIDGenerator: generateTestAllocationID,
			}
			got, err := api.ListProjects(tt.args.user)
			if tt.wantErr {
				assert.Errorf(t, err, "ListProjects(%v)", tt.args.user)
			} else {
				assert.NoErrorf(t, err, "ListProjects(%v)", tt.args.user)
			}
			assert.Equalf(t, tt.want, got, "ListProjects(%v)", tt.args.user)
		})
	}
}

func TestJS2AccountingAPI_isProjectDisabledError(t *testing.T) {
	type fields struct {
		providerSvc           ProviderMetadataService
		openstackSvc          ProviderOpenStackService
		credSvc               CredentialService
		timeSrc               TimeSource
		accountingRESTAPI     accountingRESTAPI
		projectIDGenerator    func() common.ID
		allocationIDGenerator func() common.ID
	}
	type args struct {
		err error
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name:   "nil error",
			fields: fields{},
			args: args{
				err: nil,
			},
			want: false,
		},
		{
			name:   "empty",
			fields: fields{},
			args: args{
				err: fmt.Errorf(""),
			},
			want: false,
		},
		{
			name:   "foobar",
			fields: fields{},
			args: args{
				err: fmt.Errorf("foobar"),
			},
			want: false,
		},
		{
			name:   "disabled",
			fields: fields{},
			args: args{
				err: fmt.Errorf("disabled"),
			},
			want: true,
		},
		{
			name:   "is disabled",
			fields: fields{},
			args: args{
				err: fmt.Errorf("is disabled"),
			},
			want: true,
		},
		{
			name:   "Unable to validate token because project xxx is disabled",
			fields: fields{},
			args: args{
				err: fmt.Errorf("Unable to validate token because project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa is disabled"),
			},
			want: true,
		},
		{
			name:   "Unable to validate token because project xxx is disabled ...",
			fields: fields{},
			args: args{
				// full error msg
				err: fmt.Errorf("work failed - exit status 1, Unable to validate token because project aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa is disabled (HTTP 404) (Request-ID: req-aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa)"),
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			api := JS2AccountingAPI{
				providerSvc:           tt.fields.providerSvc,
				openstackSvc:          tt.fields.openstackSvc,
				credSvc:               tt.fields.credSvc,
				timeSrc:               tt.fields.timeSrc,
				accountingRESTAPI:     tt.fields.accountingRESTAPI,
				projectIDGenerator:    tt.fields.projectIDGenerator,
				allocationIDGenerator: tt.fields.allocationIDGenerator,
			}
			assert.Equalf(t, tt.want, api.isProjectDisabledError(tt.args.err), "isProjectDisabledError(%v)", tt.args.err)
		})
	}
}
