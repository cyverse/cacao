package adapters

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"gitlab.com/cyverse/cacao/token-service/types"
	"sync"
)

// EventAdapter communicates to IncomingEventPort and implements OutgoingEventPort
type EventAdapter struct {
	handlers ports.IncomingEventHandlers
	// internal
	conn messaging2.EventConnection
}

var _ ports.IncomingEventPort = &EventAdapter{}

// NewEventAdapter creates a new EventAdapter from EventConnection
func NewEventAdapter(conn messaging2.EventConnection) *EventAdapter {
	return &EventAdapter{conn: conn}
}

// Start starts the adapter
func (adapter *EventAdapter) Start(ctx context.Context, wg *sync.WaitGroup, handlers ports.IncomingEventHandlers) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.Start",
	})

	logger.Info("starting EventAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.EventType]messaging2.EventHandlerFunc{
		cacao_common_service.TokenCreateRequestedEvent: handlerWrapper(handlers.Create),
		cacao_common_service.TokenDeleteRequestedEvent: handlerWrapper(handlers.Delete),
		cacao_common_service.TokenUpdateRequestedEvent: handlerWrapper(handlers.Update),
		cacao_common_service.TokenRevokeRequestedEvent: handlerWrapper(handlers.Revoke),
	}, wg)
}

// SetHandlers sets incoming query handlers for the event adapter
func (adapter EventAdapter) SetHandlers(handlers ports.IncomingEventHandlers) {
	adapter.handlers = handlers
}

func handlerWrapper[T any](handler func(T, ports.OutgoingEventPort)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			errorMessage := "unable to unmarshal JSON bytes into TokenModel"
			log.WithField("ceType", event.Type()).WithError(err).Error(errorMessage)
			return err
		}
		handler(request, eventOut{writer: writer})
		return nil
	}
}

type eventOut struct {
	writer messaging2.EventResponseWriter
}

// TokenCreatedEvent publishes cacao_common_service.TokenCreatedEvent
func (e eventOut) TokenCreatedEvent(model cacao_common_service.TokenCreateModel) {
	e.publish(model, cacao_common_service.TokenCreatedEvent)
}

// TokenCreateFailedEvent publishes cacao_common_service.TokenCreateFailedEvent
func (e eventOut) TokenCreateFailedEvent(model cacao_common_service.TokenCreateModel) {
	e.publish(model, cacao_common_service.TokenCreateFailedEvent)
}

// TokenUpdatedEvent publishes cacao_common_service.TokenUpdatedEvent
func (e eventOut) TokenUpdatedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenUpdatedEvent)
}

// TokenUpdateFailedEvent publishes cacao_common_service.TokenUpdateFailedEvent
func (e eventOut) TokenUpdateFailedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenUpdateFailedEvent)
}

// TokenDeletedEvent publishes cacao_common_service.TokenDeletedEvent
func (e eventOut) TokenDeletedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenDeletedEvent)
}

// TokenDeleteFailedEvent publishes cacao_common_service.TokenDeleteFailedEvent
func (e eventOut) TokenDeleteFailedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenDeleteFailedEvent)
}

// TokenRevokedEvent publishes cacao_common_service.TokenRevokedEvent
func (e eventOut) TokenRevokedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenRevokedEvent)
}

// TokenRevokeFailedEvent publishes cacao_common_service.TokenRevokeFailedEvent
func (e eventOut) TokenRevokeFailedEvent(model cacao_common_service.TokenModel) {
	e.publish(model, cacao_common_service.TokenRevokeFailedEvent)
}

func (e eventOut) publish(model interface{}, eventType cacao_common.EventType) {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "eventOut.publish",
	})

	ce, err := messaging2.CreateCloudEventWithAutoSource(model, eventType)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", eventType)
		return
	}
	err = e.writer.Write(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", eventType)
	}
}

var _ ports.OutgoingEventPort = &eventOut{}

// Created publishes cacao_common_service.TokenCreatedEvent
func (adapter *EventAdapter) Created(actor string, emulator string, token types.Token, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.Created",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenCreatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenCreatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenCreatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenCreatedEvent)
	}

	return nil
}

// CreateFailed publishes cacao_common_service.TokenCreateFailedEvent
func (adapter *EventAdapter) CreateFailed(actor string, emulator string, token types.Token, creationError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.CreateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	var cerr cacao_common_service.CacaoError
	if errors.As(creationError, &cerr) {
		session.ServiceError = cerr.GetBase()
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenCreateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenCreateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenCreateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenCreateFailedEvent)
	}

	return nil
}

// Updated publishes cacao_common_service.TokenUpdatedEvent
func (adapter *EventAdapter) Updated(actor string, emulator string, token types.Token, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.Updated",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenUpdatedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenUpdatedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenUpdatedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenUpdatedEvent)
	}

	return nil
}

// UpdateFailed publishes cacao_common_service.TokenUpdateFailedEvent
func (adapter *EventAdapter) UpdateFailed(actor string, emulator string, token types.Token, updateError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.UpdateFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	var cerr cacao_common_service.CacaoError
	if errors.As(updateError, &cerr) {
		session.ServiceError = cerr.GetBase()
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenUpdateFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenUpdateFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenUpdateFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenUpdateFailedEvent)
	}

	return nil

}

// Deleted publishes cacao_common_service.TokenDeletedEvent
func (adapter *EventAdapter) Deleted(actor string, emulator string, token types.Token, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.Deleted",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenDeletedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenDeletedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenDeletedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenDeletedEvent)
	}

	return nil

}

// DeleteFailed publishes cacao_common_service.TokenDeleteFailedEvent
func (adapter *EventAdapter) DeleteFailed(actor string, emulator string, token types.Token, deletionError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.DeleteFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	var cerr cacao_common_service.CacaoError
	if errors.As(deletionError, &cerr) {
		session.ServiceError = cerr.GetBase()
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenDeleteFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenDeleteFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenDeleteFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenDeleteFailedEvent)
	}

	return nil
}

// Revoked publishes cacao_common_service.TokenRevokedEvent
func (adapter *EventAdapter) Revoked(actor string, emulator string, token types.Token, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.Revoked",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenRevokedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenRevokedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenRevokedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenRevokedEvent)
	}

	return nil

}

// RevokeFailed publishes cacao_common_service.TokenRevokeFailedEvent
func (adapter *EventAdapter) RevokeFailed(actor string, emulator string, token types.Token, revokeError error, transactionID cacao_common.TransactionID) error {
	logger := log.WithFields(log.Fields{
		"package":  "token-service.adapters",
		"function": "EventAdapter.RevokeFailed",
	})

	session := cacao_common_service.Session{
		SessionActor:    actor,
		SessionEmulator: emulator,
	}

	var cerr cacao_common_service.CacaoError
	if errors.As(revokeError, &cerr) {
		session.ServiceError = cerr.GetBase()
	}

	model := types.ConvertToModel(session, token)

	ce, err := messaging2.CreateCloudEventWithTransactionID(model, cacao_common_service.TokenRevokeFailedEvent, messaging2.AutoPopulateCloudEventSource, transactionID)
	if err != nil {
		logger.WithError(err).Errorf("failed to create cloudevent %s", cacao_common_service.TokenRevokeFailedEvent)
		return err
	}
	err = adapter.conn.Publish(ce)
	if err != nil {
		logger.WithError(err).Errorf("failed to publish an event %s", cacao_common_service.TokenRevokeFailedEvent)
		return fmt.Errorf("failed to publish an event %s", cacao_common_service.TokenRevokeFailedEvent)
	}

	return nil
}
