package adapters

import (
	"fmt"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/token-service/types"
	"testing"
	"time"
)

func createTestMongoAdapter() *MongoAdapter {
	var config types.Config
	config.ProcessDefaults()

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		log.WithError(err).Fatal("unable to connect to MongoDB")
	}
	mongoAdapter := &MongoAdapter{
		config: &config,
		store:  store,
	}

	return mongoAdapter
}

func TestInitMongoAdapter(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	defer mongoAdapter.Close()
	assert.NotNil(t, mongoAdapter)
	assert.NotEmpty(t, mongoAdapter.store)

}

func TestMongoAdapterList(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	defer mongoAdapter.Close()
	testTime := time.Now().UTC()
	nowPlusDay := testTime.Add(time.Hour * 24)
	nowPlusWeek := testTime.Add(time.Hour * 24 * 7)
	testUser := "test_owner1"

	expectedResults := []types.Token{
		{
			ID:          "0001",
			Owner:       testUser,
			Name:        "test_token1",
			Description: "test_description1",
			Public:      false,
			Expiration:  nowPlusWeek,
			Scopes:      "test_scope1",
			Type:        "personal",
			CreatedAt:   testTime,
			UpdatedAt:   testTime,
		},
		{
			ID:               "0002",
			Owner:            testUser,
			Name:             "test_token2",
			Description:      "test_description2",
			Public:           true,
			Expiration:       nowPlusWeek,
			Start:            &nowPlusDay,
			Scopes:           "test_scope2",
			DelegatedContext: "validdelegatedcontext",
			Type:             "delegated",
			CreatedAt:        testTime,
			UpdatedAt:        testTime,
		},
	}
	err := mongoAdapter.MockList(testUser, expectedResults, nil)
	assert.NoError(t, err)
}

func TestMongoAdapterGet(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	defer mongoAdapter.Close()
	testTime := time.Now().UTC()
	nowPlusDay := testTime.Add(time.Hour * 24)
	nowPlusWeek := testTime.Add(time.Hour * 24 * 7)
	testUser := "test_owner1"
	newXid := xid.New()
	tokenID, hash, salt, _ := cacao_common.NewTokenID("personal", testUser, testTime, newXid)
	tokenXid := tokenID.XID().String()
	expectedResult := types.Token{
		ID:               cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)),
		Owner:            testUser,
		Name:             "test_token1",
		Description:      "test_description1",
		Public:           false,
		Expiration:       nowPlusWeek,
		Start:            &nowPlusDay,
		Scopes:           "test_scope1",
		Salt:             salt,
		Hash:             hash,
		DelegatedContext: "",
		Type:             "personal",
		CreatedAt:        testTime,
		UpdatedAt:        testTime,
	}
	err := mongoAdapter.MockGet(cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)), expectedResult, nil)
	assert.NoError(t, err)
	result, err := mongoAdapter.Get(cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)))
	assert.Equal(t, expectedResult, result)
}

func TestMongoAdapterCreate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	createdTime := time.Now()
	defer mongoAdapter.Close()
	testOwner := "testuser1"
	newXid := xid.New()
	testTokenID, hash, salt, _ := cacao_common.NewTokenID("personal", testOwner, createdTime, newXid)
	testExpiration := time.Now().Add(time.Hour * 96)
	tokenXid := testTokenID.XID().String()
	testToken := types.Token{
		ID:          cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)),
		Owner:       testOwner,
		Name:        "testTokenName001",
		Description: "test token description",
		Public:      false,
		Expiration:  testExpiration,
		Scopes:      "testscope1",
		Hash:        hash,
		Salt:        salt,
		Type:        "personal",
		CreatedAt:   createdTime,
		UpdatedAt:   createdTime,
	}
	err := mongoAdapter.MockCreate(testToken, nil)
	assert.NoError(t, err)
	err = mongoAdapter.Create(testToken)
	assert.NoError(t, err)
}

func TestMongoAdapterUpdate(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	createdTime := time.Now()
	defer mongoAdapter.Close()
	testOwner := "testuser1"
	newXid := xid.New()
	testTokenID, hash, salt, _ := cacao_common.NewTokenID("personal", testOwner, createdTime, newXid)
	tokenXid := testTokenID.XID().String()
	testExpiration := time.Now().Add(time.Hour * 96)
	testExistingToken := types.Token{
		ID:          cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)),
		Owner:       testOwner,
		Name:        "old_name123",
		Description: "old description 123",
		Tags:        nil,
		Public:      false,
		Expiration:  testExpiration,
		Hash:        hash,
		Salt:        salt,
		Scopes:      "testscope1",
		Type:        "personal",
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}
	testUpdates := types.Token{
		ID:          cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)),
		Owner:       testOwner,
		Name:        "newtokenname001",
		Description: "test token updated description",
		Tags:        map[string]string{"validkey": "validtag"},
	}
	err := mongoAdapter.MockUpdate(testExistingToken, testUpdates, nil)
	assert.NoError(t, err)
	err = mongoAdapter.Update(testUpdates, []string{"name", "description", "tags"}, testOwner)
	assert.NoError(t, err)
}

func TestMongoAdapterDelete(t *testing.T) {
	mongoAdapter := createTestMongoAdapter()
	createdTime := time.Now()
	defer mongoAdapter.Close()
	testOwner := "testuser1"
	newXid := xid.New()
	testTokenID, hash, salt, _ := cacao_common.NewTokenID("personal", testOwner, createdTime, newXid)
	tokenXid := testTokenID.XID().String()
	testExpiration := time.Now().Add(time.Hour * 96)
	testToken := types.Token{
		ID:          cacao_common.ID(fmt.Sprintf("token-%s", tokenXid)),
		Owner:       testOwner,
		Name:        "delete123",
		Description: "deleting 123",
		Tags:        nil,
		Public:      false,
		Expiration:  testExpiration,
		Hash:        hash,
		Salt:        salt,
		Scopes:      "testscope1",
		Type:        "personal",
		CreatedAt:   createdTime,
		UpdatedAt:   createdTime,
	}
	err := mongoAdapter.MockDelete(testOwner, testToken.ID, testToken, nil)
	assert.NoError(t, err)
	err = mongoAdapter.Delete(testOwner, testToken.ID)
	assert.NoError(t, err)
}
