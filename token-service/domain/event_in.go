package domain

import (
	"errors"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/token-service/domain/validation"
	"gitlab.com/cyverse/cacao/token-service/ports"
	"gitlab.com/cyverse/cacao/token-service/types"
	"time"
)

// EventHandlers implements IncomingEventPort
type EventHandlers struct {
	storage        ports.PersistentStoragePort
	userSvc        ports.UserService
	XIDGenerator   func() xid.ID
	TokenGenerator func(typeString, owner string, createdAt time.Time, tokenXid xid.ID) (id cacao_common.TokenID, hash string, salt string, err error)
	TimeSrc        func() time.Time
}

// BulkCreate creates tokens in bulk, one for each DelegatedContexts
func (h EventHandlers) BulkCreate(model cacao_common_service.TokenCreateBulkModel, port ports.OutgoingEventPort) {
	//TODO implement me
	panic("implement me")
}

// Revoke immediately expires the given token, effectively making it unusable
func (h EventHandlers) Revoke(request cacao_common_service.TokenModel, port ports.OutgoingEventPort) {
	// check if session emulator is a cacao api token. if it is, we don't allow for creating/editing/deleting tokens while using tokens.
	sessionActor := request.GetSessionActor()
	sessionEmulator := request.GetSessionEmulator()
	if len(sessionEmulator) > 0 {
		emulatedID := cacao_common.ID(sessionEmulator)
		emulatedPrefix := emulatedID.FullPrefix()
		if len(emulatedPrefix) > 0 && emulatedPrefix == "token" {
			// using a cacao api token. don't allow revoke.
			// TODO not sure if we actually want to restrict revoking tokens while authed with tokens. doesn't really make sense to restrict it.
			err := cacao_common_service.NewCacaoUnauthorizedError("unable to revoke tokens while authorized with tokens")
			port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, err))
			return
		}
	}

	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "EventHandlers.Revoke",
		"actor":    sessionActor,
		"emulator": sessionEmulator,
	})

	if len(sessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, err))
		return
	}

	if len(request.ID) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: token id is empty")
		logger.WithError(err).Error("bad request")
		port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, err))
		return
	}

	// get the token first
	token, err := h.storage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch token from storage before revoke")
		port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, toCacaoError(err)))
		return
	}

	// make sure user revoking is owner or admin
	actorIsAdmin := false
	if sessionActor == cacao_common_service.ReservedCacaoSystemActor {
		actorIsAdmin = true
	} else {
		actorIsAdmin, err = h.userSvc.CheckForAdmin(cacao_common_service.Actor{
			Actor:    sessionActor,
			Emulator: sessionEmulator,
		})
		if err != nil {
			logger.WithError(err).Error("issue checking for admin status")
			port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, toCacaoError(err)))
			return
		}
	}

	if token.Owner != sessionActor && !actorIsAdmin {
		cacaoErr := cacao_common_service.NewCacaoUnauthorizedError("must be token owner or admin to revoke")
		logger.WithError(err).Error("unauthorized")
		port.TokenRevokeFailedEvent(h.revokeErrorResponse(request, cacaoErr))
		return
	}

	err = h.storage.Revoke(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to revoke token in storage")
		port.TokenRevokeFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}
	logger.Infof("token revoked")
	token.Expiration = h.TimeSrc()
	port.TokenRevokedEvent(types.ConvertToModel(request.Session, token))
}

// Create creates a token
func (h EventHandlers) Create(request cacao_common_service.TokenCreateModel, port ports.OutgoingEventPort) {
	// check if session emulator is a cacao api token. if it is, we don't allow for creating/editing/deleting tokens while using tokens.
	if len(request.SessionEmulator) > 0 {
		emulatedID := cacao_common.ID(request.SessionEmulator)
		emulatedPrefix := emulatedID.FullPrefix()
		if len(emulatedPrefix) > 0 && emulatedPrefix == "token" {
			// using a cacao api token. don't allow create.
			err := cacao_common_service.NewCacaoUnauthorizedError("unable to create tokens while authorized with tokens")
			port.TokenCreateFailedEvent(h.creationErrorResponse(request, err))
			return
		}
	}

	now := h.TimeSrc().UTC()
	request.CreatedAt = now
	request.UpdatedAt = now
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "EventHandlers.Create",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: token name is empty")
		logger.WithError(err).Error("bad request")
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if len(request.Type) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: token type is empty")
		logger.WithError(err).Error("bad request")
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	sessionActor := cacao_common_service.Actor{
		Actor:    request.GetSessionActor(),
		Emulator: request.GetSessionEmulator(),
	}

	actorIsAdmin, err := h.userSvc.CheckForAdmin(sessionActor)
	if err != nil {
		logger.WithError(err).Error("issue checking for admin status")
	}

	newXid := h.XIDGenerator()
	tokenModel := cacao_common_service.TokenModel{
		ID:               cacao_common.NewIDFromXID("token", newXid),
		Owner:            request.Owner,
		Name:             request.Name,
		Description:      request.Description,
		Public:           request.Public,
		Expiration:       request.Expiration,
		Start:            request.Start,
		Scopes:           request.Scopes,
		DelegatedContext: request.DelegatedContext,
		Type:             request.Type,
		Tags:             request.Tags,
		CreatedAt:        now,
		UpdatedAt:        now,
	}

	err = validation.ValidateToken(tokenModel, actorIsAdmin)
	if err != nil {
		logger.WithError(err).Error("error validating request")
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, toCacaoError(err)))
		return
	}

	if request.Expiration.Before(now) {
		errorMessage := "expiration must occur in the future"
		err := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		logger.WithError(err).Error(errorMessage)
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	tokenID, hash, salt, err := h.TokenGenerator(string(request.Type), request.Owner, now, newXid)
	if err != nil {
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, toCacaoError(err)))
	}
	var token = types.Token{
		ID:               tokenModel.ID,
		Owner:            request.SessionActor,
		Name:             tokenModel.Name,
		Description:      tokenModel.Description,
		Public:           tokenModel.Public,
		Expiration:       tokenModel.Expiration,
		Start:            tokenModel.Start,
		Scopes:           tokenModel.Scopes,
		Hash:             hash,
		Salt:             salt,
		DelegatedContext: tokenModel.DelegatedContext,
		Type:             string(tokenModel.Type),
		Tags:             tokenModel.Tags,
		CreatedAt:        now,
		UpdatedAt:        now,
	}

	err = h.storage.Create(token)
	if err != nil {
		logger.WithError(err).Error("fail to create token in storage")
		port.TokenCreateFailedEvent(h.creationErrorResponse(request, toCacaoError(err)))
		return
	}

	logger.Info("token created")
	port.TokenCreatedEvent(cacao_common_service.TokenCreateModel{
		Session: cacao_common_service.CopySessionActors(request.Session),
		ID:      token.ID,
		Token:   tokenID.String(),
	})
}

// Update updates a token
func (h EventHandlers) Update(request cacao_common_service.TokenModel, port ports.OutgoingEventPort) {
	// check if session emulator is a cacao api token. if it is, we don't allow for creating/editing/deleting tokens while using tokens.
	requestActor := request.GetSessionActor()
	requestEmulator := request.GetSessionEmulator()

	logger := log.WithFields(log.Fields{
		"package":          "token-service.domain",
		"function":         "EventHandlers.Update",
		"actor":            requestActor,
		"emulator":         requestEmulator,
		"updateFieldNames": request.UpdateFieldNames,
	})

	if len(requestEmulator) > 0 && requestActor != cacao_common_service.ReservedCacaoSystemActor {
		emulatedID := cacao_common.ID(requestEmulator)
		emulatedPrefix := emulatedID.FullPrefix()
		if len(emulatedPrefix) > 0 && emulatedPrefix == "token" {
			// using a cacao api token. don't allow updates.
			err := cacao_common_service.NewCacaoUnauthorizedError("unable to update tokens while authorized with tokens")
			port.TokenUpdateFailedEvent(h.updateErrorResponse(request, err))
			return
		}
	}

	if len(request.ID) <= 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: token id is empty")
		logger.WithError(err).Error("bad request")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	sessionActor := cacao_common_service.Actor{
		Actor:    requestActor,
		Emulator: requestEmulator,
	}

	actorIsAdmin := false

	if requestActor == cacao_common_service.ReservedCacaoSystemActor {
		actorIsAdmin = true
	} else if len(requestActor) > 0 {
		var err error
		actorIsAdmin, err = h.userSvc.CheckForAdmin(sessionActor)
		if err != nil {
			logger.WithError(err).Error("issue checking for admin status")
			port.TokenUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
			return
		}
	} else if len(request.SessionActor) <= 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, err))
		return
	}

	var token = types.Token{
		ID: request.ID,
	}

	existingToken, err := h.storage.Get(token.ID)
	if err != nil {
		logger.WithError(err).Error("could not get token")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	if existingToken.Owner != requestActor && !actorIsAdmin {
		// only the owner of a token or admin can edit tokens.
		cacaoErr := cacao_common_service.NewCacaoUnauthorizedError("must be token owner or admin to update")
		logger.WithError(err).Error("unauthorized")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, cacaoErr))
		return
	}

	if len(request.UpdateFieldNames) == 0 {
		// update all fields
		request.UpdateFieldNames = append(request.UpdateFieldNames, "name", "description", "public", "scopes")
	}
	// TODO maybe validate the request before we actually do the update.
	update := types.ConvertFromModel(request)
	update.Owner = existingToken.Owner
	err = h.storage.Update(update, request.UpdateFieldNames, requestActor)
	if err != nil {
		logger.WithError(err).Error("fail to update token in storage")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	// get the final result
	updatedToken, err := h.storage.Get(token.ID)
	if err != nil {
		logger.WithError(err).Error("fail to re-fetch token after update")
		port.TokenUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
		return
	}

	response := types.ConvertToModel(request.Session, updatedToken)
	port.TokenUpdatedEvent(response)
	return
}

// Delete permanently removes a token
func (h EventHandlers) Delete(request cacao_common_service.TokenModel, port ports.OutgoingEventPort) {
	sessionActor := request.GetSessionActor()
	sessionEmulator := request.GetSessionEmulator()
	logger := log.WithFields(log.Fields{
		"package":  "token-service.domain",
		"function": "EventHandlers.Delete",
		"actor":    sessionActor,
		"emulator": sessionEmulator,
		"token":    request.ID,
	})

	logger.Infof("deleting token")

	// check if session emulator is a cacao api token. if it is, we don't allow for creating/editing/deleting tokens while using tokens.
	if len(sessionEmulator) > 0 {
		emulatedID := cacao_common.ID(sessionEmulator)
		emulatedPrefix := emulatedID.FullPrefix()
		if len(emulatedPrefix) > 0 && emulatedPrefix == "token" {
			// using a cacao api token. don't allow delete.
			err := cacao_common_service.NewCacaoUnauthorizedError("unable to delete tokens while authorized with tokens")
			port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, err))
			return
		}
	}

	if len(sessionActor) <= 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	if len(request.ID) <= 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: token id is empty")
		logger.WithError(err).Error("bad request")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	if !request.ID.Validate() {
		err := cacao_common_service.NewCacaoInvalidParameterError("provided token id is invalid")
		logger.WithError(err).Error("provided token id is invalid")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	// get the token first
	token, err := h.storage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch token from storage before deletion")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}

	// check first if the session actor is the owner or admin.
	actorIsAdmin := false
	if sessionActor == cacao_common_service.ReservedCacaoSystemActor {
		actorIsAdmin = true
	} else {
		actorIsAdmin, err = h.userSvc.CheckForAdmin(cacao_common_service.Actor{Actor: sessionActor})
		if err != nil {
			logger.WithError(err).Error("issue checking for admin status")
			port.TokenUpdateFailedEvent(h.updateErrorResponse(request, toCacaoError(err)))
			return
		}
	}
	if sessionActor != token.Owner && !actorIsAdmin {
		cacaoErr := cacao_common_service.NewCacaoUnauthorizedError("must be token owner or admin to delete")
		logger.WithError(err).Error("unauthorized")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, cacaoErr))
		return
	}

	err = h.storage.Delete(request.SessionActor, request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete token in storage")
		port.TokenDeleteFailedEvent(h.deletionErrorResponse(request, toCacaoError(err)))
		return
	}
	logger.Infof("token deleted")

	port.TokenDeletedEvent(cacao_common_service.TokenModel{
		Session: cacao_common_service.CopySessionActors(request.Session),
		ID:      token.ID,
		Owner:   token.Owner,
		Name:    token.Name,
	})
}

func (h EventHandlers) creationErrorResponse(request cacao_common_service.TokenCreateModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.TokenCreateModel {
	return cacao_common_service.TokenCreateModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
		Name:  request.Name,
	}
}

func (h EventHandlers) updateErrorResponse(request cacao_common_service.TokenModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.TokenModel {
	return cacao_common_service.TokenModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:               request.ID,
		Owner:            request.Owner,
		Name:             request.Name,
		UpdateFieldNames: request.UpdateFieldNames,
	}
}

func (h EventHandlers) deletionErrorResponse(request cacao_common_service.TokenModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.TokenModel {
	return cacao_common_service.TokenModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
	}
}

func (h EventHandlers) revokeErrorResponse(request cacao_common_service.TokenModel, cacaoError cacao_common_service.CacaoError) cacao_common_service.TokenModel {
	return cacao_common_service.TokenModel{
		Session: cacao_common_service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		ID:    request.ID,
		Owner: request.Owner,
	}
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

func toCacaoError(err error) cacao_common_service.CacaoError {
	var cerr cacao_common_service.CacaoError
	if errors.As(err, &cerr) {
		return cerr
	}
	return cacao_common_service.NewCacaoGeneralError(err.Error())
}
