package main

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao/maintenance/ingress"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// MyEnvConf ...
var MyEnvConf struct {
	Msg          string    `envconfig:"MAINTENANCE_MSG"`
	Start        time.Time `envconfig:"MAINTENANCE_START_TIME"` // example "2023-04-16T08:00:00-04:00"
	End          time.Time `envconfig:"MAINTENANCE_END_TIME"`
	K8SNamespace string    `envconfig:"K8S_NAMESPACE" default:"default"`
}

func init() {
	err := envconfig.Process("", &MyEnvConf)
	if err != nil {
		log.WithError(err).Fatal("fail to load config from env")
	}
	if !hasMaintenance() {
		log.Info("no maintenance planned")
		return
	}
	if MyEnvConf.End.Before(MyEnvConf.Start) {
		log.Fatal("end time is before start time")
	}
	now := time.Now()
	log.Infof("next mainteance is %s - %s", MyEnvConf.Start.Format(time.RFC3339), MyEnvConf.End.Format(time.RFC3339))
	log.Infof("now is %s", now.In(MyEnvConf.Start.Location()).Format(time.RFC3339))
	if now.Before(MyEnvConf.Start) {
		log.Info("maintenance has not started yet")
	}
	if now.After(MyEnvConf.Start) && now.Before(MyEnvConf.End) {
		log.Info("maintenance is underway")
	}
	if now.After(MyEnvConf.End) {
		log.Info("maintenance has ended")
	}
}

func main() {
	done := watchInterruptSignal()
	go waitForMaintenance()

	http.HandleFunc("/icons/favicon.ico", MaintenancePageIcon)
	http.HandleFunc("/", MaintenancePage)

	var server = &http.Server{Addr: ":8080"}

	// shutdown server when receiving signal
	go func() {
		<-done
		ctx, _ := context.WithTimeout(context.Background(), time.Second)
		err := server.Shutdown(ctx)
		if err != nil {
			log.WithError(err).Error("fail to shutdown http server")
			return
		}
	}()

	log.Infof("start serving on %s", server.Addr)
	err := server.ListenAndServe()
	if err != nil {
		log.WithError(err).Error()
	}
}

func waitForMaintenance() {
	log.Info("start go routine to wait for maintenance")

	for {
		now := time.Now()
		if hasMaintenance() {
			if now.After(MyEnvConf.End) {
				log.Info("maintenance has ended")
				ingress.EndMaintenance()
			} else if now.After(MyEnvConf.Start) && now.Before(MyEnvConf.End) {
				log.Info("maintenance is underway")
				ingress.StartMaintenance()
			}
		}
		// if maintenance is more than 1 hour away, longer polling time
		if MyEnvConf.Start.Sub(now) > time.Hour {
			time.Sleep(time.Minute * 10)
		} else {
			time.Sleep(time.Second * 20)
		}
	}
}

func printTime() {
	var t1 = time.Date(2023, 4, 16, 8, 0, 0, 0, time.FixedZone("UTC-4", -4*60*60))
	fmt.Println(t1.Format(time.RFC3339))
}

func watchInterruptSignal() <-chan bool {

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	done := make(chan bool, 1)

	go func() {
		sig := <-sigs
		log.Infof("received signal, %v", sig)
		done <- true
	}()

	return done
}

func hasMaintenance() bool {
	return !(MyEnvConf.End.IsZero() && MyEnvConf.Start.IsZero())
}
