# Maintenance Service

This service serves a simple HTML page about maintenance info.
This service will update the k8s ingress (for ion and api) to point them to this service.
When the maintenance finishes, this service will restore the ingress from configmap.

## Environment variables
In order to trigger maintenance, one of `MAINTENANCE_START_TIME` or `MAINTENANCE_END_TIME` needs to be set to a non-zero time.

| name                   | default   | description                                | example                     | 
|------------------------|-----------|--------------------------------------------|-----------------------------|
| MAINTENANCE_MSG        |           | message to display on the maintenance page |
| MAINTENANCE_START_TIME |           | start time of the maintenance              | "2023-04-16T08:00:00-04:00" |
| MAINTENANCE_END_TIME   |           | end time of the maintenance                |
| K8S_NAMESPACE          | "default" |

## How schedule a maintenance
Edit the env var of the deployment `maintenance` to set `MAINTENANCE_START_TIME` or `MAINTENANCE_END_TIME`
```shell
kubectl edit deployment maintenance
```
