package ingress

import (
	"context"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	v1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"time"
)

const (
	defaultNamespace = "default"
	// Name of the configmap to back up the ingress spec to
	maintenanceIngressBackupConfigmapName = "maintenance-ingress-backup"
	// Name of the key in configmap spec (ConfigMap.Data["backup"])
	maintenanceIngressBackupConfigmapKeyName = "backup"
	apiIngressName                           = "cacao-api-ingress"
	ionIngressName                           = "cacao-ion-ingress"
)

// NewClient creates a client from in-cluster config
func NewClient() *kubernetes.Clientset {
	config, err := rest.InClusterConfig()
	if err != nil {
		panic(err.Error())
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}
	return clientset
}

// ListIngress returns the relevant ingresses that needs to be updated for maintenance
func ListIngress(clientset *kubernetes.Clientset) ([]networkingv1.Ingress, error) {
	list, err := clientset.NetworkingV1().Ingresses(defaultNamespace).List(k8sAPIContext(), metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	result := make([]networkingv1.Ingress, 0)
	list.ListMeta.Size()
	for _, ingress := range list.Items {
		if ingress.Name == apiIngressName || ingress.Name == ionIngressName {
			result = append(result, ingress)
		}
	}
	if len(result) < 2 {
		log.WithField("ingresses", list).Panic("cannot find ingress")
	}
	return result, nil
}

// SaveIngressToConfigmap ...
func SaveIngressToConfigmap(clientset *kubernetes.Clientset, ingresses []networkingv1.Ingress) error {
	var specs = make(map[string]networkingv1.IngressSpec)
	for _, ingress := range ingresses {
		specs[ingress.Name] = ingress.Spec
	}
	marshal, err := json.Marshal(specs)
	if err != nil {
		log.WithError(err).Error("fail to marshal ingress specs for backup")
		return err
	}
	exists, err := CheckBackupConfigmap(clientset)
	if err != nil {
		log.WithError(err).Error("fail to fetch backup configmap")
		return err
	}
	// if configmap already exists, update it
	if exists {
		var fetchedCM *v1.ConfigMap
		fetchedCM, err = clientset.CoreV1().ConfigMaps(defaultNamespace).Get(k8sAPIContext(), maintenanceIngressBackupConfigmapName, metav1.GetOptions{})
		if err != nil {
			log.WithError(err).Error("fail to fetch cm before save ingress")
		}
		fetchedCM.Annotations["backupTime"] = time.Now().String()
		fetchedCM.Data[maintenanceIngressBackupConfigmapKeyName] = string(marshal)
		_, err = clientset.CoreV1().ConfigMaps(defaultNamespace).Update(k8sAPIContext(), fetchedCM, metav1.UpdateOptions{})
		if err != nil {
			log.WithError(err).Error("fail to backup ingress specs to configmap")
			return err
		}
	} else {
		// if configmap does NOT exist, create it
		cm := &v1.ConfigMap{
			ObjectMeta: metav1.ObjectMeta{
				Name:      maintenanceIngressBackupConfigmapName,
				Namespace: defaultNamespace,
				Annotations: map[string]string{
					"backupTime": time.Now().String(),
				},
			},
			Data: map[string]string{
				maintenanceIngressBackupConfigmapKeyName: string(marshal),
			},
		}
		_, err = clientset.CoreV1().ConfigMaps(defaultNamespace).Create(k8sAPIContext(), cm, metav1.CreateOptions{})
		if err != nil {
			log.WithError(err).Error("fail to backup ingress specs to configmap")
			return err
		}
	}
	return nil
}

// CheckBackupConfigmap ...
func CheckBackupConfigmap(clientset *kubernetes.Clientset) (ok bool, err error) {
	_, err = clientset.CoreV1().ConfigMaps(defaultNamespace).Get(k8sAPIContext(), maintenanceIngressBackupConfigmapName, metav1.GetOptions{})
	if err != nil {
		log.WithError(err).Errorf("fail to find backup configmap")
		if apierrors.IsNotFound(err) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

// RestoreIngressFromConfigmap ...
func RestoreIngressFromConfigmap(clientset *kubernetes.Clientset) error {
	var specs = make(map[string]networkingv1.IngressSpec)
	cm, err := clientset.CoreV1().ConfigMaps(defaultNamespace).Get(k8sAPIContext(), maintenanceIngressBackupConfigmapName, metav1.GetOptions{})
	if err != nil {
		log.WithError(err).Error("fail to find backup configmap")
		return err
	}
	backupStr, ok := cm.Data[maintenanceIngressBackupConfigmapKeyName]
	if !ok {
		return fmt.Errorf("no '%s' key in configmap", maintenanceIngressBackupConfigmapKeyName)
	}
	err = json.Unmarshal([]byte(backupStr), &specs)
	if err != nil {
		log.WithError(err).Error("fail to unmarshal ingresses specs from configmap backup")
		return err
	}
	for ingressName, ingressSpec := range specs {
		var ingress *networkingv1.Ingress
		ingress, err = clientset.NetworkingV1().Ingresses(defaultNamespace).Get(k8sAPIContext(), ingressName, metav1.GetOptions{})
		if err != nil {
			log.WithError(err).Errorf("fail to fetch ingress %s to restore it", ingressName)
			return err
		}
		ingress.Spec = ingressSpec

		_, err = clientset.NetworkingV1().Ingresses(defaultNamespace).Update(k8sAPIContext(), ingress, metav1.UpdateOptions{})
		if err != nil {
			log.WithError(err).Errorf("fail to restore ingress %s", ingressName)
			return err
		}
	}
	return nil
}

// UpdateIngresses ...
func UpdateIngresses(clientset *kubernetes.Clientset, ingresses []networkingv1.Ingress) error {
	for _, ingress := range ingresses {
		changedIngress, err := pointIngressToMaintenance(ingress)
		if err != nil {
			return err
		}
		_, err = clientset.NetworkingV1().Ingresses(defaultNamespace).Update(k8sAPIContext(), &changedIngress, metav1.UpdateOptions{})
		if err != nil {
			return err
		}
		log.Infof("updated %s ingress", ingress.Name)
	}
	return nil
}

func pointIngressToMaintenance(ingress networkingv1.Ingress) (networkingv1.Ingress, error) {
	for i, rule := range ingress.Spec.Rules {
		for j := range rule.HTTP.Paths {
			// point the ingress rule to maintenance service (this service) on port 80
			ingress.Spec.Rules[i].HTTP.Paths[j].Backend.Service.Name = "maintenance"
			ingress.Spec.Rules[i].HTTP.Paths[j].Backend.Service.Port.Number = 80
		}
	}
	return ingress, nil
}

// NeedToRestore returns true if there is a need to restore from backup.
func NeedToRestore(clientset *kubernetes.Clientset) (bool, error) {
	ingressList, err := ListIngress(clientset)
	if err != nil {
		return false, err
	}
	for _, ingress := range ingressList {
		if ifIngressPointsToMaintenance(ingress) {
			return true, nil
		}
	}
	return false, nil
}

// check if there are still rules in the ingress that points to maintenance service
func ifIngressPointsToMaintenance(ingress networkingv1.Ingress) bool {
	for i, rule := range ingress.Spec.Rules {
		for j := range rule.HTTP.Paths {
			if ingress.Spec.Rules[i].HTTP.Paths[j].Backend.Service.Name == "maintenance" {
				return true
			}
		}
	}
	return false
}

// Return a context to use to call k8s api
func k8sAPIContext() context.Context {
	ctx, _ := context.WithTimeout(context.Background(), time.Second*2)
	return ctx
}
