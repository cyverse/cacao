package ports

import (
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-service/types"
)

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// CreateTypeHandler is a handler for CreateType event
type CreateTypeHandler func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error

// UpdateTypeHandler is a handler for UpdateType event
type UpdateTypeHandler func(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error

// DeleteTypeHandler is a handler for DeleteType event
type DeleteTypeHandler func(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// CreateCustomFieldTypeHandler is a handler for CreateCustomFieldType event
type CreateCustomFieldTypeHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, transactionID cacao_common.TransactionID) error

// UpdateCustomFieldTypeHandler is a handler for UpdateCustomFieldType event
type UpdateCustomFieldTypeHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, updateFieldNames []string, transactionID cacao_common.TransactionID) error

// DeleteCustomFieldTypeHandler is a handler for DeleteCustomFieldType event
type DeleteCustomFieldTypeHandler func(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, transactionID cacao_common.TransactionID) error

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// ImportHandler is a handler for Import event
type ImportHandler func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error

// UpdateHandler is a handler for Update event
type UpdateHandler func(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error

// DeleteHandler is a handler for Delete event
type DeleteHandler func(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error

// SyncHandler is a handler for Sync event
type SyncHandler func(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error

// MockIncomingEventPort is a mock implementation of IncomingEventPort
type MockIncomingEventPort struct {
	Config *types.Config

	// TemplateType
	CreateTypeHandler CreateTypeHandler
	UpdateTypeHandler UpdateTypeHandler
	DeleteTypeHandler DeleteTypeHandler

	// TemplateCustomFieldType
	CreateCustomFieldTypeHandler CreateCustomFieldTypeHandler
	UpdateCustomFieldTypeHandler UpdateCustomFieldTypeHandler
	DeleteCustomFieldTypeHandler DeleteCustomFieldTypeHandler

	// Template
	ImportHandler ImportHandler
	UpdateHandler UpdateHandler
	DeleteHandler DeleteHandler
	SyncHandler   SyncHandler
}

// Init inits the port
func (port *MockIncomingEventPort) Init(config *types.Config) {
	port.Config = config
}

// Finalize finalizes the port
func (port *MockIncomingEventPort) Finalize() {}

// InitChannel inits a channel
func (port *MockIncomingEventPort) InitChannel(channel chan types.TemplateChannelRequest) {}

// Start starts the port
func (port *MockIncomingEventPort) Start() {}

/////////////////////////////////////////////////////////////////////////
// TemplateType
/////////////////////////////////////////////////////////////////////////

// SetCreateTypeHandler sets a handler for CreateType event
func (port *MockIncomingEventPort) SetCreateTypeHandler(createTypeHandler CreateTypeHandler) {
	port.CreateTypeHandler = createTypeHandler
}

// SetUpdateTypeHandler sets a handler for UpdateType event
func (port *MockIncomingEventPort) SetUpdateTypeHandler(updateTypeHandler UpdateTypeHandler) {
	port.UpdateTypeHandler = updateTypeHandler
}

// SetDeleteTypeHandler sets a handler for DeleteType event
func (port *MockIncomingEventPort) SetDeleteTypeHandler(deleteTypeHandler DeleteTypeHandler) {
	port.DeleteTypeHandler = deleteTypeHandler
}

// CreateType creates a template type
func (port *MockIncomingEventPort) CreateType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	return port.CreateTypeHandler(actor, emulator, templateType, transactionID)
}

// UpdateType updates a template type
func (port *MockIncomingEventPort) UpdateType(actor string, emulator string, templateType types.TemplateType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	return port.UpdateTypeHandler(actor, emulator, templateType, updateFieldNames, transactionID)
}

// DeleteType deletes a template type
func (port *MockIncomingEventPort) DeleteType(actor string, emulator string, templateType types.TemplateType, transactionID cacao_common.TransactionID) error {
	return port.DeleteTypeHandler(actor, emulator, templateType, transactionID)
}

/////////////////////////////////////////////////////////////////////////
// TemplateCustomFieldType
/////////////////////////////////////////////////////////////////////////

// SetCreateCustomFieldTypeHandler sets a handler for CreateCustomFieldType event
func (port *MockIncomingEventPort) SetCreateCustomFieldTypeHandler(createCustomFieldTypeHandler CreateCustomFieldTypeHandler) {
	port.CreateCustomFieldTypeHandler = createCustomFieldTypeHandler
}

// SetUpdateCustomFieldTypeHandler sets a handler for UpdateCustomFieldType event
func (port *MockIncomingEventPort) SetUpdateCustomFieldTypeHandler(updateCustomFieldTypeHandler UpdateCustomFieldTypeHandler) {
	port.UpdateCustomFieldTypeHandler = updateCustomFieldTypeHandler
}

// SetDeleteCustomFieldTypeHandler sets a handler for DeleteCustomFieldType event
func (port *MockIncomingEventPort) SetDeleteCustomFieldTypeHandler(deleteCustomFieldTypeHandler DeleteCustomFieldTypeHandler) {
	port.DeleteCustomFieldTypeHandler = deleteCustomFieldTypeHandler
}

// CreateCustomFieldType creates a template custom field type
func (port *MockIncomingEventPort) CreateCustomFieldType(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, transactionID cacao_common.TransactionID) error {
	return port.CreateCustomFieldTypeHandler(actor, emulator, templateCustomFieldType, transactionID)
}

// UpdateCustomFieldType updates a template custom field type
func (port *MockIncomingEventPort) UpdateCustomFieldType(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	return port.UpdateCustomFieldTypeHandler(actor, emulator, templateCustomFieldType, updateFieldNames, transactionID)
}

// DeleteCustomFieldType deletes a template custom field type
func (port *MockIncomingEventPort) DeleteCustomFieldType(actor string, emulator string, templateCustomFieldType types.TemplateCustomFieldType, transactionID cacao_common.TransactionID) error {
	return port.DeleteCustomFieldTypeHandler(actor, emulator, templateCustomFieldType, transactionID)
}

/////////////////////////////////////////////////////////////////////////
// Template
/////////////////////////////////////////////////////////////////////////

// SetImportHandler sets a handler for Import event
func (port *MockIncomingEventPort) SetImportHandler(importHandler ImportHandler) {
	port.ImportHandler = importHandler
}

// SetUpdateHandler sets a handler for Update event
func (port *MockIncomingEventPort) SetUpdateHandler(updateHandler UpdateHandler) {
	port.UpdateHandler = updateHandler
}

// SetDeleteHandler sets a handler for Delete event
func (port *MockIncomingEventPort) SetDeleteHandler(deleteHandler DeleteHandler) {
	port.DeleteHandler = deleteHandler
}

// SetSyncHandler sets a handler for Sync event
func (port *MockIncomingEventPort) SetSyncHandler(syncHandler SyncHandler) {
	port.SyncHandler = syncHandler
}

// Import imports a template
func (port *MockIncomingEventPort) Import(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
	return port.ImportHandler(actor, emulator, template, credentialID, transactionID)
}

// Update updates a template
func (port *MockIncomingEventPort) Update(actor string, emulator string, template types.Template, updateFieldNames []string, transactionID cacao_common.TransactionID) error {
	return port.UpdateHandler(actor, emulator, template, updateFieldNames, transactionID)
}

// Delete deletes a template
func (port *MockIncomingEventPort) Delete(actor string, emulator string, template types.Template, transactionID cacao_common.TransactionID) error {
	return port.DeleteHandler(actor, emulator, template, transactionID)
}

// Sync syncs a template
func (port *MockIncomingEventPort) Sync(actor string, emulator string, template types.Template, credentialID string, transactionID cacao_common.TransactionID) error {
	return port.SyncHandler(actor, emulator, template, credentialID, transactionID)
}
