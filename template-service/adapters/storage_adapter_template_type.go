package adapters

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListTypes returns all template types
func (adapter *MongoAdapter) ListTypes() ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListTypes",
	})

	results := []types.TemplateType{}

	err := adapter.Store.List(adapter.Config.TemplateTypeMongoDBCollectionName, &results)
	if err != nil {
		errorMessage := "unable to list template types"
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockListTypes sets expected results for ListTypes
func (adapter *MongoAdapter) MockListTypes(expectedTemplateTypes []types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("List", adapter.Config.TemplateTypeMongoDBCollectionName).Return(expectedTemplateTypes, expectedError)
	return nil
}

// ListTypesForProviderType returns all template types for the given ProviderType
func (adapter *MongoAdapter) ListTypesForProviderType(providerType cacao_common_service.TemplateProviderType) ([]types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.ListTypesForProviderType",
	})

	results := []types.TemplateType{}

	filter := map[string]interface{}{
		"provider_types": map[string]interface{}{
			"$in": string(providerType),
		},
	}

	err := adapter.Store.ListConditional(adapter.Config.TemplateTypeMongoDBCollectionName, filter, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list template types for provider type %s", string(providerType))
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockListTypesForProviderType sets expected results for ListTypesForProviderType
func (adapter *MongoAdapter) MockListTypesForProviderType(providerType cacao_common_service.TemplateProviderType, expectedTemplateTypes []types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"provider_types": map[string]interface{}{
			"$in": string(providerType),
		},
	}

	mock.On("ListConditional", adapter.Config.TemplateTypeMongoDBCollectionName, filter).Return(expectedTemplateTypes, expectedError)
	return nil
}

// GetType returns the template type with the name
func (adapter *MongoAdapter) GetType(templateTypeName cacao_common_service.TemplateTypeName) (types.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.GetType",
	})

	result := types.TemplateType{}

	err := adapter.Store.Get(adapter.Config.TemplateTypeMongoDBCollectionName, string(templateTypeName), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the template type for name %s", templateTypeName)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return result, nil
}

// MockGetType sets expected results for GetType
func (adapter *MongoAdapter) MockGetType(templateTypeName cacao_common_service.TemplateTypeName, expectedTemplateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.TemplateTypeMongoDBCollectionName, string(templateTypeName)).Return(expectedTemplateType, expectedError)
	return nil
}

// CreateType inserts a template type
func (adapter *MongoAdapter) CreateType(templateType types.TemplateType) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.CreateType",
	})

	err := adapter.Store.Insert(adapter.Config.TemplateTypeMongoDBCollectionName, templateType)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert a template type because name %s conflicts", templateType.Name)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert a template type with name %s", templateType.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreateType sets expected results for CreateType
func (adapter *MongoAdapter) MockCreateType(templateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.TemplateTypeMongoDBCollectionName).Return(expectedError)
	return nil
}

// UpdateType updates/edits a template type
func (adapter *MongoAdapter) UpdateType(templateType types.TemplateType, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.UpdateType",
	})

	updated, err := adapter.Store.Update(adapter.Config.TemplateTypeMongoDBCollectionName, string(templateType.Name), templateType, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the template type for name %s", templateType.Name)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the template type for name %s", templateType.Name)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdateType sets expected results for UpdateType
func (adapter *MongoAdapter) MockUpdateType(templateType types.TemplateType, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Update", adapter.Config.TemplateTypeMongoDBCollectionName, string(templateType.Name)).Return(expectedResult, expectedError)
	return nil
}

// DeleteType deletes a template type
func (adapter *MongoAdapter) DeleteType(templateTypeName cacao_common_service.TemplateTypeName) error {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.adapters",
		"function": "MongoAdapter.DeleteType",
	})

	deleted, err := adapter.Store.Delete(adapter.Config.TemplateTypeMongoDBCollectionName, string(templateTypeName))
	if err != nil {
		errorMessage := fmt.Sprintf("unable to delete the template type for name %s", templateTypeName)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !deleted {
		errorMessage := fmt.Sprintf("unable to delete the template type for name %s", templateTypeName)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockDeleteType sets expected results for DeleteType
func (adapter *MongoAdapter) MockDeleteType(templateTypeName cacao_common_service.TemplateTypeName, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Delete", adapter.Config.TemplateTypeMongoDBCollectionName, string(templateTypeName)).Return(expectedResult, expectedError)
	return nil
}
