package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"io"

	"gitlab.com/cyverse/cacao/template-service/adapters"
	"gitlab.com/cyverse/cacao/template-service/domain"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()
	config.Override()

	mongoAdapter := &adapters.MongoAdapter{}
	defer logCloserError(mongoAdapter)

	natsConn, err := config.MessagingConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Panic("fail to connect to NATS")
	}
	defer logCloserError(&natsConn)
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(config.MessagingConfig)
	if err != nil {
		log.WithError(err).Panic("fail to connect to STAN")
	}
	defer logCloserError(&stanConn)

	svc := domain.Domain{
		Storage:                      mongoAdapter,
		TemplateSource:               &adapters.GitAdapter{},
		TemplateCustomFieldTypeQuery: &adapters.TemplateCustomFieldTypeQueryAdapter{},
		Credential:                   adapters.NewCredentialAdapter(&natsConn),
		QueryIn:                      adapters.NewQueryAdapter(&natsConn),
		EventIn:                      adapters.NewEventAdapter(&stanConn),
	}
	err = svc.Init(&config)
	if err != nil {
		log.WithError(err).Panic("fail to init service")
	}

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	cacao_common.CancelWhenSignaled(cancelFunc)
	err = svc.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Panic("fail to start service")
		return
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error("fail to close")
	}
}
