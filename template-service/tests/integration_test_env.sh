#!/bin/bash

# copy over the fixtures json files to satisfy the service main function
cp ../*_fixtures.json .

# this sets necessary env var to run integration test with mongodb spawned by ./docker-compose.yaml
export MONGO_INTEGRATION_TEST=true
export MONGODB_URL=mongodb://root:example@localhost:27017/
export MONGODB_DB_NAME=cacao
export NATS_INTEGRATION_TEST=true
export NATS_URL=nats://localhost:4222
