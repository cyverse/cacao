package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/types"
)

// ListCustomFieldTypes retrieves all template custom field types of the user
func (d *QueryPortImpl) ListCustomFieldTypes(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeListModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.ListCustomFieldTypes",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
	})
	logger.Info("List template custom field types")

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateCustomFieldTypeListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			TemplateCustomFieldTypes: nil,
		}
	}

	templateCustomFieldTypes, err := d.Storage.ListCustomFieldTypes()
	if err != nil {
		logger.WithError(err).Error("fail to list custom field types in storage")
		return cacao_common_service.TemplateCustomFieldTypeListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			TemplateCustomFieldTypes: nil,
		}
	}

	models := make([]cacao_common_service.TemplateCustomFieldTypeListItemModel, 0, len(templateCustomFieldTypes))
	for _, templateCustomFieldType := range templateCustomFieldTypes {
		model := types.ConvertCustomFieldTypeToListItemModel(templateCustomFieldType)
		models = append(models, model)
	}

	logger.WithField("len", len(models)).Infof("custom field listed")
	return cacao_common_service.TemplateCustomFieldTypeListModel{
		Session:                  cacao_common_service.CopySessionActors(request.Session),
		TemplateCustomFieldTypes: models,
	}
}

// GetCustomFieldType retrieves the template custom field type
func (d *QueryPortImpl) GetCustomFieldType(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.GetCustomFieldType",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	logger.Infof("Get template custom field type %s", request.Name)

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateCustomFieldTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template custom field type name is empty")
		return cacao_common_service.TemplateCustomFieldTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	templateCustomFieldType, err := d.Storage.GetCustomFieldType(request.Name)
	if err != nil {
		logger.WithError(err).Error("fail to fetch custom field type from storage")
		return cacao_common_service.TemplateCustomFieldTypeModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Name: request.Name,
		}
	}
	logger.Infof("custom field fetched")
	return types.ConvertCustomFieldTypeToModel(cacao_common_service.CopySessionActors(request.Session), templateCustomFieldType)
}

// QueryCustomFieldType retrieves the template custom field type and query to the target
func (d *QueryPortImpl) QueryCustomFieldType(request cacao_common_service.TemplateCustomFieldTypeModel) cacao_common_service.TemplateCustomFieldTypeQueryResultModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "Domain.QueryTemplateCustomFieldType",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"name":     request.Name,
	})
	logger.Infof("Get template custom field type %s", request.Name)

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateCustomFieldTypeQueryResultModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	if len(request.Name) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: template custom field type name is empty")
		return cacao_common_service.TemplateCustomFieldTypeQueryResultModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			Name: request.Name,
		}
	}

	templateCustomFieldType, err := d.Storage.GetCustomFieldType(request.Name)
	if err != nil {
		logger.WithError(err).Error("fail to fetch custom field type from storage")
		return cacao_common_service.TemplateCustomFieldTypeQueryResultModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Name: request.Name,
		}
	}

	logger.Infof("Query template custom field type %s", request.Name)

	templateCustomFieldTypeQueryResult, err := d.TemplateCustomFieldTypeQuery.QueryTemplateCustomFieldType(request.SessionActor, request.SessionEmulator, templateCustomFieldType, request.QueryParams)
	if err != nil {
		logger.WithError(err).Error("fail to query custom field")
		return cacao_common_service.TemplateCustomFieldTypeQueryResultModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    cacao_common_service.ToCacaoError(err).GetBase(),
			},
			Name: request.Name,
		}
	}
	logger.WithFields(log.Fields{
		"name":     templateCustomFieldTypeQueryResult.Name,
		"dataType": templateCustomFieldTypeQueryResult.DataType,
	}).Infof("custom field queried")
	return types.ConvertCustomFieldTypeQueryResultToModel(cacao_common_service.CopySessionActors(request.Session), templateCustomFieldTypeQueryResult)
}
