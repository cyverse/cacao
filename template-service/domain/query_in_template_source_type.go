package domain

import (
	log "github.com/sirupsen/logrus"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// ListSourceTypes retrieves all template source types
func (d *QueryPortImpl) ListSourceTypes(request cacao_common_service.Session) cacao_common_service.TemplateSourceTypeListModel {
	logger := log.WithFields(log.Fields{
		"package":  "template-service.domain",
		"function": "QueryPortImpl.ListSourceTypes",
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
	})
	logger.Info("List template source types")

	if len(request.SessionActor) == 0 {
		err := cacao_common_service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		return cacao_common_service.TemplateSourceTypeListModel{
			Session: cacao_common_service.Session{
				SessionActor:    request.SessionActor,
				SessionEmulator: request.SessionEmulator,
				ServiceError:    err.GetBase(),
			},
			TemplateSourceTypes: nil,
		}
	}

	// return a hardcoded reply, since we probably won't be supporting any other VersionControl system any time soon.
	return cacao_common_service.TemplateSourceTypeListModel{
		Session: cacao_common_service.CopySessionActors(request),
		TemplateSourceTypes: []cacao_common_service.TemplateSourceType{
			cacao_common_service.TemplateSourceTypeGit,
		},
	}
}
