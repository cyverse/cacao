package domain

import (
	"gitlab.com/cyverse/cacao/template-service/ports"
)

// QueryPortImpl implements IncomingQueryPort
type QueryPortImpl struct {
	Storage                      ports.PersistentStoragePort
	TemplateCustomFieldTypeQuery ports.TemplateCustomFieldTypeQueryPort
}

var _ ports.IncomingQueryHandlers = (*QueryPortImpl)(nil)
