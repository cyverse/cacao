package domain

import (
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-service/ports"
	portsmocks "gitlab.com/cyverse/cacao/template-service/ports/mocks"
	"gitlab.com/cyverse/cacao/template-service/types"
)

func stringPtr(s string) *string {
	return &s
}

func TestEventPortImpl_TemplateImportRequestedEvent(t *testing.T) {
	now := time.Now().UTC()
	newTemplateID := common.NewID("template")
	newTemplateVersionID := common.NewID("templateversion")
	type fields struct {
		Storage            *portsmocks.PersistentStoragePort
		TemplateSource     *portsmocks.TemplateSourcePort
		Credential         *portsmocks.CredentialPort
		TimeSrc            *portsmocks.TimeSrc
		GeneratedID        common.ID
		GeneratedVersionID common.ID
	}
	type args struct {
		importRequest service.TemplateModel
		sink          *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "empty actor",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				}(),
				GeneratedID:        newTemplateID,
				GeneratedVersionID: newTemplateVersionID,
			},
			args: args{
				importRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "", // empty actor
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("ImportFailed",
						service.Actor{
							Actor:    "",
							Emulator: "",
						}, types.Template{
							ID:                      "",
							Owner:                   "",
							Name:                    "",
							Description:             "",
							Public:                  false,
							Source:                  service.TemplateSource{},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               time.Time{},
							UpdatedAt:               time.Time{},
						},
						service.NewCacaoInvalidParameterError("input validation error: actor is empty"),
					).Once()
					return out
				}(),
			},
			wantErr: true,
		},
		{
			name: "import template with no credential",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("CreateVersion", types.TemplateVersion{
						ID:         newTemplateVersionID,
						TemplateID: newTemplateID,
						Source: service.TemplateSource{
							Type: "git",
							URI:  "https://cyverse.org",
							AccessParameters: map[string]interface{}{
								"branch": "master",
								"path":   ".",
								"commit": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
							},
							Visibility: "public",
						},
						Disabled:     false,
						DisabledAt:   time.Time{},
						CredentialID: "",
						Metadata: service.TemplateMetadata{
							SchemaURL:      "https://cyverse.org/schema",
							SchemaVersion:  "v3",
							Name:           "template_foo",
							Author:         "user123",
							AuthorEmail:    "",
							Description:    "bar",
							DocURL:         "",
							TemplateType:   "terraform_openstack",
							Purpose:        "general",
							CacaoPreTasks:  nil,
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name:               "param1",
									Type:               "string",
									Description:        "desc123",
									Default:            stringPtr("default_val123"),
									Enum:               nil,
									DataValidationType: "",
									Required:           false,
									Editable:           false,
									Base64:             false,
								},
							},
							Filters: nil,
						},
						UIMetadata: service.TemplateUIMetadata{},
						CreatedAt:  now,
					}).Return(nil)
					storage.On("Create",
						types.Template{
							ID:              newTemplateID,
							Owner:           "testuser123",
							Name:            "template_foo",
							Description:     "bar",
							Public:          false,
							Deleted:         false,
							LatestVersionID: newTemplateVersionID,
							Source: service.TemplateSource{
								Type: "git",
								URI:  "https://cyverse.org",
								AccessParameters: map[string]interface{}{
									"branch": "master",
									"path":   ".",
								},
								Visibility: "public",
							},
							LatestVersionMetadata: service.TemplateMetadata{
								SchemaURL:      "https://cyverse.org/schema",
								SchemaVersion:  "v3",
								Name:           "template_foo",
								Author:         "user123",
								AuthorEmail:    "",
								Description:    "bar",
								DocURL:         "",
								TemplateType:   "terraform_openstack",
								Purpose:        "general",
								CacaoPreTasks:  nil,
								CacaoPostTasks: nil,
								Parameters: []service.TemplateParameter{
									{
										Name:               "param1",
										Type:               "string",
										Description:        "desc123",
										Default:            stringPtr("default_val123"),
										Enum:               nil,
										DataValidationType: "",
										Required:           false,
										Editable:           false,
										Base64:             false,
									},
								},
								Filters: nil,
							},
							LatestVersionUIMetadata: service.TemplateUIMetadata{
								SchemaURL:     "",
								SchemaVersion: "",
								Author:        "",
								AuthorEmail:   "",
								Description:   "",
								DocURL:        "",
								Steps:         nil,
							},
							CreatedAt: now,
							UpdatedAt: now,
						}).Return(nil).Once()
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					src.On("GetTemplateMetadata",
						service.TemplateSource{
							Type: "git",
							URI:  "https://cyverse.org",
							AccessParameters: map[string]interface{}{
								"branch": "master",
								"path":   ".",
							},
							Visibility: "public",
						},
						service.TemplateSourceCredential{
							Username: "",
							Password: "",
						}).Return(
						service.TemplateMetadata{
							SchemaURL:      "https://cyverse.org/schema",
							SchemaVersion:  "v3",
							Name:           "template_foo",
							Author:         "user123",
							AuthorEmail:    "",
							Description:    "bar",
							DocURL:         "",
							TemplateType:   "terraform_openstack",
							Purpose:        "general",
							CacaoPreTasks:  nil,
							CacaoPostTasks: nil,
							Parameters: []service.TemplateParameter{
								{
									Name:               "param1",
									Type:               "string",
									Description:        "desc123",
									Default:            stringPtr("default_val123"),
									Enum:               nil,
									DataValidationType: "",
									Required:           false,
									Editable:           false,
									Base64:             false,
								},
							},
							Filters: nil,
						}, service.TemplateUIMetadata{
							SchemaURL:     "",
							SchemaVersion: "",
							Author:        "",
							AuthorEmail:   "",
							Description:   "",
							DocURL:        "",
							Steps:         nil,
						}, service.TemplateSource{
							Type: "git",
							URI:  "https://cyverse.org",
							AccessParameters: map[string]interface{}{
								"branch": "master",
								"path":   ".",
								"commit": "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
							},
							Visibility: "public",
						}, nil).Once()
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					src.On("Now").Return(now).Once()
					return src
				}(),
				GeneratedID:        newTemplateID,
				GeneratedVersionID: newTemplateVersionID,
			},
			args: args{
				importRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Imported",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:                      newTemplateID,
							Owner:                   "testuser123",
							Name:                    "template_foo",
							Description:             "",
							Public:                  false,
							Source:                  service.TemplateSource{},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               now,
							UpdatedAt:               time.Time{},
						}).Once()
					return out
				}(),
			},
			wantErr: false,
		},
		{
			name: "fail to import from source", // e.g. bad git URL, git branch not exists
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					src.On("GetTemplateMetadata",
						service.TemplateSource{
							Type: "git",
							URI:  "https://cyverse.org",
							AccessParameters: map[string]interface{}{
								"branch": "master",
								"path":   ".",
							},
							Visibility: "public",
						},
						service.TemplateSourceCredential{
							Username: "",
							Password: "",
						}).Return(service.TemplateMetadata{}, service.TemplateUIMetadata{}, service.TemplateSource{}, fmt.Errorf("import_from_source_failed123")).Once()
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					src.On("Now").Return(now).Once()
					return src
				}(),
				GeneratedID:        newTemplateID,
				GeneratedVersionID: newTemplateVersionID,
			},
			args: args{
				importRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("ImportFailed",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:                      newTemplateID,
							Owner:                   "testuser123",
							Name:                    "template_foo",
							Description:             "",
							Public:                  false,
							Source:                  service.TemplateSource{},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               time.Time{},
							UpdatedAt:               time.Time{},
						},
						service.NewCacaoGeneralError("import_from_source_failed123")).Once()
					return out
				}(),
			},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &EventPortImpl{
				Storage:        tt.fields.Storage,
				TemplateSource: tt.fields.TemplateSource,
				Credential:     tt.fields.Credential,
				TimeSrc:        tt.fields.TimeSrc,
				IDGenerator: func() common.ID {
					return tt.fields.GeneratedID
				},
				VersionIDGenerator: func() common.ID {
					return tt.fields.GeneratedVersionID
				},
			}
			err := d.TemplateImportRequestedEvent(tt.args.importRequest, tt.args.sink)
			if tt.wantErr {
				assert.Errorf(t, err, "TemplateImportRequestedEvent() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				assert.NoErrorf(t, err, "TemplateImportRequestedEvent() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.fields.Storage.AssertExpectations(t)
			tt.fields.TemplateSource.AssertExpectations(t)
			tt.fields.Credential.AssertExpectations(t)
			tt.fields.TimeSrc.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestEventPortImpl_TemplateUpdateRequestedEvent(t *testing.T) {
	now := time.Now().UTC()
	type fields struct {
		Storage        *portsmocks.PersistentStoragePort
		TemplateSource *portsmocks.TemplateSourcePort
		Credential     *portsmocks.CredentialPort
		TimeSrc        ports.TimeSrc
	}
	type args struct {
		updateRequest service.TemplateModel
		sink          *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "empty actor",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				}(),
			},
			args: args{
				updateRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "", // empty actor
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed",
						service.Actor{
							Actor:    "",
							Emulator: "",
						}, types.Template{
							ID:    "template-cnm8f0598850n9abikj0",
							Owner: "",
							Name:  "",
						},
						service.NewCacaoInvalidParameterError("input validation error: actor is empty"),
					).Once()
					return out
				}(),
			},
			wantErr: assert.Error,
		},
		{
			name: "empty template ID in request",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				}(),
			},
			args: args{
				updateRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "", // empty ID
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("UpdateFailed",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:    "",
							Owner: "",
							Name:  "",
						},
						service.NewCacaoInvalidParameterError("input validation error: template id is empty"),
					).Once()
					return out
				}(),
			},
			wantErr: assert.Error,
		},
		{
			name: "update all fields",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Update",
						types.Template{
							ID:          "template-cnm8f0598850n9abikj0",
							Owner:       "testuser123",
							Name:        "template_foo",
							Description: "bar",
							Public:      false,
							Source: service.TemplateSource{
								Type: "git",
								URI:  "https://cyverse.org",
								AccessParameters: map[string]interface{}{
									"branch": "master",
									"path":   ".",
								},
								Visibility: "public",
							},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               time.Time{},
							UpdatedAt:               now,
						},
						[]string{"name", "description", "public", "source", "updated_at"},
					).Return(nil).Once()
					storage.On("Get", "testuser123", common.ID("template-cnm8f0598850n9abikj0")).Return(
						types.Template{
							ID:          "template-cnm8f0598850n9abikj0",
							Owner:       "testuser123",
							Name:        "template_foo",
							Description: "bar",
							Public:      false,
							Source: service.TemplateSource{
								Type: "git",
								URI:  "https://cyverse.org",
								AccessParameters: map[string]interface{}{
									"branch": "master",
									"path":   ".",
								},
								Visibility: "public",
							},
							LatestVersionMetadata: service.TemplateMetadata{
								SchemaURL:      "https://cyverse.org/schema",
								SchemaVersion:  "v3",
								Name:           "template_foo",
								Author:         "user123",
								AuthorEmail:    "",
								Description:    "bar",
								DocURL:         "",
								TemplateType:   "terraform_openstack",
								Purpose:        "general",
								CacaoPreTasks:  nil,
								CacaoPostTasks: nil,
								Parameters: []service.TemplateParameter{
									{
										Name:               "param1",
										Type:               "string",
										Description:        "desc123",
										Default:            stringPtr("default_val123"),
										Enum:               nil,
										DataValidationType: "",
										Required:           false,
										Editable:           false,
										Base64:             false,
									},
								},
								Filters: nil,
							},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               now.Add(-1 * time.Hour),
							UpdatedAt:               now,
						},
						nil,
					).Once()
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: &portsmocks.IncrementTimeSrc{
					Start: now,
				},
			},
			args: args{
				updateRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: []string{}, // empty array to indicate service to populate with all fields
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Updated",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:                      "template-cnm8f0598850n9abikj0",
							Owner:                   "testuser123",
							Name:                    "template_foo",
							Description:             "",
							Public:                  false,
							Source:                  service.TemplateSource{},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               time.Time{},
							UpdatedAt:               now,
						}).Once()
					return out
				}(),
			},
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &EventPortImpl{
				Storage:        tt.fields.Storage,
				TemplateSource: tt.fields.TemplateSource,
				Credential:     tt.fields.Credential,
				TimeSrc:        tt.fields.TimeSrc,
			}
			tt.wantErr(t, d.TemplateUpdateRequestedEvent(tt.args.updateRequest, tt.args.sink), fmt.Sprintf("TemplateUpdateRequestedEvent(%v, %v)", tt.args.updateRequest, tt.args.sink))
			tt.fields.Storage.AssertExpectations(t)
			tt.fields.TemplateSource.AssertExpectations(t)
			tt.fields.Credential.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}

func TestEventPortImpl_TemplateDeleteRequestedEvent(t *testing.T) {
	now := time.Now().UTC()
	type fields struct {
		Storage        ports.PersistentStoragePort
		TemplateSource ports.TemplateSourcePort
		Credential     ports.CredentialPort
		TimeSrc        ports.TimeSrc
		IDGenerator    func() common.ID
	}
	type args struct {
		deleteRequest service.TemplateModel
		sink          ports.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "empty actor",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				}(),
			},
			args: args{
				deleteRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "", // empty actor
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed",
						service.Actor{
							Actor:    "",
							Emulator: "",
						}, types.Template{
							ID:    "template-cnm8f0598850n9abikj0",
							Owner: "",
							Name:  "",
						},
						service.NewCacaoInvalidParameterError("input validation error: actor is empty"),
					).Once()
					return out
				}(),
			},
			wantErr: assert.Error,
		},
		{
			name: "empty template ID in request",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				}(),
			},
			args: args{
				deleteRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "", // empty ID
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("DeleteFailed",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:    "",
							Owner: "",
							Name:  "",
						},
						service.NewCacaoInvalidParameterError("input validation error: template id is empty"),
					).Once()
					return out
				}(),
			},
			wantErr: assert.Error,
		},
		{
			name: "delete",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("SoftDelete",
						"testuser123",
						common.ID("template-cnm8f0598850n9abikj0"),
					).Return(nil).Once()
					storage.On("Get", "testuser123", common.ID("template-cnm8f0598850n9abikj0")).Return(
						types.Template{
							ID:          "template-cnm8f0598850n9abikj0",
							Owner:       "testuser123",
							Name:        "template_foo",
							Description: "bar",
							Public:      false,
							Source: service.TemplateSource{
								Type: "git",
								URI:  "https://cyverse.org",
								AccessParameters: map[string]interface{}{
									"branch": "master",
									"path":   ".",
								},
								Visibility: "public",
							},
							LatestVersionMetadata: service.TemplateMetadata{
								SchemaURL:      "https://cyverse.org/schema",
								SchemaVersion:  "v3",
								Name:           "template_foo",
								Author:         "user123",
								AuthorEmail:    "",
								Description:    "bar",
								DocURL:         "",
								TemplateType:   "terraform_openstack",
								Purpose:        "general",
								CacaoPreTasks:  nil,
								CacaoPostTasks: nil,
								Parameters: []service.TemplateParameter{
									{
										Name:               "param1",
										Type:               "string",
										Description:        "desc123",
										Default:            stringPtr("default_val123"),
										Enum:               nil,
										DataValidationType: "",
										Required:           false,
										Editable:           false,
										Base64:             false,
									},
								},
								Filters: nil,
							},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               now.Add(-1 * time.Hour),
							UpdatedAt:               now,
						},
						nil,
					).Once()
					return storage
				}(),
				TemplateSource: func() *portsmocks.TemplateSourcePort {
					src := &portsmocks.TemplateSourcePort{}
					return src
				}(),
				Credential: func() *portsmocks.CredentialPort {
					cred := &portsmocks.CredentialPort{}
					return cred
				}(),
				TimeSrc: &portsmocks.IncrementTimeSrc{
					Start: now,
				},
			},
			args: args{
				deleteRequest: service.TemplateModel{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "",
					},
					ID:          "template-cnm8f0598850n9abikj0",
					Owner:       "testuser123",
					Name:        "template_foo",
					Description: "bar",
					Public:      false,
					Source: service.TemplateSource{
						Type: "git",
						URI:  "https://cyverse.org",
						AccessParameters: map[string]interface{}{
							"branch": "master",
							"path":   ".",
						},
						Visibility: "public",
					},
					Metadata:         service.TemplateMetadata{},
					UIMetadata:       service.TemplateUIMetadata{},
					CreatedAt:        time.Time{},
					UpdatedAt:        time.Time{},
					UpdateFieldNames: nil,
					CredentialID:     "",
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("Deleted",
						service.Actor{
							Actor:    "testuser123",
							Emulator: "",
						}, types.Template{
							ID:                      "template-cnm8f0598850n9abikj0",
							Owner:                   "testuser123",
							Name:                    "template_foo",
							Description:             "",
							Public:                  false,
							Source:                  service.TemplateSource{},
							LatestVersionMetadata:   service.TemplateMetadata{},
							LatestVersionUIMetadata: service.TemplateUIMetadata{},
							CreatedAt:               time.Time{},
							UpdatedAt:               time.Time{},
						}).Once()
					return out
				}(),
			},
			wantErr: assert.NoError,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &EventPortImpl{
				Storage:        tt.fields.Storage,
				TemplateSource: tt.fields.TemplateSource,
				Credential:     tt.fields.Credential,
				TimeSrc:        tt.fields.TimeSrc,
				IDGenerator:    tt.fields.IDGenerator,
			}
			tt.wantErr(t, d.TemplateDeleteRequestedEvent(tt.args.deleteRequest, tt.args.sink), fmt.Sprintf("TemplateDeleteRequestedEvent(%v, %v)", tt.args.deleteRequest, tt.args.sink))
		})
	}
}
