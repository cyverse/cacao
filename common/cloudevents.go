package common

import (
	"encoding/json"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
)

// CreateCloudEvent takes a JSON byte slice and uses it as the data for a new
// CloudEvent object Marshaled as a JSON byte slice
func CreateCloudEvent(request []byte, eventType string, source string, id string) ([]byte, error) {
	event := cloudevents.NewEvent()
	event.SetID(id)
	event.SetType(eventType)
	event.SetTime(time.Now())
	event.SetSource(source)
	err := event.SetData("application/json;charset=utf-8", request)
	if err != nil {
		return nil, err
	}
	return event.MarshalJSON()
}

// GetRequestFromCloudEvent will unmarshal a CloudEvent object and unmarshal the
// data field into JSON bytes
func GetRequestFromCloudEvent(msg []byte) (dataBytes []byte, err error) {
	var event cloudevents.Event
	err = json.Unmarshal(msg, &event)
	if err != nil {
		return
	}
	return event.Data(), nil
}
