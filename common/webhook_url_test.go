package common

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

// make sure the constants does not get out of sync
func TestWebhookURLPathFormat(t *testing.T) {
	assert.Equal(t, fmt.Sprintf(WebhookURLPathFormat, "{template_id}", "github"), GithubWebhookURLPathForRouter)
	assert.Equal(t, fmt.Sprintf(WebhookURLPathFormat, "{template_id}", "gitlab"), GitlabWebhookURLPathForRouter)
}
