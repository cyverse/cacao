package common

// Constants for the URL path of template webhook
const (
	GitlabWebhookURLPathForRouter = "/templates/{template_id}/webhooks/gitlab/sync"
	GithubWebhookURLPathForRouter = "/templates/{template_id}/webhooks/github/sync"
	WebhookURLPathFormat          = "/templates/%s/webhooks/%s/sync"
)
