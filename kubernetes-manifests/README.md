This directory contains all relevant kubernetes resource manifests.

* `./` directory contains any rendered files, which may be used for production releases if they exist
* `skaffold/` directory contains the base kubernetes resource manifests used directly by skaffold for build, dev, etc

To render production manifest files, use the `tools/reder_artifacts.sh`.


