package types

import (
	"context"
	"encoding/json"
	"errors"
	"net/url"
	"strings"
	"time"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"github.com/cloudevents/sdk-go/v2/event"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
)

// CachePrefix is a string prefix for cache keys, which is also used as a cache tag for invalidation.
type CachePrefix string

const (
	// CachePrefixListApplicationCredentials is the prefix on the cache key for the results of a
	// user requesting a list of ApplicationCredentials.
	CachePrefixListApplicationCredentials CachePrefix = "ListApplicationCredentials"

	// CachePrefixGetApplicationCredential is the prefix on the cache key for the results of a
	// user requesting a single ApplicationCredential.
	CachePrefixGetApplicationCredential CachePrefix = "GetApplicationCredential"

	// CachePrefixListFlavors is the prefix on the cache key for the results of a
	// user requesting a list of flavors.
	CachePrefixListFlavors CachePrefix = "ListFlavors"

	// CachePrefixGetFlavor is the prefix on the cache key for the results of a
	// user requesting a single flavor.
	CachePrefixGetFlavor CachePrefix = "GetFlavor"

	// CachePrefixListImages is the prefix on the cache key for the results of a
	// user requesting a list of images.
	CachePrefixListImages CachePrefix = "ListImages"

	// CachePrefixGetImage is the rpefix on the cache key for the results of a
	// user requesting a single image.
	CachePrefixGetImage CachePrefix = "GetImage"

	// CachePrefixListProjects is the prefix on the cache key for the results of a
	// user requesting a list of Projects.
	CachePrefixListProjects CachePrefix = "ListProjects"

	// CachePrefixListProjectsWithCredList is the prefix on the cache key for the results of a
	// user requesting a list of Projects using a list of credentials.
	//
	// Note this is different from CachePrefixListProjects which uses a single credential.
	CachePrefixListProjectsWithCredList CachePrefix = "ListProjectsWithCredList"

	// CachePrefixGetProject is the rpefix on the cache key for the results of a
	// user requesting a single Project.
	CachePrefixGetProject CachePrefix = "GetProject"

	// CachePrefixListRegions is the prefix on the cache key for the results of a
	// user requesting a list of Regions.
	CachePrefixListRegions CachePrefix = "ListRegions"

	// CachePrefixListCatalog is the prefix on the cache key for the results of a
	// user requesting a list of Catalog.
	CachePrefixListCatalog CachePrefix = "ListCatalog"

	// CachePrefixListZone is the prefix on the cache key for the results of a
	// user requesting a list of DNS zone.
	CachePrefixListZone CachePrefix = "ListZone"

	// CachePrefixListRecordset is the prefix on the cache key for the results of a
	// user requesting a list of DNS recordset.
	CachePrefixListRecordset CachePrefix = "ListRecordset"
)

// QueryReply defines a type that can send a response of some sort.
type QueryReply interface {
	Reply(cloudevents.Event) error
}

// CloudEventRequest is what gets comes in as a request
type CloudEventRequest struct {
	CloudEvent event.Event
	Replyer    QueryReply
}

// OpenStackGetter represents a function that calls `openstack show` via
// os/exec.CommandContext and returns the output.
type OpenStackGetter func(context.Context, Environment, string) ([]byte, error)

// Credential ...
type Credential struct {
	Username     string
	ID           string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	OpenStackEnv Environment
}

// ConvertCredential converts credential service model to domain(openstack) credential
func ConvertCredential(cred service.CredentialModel) (Credential, error) {
	var environment Environment
	if err := json.Unmarshal([]byte(cred.Value), &environment); err != nil {
		return Credential{}, providers.NewNotOpenStackCredentialError(cred.ID, err.Error())
	}
	return Credential{
		Username:     cred.Username,
		ID:           cred.ID,
		CreatedAt:    cred.CreatedAt,
		UpdatedAt:    cred.UpdatedAt,
		OpenStackEnv: environment,
	}, nil
}

// Environment is environment variables represented in a key-value pairs fashion.
// This is used to carry OpenStack credential info and pass to OpenStack CLI.
type Environment map[string]string

// OpenStackCatalogEntry is an entry in the catalog, basically endpoints for an openstack service.
// TODO this duplicate with types in cacao-common/service/providers
type OpenStackCatalogEntry struct {
	Name      string                     `json:"name"` // name of the service, e.g. "nova"
	Type      string                     `json:"type"` // service type, e.g. "compute"
	Endpoints []OpenStackCatalogEndpoint `json:"endpoints"`
}

// OpenStackCatalogEndpoint is an REST endpoint for a service
// TODO this duplicate with types in cacao-common/service/providers
type OpenStackCatalogEndpoint struct {
	ID        string `json:"id"`
	Interface string `json:"interface"` // type of interface, e.g. "public", "admin"
	RegionID  string `json:"region_id"`
	URL       string `json:"url"`
	Region    string `json:"region"`
}

// ApplicationCredential is the structure returned by cli when creating an application credential
type ApplicationCredential struct {
	Description string    `json:"description"`
	ExpiresAt   time.Time `json:"expires_at"`
	ID          string    `json:"id"`
	Name        string    `json:"name"`
	ProjectID   string    `json:"project_id"`
	Roles       string    `json:"roles"`
	Secret      string    `json:"secret"`
	//System       interface{} `json:"system"`
	Unrestricted bool   `json:"unrestricted"`
	UserID       string `json:"user_id"`
}

var (
	// ErrApplicationCredentialNotFound HTTP 404, happens when the application credential is not found, e.g. the application credential ID is not found.
	ErrApplicationCredentialNotFound = errors.New("could not find application credential")
	// ErrAuthentication is HTTP 401, happens when the credential is bad.
	// Note: if application credential ID does NOT exist, ErrApplicationCredentialNotFound will be used instead.
	ErrAuthentication = errors.New("the request you have made requires authentication")
)

// UserLoginEvent ...
type UserLoginEvent struct {
	Token   string                 `json:"token"`
	Profile map[string]interface{} `json:"profile"`
}

// CredentialMetadata is metadata stored by provider-openstack-service to quickly distinguish which project a credential belongs to.
type CredentialMetadata struct {
	// Credential ID
	ID string `bson:"_id"`
	// Owner of the credential
	Owner     string    `bson:"owner"`
	UpdatedAt time.Time `bson:"updated_at"` // use updated_at timestamp as an identity of if the credential is still the same
	//ValueHash   string   `bson:"value_hash"`
	//ProviderIDs []string `bson:"provider_ids"`

	// OpenStack auth URL
	AuthURL string `bson:"os_auth_url"`
	// OpenStack Domain ID
	DomainID string `bson:"os_domain_id"`
	// OpenStack Domain Name
	DomainName string `bson:"os_domain_name"`
	// OpenStack User ID
	UserID string `bson:"user_id"`
	// ID of the OpenStack project that the credential is associated with. If the credential is unscoped, this field should be empty string.
	ProjectID string `bson:"os_project_id"`
	// Name of the OpenStack project that the credential is associated with. If the credential is unscoped, this field should be empty string.
	ProjectName string `bson:"os_project_name"`

	// timestamp of when this metadata is updated in storage, this will be useful if we implement some cron refresh
	MetadataUpdatedAt time.Time `bson:"metadata_updated_at"`
}

// CredentialMetadataListFilter contains filters for filtering the listing of
// credential metadata
type CredentialMetadataListFilter struct {
	// Owner filters list result by owner of credential
	Owner string
	// ProjectID filters list result by project ID (application credential)
	ProjectID string
	// ProjectID filters list result by project name (application credential)
	ProjectName string
}

// AuthURLComparison return true if considered the same URL for the purpose of identify as the same OpenStack provider.
func AuthURLComparison(url1, url2 *url.URL) (same bool) {
	if url1 == nil || url2 == nil {
		// check nil, just in case
		return false
	}
	if *url1 == *url2 {
		return true
	}
	if url1.Hostname() != url2.Hostname() {
		return false
	}
	var port1, port2 string
	port1 = url1.Port()
	port2 = url2.Port()
	if port1 == "" {
		if url1.Scheme == "https" {
			port1 = "443"
		} else if url1.Scheme == "http" {
			port1 = "80"
		} else {
			return false
		}
	}
	if port2 == "" {
		if url2.Scheme == "https" {
			port2 = "443"
		} else if url2.Scheme == "http" {
			port2 = "80"
		} else {
			// there shouldn't be other schemes
			return false
		}
	}
	if port1 != port2 {
		return false
	}
	if url1.Path == url2.Path {
		return true
	}
	if url1.Path == "" || url2.Path == "" {
		// accommodate the fact something auth urls are missing path
		return true
	}
	if strings.HasSuffix(url1.Path, "/") && !strings.HasSuffix(url2.Path, "/") && url1.Path[:len(url1.Path)-1] == url2.Path {
		return true
	}
	if !strings.HasSuffix(url1.Path, "/") && strings.HasSuffix(url2.Path, "/") && url1.Path == url2.Path[:len(url2.Path)-1] {
		return true
	}
	return false
}
