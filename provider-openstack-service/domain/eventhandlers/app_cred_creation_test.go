package eventhandlers

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	cachemocks "gitlab.com/cyverse/cacao/provider-openstack-service/domain/caching/mocks"
	credsrcmocks "gitlab.com/cyverse/cacao/provider-openstack-service/domain/credsrc/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/domain/eventhandlers/mocks"
	portsmocks "gitlab.com/cyverse/cacao/provider-openstack-service/ports/mocks"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

// this return an argument matcher that matches anything of context.Context type (not nil)
func anyNotNilContext() interface{} {
	return mock.MatchedBy(func(ctx context.Context) bool { return ctx != nil })
}

func TestAppCredCreationHandler_Handle(t *testing.T) {
	// some common objects used to setup mocks, this helps avoids defining the same object repeatedly
	type sharedObjs struct {
		request   providers.ProviderRequest
		appCred   types.ApplicationCredential
		savedCred service.CredentialModel
	}
	type args struct {
		sink func(*testing.T, args, sharedObjs) *portsmocks.OutgoingEvents
	}
	type fields struct {
		appCredFac       func(sharedObjs) *mocks.AppCredentialFactory
		appCredConverter func(sharedObjs) *mocks.AppCredentialConverter
		credMS           func(sharedObjs) *portsmocks.CredentialMS
	}
	tests := []struct {
		name       string
		sharedObjs sharedObjs
		fields     fields
		args       args
	}{
		{
			name: "success - CredentialID in request",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
				appCred: types.ApplicationCredential{
					Description:  "",
					ExpiresAt:    time.Time{},
					ID:           "new-app-cred-123",
					Name:         "new-app-cred-name-123",
					ProjectID:    "my-openstack-project-ID-123",
					Roles:        "",
					Secret:       "new-app-cred-secret-123",
					Unrestricted: false,
					UserID:       "",
				},
				savedCred: service.CredentialModel{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Username: "actor123",
					Value: func() string {
						var env = map[string]string{
							// these values are intentionally different than credFromRequest to test that what is saved to cred MS is using provider metadata for this field
							"OS_AUTH_TYPE":                     "v3applicationcredential",
							"OS_AUTH_URL":                      "https://www.cyverse.org/foobar",
							"OS_IDENTITY_API_VERSION":          "333",
							"OS_REGION_NAME":                   "region-abc",
							"OS_INTERFACE":                     "interface-abc",
							"OS_APPLICATION_CREDENTIAL_ID":     "new-app-cred-123",
							"OS_APPLICATION_CREDENTIAL_SECRET": "new-app-cred-secret-123",
						}
						data, _ := json.Marshal(env)
						return string(data)
					}(),
					Type:        "openstack",
					ID:          "new-app-cred-name-123", // same as app cred name
					Description: "openstack application credential created by CACAO for project my-openstack-project-ID-123",
					IsSystem:    false,
					IsHidden:    false,
					Visibility:  "",
					Tags: map[string]string{
						"provider-d17e30598850n9abikl0": "",
						"application":                   "",
						"cacao_managed":                 "true",
						"cacao_openstack_project_id":    "my-os-project-123",
						"cacao_provider":                "provider-d17e30598850n9abikl0",
						"os_auth_type":                  "v3applicationcredential",
					},
				},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					fac.On("Create", anyNotNilContext(), objs.request).Return(objs.appCred, nil)
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					conv.On("ConvertToCredObj",
						anyNotNilContext(),
						objs.appCred,
						service.Session{
							SessionActor:    "actor123",
							SessionEmulator: "emulator123",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(&objs.savedCred, nil)
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					credMS.On("CreateCredential", anyNotNilContext(), "actor123", "emulator123", objs.savedCred).Return(nil)
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreated", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor123",
								SessionEmulator: "emulator123",
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: "new-app-cred-name-123", // same as app cred name
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "success - Unscoped OpenStack Token in request",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "foobar-token",
							Scope: providers.ProjectScope{},
						},
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
				appCred: types.ApplicationCredential{
					Description:  "",
					ExpiresAt:    time.Time{},
					ID:           "new-app-cred-123",
					Name:         "new-app-cred-name-123",
					ProjectID:    "my-openstack-project-ID-123",
					Roles:        "",
					Secret:       "new-app-cred-secret-123",
					Unrestricted: false,
					UserID:       "",
				},
				savedCred: service.CredentialModel{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Username: "actor123",
					Value: func() string {
						var env = map[string]string{
							// these values are intentionally different than credFromRequest to test that what is saved to cred MS is using provider metadata for this field
							"OS_AUTH_TYPE":                     "v3applicationcredential",
							"OS_AUTH_URL":                      "https://www.cyverse.org/foobar",
							"OS_IDENTITY_API_VERSION":          "333",
							"OS_REGION_NAME":                   "region-abc",
							"OS_INTERFACE":                     "interface-abc",
							"OS_APPLICATION_CREDENTIAL_ID":     "new-app-cred-123",
							"OS_APPLICATION_CREDENTIAL_SECRET": "new-app-cred-secret-123",
						}
						data, _ := json.Marshal(env)
						return string(data)
					}(),
					Type:        "openstack",
					ID:          "new-app-cred-name-123", // same as app cred name
					Description: "openstack application credential created by CACAO for project my-openstack-project-ID-123",
					IsSystem:    false,
					IsHidden:    false,
					Visibility:  "",
					Tags: map[string]string{
						"provider-d17e30598850n9abikl0": "",
						"application":                   "",
						"cacao_managed":                 "true",
						"cacao_openstack_project_id":    "my-os-project-123",
						"cacao_provider":                "provider-d17e30598850n9abikl0",
						"os_auth_type":                  "v3applicationcredential",
					},
				},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					fac.On("Create", anyNotNilContext(), objs.request).Return(objs.appCred, nil)
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					conv.On("ConvertToCredObj",
						anyNotNilContext(),
						objs.appCred,
						service.Session{
							SessionActor:    "actor123",
							SessionEmulator: "emulator123",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(&objs.savedCred, nil)
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					credMS.On("CreateCredential", anyNotNilContext(), "actor123", "emulator123", objs.savedCred).Return(nil)
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreated", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor123",
								SessionEmulator: "emulator123",
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: "new-app-cred-name-123", // same as app cred name
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "empty request",
			sharedObjs: sharedObjs{
				request:   providers.ProviderRequest{},
				appCred:   types.ApplicationCredential{},
				savedCred: service.CredentialModel{},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreateFailed", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								ServiceError: service.NewCacaoInvalidParameterError("request has no actor").GetBase(),
							},
						},
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "failed to parse request, bad cred",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: "non-existent-cred-type", // bad credential in request
						Data: nil,
					},
					Args: "bad args",
				},
				appCred:   types.ApplicationCredential{},
				savedCred: service.CredentialModel{},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreateFailed", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoGeneralError("'' expected a map, got 'string'").GetBase(),
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: "",
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "failed to parse request, bad args",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "foobar-token",
							Scope: providers.ProjectScope{},
						},
					},
					Args: "bad args",
				},
				appCred:   types.ApplicationCredential{},
				savedCred: service.CredentialModel{},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreateFailed", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoGeneralError("'' expected a map, got 'string'").GetBase(),
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: "",
					}).Once()
					return eventOut
				},
			},
		},
		{
			name: "failed to create app cred",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "foobar-token",
							Scope: providers.ProjectScope{},
						},
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
				appCred: types.ApplicationCredential{
					Description:  "",
					ExpiresAt:    time.Time{},
					ID:           "new-app-cred-123",
					Name:         "app-cred-123",
					ProjectID:    "my-openstack-project-ID-123",
					Roles:        "",
					Secret:       "new-app-cred-secret-123",
					Unrestricted: false,
					UserID:       "",
				},
				savedCred: service.CredentialModel{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Username: "actor123",
					Value: func() string {
						var env = map[string]string{
							// these values are intentionally different than credFromRequest to test that what is saved to cred MS is using provider metadata for this field
							"OS_AUTH_TYPE":                     "v3applicationcredential",
							"OS_AUTH_URL":                      "https://www.cyverse.org/foobar",
							"OS_IDENTITY_API_VERSION":          "333",
							"OS_REGION_NAME":                   "region-abc",
							"OS_INTERFACE":                     "interface-abc",
							"OS_APPLICATION_CREDENTIAL_ID":     "new-app-cred-123",
							"OS_APPLICATION_CREDENTIAL_SECRET": "new-app-cred-secret-123",
						}
						data, _ := json.Marshal(env)
						return string(data)
					}(),
					Type:        "openstack",
					ID:          fmt.Sprintf("cacao-%s-%s", "provider-d17e30598850n9abikl0", "my-openstack-project-ID-123"),
					Description: "openstack application credential created by CACAO for project my-openstack-project-ID-123",
					IsSystem:    false,
					IsHidden:    false,
					Visibility:  "",
					Tags: map[string]string{
						"provider-d17e30598850n9abikl0": "",
						"application":                   "",
						"cacao_managed":                 "true",
						"cacao_openstack_project_id":    "my-os-project-123",
						"cacao_provider":                "provider-d17e30598850n9abikl0",
						"os_auth_type":                  "v3applicationcredential",
					},
				},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					fac.On("Create", anyNotNilContext(), objs.request).Return(types.ApplicationCredential{}, fmt.Errorf("failed")) // fail to create app cred
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreateFailed",
						providers.CreateApplicationCredentialResponse{
							BaseProviderReply: providers.BaseProviderReply{
								Session: service.Session{
									SessionActor:    "actor123",
									SessionEmulator: "emulator123",
									ServiceError: service.CacaoErrorBase{
										StandardMessage:   "work failed",
										ContextualMessage: "failed",
										Options:           service.CacaoErrorOptions{},
									},
								},
								Operation: "",
								Provider:  "provider-d17e30598850n9abikl0",
							},
							CredentialID: "",
						}).Once()
					return eventOut
				},
			},
		},
		{
			name: "failed to convert app cred to cred object",
			sharedObjs: sharedObjs{
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderOpenStackTokenCredential,
						Data: providers.OpenStackTokenCredential{
							Token: "foobar-token",
							Scope: providers.ProjectScope{},
						},
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
				appCred: types.ApplicationCredential{
					Description:  "",
					ExpiresAt:    time.Time{},
					ID:           "new-app-cred-123",
					Name:         "app-cred-123",
					ProjectID:    "my-openstack-project-ID-123",
					Roles:        "",
					Secret:       "new-app-cred-secret-123",
					Unrestricted: false,
					UserID:       "",
				},
				savedCred: service.CredentialModel{},
			},
			fields: fields{
				appCredFac: func(objs sharedObjs) *mocks.AppCredentialFactory {
					fac := &mocks.AppCredentialFactory{}
					fac.On("Create", anyNotNilContext(), objs.request).Return(objs.appCred, nil)
					return fac
				},
				appCredConverter: func(objs sharedObjs) *mocks.AppCredentialConverter {
					conv := &mocks.AppCredentialConverter{}
					conv.On("ConvertToCredObj",
						anyNotNilContext(),
						objs.appCred,
						service.Session{
							SessionActor:    "actor123",
							SessionEmulator: "emulator123",
						},
						common.ID("provider-d17e30598850n9abikl0"),
					).Return(nil, fmt.Errorf("failed")) // failed to convert app cred
					return conv
				},
				credMS: func(objs sharedObjs) *portsmocks.CredentialMS {
					credMS := &portsmocks.CredentialMS{}
					return credMS
				},
			},
			args: args{
				sink: func(t *testing.T, args args, obj sharedObjs) *portsmocks.OutgoingEvents {
					eventOut := &portsmocks.OutgoingEvents{}
					eventOut.On("EventApplicationCredentialsCreateFailed", providers.CreateApplicationCredentialResponse{
						BaseProviderReply: providers.BaseProviderReply{
							Session: service.Session{
								SessionActor:    "actor123",
								SessionEmulator: "emulator123",
								ServiceError: service.CacaoErrorBase{
									StandardMessage:   "work failed",
									ContextualMessage: "failed",
									Options:           service.CacaoErrorOptions{},
								},
							},
							Operation: "",
							Provider:  "provider-d17e30598850n9abikl0",
						},
						CredentialID: "",
					}).Once()
					return eventOut
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			appCredFac := tt.fields.appCredFac(tt.sharedObjs)
			appCredConv := tt.fields.appCredConverter(tt.sharedObjs)
			credMS := tt.fields.credMS(tt.sharedObjs)
			sink := tt.args.sink(t, tt.args, tt.sharedObjs)
			h := AppCredCreationHandler{
				appCredFac:       appCredFac,
				appCredConverter: appCredConv,
				credMS:           credMS,
			}
			h.Handle(tt.sharedObjs.request, sink)
			appCredFac.AssertExpectations(t)
			appCredConv.AssertExpectations(t)
			credMS.AssertExpectations(t)
			sink.AssertExpectations(t)
		})
	}
}

func Test_appCredFactory_Create(t *testing.T) {
	type args struct {
		ctx     context.Context
		request providers.ProviderRequest
	}
	type fields struct {
		openstack func() *cachemocks.OpenStackOperationCache
		credFac   func(args args) *credsrcmocks.CredentialFactory
		timeSrc   func() *portsmocks.TimeSrc
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    types.ApplicationCredential
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				openstack: func() *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					appCredName := fmt.Sprintf("cacao_%s_%d_%s", "my-openstack-project-123", 1257894000, "custom-name-123")
					openStack.On("ListProjects",
						anyNotNilContext(), types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							OpenStackEnv: testDeleteAppCredEnv,
						}).Return([]providers.Project{
						{
							ID:          "project-id-123456789",
							Name:        "my-openstack-project-123",
							Description: "",
							DomainID:    "",
							Enabled:     false,
							IsDomain:    false,
							ParentID:    "",
							Properties:  nil,
						},
					}, nil)
					openStack.On("CreateApplicationCredential",
						anyNotNilContext(),
						types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							OpenStackEnv: testDeleteAppCredEnv,
						},
						providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						appCredName,
					).Return(
						types.ApplicationCredential{
							Description:  "",
							ExpiresAt:    time.Time{},
							ID:           "new-app-cred-123",
							Name:         appCredName,
							ProjectID:    "my-openstack-project-ID-123",
							Roles:        "",
							Secret:       "new-app-cred-secret-123",
							Unrestricted: false,
							UserID:       "",
						}, nil)
					openStack.On("InvalidateCache", types.CachePrefixListProjects, "actor123").Return(nil)
					return openStack
				},
				credFac: func(args args) *credsrcmocks.CredentialFactory {
					credFac := &credsrcmocks.CredentialFactory{}
					credFac.On("GetCredential", anyNotNilContext(), args.request).Return(
						&types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							CreatedAt:    time.Time{},
							UpdatedAt:    time.Time{},
							OpenStackEnv: testDeleteAppCredEnv,
						}, nil)
					return credFac
				},
				timeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					src.On("Now").Return(time.Unix(1257894000, 0))
					return src
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
			},
			want: types.ApplicationCredential{
				Description:  "",
				ExpiresAt:    time.Time{},
				ID:           "new-app-cred-123",
				Name:         fmt.Sprintf("cacao_%s_%d_%s", "my-openstack-project-123", 1257894000, "custom-name-123"),
				ProjectID:    "my-openstack-project-ID-123",
				Roles:        "",
				Secret:       "new-app-cred-secret-123",
				Unrestricted: false,
				UserID:       "",
			},
			wantErr: false,
		},
		{
			name: "fail to get credential",
			fields: fields{
				openstack: func() *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					return openStack
				},
				credFac: func(args args) *credsrcmocks.CredentialFactory {
					credFac := &credsrcmocks.CredentialFactory{}
					credFac.On("GetCredential", anyNotNilContext(), args.request).Return(
						nil, fmt.Errorf("failed"))
					return credFac
				},
				timeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
			},
			want:    types.ApplicationCredential{},
			wantErr: true,
		},
		{
			name: "fail to list project",
			fields: fields{
				openstack: func() *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					openStack.On("InvalidateCache", types.CachePrefixListProjects, "actor123").Return(nil)
					openStack.On("ListProjects", anyNotNilContext(), types.Credential{
						Username:     "actor123",
						ID:           "cred-id-123",
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}).Return(nil, fmt.Errorf("failed"))
					return openStack
				},
				credFac: func(args args) *credsrcmocks.CredentialFactory {
					credFac := &credsrcmocks.CredentialFactory{}
					credFac.On("GetCredential", anyNotNilContext(), args.request).Return(
						&types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							CreatedAt:    time.Time{},
							UpdatedAt:    time.Time{},
							OpenStackEnv: testDeleteAppCredEnv,
						}, nil)
					return credFac
				},
				timeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
			},
			want:    types.ApplicationCredential{},
			wantErr: true,
		},
		{
			name: "project not exists",
			fields: fields{
				openstack: func() *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					openStack.On("InvalidateCache", types.CachePrefixListProjects, "actor123").Return(nil)
					openStack.On("ListProjects", anyNotNilContext(), types.Credential{
						Username:     "actor123",
						ID:           "cred-id-123",
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}).Return([]providers.Project{
						{
							ID:   "project-id-123456789",
							Name: "different-project",
						},
					}, nil)
					return openStack
				},
				credFac: func(args args) *credsrcmocks.CredentialFactory {
					credFac := &credsrcmocks.CredentialFactory{}
					credFac.On("GetCredential", anyNotNilContext(), args.request).Return(
						&types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							CreatedAt:    time.Time{},
							UpdatedAt:    time.Time{},
							OpenStackEnv: testDeleteAppCredEnv,
						}, nil)
					return credFac
				},
				timeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					return src
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
			},
			want:    types.ApplicationCredential{},
			wantErr: true,
		},
		{
			name: "fail to create app cred",
			fields: fields{
				openstack: func() *cachemocks.OpenStackOperationCache {
					openStack := &cachemocks.OpenStackOperationCache{}
					appCredName := fmt.Sprintf("cacao_%s_%d_%s", "my-openstack-project-123", 1257894000, "custom-name-123")
					openStack.On("InvalidateCache", types.CachePrefixListProjects, "actor123").Return(nil)
					openStack.On("ListProjects", anyNotNilContext(), types.Credential{
						Username:     "actor123",
						ID:           "cred-id-123",
						CreatedAt:    time.Time{},
						UpdatedAt:    time.Time{},
						OpenStackEnv: testDeleteAppCredEnv,
					}).Return([]providers.Project{
						{
							ID:          "project-id-123456789",
							Name:        "my-openstack-project-123",
							Description: "",
							DomainID:    "",
							Enabled:     false,
							IsDomain:    false,
							ParentID:    "",
							Properties:  nil,
						},
					}, nil)
					openStack.On("CreateApplicationCredential",
						anyNotNilContext(),
						types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							CreatedAt:    time.Time{},
							UpdatedAt:    time.Time{},
							OpenStackEnv: testDeleteAppCredEnv,
						},
						providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						appCredName,
					).Return(
						types.ApplicationCredential{}, fmt.Errorf("failed"))
					return openStack
				},
				credFac: func(args args) *credsrcmocks.CredentialFactory {
					credFac := &credsrcmocks.CredentialFactory{}
					credFac.On("GetCredential", anyNotNilContext(), args.request).Return(
						&types.Credential{
							Username:     "actor123",
							ID:           "cred-id-123",
							CreatedAt:    time.Time{},
							UpdatedAt:    time.Time{},
							OpenStackEnv: testDeleteAppCredEnv,
						}, nil)
					return credFac
				},
				timeSrc: func() *portsmocks.TimeSrc {
					src := &portsmocks.TimeSrc{}
					src.On("Now").Return(time.Unix(1257894000, 0))
					return src
				},
			},
			args: args{
				ctx: context.Background(),
				request: providers.ProviderRequest{
					Session: service.Session{
						SessionActor:    "actor123",
						SessionEmulator: "emulator123",
					},
					Operation: "",
					Provider:  "provider-d17e30598850n9abikl0",
					Credential: providers.ProviderCredential{
						Type: providers.ProviderCredentialID,
						Data: "cred-id-123",
					},
					Args: providers.ApplicationCredentialCreationArgs{
						Scope: providers.ProjectScope{
							Project: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-project-123",
							},
							Domain: struct {
								Name string `json:"name,omitempty"`
								ID   string `json:"id,omitempty"`
							}{
								Name: "my-openstack-domain-123",
							},
						},
						NamePostfix: "custom-name-123",
					},
				},
			},
			want:    types.ApplicationCredential{},
			wantErr: true,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			openstack := tt.fields.openstack()
			credFac := tt.fields.credFac(tt.args)
			timeSrc := tt.fields.timeSrc()
			f := appCredFactory{
				openStack: openstack,
				credFac:   credFac,
				timeSrc:   timeSrc,
			}
			got, err := f.Create(tt.args.ctx, tt.args.request)
			if tt.wantErr {
				if !assert.Errorf(t, err, "Create(%v, %v)", tt.args.ctx, tt.args.request) {
					return
				}
			} else {
				if !assert.NoErrorf(t, err, "Create(%v, %v)", tt.args.ctx, tt.args.request) {
					return
				}
			}
			assert.Equalf(t, tt.want, got, "Create(%v, %v)", tt.args.ctx, tt.args.request)
			openstack.AssertExpectations(t)
			credFac.AssertExpectations(t)
			timeSrc.AssertExpectations(t)
		})
	}
}

func Test_appCredConverter_ConvertToCredObj(t *testing.T) {
	type fields struct {
		providerMS func() *portsmocks.ProviderMetadataMS
	}
	type args struct {
		ctx        context.Context
		appCred    types.ApplicationCredential
		session    service.Session
		providerID common.ID
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *service.CredentialModel
		wantErr bool
	}{
		{
			name: "normal",
			fields: fields{
				providerMS: func() *portsmocks.ProviderMetadataMS {
					providerMS := &portsmocks.ProviderMetadataMS{}
					providerMS.On("GetMetadata", anyNotNilContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://www.cyverse.org/foobar",
							"OS_IDENTITY_API_VERSION": "333",
							"OS_REGION_NAME":          "region-abc",
							"OS_INTERFACE":            "interface-abc",
						}, nil)
					return providerMS
				},
			},
			args: args{
				ctx: context.Background(),
				appCred: types.ApplicationCredential{
					ID:        "foobar",
					Name:      "app-cred-name-123",
					ProjectID: "my-os-project-123",
					Secret:    "foobar-secret",
				},
				session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				providerID: "provider-d17e30598850n9abikl0",
			},
			want: &service.CredentialModel{
				Session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				ID:       "app-cred-name-123",
				Name:     "app-cred-name-123", // same as app cred name
				Username: "actor123",
				Type:     "openstack",
				Value: func() string {
					var env = map[string]string{
						"OS_AUTH_TYPE":                     "v3applicationcredential",
						"OS_AUTH_URL":                      "https://www.cyverse.org/foobar",
						"OS_IDENTITY_API_VERSION":          "333",
						"OS_REGION_NAME":                   "region-abc",
						"OS_INTERFACE":                     "interface-abc",
						"OS_APPLICATION_CREDENTIAL_ID":     "foobar",
						"OS_APPLICATION_CREDENTIAL_SECRET": "foobar-secret",
					}
					data, _ := json.Marshal(env)
					return string(data)
				}(),
				Description: "openstack application credential created by CACAO for project my-os-project-123",
				Tags: map[string]string{
					"provider-d17e30598850n9abikl0": "",
					"application":                   "",
					"cacao_managed":                 "true",
					"cacao_openstack_project_id":    "my-os-project-123",
					"cacao_provider":                "provider-d17e30598850n9abikl0",
					"os_auth_type":                  "v3applicationcredential",
				},
			},
			wantErr: false,
		},
		{
			name: "empty app cred name",
			fields: fields{
				providerMS: func() *portsmocks.ProviderMetadataMS {
					providerMS := &portsmocks.ProviderMetadataMS{}
					return providerMS
				},
			},
			args: args{
				ctx: context.Background(),
				appCred: types.ApplicationCredential{
					ID:        "foobar",
					Name:      "", // empty name
					ProjectID: "my-os-project-123",
					Secret:    "foobar-secret",
				},
				session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				providerID: "provider-d17e30598850n9abikl0",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "provider metadata missing required field OS_INTERFACE",
			fields: fields{
				providerMS: func() *portsmocks.ProviderMetadataMS {
					providerMS := &portsmocks.ProviderMetadataMS{}
					providerMS.On("GetMetadata", anyNotNilContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{
							"OS_AUTH_URL":             "https://www.cyverse.org/foobar",
							"OS_IDENTITY_API_VERSION": "333",
							"OS_REGION_NAME":          "region-abc",
						}, // missing OS_INTERFACE
						nil,
					)
					return providerMS
				},
			},
			args: args{
				ctx: context.Background(),
				appCred: types.ApplicationCredential{
					ID:        "foobar",
					Name:      "foobar-123",
					ProjectID: "my-os-project-123",
					Secret:    "foobar-secret",
				},
				session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				providerID: "provider-d17e30598850n9abikl0",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "empty provider metadata",
			fields: fields{
				providerMS: func() *portsmocks.ProviderMetadataMS {
					providerMS := &portsmocks.ProviderMetadataMS{}
					providerMS.On("GetMetadata", anyNotNilContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						map[string]interface{}{}, // empty metadata
						nil,
					)
					return providerMS
				},
			},
			args: args{
				ctx: context.Background(),
				appCred: types.ApplicationCredential{
					ID:        "foobar",
					Name:      "foobar-123",
					ProjectID: "my-os-project-123",
					Secret:    "foobar-secret",
				},
				session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				providerID: "provider-d17e30598850n9abikl0",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "fail to get provider metadata",
			fields: fields{
				providerMS: func() *portsmocks.ProviderMetadataMS {
					providerMS := &portsmocks.ProviderMetadataMS{}
					providerMS.On("GetMetadata", anyNotNilContext(), "actor123", "emulator123", common.ID("provider-d17e30598850n9abikl0")).Return(
						nil,
						fmt.Errorf("failed"), // errored
					)
					return providerMS
				},
			},
			args: args{
				ctx: context.Background(),
				appCred: types.ApplicationCredential{
					ID:        "foobar",
					Name:      "foobar-123",
					ProjectID: "my-os-project-123",
					Secret:    "foobar-secret",
				},
				session: service.Session{
					SessionActor:    "actor123",
					SessionEmulator: "emulator123",
				},
				providerID: "provider-d17e30598850n9abikl0",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			providerMS := tt.fields.providerMS()
			h := appCredConverter{
				providerMS: providerMS,
			}
			got, err := h.ConvertToCredObj(tt.args.ctx, tt.args.appCred, tt.args.session, tt.args.providerID)
			if tt.wantErr {
				assert.Errorf(t, err, "ConvertToCredObj(%v, %v, %v, %v)", tt.args.ctx, tt.args.appCred, tt.args.session, tt.args.providerID)
			} else {
				assert.NoErrorf(t, err, "ConvertToCredObj(%v, %v, %v, %v)", tt.args.ctx, tt.args.appCred, tt.args.session, tt.args.providerID)
			}
			assert.Equalf(t, tt.want, got, "ConvertToCredObj(%v, %v, %v, %v)", tt.args.ctx, tt.args.appCred, tt.args.session, tt.args.providerID)
			providerMS.AssertExpectations(t)
		})
	}
}
