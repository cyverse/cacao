package queryhandlers

import (
	"context"
	"net/url"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
)

func errorReply(request providers.ProviderRequest, err error) providers.BaseProviderReply {
	var svcErr service.CacaoErrorBase
	if svcErr1, ok := err.(service.CacaoError); ok {
		svcErr = svcErr1.GetBase()
	} else {
		svcErr = service.NewCacaoGeneralError(err.Error()).GetBase()
	}
	return providers.BaseProviderReply{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    svcErr,
		},
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

func requestToBaseReply(request providers.ProviderRequest) providers.BaseProviderReply {
	return providers.BaseProviderReply{
		Session:   request.Session,
		Operation: request.Operation,
		Provider:  request.Provider,
	}
}

// getting OS_AUTH_URL from provider metadata, this url is being used as comparison criteria to determine if a credential is suitable on a provider.
func getOpenStackAuthURLFromProviderMetadata(ctx context.Context, providerMetadataMS ports.ProviderMetadataMS, request providers.ProviderRequest) (*url.URL, error) {
	metadata, err := providerMetadataMS.GetMetadata(ctx, request.SessionActor, request.SessionEmulator, request.Provider)
	if err != nil {
		return nil, err
	}
	var openstackMetadata struct {
		AuthURL           string `mapstructure:"OS_AUTH_URL"`
		RegionName        string `mapstructure:"OS_REGION_NAME"`
		ProjectDomainName string `mapstructure:"OS_PROJECT_DOMAIN_NAME"`
		ProjectDomainID   string `mapstructure:"OS_PROJECT_DOMAIN_ID"`
	}
	err = mapstructure.Decode(metadata, &openstackMetadata)
	if err != nil {
		return nil, err
	}
	metadataAuthURL, err := url.Parse(openstackMetadata.AuthURL)
	if err != nil {
		return nil, err
	}
	return metadataAuthURL, nil
}
