package caching

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/eko/gocache/cache"
	"github.com/eko/gocache/store"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"time"
)

// OpenStackOperationCacheLayer is a cache layer wrapped around ports.OpenStack
type OpenStackOperationCacheLayer struct {
	openstack  ports.OpenStack
	opCache    cache.CacheInterface
	defaultTTL time.Duration
}

var _ OpenStackOperationCache = (*OpenStackOperationCacheLayer)(nil)

// OpenStackOperationCache is a wrapper on top of ports.OpenStack
type OpenStackOperationCache interface {
	ports.OpenStack
	InvalidateCache(prefix types.CachePrefix, username string) error
}

// NewOpenStackOperationCache ...
func NewOpenStackOperationCache(openstack ports.OpenStack, opCache cache.CacheInterface, defaultTTL time.Duration) OpenStackOperationCacheLayer {
	if defaultTTL < time.Second {
		defaultTTL = time.Second
	}
	return OpenStackOperationCacheLayer{
		openstack:  openstack,
		opCache:    opCache,
		defaultTTL: defaultTTL,
	}
}

// AuthenticationTest ...
func (oc OpenStackOperationCacheLayer) AuthenticationTest(ctx context.Context, credential types.Environment) error {
	return oc.openstack.AuthenticationTest(ctx, credential)
}

// InspectCredential ...
func (oc OpenStackOperationCacheLayer) InspectCredential(ctx context.Context, credential types.Credential) (types.CredentialMetadata, error) {
	return oc.openstack.InspectCredential(ctx, credential)
}

// GetApplicationCredential get a specific application credential, this share cache with ListApplicationCredentials.
func (oc OpenStackOperationCacheLayer) GetApplicationCredential(ctx context.Context, credential types.Credential, id string) (*providers.ApplicationCredential, error) {
	appCredList, err := oc.ListApplicationCredentials(ctx, credential)
	if err != nil {
		return nil, err
	}
	for _, appCred := range appCredList {
		if appCred.ID == id {
			return &appCred, nil
		}
	}
	return oc.openstack.GetApplicationCredential(ctx, credential, id)
}

// GetFlavor get a specific flavor, this share cache with ListFlavors.
func (oc OpenStackOperationCacheLayer) GetFlavor(ctx context.Context, credential types.Credential, id string, region string) (*providers.Flavor, error) {
	flavors, err := oc.ListFlavors(ctx, credential, region)
	if err != nil {
		return nil, err
	}
	for _, f := range flavors {
		if f.ID == id {
			return &f, nil
		}
	}
	return oc.openstack.GetFlavor(ctx, credential, id, region)
}

// GetImage get a specific image, this share cache with ListImages.
func (oc OpenStackOperationCacheLayer) GetImage(ctx context.Context, credential types.Credential, id string, region string) (*providers.OpenStackImage, error) {
	images, err := oc.ListImages(ctx, credential, region)
	if err != nil {
		return nil, err
	}
	for _, img := range images {
		if img.ID == id {
			return &img, nil
		}
	}
	return oc.openstack.GetImage(ctx, credential, id, region)
}

// GetProject get a specific project, this share cache with ListProjects.
func (oc OpenStackOperationCacheLayer) GetProject(ctx context.Context, credential types.Credential, id string) (*providers.Project, error) {
	project, err := oc.ListProjects(ctx, credential)
	if err != nil {
		return nil, err
	}
	for _, proj := range project {
		if proj.ID == id {
			return &proj, nil
		}
	}
	return oc.openstack.GetProject(ctx, credential, id)
}

// GetToken ...
func (oc OpenStackOperationCacheLayer) GetToken(ctx context.Context, credential types.Credential) (*providers.Token, error) {
	return oc.openstack.GetToken(ctx, credential)
}

// ListRegions list regions, this will use cache.
func (oc OpenStackOperationCacheLayer) ListRegions(ctx context.Context, credential types.Credential) ([]providers.Region, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListRegions(ctx, credential)
	}
	const prefix = types.CachePrefixListRegions
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.Region](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListRegions(ctx, credential)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.Region](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListApplicationCredentials list application credential user owns, this will use cache.
func (oc OpenStackOperationCacheLayer) ListApplicationCredentials(ctx context.Context, credential types.Credential) ([]providers.ApplicationCredential, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListApplicationCredentials(ctx, credential)
	}
	const prefix = types.CachePrefixListApplicationCredentials
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.ApplicationCredential](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListApplicationCredentials(ctx, credential)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.ApplicationCredential](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListFlavors list flavors in a region, this will use cache.
func (oc OpenStackOperationCacheLayer) ListFlavors(ctx context.Context, credential types.Credential, region string) ([]providers.Flavor, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListFlavors(ctx, credential, region)
	}
	const prefix = types.CachePrefixListFlavors
	key := GetCacheKeyRegional(prefix, credential.Username, credential.ID, region)
	list, err := getCachedJSONValue[[]providers.Flavor](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListFlavors(ctx, credential, region)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.Flavor](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListImages list images in a region, this will use cache.
func (oc OpenStackOperationCacheLayer) ListImages(ctx context.Context, credential types.Credential, region string) ([]providers.OpenStackImage, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListImages(ctx, credential, region)
	}
	const prefix = types.CachePrefixListImages
	key := GetCacheKeyRegional(prefix, credential.Username, credential.ID, region)
	list, err := getCachedJSONValue[[]providers.OpenStackImage](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListImages(ctx, credential, region)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.OpenStackImage](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListProjects list projects, this will use cache.
func (oc OpenStackOperationCacheLayer) ListProjects(ctx context.Context, credential types.Credential) ([]providers.Project, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListProjects(ctx, credential)
	}
	const prefix = types.CachePrefixListProjects
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.Project](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListProjects(ctx, credential)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.Project](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListAllProjectsUsingCredentialList list projects using a list of credential, this will use cache.
func (oc OpenStackOperationCacheLayer) ListAllProjectsUsingCredentialList(ctx context.Context, credentials []types.Credential) (map[string][]providers.Project, error) {
	key, cacheable, err := oc.cacheKeyForListAllProjectsUsingCredentialList(credentials)
	if err != nil {
		return nil, err
	}
	if !cacheable {
		return oc.openstack.ListAllProjectsUsingCredentialList(ctx, credentials)
	}
	mapping, err := getCachedJSONValue[map[string][]providers.Project](oc.opCache, key)
	if err == nil {
		return mapping, nil
	}
	return oc.openstack.ListAllProjectsUsingCredentialList(ctx, credentials)
}

func (oc OpenStackOperationCacheLayer) cacheKeyForListAllProjectsUsingCredentialList(credentials []types.Credential) (cacheKey string, cacheable bool, err error) {
	// use a hash of cred IDs in the list as part of the cache key, since cache key include username, so chances of collision should be very small
	credIDListHash := sha256.New()
	for _, cred := range credentials {
		if oc.notCacheable(cred) {
			// if there is a single cred in the list that is not cacheable, then skip cache entirely
			return "", false, nil
		}

		// base64 encode the ID, since cred ID could be arbitrary string, and we need a way to distinguish separator character
		_, err = credIDListHash.Write([]byte(base64.StdEncoding.EncodeToString([]byte(cred.ID))))
		if err != nil {
			return "", true, err
		}

		// write separator (_ is not in character sets of base64.StdEncoding, so it is distinguishable and safe to use as separator for hash)
		_, err = credIDListHash.Write([]byte("_"))
		if err != nil {
			return "", true, err
		}
	}
	if len(credentials) == 0 {
		return "", false, nil
	}
	const prefix = types.CachePrefixListProjectsWithCredList
	// NOTE: we are making the assumption here that all credential in the list belong the same user
	key := GetCacheKey(prefix, credentials[0].Username, base64.StdEncoding.EncodeToString(credIDListHash.Sum(nil)))
	return key, true, nil
}

// ListCatalog list catalogs, this will use cache.
func (oc OpenStackOperationCacheLayer) ListCatalog(ctx context.Context, credential types.Credential) ([]providers.CatalogEntry, error) {
	if oc.notCacheable(credential) {
		return oc.openstack.ListCatalog(ctx, credential)
	}
	const prefix = types.CachePrefixListCatalog
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.CatalogEntry](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListCatalog(ctx, credential)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.CatalogEntry](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListZone list DNS zones, this will use cache.
func (oc OpenStackOperationCacheLayer) ListZone(ctx context.Context, credential types.Credential) ([]providers.DNSZone, error) { // FIXME return type
	if oc.notCacheable(credential) {
		return oc.openstack.ListZone(ctx, credential)
	}
	const prefix = types.CachePrefixListZone
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.DNSZone](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListZone(ctx, credential)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.DNSZone](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// ListRecordset list DNS recordset, this will use cache.
func (oc OpenStackOperationCacheLayer) ListRecordset(ctx context.Context, credential types.Credential, zone string) ([]providers.DNSRecordset, error) { // FIXME return type
	if oc.notCacheable(credential) {
		return oc.openstack.ListRecordset(ctx, credential, zone)
	}
	const prefix = types.CachePrefixListRecordset
	key := GetCacheKey(prefix, credential.Username, credential.ID)
	list, err := getCachedJSONValue[[]providers.DNSRecordset](oc.opCache, key)
	if err == nil {
		return list, nil
	}

	list, err = oc.openstack.ListRecordset(ctx, credential, zone)
	if err == nil {
		cacheTags := GetCacheTags(prefix, credential.Username, credential.ID)
		err2 := saveCachedJSONValue[[]providers.DNSRecordset](oc.opCache, key, cacheTags, list, oc.defaultTTL)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Error("fail to save to cache")
		}
	}
	return list, err
}

// CreateApplicationCredential ...
func (oc OpenStackOperationCacheLayer) CreateApplicationCredential(ctx context.Context, credential types.Credential, scope providers.ProjectScope, name string) (types.ApplicationCredential, error) {
	return oc.openstack.CreateApplicationCredential(ctx, credential, scope, name)
}

// DeleteApplicationCredential ...
func (oc OpenStackOperationCacheLayer) DeleteApplicationCredential(ctx context.Context, credential types.Credential, idOrName string) error {
	return oc.openstack.DeleteApplicationCredential(ctx, credential, idOrName)
}

// notCacheable checks if a credential is cache-able.
// Token (ID == "") credential is not cache-able.
func (oc OpenStackOperationCacheLayer) notCacheable(credential types.Credential) bool {
	return credential.ID == "" || credential.Username == ""
}

// InvalidateCache invalidate cache for a user on a specific operation.
func (oc OpenStackOperationCacheLayer) InvalidateCache(prefix types.CachePrefix, username string) error {
	return oc.opCache.Invalidate(store.InvalidateOptions{Tags: []string{
		getPrefixCacheTag(prefix),
		getUsernameCacheTag(username),
	}})
}

func getCachedJSONValue[T any](c cache.CacheInterface, key string) (T, error) {
	var val T
	cachedValue, err := c.Get(key)
	if err != nil {
		log.WithField("key", key).WithError(err).Trace("cache miss")
		return val, err
	}
	switch cachedValue.(type) {
	case []byte:
		err = json.Unmarshal(cachedValue.([]byte), &val)
		if err != nil {
			return val, err
		}
	case string:
		err = json.Unmarshal([]byte(cachedValue.(string)), &val)
		if err != nil {
			return val, err
		}
	default:
		err = fmt.Errorf("cached value not []byte nor string")
		err2 := c.Delete(key)
		if err2 != nil {
			log.WithField("key", key).WithError(err).Warn("fail to delete invalid cached value, cached value is not []byte nor string")
		} else {
			log.WithField("key", key).WithError(err).Warn("cached value is not []byte nor string")
		}
		return val, err
	}
	return val, err
}

func saveCachedJSONValue[T any](c cache.CacheInterface, key string, tags []string, value T, ttl time.Duration) error {
	marshal, err := json.Marshal(value)
	if err != nil {
		return err
	}
	return c.Set(key, marshal, &store.Options{
		Cost:       int64(len(marshal)),
		Expiration: ttl,
		Tags:       tags,
	})
}

// GetCacheKey accepts a prefix and generates a key that can be used to retrieve a cached value for non-regional
// operation. By convention, this prefix should match the function which is calling GetCacheKey.
func GetCacheKey(prefix types.CachePrefix, username string, credID string) string {
	return fmt.Sprintf("%s.%s.%s", prefix, username, credID)
}

// GetCacheKeyRegional accepts a prefix and generates a key that can be used to retrieve a cached value for a regional operation.
// By convention, this prefix should match the function which is calling GetCacheKeyRegional.
func GetCacheKeyRegional(prefix types.CachePrefix, username string, credID string, region string) string {
	return fmt.Sprintf("%s.%s.%s.%s", prefix, username, credID, region)
}

// GetCacheTags returns the cache tags for a cached value. The tags are used for invalidate values.
func GetCacheTags(prefix types.CachePrefix, username string, credID string) []string {
	return []string{getPrefixCacheTag(prefix), getUsernameCacheTag(username), getCredIDCacheTag(credID)}
}

func getPrefixCacheTag(prefix types.CachePrefix) string {
	return string(prefix)
}

func getUsernameCacheTag(username string) string {
	// add a prefix to prevent collision with other tag, since username is user supplied
	return "username-" + username
}

func getCredIDCacheTag(credID string) string {
	// add a prefix to prevent collision with other tag, since cred ID is user supplied
	return "credid-" + credID
}
