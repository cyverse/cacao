package caching

import (
	"github.com/eko/gocache/cache"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"testing"
	"time"
)

func TestOpenStackOperationCacheLayer_cacheKeyForListAllProjectsUsingCredentialList(t *testing.T) {
	type fields struct {
		openstack  ports.OpenStack
		opCache    cache.CacheInterface
		defaultTTL time.Duration
	}
	type args struct {
		credentials []types.Credential
	}
	tests := []struct {
		name          string
		fields        fields
		args          args
		wantCacheKey  string
		wantCacheable bool
		wantErr       bool
	}{
		{
			name:   "1 cred",
			fields: fields{},
			args: args{
				credentials: []types.Credential{
					{
						Username:     "actor123",
						ID:           "cred123",
						OpenStackEnv: map[string]string{},
					},
				},
			},
			wantCacheKey:  string(types.CachePrefixListProjectsWithCredList) + ".actor123.sKVpMUE2jvc+uQeBoLeoRQRX43NuedwIfgousEBR2Cc=",
			wantCacheable: true,
			wantErr:       false,
		},
		{
			name:   "2 cred",
			fields: fields{},
			args: args{
				credentials: []types.Credential{
					{
						Username:     "actor123",
						ID:           "cred123",
						OpenStackEnv: map[string]string{},
					},
					{
						Username:     "actor123",
						ID:           "cred456",
						OpenStackEnv: map[string]string{},
					},
				},
			},
			wantCacheKey:  string(types.CachePrefixListProjectsWithCredList) + ".actor123.Vq7osSwI/Jt34dXJl/ON2cMFo7z6yH6Qy/FXUxjZFH4=",
			wantCacheable: true,
			wantErr:       false,
		},
		{
			name:   "not cacheable",
			fields: fields{},
			args: args{
				credentials: []types.Credential{
					{
						Username: "actor123",
						// token credential does not have ID
						ID: "",
						OpenStackEnv: map[string]string{
							"OS_AUTH_URL":             "https://cyverse.org",
							"OS_IDENTITY_API_VERSION": "3",
							"OS_REGION_NAME":          "region-123",
							"OS_INTERFACE":            "interface-123",
							"OS_TOKEN":                "token-123", // token credential is not cacheable
						},
					},
				},
			},
			wantCacheKey:  "",
			wantCacheable: false,
			wantErr:       false,
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			oc := OpenStackOperationCacheLayer{
				openstack:  tt.fields.openstack,
				opCache:    tt.fields.opCache,
				defaultTTL: tt.fields.defaultTTL,
			}
			gotCacheKey, gotCacheable, err := oc.cacheKeyForListAllProjectsUsingCredentialList(tt.args.credentials)
			if (err != nil) != tt.wantErr {
				t.Errorf("cacheKeyForListAllProjectsUsingCredentialList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotCacheKey != tt.wantCacheKey {
				t.Errorf("cacheKeyForListAllProjectsUsingCredentialList() gotCacheKey = %v, want %v", gotCacheKey, tt.wantCacheKey)
			}
			if gotCacheable != tt.wantCacheable {
				t.Errorf("cacheKeyForListAllProjectsUsingCredentialList() gotCacheable = %v, want %v", gotCacheable, tt.wantCacheable)
			}
		})
	}
}
