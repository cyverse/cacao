package triggeredactions

import (
	"context"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/provider-openstack-service/ports"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// InspectCredentialWithWaitGroup is InspectCredential but with WaitGroup and ignore error.
func InspectCredentialWithWaitGroup(wg *sync.WaitGroup, openstack ports.OpenStack, credential *types.Credential, metadataStorage ports.CredentialMetadataStorage, session service.Session, credID string) {
	defer wg.Done()

	ctx, cancel := context.WithTimeout(context.TODO(), time.Minute)
	defer cancel()
	_ = InspectCredential(ctx, openstack, credential, metadataStorage, session, credID)
}

// InspectCredential inspects the given credential using the OpenStack API and saves the metadata
// in the storage.
func InspectCredential(ctx context.Context, openstack ports.OpenStack, credential *types.Credential, metadataStorage ports.CredentialMetadataStorage, session service.Session, credID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "triggeredactions",
		"function": "InspectCredential",
		"actor":    session.SessionActor,
		"emulator": session.SessionEmulator,
		"credID":   credID,
	})

	credentialMetadata, err := openstack.InspectCredential(ctx, *credential)
	if err != nil {
		logger.WithError(err).Error("failed to inspect credential to get its metadata")
		return err
	}
	err = metadataStorage.SaveCredential(credential, credentialMetadata)
	if err != nil {
		logger.WithError(err).Error("failed to save credential metadata")
		return err
	}
	return nil
}

// InspectUserCredentials inspect all openstack credential that a user owns and save the metadata for those credentials to storage.
func InspectUserCredentials(wg *sync.WaitGroup, openstack ports.OpenStack, credentialMS ports.CredentialMS, metadataStorage ports.CredentialMetadataStorage, username string, session service.Session) {
	defer wg.Done()

	ctx, cancel := context.WithTimeout(context.TODO(), time.Minute)
	defer cancel()

	err := inspectUserCredentials(ctx, openstack, credentialMS, metadataStorage, username, session)
	if err != nil {
		log.WithFields(log.Fields{
			"package":  "triggeredactions",
			"function": "InspectUserCredentials",
			"actor":    session.SessionActor,
			"emulator": session.SessionEmulator,
			"username": username,
		}).WithError(err).Error()
	}
}

func inspectUserCredentials(ctx context.Context, openstack ports.OpenStack, credentialMS ports.CredentialMS, metadataStorage ports.CredentialMetadataStorage, username string, session service.Session) error {
	credentials, err := credentialMS.ListCredentialsByTag(ctx, username, "", "")
	if err != nil {
		return err
	}
	for _, cred := range credentials {
		err = InspectCredential(ctx, openstack, &cred, metadataStorage, session, cred.ID)
		if err != nil {
			return err
		}
	}
	return nil
}
