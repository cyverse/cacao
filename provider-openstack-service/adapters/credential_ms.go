package adapters

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao-common/service/providers"
	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) *CredentialMicroservice {
	return &CredentialMicroservice{queryConn: queryConn, eventConn: eventConn}
}

// GetCredential ...
func (svc CredentialMicroservice) GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.GetCredential",
		"actor":    actor,
		"emulator": emulator,
		"ID":       ID,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credential, err := client.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return nil, err
	}
	converted, err := svc.convertCredential(credential)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cred value")
		return nil, err
	}
	logger.Info("credential fetched")
	return &converted, nil
}

// ListCredentials list the enabled openstack credential that actor owns
func (svc CredentialMicroservice) ListCredentials(ctx context.Context, actor, emulator string) ([]service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentials",
		"actor":    actor,
		"emulator": emulator,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credList, err := client.List(ctx, service.Actor{Actor: actor, Emulator: emulator}, service.CredentialListFilter{
		Username: actor,
		Type:     service.OpenStackCredentialType,
		IsSystem: nil,
		Disabled: common.PtrOf(false),
	})
	if err != nil {
		return nil, err
	}
	for i := range credList {
		// list operation does not include value (redacted), thus re-fetch each cred individually for the value.
		refetchedCred, err := client.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, credList[i].ID)
		if err != nil {
			logger.WithError(err).Error("fail to re-fetch cred for cred value")
			continue
		}
		if refetchedCred == nil {
			// guard against nil
			logger.Error("Get return nil on success")
			continue
		}
		credList[i].Value = refetchedCred.Value
	}
	logger.WithFields(log.Fields{
		"listCount": len(credList),
	}).Info("credential fetched")
	return credList, nil
}

// ListCredentialsByTag list the enabled openstack credential that actor owns that has the specified tagName
func (svc CredentialMicroservice) ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentialsByTag",
		"actor":    actor,
		"emulator": emulator,
		"tag":      tagName,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	tagsFilter := make(map[string]string)
	if tagName != "" {
		tagsFilter[tagName] = ""
	}
	credList, err := client.List(ctx, service.Actor{Actor: actor, Emulator: emulator}, service.CredentialListFilter{
		Username: actor,
		Type:     service.OpenStackCredentialType,
		IsSystem: nil,
		Disabled: common.PtrOf(false),
		Tags:     tagsFilter,
	})
	if err != nil {
		logger.WithError(err).Error("fail to list credentials")
		return nil, err
	}
	openstackCredList := make([]types.Credential, 0)
	for _, cred := range credList {
		// list operation does not include value (redacted), thus re-fetch each cred individually for the value.
		refetchedCred, err := svc.GetCredential(ctx, actor, emulator, cred.ID)
		if err != nil {
			logger.WithError(err).Error("fail to re-fetch cred for cred value")
			continue
		}
		if refetchedCred == nil {
			// guard against nil
			logger.Error("GetCredential return nil on success")
			continue
		}
		openstackCredList = append(openstackCredList, *refetchedCred)
	}
	logger.WithFields(log.Fields{
		"listCount": len(credList),
	}).Info("credential fetched")

	if len(openstackCredList) == 0 {
		return []types.Credential{}, nil
	}
	return openstackCredList, nil
}

func (svc CredentialMicroservice) credentialHasTag(cred *service.CredentialModel, tagName string) bool {
	for _, tag := range cred.GetTags() {
		if tag == tagName {
			return true
		}
	}
	return false
}

func (svc CredentialMicroservice) convertCredential(cred *service.CredentialModel) (types.Credential, error) {
	var environment types.Environment
	if err := json.Unmarshal([]byte(cred.Value), &environment); err != nil {
		return types.Credential{}, providers.NewNotOpenStackCredentialError(cred.ID, err.Error())
	}
	return types.Credential{
		Username:     cred.Username,
		ID:           cred.ID,
		CreatedAt:    cred.CreatedAt,
		UpdatedAt:    cred.UpdatedAt,
		OpenStackEnv: environment,
	}, nil
}

// CreateCredential creates a credential via credential microservice
func (svc CredentialMicroservice) CreateCredential(ctx context.Context, actor, emulator string, cred service.CredentialModel) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.CreateCredential",
		"actor":    actor,
		"emulator": emulator,
		"credID":   cred.ID,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, svc.eventConn)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	credID, err := client.Add(ctx, service.Actor{Actor: actor, Emulator: emulator}, cred)
	if err == nil {
		logger.WithField("credID", credID).Info("credential created")
	}
	return err
}

// TagCredential update/add tags for a credential
func (svc CredentialMicroservice) TagCredential(ctx context.Context, actor, emulator string, ID string, tags map[string]string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.TagCredential",
		"actor":    actor,
		"emulator": emulator,
		"credID":   ID,
	})
	if len(tags) == 0 {
		logger.Info("no tags specified")
		return nil
	}
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, svc.eventConn)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	err = client.Update(ctx, service.Actor{Actor: actor, Emulator: emulator}, ID, service.CredentialUpdate{UpdateOrAddTags: tags})
	if err != nil {
		logger.WithError(err).Error("fail to tag credential")
		return err
	}
	return nil
}

// DeleteCredential deletes a credential
func (svc CredentialMicroservice) DeleteCredential(ctx context.Context, actor, emulator, ID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.DeleteCredential",
		"actor":    actor,
		"emulator": emulator,
		"credID":   ID,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, svc.eventConn)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	err = client.Delete(ctx, service.Actor{Actor: actor, Emulator: emulator}, ID)
	if err != nil {
		logger.WithError(err).Error("fail to delete credential")
		return err
	}
	return nil
}
