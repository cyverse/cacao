package adapters

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao/provider-openstack-service/types"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// CredentialMetadataMongoAdapter stores credential metadata for openstack credentials. Note the
// data stores here is a long-term cache, though the information is unlikely to
// change, this should not be treated as the ultimate source of truth, the
// upstream openstack server should be.
type CredentialMetadataMongoAdapter struct {
	conn *db.MongoDBConnection
}

// NewCredentialMetadataMongoAdapter ...
func NewCredentialMetadataMongoAdapter(conf *types.Configuration) (*CredentialMetadataMongoAdapter, error) {
	conn, err := db.NewMongoDBConnection(&conf.MongoDBConfig)
	if err != nil {
		return nil, err
	}
	return &CredentialMetadataMongoAdapter{
		conn: conn,
	}, nil
}

const credMetadataCollectionName = "openstack_credential_metadata"

// ListCredentials list all credential metadata stored for a user.
//
// Filter by project ID/name if they are specified (non-empty)
func (ma *CredentialMetadataMongoAdapter) ListCredentials(filters types.CredentialMetadataListFilter) ([]types.CredentialMetadata, error) {
	var result []types.CredentialMetadata
	var filter = bson.M{
		"owner": filters.Owner,
	}
	if filters.ProjectID != "" {
		filter["os_project_id"] = filters.ProjectID
	}
	if filters.ProjectName != "" {
		filter["os_project_name"] = filters.ProjectName
	}
	err := ma.conn.List(credMetadataCollectionName, filter, &result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetCredential gets the metadata for a single credential
func (ma *CredentialMetadataMongoAdapter) GetCredential(owner string, credID string) (types.CredentialMetadata, error) {
	var result types.CredentialMetadata
	var filter = bson.M{
		"_id":   credID,
		"owner": owner,
	}
	err := ma.conn.Get(credMetadataCollectionName, filter, &result)
	if err != nil {
		return result, err
	}
	return result, nil
}

// SaveCredential upsert the credential metadata record to mongodb
func (ma *CredentialMetadataMongoAdapter) SaveCredential(cred *types.Credential, metadata types.CredentialMetadata) error {
	filter := bson.M{
		"_id": metadata.ID,
	}
	metadata.MetadataUpdatedAt = time.Now().UTC()
	update := bson.M{
		"$set": metadata,
	}
	result, err := ma.conn.Database.Collection(credMetadataCollectionName).UpdateOne(context.TODO(), filter, update, options.Update().SetUpsert(true))
	if err != nil {
		return err
	}
	log.WithFields(log.Fields{
		"modifiedCount": result.ModifiedCount,
		"credID":        metadata.ID,
	}).Info("credential metadata upsert")
	return nil
}
