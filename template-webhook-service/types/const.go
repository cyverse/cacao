package types

import "gitlab.com/cyverse/cacao-common/service"

const (
	// natsQueueGroup is the NATS Queue Group for this service
	natsQueueGroup = "template_webhook_queue_group"
	// natsWildcardSubject is the NATS subject to subscribe
	natsWildcardSubject = string(service.NatsSubjectTemplateWebhook) + ".>"

	// DefaultNatsClientID is a default NATS Client ID
	DefaultNatsClientID = "template-webhook-service"

	// natsDurableName is the NATS Durable Name for this service
	natsDurableName = "template_webhook_durable"

	// DefaultMongoDBURL is a default MongoDB URL
	DefaultMongoDBURL = "mongodb://localhost:27017"
	// DefaultMongoDBName is a default MongoDB Name
	DefaultMongoDBName = "cacao_template_webhook"
	// DefaultWebhookMongoDBCollectionName is a default MongoDB Collection Name
	DefaultWebhookMongoDBCollectionName = "template_webhook"

	// TemplateWebhookIDPrefix is the prefix for XID for Template Webhook ID
	TemplateWebhookIDPrefix = "templatewebhook"
)
