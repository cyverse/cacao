package types

import (
	"crypto/hmac"
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

// https://docs.github.com/en/webhooks/using-webhooks/validating-webhook-deliveries
func TestGithubWebhookSignatureVerify(t *testing.T) {
	type args struct {
		key          []byte
		signatureHex string
		requestBody  []byte
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "github example",
			args: args{
				key:          []byte("It's a Secret to Everybody"),
				signatureHex: "757107ea0eb2509fc211221cce984b8a37570b6d7586c22c46f4379c8b043e17",
				requestBody:  []byte("Hello, World!"),
			},
			want: true,
		},
		{
			name: "github example with sha256= prefix",
			args: args{
				key:          []byte("It's a Secret to Everybody"),
				signatureHex: "sha256=57107ea0eb2509fc211221cce984b8a37570b6d7586c22c46f4379c8b043e17",
				requestBody:  []byte("Hello, World!"),
			},
			want: false, // we expect the input to be strip of the "sha256=" prefix
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GithubWebhookSignatureVerify(tt.args.key, tt.args.signatureHex, tt.args.requestBody); got != tt.want {
				t.Errorf("GithubWebhookSignatureVerify() = %v, want %v", got, tt.want)
			}
		})
	}

	t.Run("random", func(t *testing.T) {
		for i := 0; i < 1000; i++ {
			_, keyBytes := GenerateGithubWebhookSecretKey()
			h := hmac.New(sha256.New, keyBytes)
			var data = make([]byte, 1024)
			_, err := rand.Read(data)
			if !assert.NoError(t, err) {
				return
			}
			h.Write(data)
			assert.True(t, GithubWebhookSignatureVerify(keyBytes, hex.EncodeToString(h.Sum(nil)), data))
		}
	})
}

func BenchmarkGenerateGithubWebhookSecretKey(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _ = GenerateGithubWebhookSecretKey()
	}
}

func BenchmarkGithubWebhookSignatureVerify(b *testing.B) {
	_, keyBytes := GenerateGithubWebhookSecretKey()
	// runtime of signature verify depends on length of the request body
	for _, bodyLen := range []int{512, 1024, 2048, 10240, 100 << 10} {
		b.Run(fmt.Sprintf("body_len_%d", bodyLen), func(b *testing.B) {
			result := make([]bool, b.N)
			h := hmac.New(sha256.New, keyBytes)
			var data = make([]byte, bodyLen)
			_, err := rand.Read(data)
			if err != nil {
				b.FailNow()
				return
			}
			h.Write(data)
			signature := hex.EncodeToString(h.Sum(nil))
			for i := 0; i < b.N; i++ {
				result[i] = GithubWebhookSignatureVerify(keyBytes, signature, data)
			}
		})
	}
}

func TestGitlabWebhookTokenCompare(t *testing.T) {
	for i := 0; i < 1000; i++ {
		token, hash, salt := GenerateGitlabWebhookToken()
		if !assert.True(t, GitlabWebhookTokenCompare(token, hash, salt)) {
			return
		}
	}
}

func BenchmarkGenerateGitlabWebhookToken(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_, _, _ = GenerateGitlabWebhookToken()
	}
}

func BenchmarkGitlabWebhookTokenCompare(b *testing.B) {
	token, hash, salt := GenerateGitlabWebhookToken()
	result := make([]bool, b.N)
	for i := 0; i < b.N; i++ {
		result[i] = GitlabWebhookTokenCompare(token, hash, salt)
	}
}
