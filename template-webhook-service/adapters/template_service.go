package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// TemplateServiceClient implements ports.TemplateService
type TemplateServiceClient struct {
	queryConn messaging2.QueryConnection
	client    service.TemplateClient
}

// NewTemplateServiceClient ...
func NewTemplateServiceClient(queryConn messaging2.QueryConnection) (*TemplateServiceClient, error) {
	client, err := service.NewNatsTemplateClientFromConn(queryConn, nil)
	if err != nil {
		return nil, err
	}
	return &TemplateServiceClient{
		queryConn: queryConn,
		client:    client,
	}, nil
}

// CheckPermission checks if actor has permission to create/delete webhook for the template
// Actor has to be owner of the template.
func (svc *TemplateServiceClient) CheckPermission(ctx context.Context, actor service.Actor, templateID common.ID) (allowed bool, err error) {
	template, err := svc.client.Get(ctx, actor, templateID)
	if err != nil {
		return false, err
	}
	return template.GetOwner() == actor.Actor, nil
}
