package adapters

import (
	"context"
	"encoding/json"
	"sync"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	conn     messaging2.QueryConnection
	handlers ports.IncomingQueryHandlers
}

var _ ports.IncomingQueryPort = &QueryAdapter{}

// NewQueryAdapter ...
func NewQueryAdapter(queryConn messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		conn:     queryConn,
		handlers: nil,
	}
}

// SetHandlers ...
func (adapter *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	log.Info("starting QueryAdapter")
	return adapter.conn.Listen(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		service.TemplateWebhookListQueryOp: adapter.handleTemplateWebhookListQuery,
	}, wg)
}

func (adapter *QueryAdapter) handleTemplateWebhookListQuery(ctx context.Context, requestCe cloudevents.Event, writer messaging2.ReplyWriter) {
	logger := log.StandardLogger()

	var listRequest service.TemplateWebhookListRequest
	err := json.Unmarshal(requestCe.Data(), &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		adapter.marshalErrorReplyForListRequest(errorMessage, writer)
		return
	}
	listReply := adapter.handlers.List(ctx, listRequest)
	adapter.reply(listReply, cacao_common.QueryOp(requestCe.Type()), writer)
}

func (adapter *QueryAdapter) marshalErrorReplyForListRequest(errorMessage string, writer messaging2.ReplyWriter) {
	var listReply = service.WorkspaceListModel{
		Session: service.Session{
			ServiceError: service.NewCacaoMarshalError(errorMessage).GetBase(),
		},
	}
	adapter.reply(listReply, service.WorkspaceListQueryOp, writer)
}

func (adapter *QueryAdapter) reply(replyBody interface{}, op cacao_common.QueryOp, writer messaging2.ReplyWriter) {
	replyCe, err := messaging2.CreateCloudEventWithAutoSource(replyBody, op)
	if err != nil {
		log.WithError(err).WithField("ceType", op).Error("fail to create cloudevent for reply")
		return
	}
	err = writer.Write(replyCe)
	if err != nil {
		log.WithError(err).WithField("ceType", op).Error("fail to write reply")
	}
}
