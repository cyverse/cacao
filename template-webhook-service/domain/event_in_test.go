package domain

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	portsmocks "gitlab.com/cyverse/cacao/template-webhook-service/ports/mocks"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

func TestEventHandlers_Create(t *testing.T) {
	now := time.Now().UTC()
	testCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	type fields struct {
		TemplateService                *portsmocks.TemplateService
		Storage                        *portsmocks.PersistentStoragePort
		GenerateWebhookID              func() common.ID
		GenerateGitlabWebhookToken     func() (token string, hash, salt []byte)
		GenerateGithubWebhookSecretKey func() (key string, keyBytes []byte)
		TimeSrc                        func() time.Time
	}
	type args struct {
		request service.TemplateWebhookCreationRequest
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "empty struct",
			fields: fields{
				TemplateService:                &portsmocks.TemplateService{},
				Storage:                        &portsmocks.PersistentStoragePort{},
				GenerateWebhookID:              nil,
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc:                        nil,
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								ServiceError: service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
						},
					).Once()
					return out
				}(),
			},
			wantErr: true,
		},
		{
			name: "empty actor",
			fields: fields{
				TemplateService: &portsmocks.TemplateService{},
				Storage:         &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc:                        nil,
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "gitlab",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      now,
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID:     "template-d3jng0598850n9abiklg",
								Platform:       "gitlab",
								Gitlab:         service.TemplateGitlabWebhook{},
								Github:         service.TemplateGithubWebhook{},
								CreatedAt:      time.Time{},
								LastExecutedAt: time.Time{},
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "empty webhook platform",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(true, nil).Once()
					return svc
				}(),
				Storage: &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoInvalidParameterError("invalid platform type").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "",
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "invalid webhook platform",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(true, nil).Once()
					return svc
				}(),
				Storage: &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "git",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoInvalidParameterError("invalid platform type").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "git",
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "bad id",
			fields: fields{
				TemplateService: &portsmocks.TemplateService{},
				Storage:         &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID: "template-",
						Platform:   "gitlab",
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoInvalidParameterError("input validation error: ill-formed template id").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-",
								Platform:   "gitlab",
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "unauthorized",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(false, nil).Once()
					return svc
				}(),
				Storage: &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "gitlab",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoUnauthorizedError("permission denied").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "gitlab",
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "permission check failed",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(false, fmt.Errorf("bad")).Once()
					return svc
				}(),
				Storage: &portsmocks.PersistentStoragePort{},
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken:     nil,
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "gitlab",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoGeneralError("permission check failed").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "gitlab",
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "gitlab_success",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(true, nil).Once()
					return svc
				}(),
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", mock.Anything, types.TemplateWebhook{
						WebhookID:  "templatewebhook-cp5uu7po4u50l189nckg",
						TemplateID: "template-d3jng0598850n9abiklg",
						Owner:      "testuser123",
						Platform:   "gitlab",
						Gitlab: types.TemplateWebhookGitlabConfig{
							SecretTokenHash: []byte("aaaaaaaaaa"),
							SecretTokenSalt: []byte("bbbbb"),
						},
						Github:         types.TemplateWebhookGithubConfig{},
						CreatedAt:      now,
						LastExecutedAt: time.Time{},
					}).Return(nil)
					return storage
				}(),
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken: func() (token string, hash, salt []byte) {
					token = "token-12345"
					hash = []byte("aaaaaaaaaa")
					salt = []byte("bbbbb")
					return
				},
				GenerateGithubWebhookSecretKey: nil,
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "gitlab",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreatedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "gitlab",
								Gitlab: service.TemplateGitlabWebhook{
									Token: "token-12345",
								},
								CreatedAt:      now,
								LastExecutedAt: time.Time{},
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "github_success",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(true, nil).Once()
					return svc
				}(),
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", mock.Anything, types.TemplateWebhook{
						WebhookID:  "templatewebhook-cp5uu7po4u50l189nckg",
						TemplateID: "template-d3jng0598850n9abiklg",
						Owner:      "testuser123",
						Platform:   "github",
						Gitlab:     types.TemplateWebhookGitlabConfig{},
						Github: types.TemplateWebhookGithubConfig{
							Key: []byte("key-12345"),
						},
						CreatedAt:      now,
						LastExecutedAt: time.Time{},
					}).Return(nil)
					return storage
				}(),
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken: nil,
				GenerateGithubWebhookSecretKey: func() (key string, keyBytes []byte) {
					return "key-12345", []byte("key-12345")
				},
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "github",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreatedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID: "template-d3jng0598850n9abiklg",
								Platform:   "github",
								Gitlab:     service.TemplateGitlabWebhook{},
								Github: service.TemplateGithubWebhook{
									Key: "key-12345",
								},
								CreatedAt:      now,
								LastExecutedAt: time.Time{},
							},
						},
					).Once()
					return out
				}(),
			},
		},
		{
			name: "storage errored",
			fields: fields{
				TemplateService: func() *portsmocks.TemplateService {
					svc := &portsmocks.TemplateService{}
					svc.On("CheckPermission",
						mock.Anything,
						service.Actor{
							Actor:    "testuser123",
							Emulator: "emulator123",
						},
						common.ID("template-d3jng0598850n9abiklg"),
					).Return(true, nil).Once()
					return svc
				}(),
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Create", mock.Anything, types.TemplateWebhook{
						WebhookID:  "templatewebhook-cp5uu7po4u50l189nckg",
						TemplateID: "template-d3jng0598850n9abiklg",
						Owner:      "testuser123",
						Platform:   "github",
						Gitlab:     types.TemplateWebhookGitlabConfig{},
						Github: types.TemplateWebhookGithubConfig{
							Key: []byte("key-12345"),
						},
						CreatedAt:      now,
						LastExecutedAt: time.Time{},
					}).Return(fmt.Errorf("bad"))
					return storage
				}(),
				GenerateWebhookID: func() common.ID {
					return "templatewebhook-cp5uu7po4u50l189nckg"
				},
				GenerateGitlabWebhookToken: nil,
				GenerateGithubWebhookSecretKey: func() (key string, keyBytes []byte) {
					return "key-12345", []byte("key-12345")
				},
				TimeSrc: func() time.Time {
					return now
				},
			},
			args: args{
				request: service.TemplateWebhookCreationRequest{
					Session: service.Session{SessionActor: "testuser123", SessionEmulator: "emulator123"},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "github",
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookCreateFailedEvent",
						service.TemplateWebhookCreationResponse{
							Session: service.Session{
								SessionActor:    "testuser123",
								SessionEmulator: "emulator123",
								ServiceError:    service.NewCacaoGeneralError("bad").GetBase(),
							},
							TemplateWebhook: service.TemplateWebhook{
								TemplateID:     "template-d3jng0598850n9abiklg",
								Platform:       "github",
								Gitlab:         service.TemplateGitlabWebhook{},
								Github:         service.TemplateGithubWebhook{},
								CreatedAt:      time.Time{},
								LastExecutedAt: time.Time{},
							},
						},
					).Once()
					return out
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				TemplateService:                tt.fields.TemplateService,
				Storage:                        tt.fields.Storage,
				GenerateWebhookID:              tt.fields.GenerateWebhookID,
				GenerateGitlabWebhookToken:     tt.fields.GenerateGitlabWebhookToken,
				GenerateGithubWebhookSecretKey: tt.fields.GenerateGithubWebhookSecretKey,
				TimeSrc:                        tt.fields.TimeSrc,
			}
			h.Create(testCtx, tt.args.request, tt.args.sink)
			tt.args.sink.AssertExpectations(t)
			tt.fields.Storage.AssertExpectations(t)
		})
	}
}

func TestEventHandlers_Delete(t *testing.T) {
	testCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()
	now := time.Now().UTC()
	getTime := func() time.Time { return now }
	type fields struct {
		Storage *portsmocks.PersistentStoragePort
		TimeSrc func() time.Time
	}
	type args struct {
		request service.TemplateWebhookDeletionRequest
		sink    *portsmocks.OutgoingEventPort
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "empty",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: service.TemplateWebhookDeletionRequest{},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookDeleteFailedEvent", service.TemplateWebhookDeletionResponse{
						Session: service.Session{
							ServiceError: service.NewCacaoInvalidParameterError("input validation error: actor is empty").GetBase(),
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "empty id",
			fields: fields{
				Storage: &portsmocks.PersistentStoragePort{},
				TimeSrc: getTime,
			},
			args: args{
				request: service.TemplateWebhookDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "",
						Platform:       "",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookDeleteFailedEvent", service.TemplateWebhookDeletionResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoInvalidParameterError("input validation error: template id is empty").GetBase(),
						},
						TemplateWebhook: service.TemplateWebhook{
							TemplateID: "",
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "actor != owner",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Delete", mock.Anything, "testuser123", common.ID("template-d3jng0598850n9abiklg")).Return(
						service.NewCacaoNotFoundError("bad")).Once()
					return storage
				}(),
				TimeSrc: getTime,
			},
			args: args{
				request: service.TemplateWebhookDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookDeleteFailedEvent", service.TemplateWebhookDeletionResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
							ServiceError:    service.NewCacaoNotFoundError("bad").GetBase(),
						},
						TemplateWebhook: service.TemplateWebhook{
							TemplateID:     "template-d3jng0598850n9abiklg",
							Platform:       "",
							Gitlab:         service.TemplateGitlabWebhook{},
							Github:         service.TemplateGithubWebhook{},
							CreatedAt:      time.Time{},
							LastExecutedAt: time.Time{},
						},
					}).Once()
					return out
				}(),
			},
		},
		{
			name: "success",
			fields: fields{
				Storage: func() *portsmocks.PersistentStoragePort {
					storage := &portsmocks.PersistentStoragePort{}
					storage.On("Delete", mock.Anything, "testuser123", common.ID("template-d3jng0598850n9abiklg")).Return(nil).Once()
					return storage
				}(),

				TimeSrc: getTime,
			},
			args: args{
				request: service.TemplateWebhookDeletionRequest{
					Session: service.Session{
						SessionActor:    "testuser123",
						SessionEmulator: "emulator123",
					},
					TemplateWebhook: service.TemplateWebhook{
						TemplateID:     "template-d3jng0598850n9abiklg",
						Platform:       "",
						Gitlab:         service.TemplateGitlabWebhook{},
						Github:         service.TemplateGithubWebhook{},
						CreatedAt:      time.Time{},
						LastExecutedAt: time.Time{},
					},
				},
				sink: func() *portsmocks.OutgoingEventPort {
					out := &portsmocks.OutgoingEventPort{}
					out.On("TemplateWebhookDeletedEvent", service.TemplateWebhookDeletionResponse{
						Session: service.Session{
							SessionActor:    "testuser123",
							SessionEmulator: "emulator123",
						},
						TemplateWebhook: service.TemplateWebhook{
							TemplateID: "template-d3jng0598850n9abiklg",
						},
					}).Once()
					return out
				}(),
			},
		},
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := &EventHandlers{
				Storage: tt.fields.Storage,
				TimeSrc: tt.fields.TimeSrc,
			}
			h.Delete(testCtx, tt.args.request, tt.args.sink)
			tt.fields.Storage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
