package domain

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/template-webhook-service/ports"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
)

// EventHandlers implements IncomingEventPort
type EventHandlers struct {
	TemplateService                ports.TemplateService
	Storage                        ports.PersistentStoragePort
	GenerateWebhookID              func() common.ID
	GenerateGitlabWebhookToken     func() (token string, hash, salt []byte)
	GenerateGithubWebhookSecretKey func() (key string, keyBytes []byte)
	TimeSrc                        func() time.Time
}

var _ ports.IncomingEventHandlers = &EventHandlers{}

// Create creates a template webhook
func (h *EventHandlers) Create(ctx context.Context, request service.TemplateWebhookCreationRequest, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"template": request.TemplateID,
	})
	if len(request.SessionActor) == 0 {
		err := service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	if !request.TemplateID.Validate() || request.TemplateID.FullPrefix() != "template" {
		// if we are using ID supplied by client, then we need to validate it
		err := service.NewCacaoInvalidParameterError("input validation error: ill-formed template id")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	allowed, err := h.TemplateService.CheckPermission(ctx, service.ActorFromSession(request.Session), request.TemplateID)
	if err != nil {
		logger.WithError(err).Error("fail to check permission")
		cerr := service.NewCacaoGeneralError("permission check failed")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, cerr))
		return
	}
	if !allowed {
		cerr := service.NewCacaoUnauthorizedError("permission denied")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, cerr))
		return
	}

	now := h.TimeSrc()
	var webhook = types.TemplateWebhook{
		WebhookID:      h.GenerateWebhookID(),
		TemplateID:     request.TemplateID,
		Owner:          request.SessionActor,
		Platform:       request.Platform,
		Gitlab:         types.TemplateWebhookGitlabConfig{},
		Github:         types.TemplateWebhookGithubConfig{},
		CreatedAt:      now,
		LastExecutedAt: time.Time{},
	}

	var response = service.TemplateWebhookCreationResponse{
		Session: service.CopySessionActors(request.Session),
		TemplateWebhook: service.TemplateWebhook{
			TemplateID: request.TemplateID,
			Platform:   request.Platform,
			Gitlab: struct {
				Token string `json:"token"`
			}{
				Token: "",
			},
			Github: struct {
				Key string `json:"key"`
			}{
				Key: "",
			},
			CreatedAt:      time.Time{},
			LastExecutedAt: time.Time{},
		},
	}

	switch request.Platform {
	case service.TemplateWebhookGitlabPlatform:
		token, hash, salt := h.GenerateGitlabWebhookToken()
		webhook.Gitlab.SecretTokenHash = hash
		webhook.Gitlab.SecretTokenSalt = salt
		response.Gitlab.Token = token
	case service.TemplateWebhookGithubPlatform:
		keyStr, key := h.GenerateGithubWebhookSecretKey()
		webhook.Github.Key = key
		response.Github.Key = keyStr
	default:
		err := service.NewCacaoInvalidParameterError("invalid platform type")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, err))
		return
	}

	logger.WithField("id", webhook.WebhookID).Debugf("Creating webhook")

	err = h.Storage.Create(ctx, webhook)
	if err != nil {
		logger.WithError(err).Error("fail to create template webhook in storage")
		sink.TemplateWebhookCreateFailedEvent(h.creationErrorResponse(request, service.ToCacaoError(err)))
		return
	}

	logger.WithFields(log.Fields{
		"id":       webhook.WebhookID,
		"platform": webhook.Platform,
	}).Info("template webhook created")
	response.CreatedAt = now
	sink.TemplateWebhookCreatedEvent(response)
}

func (h *EventHandlers) creationErrorResponse(request service.TemplateWebhookCreationRequest, cacaoError service.CacaoError) service.TemplateWebhookCreationResponse {
	return service.TemplateWebhookCreationResponse{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		TemplateWebhook: service.TemplateWebhook{
			TemplateID: request.TemplateID,
			Platform:   request.Platform,
		},
	}
}

// Delete deletes the template webhook
func (h *EventHandlers) Delete(ctx context.Context, request service.TemplateWebhookDeletionRequest, sink ports.OutgoingEventPort) {
	logger := log.WithFields(log.Fields{
		"actor":    request.SessionActor,
		"emulator": request.SessionEmulator,
		"template": request.TemplateID,
	})

	logger.Infof("deleting template webhook")
	if len(request.SessionActor) == 0 {
		err := service.NewCacaoInvalidParameterError("input validation error: actor is empty")
		logger.WithError(err).Error("bad request")
		sink.TemplateWebhookDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	if len(request.TemplateID) == 0 {
		err := service.NewCacaoInvalidParameterError("input validation error: template id is empty")
		logger.WithError(err).Error("bad request")
		sink.TemplateWebhookDeleteFailedEvent(h.deletionErrorResponse(request, err))
		return
	}

	err := h.Storage.Delete(ctx, request.SessionActor, request.TemplateID)
	if err != nil {
		logger.WithError(err).Error("fail to delete template webhook in storage")
		sink.TemplateWebhookDeleteFailedEvent(h.deletionErrorResponse(request, service.ToCacaoError(err)))
		return
	}
	logger.Infof("template webhook deleted")

	sink.TemplateWebhookDeletedEvent(service.TemplateWebhookDeletionResponse{
		Session: service.CopySessionActors(request.Session),
		TemplateWebhook: service.TemplateWebhook{
			TemplateID: request.TemplateID,
		},
	})
}

func (h *EventHandlers) deletionErrorResponse(request service.TemplateWebhookDeletionRequest, cacaoError service.CacaoError) service.TemplateWebhookDeletionResponse {
	return service.TemplateWebhookDeletionResponse{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    cacaoError.GetBase(),
		},
		TemplateWebhook: service.TemplateWebhook{
			TemplateID: request.TemplateID,
		},
	}
}
