package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao/template-webhook-service/adapters"
	"gitlab.com/cyverse/cacao/template-webhook-service/domain"
	"gitlab.com/cyverse/cacao/template-webhook-service/types"
	"io"
)

func main() {
	var config types.Config
	err := envconfig.Process("", &config)
	if err != nil {
		log.Fatal(err.Error())
	}
	config.ProcessDefaults()
	config.Override()

	level, err := log.ParseLevel(config.LogLevel)
	if err != nil {
		log.WithError(err).Fatal("Error parsing log level")
		return
	}
	log.SetLevel(level)

	log.SetReportCaller(true)

	natsConn, err := config.NatsStanConfig.ConnectNats()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logError(&natsConn)
	stanConn, err := config.NatsStanConfig.ConnectStan()
	if err != nil {
		log.WithError(err).Error()
		return
	}
	defer logError(&stanConn)

	// add and initialize the storage adapter
	mongoAdapter := &adapters.MongoAdapter{}
	defer logError(mongoAdapter)

	eventAdapter := adapters.NewEventAdapter(&stanConn)

	templateServiceClient, err := adapters.NewTemplateServiceClient(&natsConn)
	if err != nil {
		log.WithError(err).Error()
		return
	}

	httpServer := adapters.NewHTTPServer(config)

	var svc = domain.Domain{
		Storage:         mongoAdapter,
		QueryIn:         adapters.NewQueryAdapter(&natsConn),
		EventIn:         eventAdapter,
		EventOut:        eventAdapter,
		HTTPServer:      httpServer,
		TemplateService: templateServiceClient,
	}
	err = svc.Init(&config)
	if err != nil {
		log.WithError(err).Error("fail to init service")
		return
	}

	serviceContext, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	svc.Start(serviceContext)
}

func logError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
