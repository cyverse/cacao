package main

import (
	"context"
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/xid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/adapters"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
	"io"
)

var globalConf types.Config

func init() {
	err := envconfig.Process("", &globalConf.MiscConfig)
	if err != nil {
		log.Fatal(err)
	}
	logLevel, err := log.ParseLevel(globalConf.MiscConfig.LogLevel)
	if err != nil {
		log.Fatal(err)
	}
	log.SetLevel(logLevel)
	err = globalConf.MiscConfig.Validate()
	if err != nil {
		log.Fatal(err)
	}
	err = envconfig.Process("", &globalConf.Messaging)
	if err != nil {
		log.Fatal(err)
	}
	err = envconfig.Process("AWM", &globalConf.AWMStanConfig)
	if err != nil {
		log.Fatal(err)
	}
	if globalConf.MiscConfig.PodName == "" {
		globalConf.Messaging.ClientID = "deployment-exec-" + xid.New().String()
	} else {
		globalConf.Messaging.ClientID = globalConf.MiscConfig.PodName
	}
	err = envconfig.Process("", &globalConf.MongoConfig)
	if err != nil {
		log.Fatal(err)
	}
	globalConf.Override()
	fmt.Printf("%+v\n", globalConf)
}

func main() {
	runStorage := adapters.NewTFRunMongoStorage(globalConf.MongoConfig)
	defer logCloserError(runStorage)

	natsConn, err := globalConf.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Panic("fail to connect to NATS")
	}
	defer logCloserError(&natsConn)
	stanConn, err := globalConf.Messaging.ConnectStan()
	if err != nil {
		log.WithError(err).Panic("fail to connect to STAN")
	}
	defer logCloserError(&stanConn)

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	svc := newService(&natsConn, &stanConn, runStorage)
	if err := svc.Init(globalConf); err != nil {
		log.WithError(err).Panic("fail to init service")
	}
	if err := svc.Start(serviceCtx); err != nil {
		log.WithError(err).Panic()
	}
}

func newService(nc *messaging2.NatsConnection, sc *messaging2.StanConnection, storage *adapters.TFRunMongoStorage) domain.Domain {

	stanAdapter := adapters.NewStanAdapter(sc)
	return domain.Domain{
		QuerySrc:        adapters.NewQueryAdapter(nc),
		EventSrc:        stanAdapter,
		TFRunStorage:    storage,
		WorkflowStorage: adapters.NewDeploymentWorkflowStorage(globalConf.MongoConfig),
		KeySrc:          adapters.NewEncryptionKeyEnvVar("ENCRYPTION_KEY"),
		TimeSrc:         adapters.UTCTimeSrc{},
		SSHKeySrc: adapters.SSHKeySrc{
			SSHKey: globalConf.MiscConfig.CacaoSSHKey,
		},
		MetadataSvc:    adapters.NewDeploymentMetadataService(nc),
		WorkspaceMS:    adapters.NewWorkspaceMicroservice(nc),
		TemplateMS:     adapters.NewTemplateMicroservice(nc),
		CredentialMS:   adapters.NewCredentialMicroservice(nc),
		ProviderMS:     adapters.NewProviderMicroservice(nc),
		AWM:            adapters.NewArgoWorkflowMediator(sc),
		WhitelistCIDRs: globalConf.MiscConfig.WhitelistCIDRs,
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
		return
	}
}
