package types

import (
	"fmt"
	"net"

	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/db"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"golang.org/x/crypto/ssh"
)

// IncomingEvent is a incoming event to be handled
type IncomingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	CloudEvent() cloudevents.Event
}

// OutgoingEvent is a outgoing event to be published
type OutgoingEvent interface {
	EventType() common.EventType
	Transaction() common.TransactionID
	ToCloudEvent(source string) (cloudevents.Event, error)
}

// MiscConfig are miscellaneous configurations that can be load from env var.
type MiscConfig struct {
	PodName  string `envconfig:"POD_NAME"`
	LogLevel string `envconfig:"LOG_LEVEL" default:"trace"`
	// CACAO's public SSH key
	CacaoSSHKey string `envconfig:"CACAO_SSH_KEY"`
	// comma separated list, e.g. export CACAO_WHITELIST_CIDRS=127.0.0.1/24,8.8.8.8/32
	WhitelistCIDRs     []string `envconfig:"CACAO_WHITELIST_CIDRS"`
	LogsStorageBaseURL string   `envconfig:"LOGS_STORAGE_BASE_URL"`
	LogsJWTKeyBase64   string   `envconfig:"LOGS_JWT_KEY_BASE64"`
}

// Validate ...
func (envConf MiscConfig) Validate() error {
	if envConf.CacaoSSHKey == "" {
		return fmt.Errorf("ssh key is missing")
	}
	_, _, _, _, err := ssh.ParseAuthorizedKey([]byte(envConf.CacaoSSHKey))
	if err != nil {
		return fmt.Errorf("fail to parse CACAO's public ssh key in EnvConfig, %w", err)
	}
	err = envConf.validateCIDR()
	if err != nil {
		return err
	}
	return nil
}

func (envConf MiscConfig) validateCIDR() error {
	for _, cidr := range envConf.WhitelistCIDRs {
		_, _, err := net.ParseCIDR(cidr)
		if err != nil {
			return fmt.Errorf("fail to parse CIDR, %s, %w", cidr, err)
		}
	}
	return nil
}

// Config ...
type Config struct {
	MiscConfig MiscConfig
	Messaging  messaging2.NatsStanMsgConfig
	// STAN config for AWM
	AWMStanConfig messaging2.NatsStanMsgConfig
	MongoConfig   db.MongoDBConfig
}

// Override ...
func (conf *Config) Override() {
	// we are only serving 1 type of query in this service, so specific subject should suffice, no need for a wildcard subject.
	conf.Messaging.WildcardSubject = string(service.DeploymentGetLogsLocationQueryType)
	conf.Messaging.DurableName = "deployment-terraform"
	conf.Messaging.QueueGroup = "deployment-terraform"
}

// TerraformOpenStackDeploymentType is a deployment type for Terraform template engine and OpenStack cloud provider
const TerraformOpenStackDeploymentType service.TemplateTypeName = "openstack_terraform"

// TerraformAWSDeploymentType is a deployment type for Terraform template engine and AWS cloud provider
const TerraformAWSDeploymentType service.TemplateTypeName = "aws_terraform"

// Actor is the user that performs certain action.
// FIXME remove this alias, once migration is done
type Actor = service.Actor

// ActorFromSession construct Actor from service.Session
func ActorFromSession(session service.Session) Actor {
	return Actor{
		Actor:    session.GetSessionActor(),
		Emulator: session.GetSessionEmulator(),
	}
}

// CopySessionActors copies over the actor and emulator in Session.
func CopySessionActors(session service.Session) service.Session {
	return service.Session{
		SessionActor:    session.SessionActor,
		SessionEmulator: session.SessionEmulator,
	}
}

// ErrorToServiceError converts built-in error object into service error that can be transported in events.
func ErrorToServiceError(err error) service.CacaoError {
	if err == nil {
		return &service.CacaoErrorBase{}
	}
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}
	return serviceError
}

// ProviderCredentialPair is a cloud credential associated with a provider.
type ProviderCredentialPair struct {
	Provider   common.ID
	Credential *service.CredentialModel
}
