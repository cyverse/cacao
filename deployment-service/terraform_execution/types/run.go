package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"time"
)

// DeploymentRun contains data relevant to preflight and execution stage of run.
// The difference between this definition of Run and the one used by Metadata
// service is that this one includes data that is specific to template-engine
// (Terraform) and cloud provider.
type DeploymentRun struct {
	ID           common.ID                `bson:"_id"`
	Deployment   common.ID                `bson:"deployment"`
	TemplateType service.TemplateTypeName `bson:"template_type"`
	CreatedBy    deploymentcommon.Creator `bson:",inline"`
	// transaction ID of the create run request
	CreateRunTID         common.TransactionID                        `bson:"create_run_tid"`
	PrimaryProvider      common.ID                                   `bson:"primary_provider"`
	PrerequisiteTemplate deploymentcommon.TemplateSnapshot           `bson:"prerequisite_template"`
	TemplateSnapshot     deploymentcommon.TemplateSnapshot           `bson:"template"`
	Parameters           deploymentcommon.DeploymentParameters       `bson:"parameters"`
	CloudCredentials     deploymentcommon.ProviderCredentialMappings `bson:"cloud_creds"`
	GitCredential        string                                      `bson:"git_cred"`
	Status               deploymentcommon.DeploymentRunStatus        `bson:"status"`
	//StatusMsg            string                                `bson:"status_msg"`
	StateUpdatedAt time.Time                        `bson:"state_updated_at"`
	TerraformState *deploymentcommon.TerraformState `bson:"tf_state"`
}

// DeploymentRunUpdate is used for updating deployment run, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields that has nil value is not updated in storage.
type DeploymentRunUpdate struct {
	Status         *deploymentcommon.DeploymentRunStatus `bson:"status"`
	StateUpdatedAt *time.Time                            `bson:"state_updated_at"`
	TerraformState *deploymentcommon.TerraformState      `bson:"tf_state"`
}

// ToBSON converts the update struct to bson
func (update DeploymentRunUpdate) ToBSON() map[string]interface{} {
	result := make(map[string]interface{})
	if update.Status != nil {
		result["status"] = *update.Status
	}
	if update.StateUpdatedAt != nil {
		result["state_updated_at"] = *update.StateUpdatedAt
	}
	if update.TerraformState != nil {
		result["tf_state"] = *update.TerraformState
	}
	return result
}

// DeploymentRunFilter is filter for searching deployment run
type DeploymentRunFilter struct {
	ID         common.ID `bson:"_id"`
	Deployment common.ID `bson:"deployment"`
}

// ToBSON converts the filter into bson
func (filter DeploymentRunFilter) ToBSON() map[string]interface{} {
	result := make(map[string]interface{})
	if filter.ID != "" {
		result["_id"] = filter.ID
	}
	if filter.Deployment != "" {
		result["deployment"] = filter.Deployment
	}
	return result
}
