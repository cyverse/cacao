package ports

import (
	"context"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// EventSrc is a sink to publish events into
type EventSrc interface {
	SetHandlers(handlers EventHandlers)
	Start(ctx context.Context, wg *sync.WaitGroup) error
}

// EventHandlers represents the handlers for all event operations this service supports.
type EventHandlers interface {
	EventStartRunRequested(common.TransactionID, deploymentevents.StartRunRequest, OutgoingEventSink)
	EventRunPreflightSucceeded(common.TransactionID, deploymentevents.RunPreflightResult, OutgoingEventSink)
	DeploymentDeletionStarted(service.DeploymentDeletionResult, OutgoingEventSink)
	EventStartStateRefreshRequested(common.TransactionID, deploymentevents.StartStateRefreshRequest, OutgoingEventSink)
	WorkflowSucceededEvent(awmclient.WorkflowSucceeded, OutgoingEventSink)
	WorkflowFailedEvent(awmclient.WorkflowFailed, OutgoingEventSink)
}

// OutgoingEventSink represents all the events this service will be publishing. Each method in this interface will publish an event of the method name.
type OutgoingEventSink interface {
	EventRunPreflightStarted(deploymentevents.RunPreflightStarted)                            // deploymentevents.EventRunPreflightStarted
	EventRunPreflightStartFailed(deploymentevents.RunPreflightStartFailed)                    // deploymentevents.EventRunPreflightStartFailed
	EventRunExecutionFailed(deploymentevents.RunExecutionFailed)                              // deploymentevents.EventRunExecutionFailed
	EventRunExecutionStarted(deploymentevents.RunExecutionStarted)                            // deploymentevents.EventRunExecutionStarted
	EventDeploymentStateRefreshStarted(deploymentevents.DeploymentStateRefreshStarted)        // deploymentevents.EventDeploymentStateRefreshStarted
	EventDeploymentStateRefreshSucceeded(deploymentevents.DeploymentStateRefreshSucceeded)    // deploymentevents.EventDeploymentStateRefreshSucceeded
	EventDeploymentStateRefreshFailed(deploymentevents.DeploymentStateRefreshFailed)          // deploymentevents.EventDeploymentStateRefreshFailed
	EventDeploymentDeletionCleanupFailed(deploymentevents.DeploymentDeletionCleanupResult)    // deploymentevents.EventDeploymentDeletionCleanupFailed
	EventDeploymentDeletionCleanupStarted(deploymentevents.DeploymentDeletionCleanupStarted)  // deploymentevents.EventDeploymentDeletionCleanupStarted
	EventRunPreflightSucceeded(deploymentevents.RunPreflightResult)                           // deploymentevents.EventRunPreflightSucceeded
	EventRunPreflightFailed(deploymentevents.RunPreflightResult)                              // deploymentevents.EventRunPreflightFailed
	EventDeploymentDeletionCleanupSucceeded(deploymentevents.DeploymentDeletionCleanupResult) // deploymentevents.EventDeploymentDeletionCleanupSucceeded
	//EventDeploymentDeletionCleanupFailed(deploymentevents.DeploymentDeletionCleanupResult)    //	deploymentevents.EventDeploymentDeletionCleanupFailed
	EventRunExecutionSucceeded(deploymentevents.RunExecutionSucceeded) // deploymentevents.EventRunExecutionSucceeded
	//EventRunExecutionFailed(deploymentevents.RunExecutionFailed)                              // deploymentevents.EventRunExecutionFailed
}

// QuerySrc is source of incoming queries
type QuerySrc interface {
	Start(ctx context.Context, wg *sync.WaitGroup, handlers QueryHandlers) error
}

// QueryHandlers ...
type QueryHandlers interface {
	ListLogsLocation(query service.DeploymentLogsLocationQuery) service.DeploymentLogsLocationReply
}

// TFRunStorage is storage for Terraform Run
type TFRunStorage interface {
	Init() error
	Get(runID common.ID) (types.DeploymentRun, error)
	Create(run types.DeploymentRun) error
	Update(runID common.ID, update types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (updated bool, err error)
	Search(filter types.DeploymentRunFilter) ([]types.DeploymentRun, error)
}

// DeploymentWorkflowStorage is storage for DeploymentWorkflow
type DeploymentWorkflowStorage interface {
	Init() error
	CreateDeploymentPreflightWorkflow(deployment common.ID, templateType service.TemplateTypeName, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreateDeploymentExecWorkflow(deployment common.ID, templateType service.TemplateTypeName, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreateDeploymentRefreshWorkflow(deployment common.ID, templateType service.TemplateTypeName, run common.ID, awmProvider awmclient.AWMProvider, workflowName string) error
	CreateDeploymentDeletionWorkflow(deployment common.ID, templateType service.TemplateTypeName, awmProvider awmclient.AWMProvider, workflowName string) error
	CreatePrerequisiteDeletionWorkflow(deployment common.ID, templateType service.TemplateTypeName, awmProvider awmclient.AWMProvider, workflowName string) error
	Search(provider awmclient.AWMProvider, workflowName string) (types.DeploymentWorkflow, error)
	WorkflowSucceededAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error
	WorkflowFailedAt(provider awmclient.AWMProvider, workflowName string, endsAt time.Time) error

	ListDeploymentDeletionWorkflows(deploymentID common.ID) ([]types.DeploymentWorkflow, error)
	ListDeploymentRunWorkflows(deploymentID common.ID, runID common.ID) ([]types.DeploymentWorkflow, error)
}

// DeploymentMetadataService ...
type DeploymentMetadataService interface {
	Get(actor types.Actor, id common.ID) (service.Deployment, error)
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor types.Actor, id common.ID) (*service.WorkspaceModel, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor types.Actor, id common.ID) (service.Template, error)
	GetTemplateVersion(actor types.Actor, id common.ID) (*service.TemplateVersion, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor types.Actor, id common.ID) (*service.ProviderModel, error)
	List(actor types.Actor) ([]service.ProviderModel, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor types.Actor, owner, id string) (*service.CredentialModel, error)
	// List return a list of credential ID
	List(actor types.Actor, owner string) ([]service.CredentialModel, error)
	// ListPublicSSHKeys return a list of credential ID that is of type ssh
	ListPublicSSHKeys(actor types.Actor, owner string) ([]service.CredentialModel, error)
}

// ArgoWorkflowMediator is an interface to interact with ArgoWorkflowMediator
type ArgoWorkflowMediator interface {
	Create(ctx context.Context, provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) (awmProvider awmclient.AWMProvider, wfName string, err error)
	CreateAsync(provider common.ID, username string, workflowFilename string, opts ...awmclient.AWMWorkflowCreateOpt) error
}

// KeySrc is an way to get encryption key
type KeySrc interface {
	GetKey() []byte
}

// SSHKeySrc is a way to get public ssh key to use to inject into users' VMs.
type SSHKeySrc interface {
	GetPublicSSHKey() string
}

// TimeSrc is a source of current time
type TimeSrc interface {
	Now() time.Time
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	TFRunStorage    TFRunStorage
	WorkflowStorage DeploymentWorkflowStorage
	KeySrc          KeySrc
	TimeSrc         TimeSrc
	SSHKeySrc       SSHKeySrc

	MetadataSvc  DeploymentMetadataService
	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
	AWM          ArgoWorkflowMediator
}
