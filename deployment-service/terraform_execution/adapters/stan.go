package adapters

import (
	"context"
	"encoding/json"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"sync"
)

// Event implements types.IncomingEvent & types.OutgoingEvent
type Event cloudevents.Event

// EventType returns the event type
func (e Event) EventType() common.EventType {
	return common.EventType(cloudevents.Event(e).Type())
}

// Transaction returns the transaction ID
func (e Event) Transaction() common.TransactionID {
	return messaging2.GetTransactionID((*cloudevents.Event)(&e))
}

// CloudEvent returns the event as a cloudevent
func (e Event) CloudEvent() cloudevents.Event {
	return cloudevents.Event(e)
}

// ToCloudEvent returns the event as a cloudevent with the specified source
func (e Event) ToCloudEvent(source string) (cloudevents.Event, error) {
	ce := cloudevents.Event(e)
	ce.SetSource(source)
	return ce, nil
}

// StanAdapter is an adapter for STAN that does both publishing and subscription
type StanAdapter struct {
	conn     messaging2.EventConnection
	handlers ports.EventHandlers
}

var _ ports.EventSrc = &StanAdapter{}

// NewStanAdapter ...
func NewStanAdapter(conn messaging2.EventConnection) *StanAdapter {
	return &StanAdapter{conn: conn}
}

// SetHandlers ...
func (src *StanAdapter) SetHandlers(handlers ports.EventHandlers) {
	src.handlers = handlers
}

// Start starts STAN subscription and start receiving events
func (src *StanAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "StanAdapter.Start",
	})
	err := src.conn.Listen(ctx, map[common.EventType]messaging2.EventHandlerFunc{
		deploymentevents.EventStartRunRequested:          src.eventStartRunRequested,
		deploymentevents.EventRunPreflightSucceeded:      src.eventRunPreflightSucceeded,
		service.DeploymentDeletionStarted:                handlerWrapper(src.handlers.DeploymentDeletionStarted),
		deploymentevents.EventStartStateRefreshRequested: src.eventStartStateRefreshRequested,
		awmclient.WorkflowSucceededEvent:                 handlerWrapper(src.handlers.WorkflowSucceededEvent),
		awmclient.WorkflowFailedEvent:                    handlerWrapper(src.handlers.WorkflowFailedEvent),
	}, wg)
	if err != nil {
		return err
	}
	logger.Info("start to listen to events")
	return nil
}

func handlerWrapper[T any](handler func(T, ports.OutgoingEventSink)) messaging2.EventHandlerFunc {
	return func(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
		var request T
		err := json.Unmarshal(event.Data(), &request)
		if err != nil {
			return err
		}
		handler(request, StanAdapterOutgoing{writer: writer})
		return nil
	}
}

func (src *StanAdapter) eventStartRunRequested(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
	var request deploymentevents.StartRunRequest
	err := json.Unmarshal(event.Data(), &request)
	if err != nil {
		return err
	}
	src.handlers.EventStartRunRequested(messaging2.GetTransactionID(&event), request, StanAdapterOutgoing{writer: writer})
	return nil
}

func (src *StanAdapter) eventRunPreflightSucceeded(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
	var request deploymentevents.RunPreflightResult
	err := json.Unmarshal(event.Data(), &request)
	if err != nil {
		return err
	}
	src.handlers.EventRunPreflightSucceeded(messaging2.GetTransactionID(&event), request, StanAdapterOutgoing{writer: writer})
	return nil
}

func (src *StanAdapter) eventStartStateRefreshRequested(ctx context.Context, event cloudevents.Event, writer messaging2.EventResponseWriter) error {
	var request deploymentevents.StartStateRefreshRequest
	err := json.Unmarshal(event.Data(), &request)
	if err != nil {
		return err
	}
	src.handlers.EventStartStateRefreshRequested(messaging2.GetTransactionID(&event), request, StanAdapterOutgoing{writer: writer})
	return nil
}
