package adapters

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
)

// ArgoWorkflowMediator is a client to interact with ArgoWorkflowMediator
type ArgoWorkflowMediator struct {
	conn *messaging2.StanConnection
}

// NewArgoWorkflowMediator creates a new ArgoWorkflowMediator
func NewArgoWorkflowMediator(conn *messaging2.StanConnection) ArgoWorkflowMediator {
	return ArgoWorkflowMediator{
		conn: conn,
	}
}

// Create perform sync workflow creation
func (awm ArgoWorkflowMediator) Create(
	ctx context.Context,
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...awmclient.AWMWorkflowCreateOpt) (awmProvider awmclient.AWMProvider, wfName string, err error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ArgoWorkflowMediator.Create",
		"provider": provider,
		"username": username,
		"wf":       workflowFilename,
	})
	logger.Trace()
	logger.Warn("Implement ArgoWorkflowMediator adapter")

	client := awmclient.NewArgoWorkflowMediator(awm.conn)
	return client.Create(ctx, awmclient.AWMProvider(provider.String()), username, workflowFilename, opts...)
}

// CreateAsync perform async creation
func (awm ArgoWorkflowMediator) CreateAsync(
	provider common.ID,
	username string,
	workflowFilename string,
	opts ...awmclient.AWMWorkflowCreateOpt) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "ArgoWorkflowMediator.CreateAsync",
		"provider": provider,
		"username": username,
		"wf":       workflowFilename,
	})
	logger.Trace()
	logger.Warn("Implement ArgoWorkflowMediator adapter")

	client := awmclient.NewArgoWorkflowMediator(awm.conn)
	return client.CreateAsync(awmclient.AWMProvider(provider.String()), username, workflowFilename, opts...)
}
