package domainutils

import (
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
)

// https://www.terraform.io/docs/language/expressions/types.html#types

func TestParamToAnsibleVars(t *testing.T) {
	type args struct {
		parameters []deploymentcommon.DeploymentParameter
	}
	tests := []struct {
		name    string
		args    args
		want    map[string]interface{}
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "username", Type: "string", Value: "test_user123"},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"username": map[string]string{"type": "string"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"username": "test_user123",
				},
			},
			wantErr: false,
		},
		{
			name: "basic TF types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "username", Type: "string", Value: "test_user123"},
					{Name: "count", Type: "number", Value: 123},
					{Name: "flag", Type: "bool", Value: true},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"username": map[string]string{"type": "string"},
					"count":    map[string]string{"type": "number"},
					"flag":     map[string]string{"type": "bool"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"username": "test_user123",
					"count":    123,
					"flag":     true,
				},
			},
			wantErr: false,
		},
		{
			name: "more complex TF types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "list1", Type: "list", Value: []string{"foo", "bar"}},
					{Name: "map1", Type: "map", Value: map[string]interface{}{"foo": "bar"}},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"list1": map[string]string{"type": "list"},
					"map1":  map[string]string{"type": "map"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"list1": []string{"foo", "bar"},
					"map1":  map[string]interface{}{"foo": "bar"},
				},
			},
			wantErr: false,
		},
		{
			name: "cacao-specific types 1",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "flavor", Type: "cacao_provider_flavor", Value: "tiny1"},
					{Name: "image", Type: "cacao_provider_image", Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
					{Name: "project", Type: templateProviderProjectType, Value: "my-project"},
					{Name: "count", Type: "integer", Value: 123},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"flavor":  map[string]string{"type": "string"},
					"image":   map[string]string{"type": "string"},
					"project": map[string]string{"type": "string"},
					"count":   map[string]string{"type": "number"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"flavor":  "tiny1",
					"image":   "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					"project": "my-project",
					"count":   123,
				},
			},
			wantErr: false,
		},
		{
			name: "cacao-specific types 2",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "key_pair", Type: TemplateProviderKeyPairType, Value: "my-key-pair"},
					{Name: "ext_net", Type: TemplateProviderExternalNetworkType, Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa"},
					{Name: "ext_subnet", Type: TemplateProviderExternalSubnetType, Value: "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb"},
					{Name: "cloud_init", Type: TemplateCloudInitType, Value: "#cloud-init\nusers:\n  - default\n"},
				},
			},
			want: map[string]interface{}{
				"TF_MODULE": tfModuleName,
				"TF_INPUT_VARS": map[string]interface{}{
					"key_pair":   map[string]string{"type": "string"},
					"ext_net":    map[string]string{"type": "string"},
					"ext_subnet": map[string]string{"type": "string"},
					"cloud_init": map[string]string{"type": "string"},
				},
				"TF_INPUT_VALUES": map[string]interface{}{
					"key_pair":   "my-key-pair",
					"ext_net":    "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
					"ext_subnet": "bbbbbbbb-bbbb-bbbb-bbbb-bbbbbbbbbbbb",
					"cloud_init": "#cloud-init\nusers:\n  - default\n",
				},
			},
			wantErr: false,
		},
		{
			name: "unknown types",
			args: args{
				parameters: []deploymentcommon.DeploymentParameter{
					{Name: "foo", Type: "bar", Value: "foobar"},
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParamToAnsibleVars(tt.args.parameters)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParamToAnsibleVars() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParamToAnsibleVars() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParameterConverter_Convert(t *testing.T) {
	type args struct {
		templateParam []service.TemplateParameter
		paramValues   service.DeploymentParameterValues
	}
	tests := []struct {
		name    string
		args    args
		want    []deploymentcommon.DeploymentParameter
		wantErr bool
	}{
		{
			name: "simple",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "username",
						Type: "string",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"username": "test_user123",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "username",
					Type:  "string",
					Value: "test_user123",
				},
			},
			wantErr: false,
		},
		{
			name: "more types",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "instance_name",
						Type: "string",
					},
					{
						Name: "instance_count",
						Type: "integer",
					},
					{
						Name: "flavor",
						Type: "cacao_provider_flavor",
					},
					{
						Name: "keypair",
						Type: "string",
					},
					{
						Name: "image",
						Type: "cacao_provider_image",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_name":  "foobar",
					"instance_count": 123,
					"flavor":         "tiny1",
					"keypair":        "myKeyPair",
					"image":          "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "instance_name",
					Type:  "string",
					Value: "foobar",
				},
				{
					Name:  "instance_count",
					Type:  "integer",
					Value: int64(123),
				},
				{
					Name:  "flavor",
					Type:  "cacao_provider_flavor",
					Value: "tiny1",
				},
				{
					Name:  "keypair",
					Type:  "string",
					Value: "myKeyPair",
				},
				{
					Name:  "image",
					Type:  "cacao_provider_image",
					Value: "aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa",
				},
			},
			wantErr: false,
		},
		{
			name: "bad string value",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "instance_name",
						Type: "string",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_name": 123,
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad int value",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "instance_count",
						Type: "integer",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_count": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "bad float value",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "decimal_val",
						Type: "float",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"decimal_val": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "default value",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name:    "count",
						Default: 123,
						Type:    "integer",
					},
				},
				paramValues: service.DeploymentParameterValues{},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "count",
					Type:  "integer",
					Value: int64(123),
				},
			},
			wantErr: false,
		},
		{
			name: "default value wrong type",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name:    "count",
						Default: "bar",
						Type:    "integer",
					},
				},
				paramValues: service.DeploymentParameterValues{},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "unmarshal from json",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "instance_count",
						Type: "integer",
					},
					{
						Name: "count",
						Type: "integer",
					},
					{
						Name: "f1",
						Type: "float",
					},
					{
						Name: "f2",
						Type: "float",
					},
				},
				paramValues: service.DeploymentParameterValues{
					"instance_count": func() interface{} {
						var result interface{}
						_ = json.Unmarshal([]byte("123"), &result)
						return result
					}(),
					"count": func() interface{} {
						var result interface{}
						_ = json.Unmarshal([]byte("16.5"), &result)
						return result
					}(),
					"f1": func() interface{} {
						var result interface{}
						_ = json.Unmarshal([]byte("16.5"), &result)
						return result
					}(),
					"f2": func() interface{} {
						var result interface{}
						_ = json.Unmarshal([]byte("123"), &result)
						return result
					}(),
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "instance_count",
					Type:  "integer",
					Value: int64(123),
				},
				{
					Name:  "count",
					Type:  "integer",
					Value: int64(16),
				},
				{
					Name:  "f1",
					Type:  "float",
					Value: float64(16.5),
				},
				{
					Name:  "f2",
					Type:  "float",
					Value: float64(123),
				},
			},
			wantErr: false,
		},
		{
			name: "enum",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name:    "power_state",
						Default: "active",
						Type:    "string",
						Enum:    []interface{}{"active", "shutoff", "suspend"},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "suspend",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "power_state",
					Type:  "string",
					Value: "suspend",
				},
			},
			wantErr: false,
		},
		{
			name: "enum bad value",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name:    "power_state",
						Default: "active",
						Type:    "string",
						Enum:    []interface{}{"active", "shutoff", "suspend"},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "foo",
				},
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "enum value bad type",
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name:    "power_state",
						Default: "active",
						Type:    "string",
						Enum:    []interface{}{123, 456, 789},
					},
				},
				paramValues: service.DeploymentParameterValues{
					"power_state": "suspend",
				},
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			pc := ParameterConverter{}
			got, err := pc.Convert(tt.args.templateParam, tt.args.paramValues)
			if tt.wantErr {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
			if len(got) == 1 {
				// assert.Equal cares about list order, so only used on len==1
				assert.Equal(t, tt.want, got)
				return
			}
			assert.Len(t, got, len(tt.want))
			for _, elem := range got {
				assert.Contains(t, tt.want, elem)
			}
			for _, elem := range tt.want {
				assert.Contains(t, got, elem)
			}
		})
	}
}

func TestParameterConverter_ConvertWithOverrideValue(t *testing.T) {
	type args struct {
		templateParam       []service.TemplateParameter
		paramValues         service.DeploymentParameterValues
		overrideParamValues map[string]interface{}
	}
	tests := []struct {
		name    string
		args    args
		want    []deploymentcommon.DeploymentParameter
		wantErr bool
	}{
		{
			name: "override " + TemplateProviderRegionType,
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "foo",
						Type: TemplateProviderRegionType,
					},
				},
				paramValues: service.DeploymentParameterValues{
					"foo": "some_value",
				},
				overrideParamValues: map[string]interface{}{
					TemplateProviderRegionType: "region-123",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "foo",
					Type:  TemplateProviderRegionType,
					Value: "region-123",
				},
			},
			wantErr: false,
		},
		{
			name: fmt.Sprintf("override %s, %s, %s, %s", TemplateUnixUsernameType, TemplateUsernameType, TemplateProviderKeyPairType, TemplateCloudInitType),
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "foo0",
						Type: TemplateUnixUsernameType,
					},
					{
						Name: "foo1",
						Type: TemplateUsernameType,
					},
					{
						Name: "foo2",
						Type: TemplateProviderKeyPairType,
					},
					{
						Name: "foo3",
						Type: TemplateCloudInitType,
					},
				},
				paramValues: service.DeploymentParameterValues{
					"foo0": "some_value0",
					"foo1": "some_value1",
					"foo2": "some_value2",
					"foo3": "some_value3",
				},
				overrideParamValues: map[string]interface{}{
					TemplateUsernameType:        "testuser123@example.com",
					TemplateProviderKeyPairType: "keypair123",
					TemplateCloudInitType:       "#cloud-init\n",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "foo0",
					Type:  TemplateUnixUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "foo1",
					Type:  TemplateUsernameType,
					Value: "testuser123@example.com",
				},
				{
					Name:  "foo2",
					Type:  TemplateProviderKeyPairType,
					Value: "keypair123",
				},
				{
					Name:  "foo3",
					Type:  TemplateCloudInitType,
					Value: "#cloud-init\n",
				},
			},
			wantErr: false,
		},
		{
			name: fmt.Sprintf("override %s, %s, extra %s", TemplateUsernameType, TemplateProviderKeyPairType, TemplateCloudInitType),
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "foo1",
						Type: TemplateUsernameType,
					},
					{
						Name: "foo2",
						Type: TemplateProviderKeyPairType,
					},
				},
				paramValues: service.DeploymentParameterValues{
					"foo1": "some_value1",
					"foo2": "some_value2",
					"foo3": "some_value3",
				},
				overrideParamValues: map[string]interface{}{
					TemplateUsernameType:        "testuser123",
					TemplateProviderKeyPairType: "keypair123",
					TemplateCloudInitType:       "#cloud-init\n",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "foo1",
					Type:  TemplateUsernameType,
					Value: "testuser123",
				},
				{
					Name:  "foo2",
					Type:  TemplateProviderKeyPairType,
					Value: "keypair123",
				},
			},
			wantErr: false,
		},
		{
			name: fmt.Sprintf("override %s", TemplateUnixUsernameType),
			args: args{
				templateParam: []service.TemplateParameter{
					{
						Name: "foo1",
						Type: TemplateUnixUsernameType,
					},
				},
				paramValues: service.DeploymentParameterValues{
					"foo1": "some_value1",
					"foo2": "some_value2",
					"foo3": "some_value3",
				},
				overrideParamValues: map[string]interface{}{
					TemplateUsernameType:        "123testuser123",
					TemplateProviderKeyPairType: "keypair123",
					TemplateCloudInitType:       "#cloud-init\n",
				},
			},
			want: []deploymentcommon.DeploymentParameter{
				{
					Name:  "foo1",
					Type:  TemplateUnixUsernameType,
					Value: "user-de406364",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var pc ParameterConverter
			got, err := pc.Convert(tt.args.templateParam, tt.args.paramValues, OverrideValueForTypes(tt.args.overrideParamValues))
			if tt.wantErr {
				assert.Error(t, err)
				return
			}
			assert.NoError(t, err)
			if len(got) == 1 {
				// assert.Equal cares about list order, so only used on len==1
				assert.Equal(t, tt.want, got)
				return
			}
			assert.Len(t, got, len(tt.want))
			assert.ElementsMatch(t, tt.want, got)
		})
	}
}

func Test_unixUsername(t *testing.T) {
	type args struct {
		fullUsername string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "email",
			args: args{
				fullUsername: "foobar@arizona.edu",
			},
			want: "foobar",
		},
		{
			name: "email2",
			args: args{
				fullUsername: "foo#bar@arizona.edu",
			},
			want: "foo",
		},
		{
			name: "foobar",
			args: args{
				fullUsername: "foobar",
			},
			want: "foobar",
		},
		{
			name: "FoObAr mix case",
			args: args{
				fullUsername: "FoObAr",
			},
			want: "foobar",
		},
		{
			name: "FOOBAR all cap",
			args: args{
				fullUsername: "FOOBAR",
			},
			want: "foobar",
		},
		{
			name: "foo1bar",
			args: args{
				fullUsername: "foo1bar",
			},
			want: "foo1bar",
		},
		{
			name: "foo_bar1",
			args: args{
				fullUsername: "foo_bar1",
			},
			want: "foo_bar1",
		},
		{
			name: "foo_bar-1",
			args: args{
				fullUsername: "foo_bar-1",
			},
			want: "foo_bar-1",
		},
		{
			name: "start with number",
			args: args{
				fullUsername: "1foobar@arizona.edu",
			},
			want: "user-3532e708",
		},
		{
			name: "start with underscore",
			args: args{
				fullUsername: "_foobar@arizona.edu",
			},
			want: "user-25847455",
		},
		{
			name: "start with dash",
			args: args{
				fullUsername: "-foobar@arizona.edu",
			},
			want: "user-7901ee93",
		},
		{
			name: "too long",
			args: args{
				fullUsername: "a123456789012345678901234567890123456789012345678901234567890",
			},
			want: "a1234567890123456789012345678901",
		},
		{
			name: "url",
			args: args{
				fullUsername: "https://cyverse.org/user/foobar",
			},
			want: "https",
		},
		{
			name: "emoji",
			args: args{
				fullUsername: "foo\U0000263Abar",
			},
			want: "foo",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, unixUsername(tt.args.fullUsername), "unixUsername(%v)", tt.args.fullUsername)
		})
	}
}
