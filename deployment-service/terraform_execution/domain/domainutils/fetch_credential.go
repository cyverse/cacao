package domainutils

import (
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// FindPrimaryCloudCredential finds the cloud credential ID for primary provider of a run
func FindPrimaryCloudCredential(tfRun types.DeploymentRun) string {
	for _, pair := range tfRun.CloudCredentials {
		if pair.Provider == tfRun.PrimaryProvider {
			return pair.Credential.String()
		}
	}
	return ""
}
