package domainutils

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
)

// EncodeGitCred encrypts and encodes git credential, also convert from the template source cred format to the format accepted by AWM.
func EncodeGitCred(cred *service.CredentialModel, keySrc ports.KeySrc) (string, error) {
	templateSrcCred, err := unmarshalTemplateSrcCred(cred)
	if err != nil {
		return "", err
	}
	gitCredMarshaled, err := marshalGitCredential(templateSrcCred)
	if err != nil {
		return "", err
	}
	cipher, err := encrypt(gitCredMarshaled, keySrc)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", cred.ID)
	}
	return base64Encode(cipher), nil
}

func unmarshalTemplateSrcCred(cred *service.CredentialModel) (service.TemplateSourceCredential, error) {
	// unmarshal to check schema
	var credOut service.TemplateSourceCredential
	err := json.Unmarshal([]byte(cred.Value), &credOut)
	if err != nil {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is not valid template source credential", cred.ID)
	}

	if credOut.Username == "" {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is missing username", cred.ID)
	}
	if credOut.Password == "" {
		return service.TemplateSourceCredential{}, fmt.Errorf("credential %s is missing password", cred.ID)
	}
	return credOut, nil
}

func marshalGitCredential(cred service.TemplateSourceCredential) ([]byte, error) {
	var credOut = awmclient.GitCredential{
		Username: cred.Username,
		Password: cred.Password,
	}
	return json.Marshal(credOut)
}

// EncodeCloudCred encrypts and encodes a cloud credential.
func EncodeCloudCred(cred *service.CredentialModel, keySrc ports.KeySrc) (string, error) {
	switch cred.Type {
	case service.OpenStackCredentialType:
		return EncodeOpenstackCred(cred, keySrc)
	case service.AWSCredentialType:
		return EncodeAWSCred(cred, keySrc)
	default:
		return "", service.NewCacaoInvalidParameterError("unsupported cloud credential type, cannot encode cloud credential")
	}
}

// ValidateCloudCredential validates a cloud credential
func ValidateCloudCredential(cred *service.CredentialModel) error {
	switch cred.Type {
	case service.OpenStackCredentialType:
		return ValidateOpenstackCredential(cred)
	case service.AWSCredentialType:
		return ValidateAWSCredential(cred)
	default:
		return service.NewCacaoInvalidParameterError("unsupported cloud credential type")
	}
}

// EncodeOpenstackCred encrypts and encodes OpenStack credential.
func EncodeOpenstackCred(cred *service.CredentialModel, keySrc ports.KeySrc) (string, error) {
	if err := ValidateOpenstackCredential(cred); err != nil {
		return "", fmt.Errorf("credential %s is not valid openstack credential", cred.ID)
	}
	cipher, err := encrypt([]byte(cred.Value), keySrc)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", cred.ID)
	}
	return base64Encode(cipher), nil
}

// ValidateOpenstackCredential validates an OpenStack credential
func ValidateOpenstackCredential(cred *service.CredentialModel) error {
	if cred.ID == "" {
		return errors.New("bad cloud credential ID")
	}
	if cred.Username == "" {
		return errors.New("cloud credential has no owner")
	}
	if cred.Type != service.OpenStackCredentialType {
		return errors.New("credential type not openstack")
	}
	if cred.Value == "" {
		return errors.New("empty cloud credential value")
	}
	var osCred awmclient.OpenStackCredential
	err := json.Unmarshal([]byte(cred.Value), &osCred)
	if err != nil {
		return errors.New("bad cloud credential value")
	}
	// TODO more validation on awmclient.OpenStackCredential
	return nil
}

// EncodeAWSCred encrypts and encodes AWS credential.
func EncodeAWSCred(cred *service.CredentialModel, keySrc ports.KeySrc) (string, error) {
	if err := ValidateAWSCredential(cred); err != nil {
		return "", fmt.Errorf("credential %s is not valid aws credential", cred.ID)
	}
	cipher, err := encrypt([]byte(cred.Value), keySrc)
	if err != nil {
		return "", fmt.Errorf("fail to encrypt credential %s", cred.ID)
	}
	return base64Encode(cipher), nil
}

// ValidateAWSCredential validates AWS credential
func ValidateAWSCredential(cred *service.CredentialModel) error {
	if cred.ID == "" {
		return errors.New("bad cloud credential ID")
	}
	if cred.Username == "" {
		return errors.New("cloud credential has no owner")
	}
	if cred.Type != "aws" {
		return errors.New("credential type not aws")
	}
	if cred.Value == "" {
		return errors.New("empty cloud credential value")
	}
	var awsCred awmclient.AWSCredential
	err := json.Unmarshal([]byte(cred.Value), &awsCred)
	if err != nil {
		return errors.New("bad cloud credential value")
	}
	return nil
}

// ValidateGitCredential validates a git credential
func ValidateGitCredential(credential *service.CredentialModel) error {
	if credential.ID == "" {
		return service.NewCacaoInvalidParameterError("git credential object has bad ID")
	}
	if credential.Type != "git" {
		return service.NewCacaoInvalidParameterError("git credential object must be of type 'git'")
	}
	if credential.Value == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty value")
	}
	if credential.Username == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty owner")
	}
	var gitCred service.TemplateSourceCredential
	err := json.Unmarshal([]byte(credential.Value), &gitCred)
	if err != nil {
		return service.NewCacaoInvalidParameterError(fmt.Sprintf("git credential object has bad value, %s", err.Error()))
	}
	if gitCred.Username == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty username")
	}
	if gitCred.Password == "" {
		return service.NewCacaoInvalidParameterError("git credential object has empty password")
	}
	return nil
}

func encrypt(input []byte, keySrc ports.KeySrc) ([]byte, error) {
	encryptionKey := keySrc.GetKey()
	cipher, err := AESEncrypt(encryptionKey, input)
	if err != nil {
		return nil, err
	}
	return cipher, nil
}

func base64Encode(input []byte) string {
	return base64.StdEncoding.EncodeToString(input)
}
