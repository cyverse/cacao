package queryhandlers

import (
	"crypto/rand"
	"fmt"
	log "github.com/sirupsen/logrus"
	"testing"
)

// ~6900 ns/op
func Benchmark_generateJWTToken(b *testing.B) {
	log.SetLevel(log.ErrorLevel)
	key := make([]byte, 40)
	_, err := rand.Read(key)
	if err != nil {
		b.Fatal(err)
		return
	}
	var h = LogsLocationHandler{
		jwtKey: key,
	}
	results := make([]string, 0, b.N)
	for i := 0; i < b.N; i++ {
		token, err := h.generateJWTToken(fmt.Sprintf("workflow_name-%d", i))
		if err != nil {
			b.Fatal(err)
		}
		results = append(results, token)
	}
	b.ReportAllocs()
	if len(results) > 3 {
		b.Log(len(results[2]))
	}
}
