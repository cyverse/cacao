package staterefresh

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/domainutils"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/tfrun"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// PrerequisiteGatherer gathers the Prerequisite for a run
type PrerequisiteGatherer struct {
	runStorage ports.TFRunStorage
	credMS     ports.CredentialMicroservice
}

// NewPrerequisiteGatherer ...
func NewPrerequisiteGatherer(runStorage ports.TFRunStorage, credMS ports.CredentialMicroservice) PrerequisiteGatherer {
	return PrerequisiteGatherer{
		runStorage: runStorage,
		credMS:     credMS,
	}
}

// Gather gathers Prerequisite for a run
func (g PrerequisiteGatherer) Gather(runID common.ID) (Prerequisite, error) {
	if !runID.Validate() || runID.FullPrefix() != service.RunIDPrefix {
		return Prerequisite{}, service.NewCacaoInvalidParameterError("bad run ID")
	}

	var data Prerequisite
	err := g.gatherRun(runID, &data)
	if err != nil {
		return Prerequisite{}, err
	}
	err = g.gatherCloudCredential(data.TFRun, &data)
	if err != nil {
		return Prerequisite{}, err
	}
	err = g.gatherGitCredentialIfNecessary(data.TFRun, &data)
	if err != nil {
		return Prerequisite{}, err
	}
	return data, nil
}

func (g PrerequisiteGatherer) gatherRun(runID common.ID, data *Prerequisite) error {
	tfRun, err := g.runStorage.Get(runID)
	if err != nil {
		return err
	}
	err = data.ValidateAndSetTFRun(tfRun)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherCloudCredential(tfRun types.DeploymentRun, data *Prerequisite) error {
	credID := domainutils.FindPrimaryCloudCredential(tfRun)
	if credID == "" {
		return service.NewCacaoInvalidParameterError("credential for primary provider is missing")
	}
	actor := types.Actor{
		Actor:    tfRun.CreatedBy.User,
		Emulator: tfRun.CreatedBy.Emulator,
	}
	credential, err := g.credMS.Get(actor, actor.Actor, credID)
	if err != nil {
		return err
	}
	err = data.ValidateAndSetCloudCredential(credential)
	if err != nil {
		return err
	}
	return nil
}

func (g PrerequisiteGatherer) gatherGitCredentialIfNecessary(tfRun types.DeploymentRun, data *Prerequisite) error {
	if tfRun.GitCredential == "" {
		// skip if there is no git credential
		return nil
	}

	actor := types.Actor{
		Actor:    tfRun.CreatedBy.User,
		Emulator: tfRun.CreatedBy.Emulator,
	}
	credential, err := g.credMS.Get(actor, actor.Actor, tfRun.GitCredential)
	if err != nil {
		return err
	}
	err = data.ValidateAndSetGitCredential(credential)
	if err != nil {
		return err
	}
	return nil
}

// Prerequisite is data that is required to start the execution stage of the run.
type Prerequisite struct {
	TFRun     types.DeploymentRun
	CloudCred *service.CredentialModel
	GitCred   *service.CredentialModel
}

// ValidateAndSetTFRun validates and sets Terraform run
func (data *Prerequisite) ValidateAndSetTFRun(tfRun types.DeploymentRun) error {
	err := tfrun.Validate(tfRun)
	if err != nil {
		return err
	}
	data.TFRun = tfRun
	return nil
}

// ValidateAndSetCloudCredential validates and sets cloud credential
func (data *Prerequisite) ValidateAndSetCloudCredential(credential *service.CredentialModel) error {
	err := domainutils.ValidateCloudCredential(credential)
	if err != nil {
		return err
	}
	data.CloudCred = credential
	return nil
}

// ValidateAndSetGitCredential validates and sets git credential
func (data *Prerequisite) ValidateAndSetGitCredential(credential *service.CredentialModel) error {
	err := domainutils.ValidateGitCredential(credential)
	if err != nil {
		return err
	}
	data.GitCred = credential
	return nil
}

// TemplateExecutor executes the execution template for a Terraform run
type TemplateExecutor struct{}

// Execute executes the execution template
func (exec TemplateExecutor) Execute(awm ports.ArgoWorkflowMediator, keySrc ports.KeySrc, data Prerequisite) (awmProvider awmclient.AWMProvider, wfName string, err error) {
	tfWfData, err := exec.prepareTerraformWorkflowData(keySrc, data)
	if err != nil {
		return "", "", err
	}
	awmProvider, wfName, err = awm.Create(
		context.TODO(),
		data.TFRun.PrimaryProvider,
		data.TFRun.CreatedBy.User,
		awmclient.TerraformWorkflowFilename,
		awmclient.AWMTerraformWorflowData(tfWfData),
	)
	if err != nil {
		return "", "", err
	}
	return awmProvider, wfName, nil
}

func (exec TemplateExecutor) prepareTerraformWorkflowData(keySrc ports.KeySrc, data Prerequisite) (awmclient.TerraformWorkflowData, error) {
	var err error
	tfWfData := exec.initializeWorkflowData(data)
	tfWfData, err = exec.prepareAnsibleVars(data, tfWfData)
	if err != nil {
		return tfWfData, err
	}
	tfWfData, err = exec.prepareCredential(data, tfWfData, keySrc)
	if err != nil {
		return tfWfData, err
	}
	return tfWfData, nil
}

// this initializes some fields in workflow data that can directly inferred from Prerequisite without extra
// logic/computation. the rest of fields are left as default value.
func (exec TemplateExecutor) initializeWorkflowData(data Prerequisite) awmclient.TerraformWorkflowData {
	return awmclient.TerraformWorkflowData{
		Username:          data.TFRun.CreatedBy.User,
		Deployment:        data.TFRun.Deployment,
		TemplateID:        data.TFRun.TemplateSnapshot.TemplateID,
		TemplateTypeName:  data.TFRun.TemplateType,
		TerraformStateKey: data.TFRun.Deployment.String(), // use deployment ID as the key
		GitURL:            data.TFRun.TemplateSnapshot.GitURL,
		GitTrackedUpStream: struct {
			Branch string `mapstructure:"branch"`
			Tag    string `mapstructure:"tag"`
			Commit string `mapstructure:"commit"`
		}{
			Branch: data.TFRun.TemplateSnapshot.UpstreamTracked.Branch,
			Tag:    data.TFRun.TemplateSnapshot.UpstreamTracked.Tag,
			Commit: data.TFRun.TemplateSnapshot.CommitHash,
		},
		Path:                  data.TFRun.TemplateSnapshot.SubPath,
		CloudCredID:           "",
		AnsibleVars:           nil,
		CloudCredentialBase64: "",
		GitCredID:             "",
		GitCredentialBase64:   "",
	}
}

func (exec TemplateExecutor) prepareAnsibleVars(data Prerequisite, wfData awmclient.TerraformWorkflowData) (awmclient.TerraformWorkflowData, error) {
	ansibleVars, err := domainutils.ParamToAnsibleVars(data.TFRun.Parameters)
	if err != nil {
		return wfData, err
	}
	ansibleVars["TF_OPERATION"] = "refresh"
	wfData.AnsibleVars = ansibleVars
	return wfData, nil
}

func (exec TemplateExecutor) prepareCredential(data Prerequisite, wfData awmclient.TerraformWorkflowData, keySrc ports.KeySrc) (awmclient.TerraformWorkflowData, error) {
	cloudCredBase64, err := domainutils.EncodeCloudCred(data.CloudCred, keySrc)
	if err != nil {
		return wfData, err
	}
	wfData.CloudCredID = data.CloudCred.ID
	wfData.CloudCredentialBase64 = cloudCredBase64
	if data.GitCred != nil {
		gitCredBase64, err := domainutils.EncodeGitCred(data.GitCred, keySrc)
		if err != nil {
			return wfData, err
		}
		wfData.GitCredID = data.GitCred.ID
		wfData.GitCredentialBase64 = gitCredBase64
	}
	return wfData, nil
}
