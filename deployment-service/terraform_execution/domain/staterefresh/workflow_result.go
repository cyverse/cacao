package staterefresh

import (
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// WorkflowHandler handles result(success/failure) of execution workflow
type WorkflowHandler struct {
	runStorage ports.TFRunStorage
	timeSrc    ports.TimeSrc
}

// NewWorkflowHandler ...
func NewWorkflowHandler(dependencies ports.Ports) WorkflowHandler {
	return WorkflowHandler{
		runStorage: dependencies.TFRunStorage,
		timeSrc:    dependencies.TimeSrc,
	}
}

// HandleWorkflowSuccess handles the success result of execution workflow.
// This will parse Terraform state from workflow outputs.
// Save the Terraform state and update the status of Terraform run in storage.
// Convert Terraform state to state view, and emit EventRunExecutionSucceeded event.
func (h WorkflowHandler) HandleWorkflowSuccess(tid common.TransactionID, event awmclient.WorkflowSucceeded, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":    "staterefresh",
		"function":   "WorkflowHandler.HandleWorkflowSuccess",
		"workflow":   workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	if !workflow.Run.Validate() || workflow.Run.FullPrefix() != service.RunIDPrefix {
		logger.Error("workflow object has bad run ID")
		return
	}
	tfRun, err := h.runStorage.Get(workflow.Run)
	if err != nil {
		// either connection to storage has error, or run does not exist in storage which means an inconsistency between workflow storage and run storage
		logger.WithError(err).Error("fail to fetch terraform run from storage")
		h.executionFailedEventWithoutRun(tid, workflow, err, sink)
	} else {
		h.handleWorkflowSuccess(logger, event, tfRun, sink)
	}
}

func (h WorkflowHandler) executionFailedEventWithoutRun(tid common.TransactionID, workflow types.DeploymentWorkflow, err error, sink ports.OutgoingEventSink) {
	eventBody := deploymentevents.RunExecutionFailed{
		Session: service.Session{
			SessionActor:    "", // TODO add this when workflow event has actor
			SessionEmulator: "",
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		TemplateType: workflow.TemplateType,
		Deployment:   workflow.Deployment,
		Run:          workflow.Run,
	}
	sink.EventRunExecutionFailed(eventBody)
}

func (h WorkflowHandler) handleWorkflowSuccess(logger *log.Entry, event awmclient.WorkflowSucceeded, tfRun types.DeploymentRun, sink ports.OutgoingEventSink) {
	tfState, err := h.parseTFStateFromWorkflowOutputs(event.WfOutputs)
	if err != nil {
		logger.WithError(err).Error("fail to parse terraform state from workflow outputs")
		h.stateRefreshFailedEvent(tfRun, service.DeploymentStateView{}, err, sink)
		return
	}
	logger.Infof("tf state has %d resource", len(tfState.Resources))

	err = h.updateRunWithSuccessResult(tfRun.ID, tfState)
	if err != nil {
		logger.WithError(err).Error("fail to update run")
		h.stateRefreshFailedEvent(tfRun, service.DeploymentStateView{}, err, sink)
		return
	}
	stateView, err := h.convertTFStateToStateView(tfRun, tfState)
	if err != nil {
		logger.WithError(err).Error("fail to convert terraform state to state view")
		h.stateRefreshFailedEvent(tfRun, service.DeploymentStateView{}, err, sink)
	} else {
		logger.Infof("execution stage succeeded")
		h.stateRefreshSuccessEvent(tfRun, stateView, sink)
	}
}

func (h WorkflowHandler) parseTFStateFromWorkflowOutputs(wfOutputs map[string]interface{}) (deploymentcommon.TerraformState, error) {
	// parse tf state from wf outputs
	var tfState deploymentcommon.TerraformState
	err := mapstructure.Decode(wfOutputs, &tfState)
	if err != nil {
		return deploymentcommon.TerraformState{}, err
	}
	if tfState.Resources == nil {
		// this ensures that resources key is at least present in workflow outputs
		return deploymentcommon.TerraformState{}, service.NewCacaoInvalidParameterError("workflow output cannot be parsed as terraform state")
	}
	return tfState, nil
}

// update run with Terraform state
func (h WorkflowHandler) updateRunWithSuccessResult(runID common.ID, tfState deploymentcommon.TerraformState) error {
	now := h.timeSrc.Now()
	runUpdate := types.DeploymentRunUpdate{
		StateUpdatedAt: &now,
		TerraformState: &tfState,
	}
	updated, err := h.runStorage.Update(runID, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to update terraform run in storage, run %s not found", runID.String())
	}
	return nil
}

func (h WorkflowHandler) convertTFStateToStateView(tfRun types.DeploymentRun, tfState deploymentcommon.TerraformState) (service.DeploymentStateView, error) {
	var parser deploymentcommon.TerraformStateParser
	stateView, err := parser.ToStateView(tfState, tfRun.PrimaryProvider)
	if err != nil {
		return service.DeploymentStateView{}, err
	}
	return stateView, nil
}

func (h WorkflowHandler) stateRefreshSuccessEvent(tfRun types.DeploymentRun, stateView service.DeploymentStateView, sink ports.OutgoingEventSink) {
	eventBody := deploymentevents.DeploymentStateRefreshSucceeded{
		Session: service.Session{
			SessionActor:    tfRun.CreatedBy.User,
			SessionEmulator: tfRun.CreatedBy.Emulator,
		},
		TemplateType: tfRun.TemplateType,
		Deployment:   tfRun.Deployment,
		Run:          tfRun.ID,
		StateView:    stateView,
	}
	sink.EventDeploymentStateRefreshSucceeded(eventBody)
}

func (h WorkflowHandler) stateRefreshFailedEvent(tfRun types.DeploymentRun, stateView service.DeploymentStateView, err error, sink ports.OutgoingEventSink) {
	eventBody := deploymentevents.DeploymentStateRefreshFailed{
		Session: service.Session{
			SessionActor:    tfRun.CreatedBy.User,
			SessionEmulator: tfRun.CreatedBy.Emulator,
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		ID:    tfRun.Deployment,
		State: stateView,
	}
	sink.EventDeploymentStateRefreshFailed(eventBody)
}

// HandleWorkflowFailure handles the failed workflow.
// This will update the status of Terraform run in storage.
// And emit EventRunExecutionFailed event.
func (h WorkflowHandler) HandleWorkflowFailure(tid common.TransactionID, event awmclient.WorkflowFailed, workflow types.DeploymentWorkflow, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":    "staterefresh",
		"function":   "WorkflowHandler.HandleWorkflowFailure",
		"workflow":   workflow.WorkflowName,
		"deployment": workflow.Deployment,
		"run":        workflow.Run,
	})
	if !workflow.Run.Validate() || workflow.Run.FullPrefix() != service.RunIDPrefix {
		logger.Error("workflow object has bad run ID")
		return
	}
	tfRun, err := h.runStorage.Get(workflow.Run)
	if err != nil {
		// either connection to storage has error, or run does not exist in storage which means an inconsistency between workflow storage and run storage
		logger.WithError(err).Error("fail to fetch terraform run from storage")
		return
	}
	h.handleWorkflowFailure(logger, event, tfRun, sink)
}

func (h WorkflowHandler) handleWorkflowFailure(logger *log.Entry, event awmclient.WorkflowFailed, tfRun types.DeploymentRun, sink ports.OutgoingEventSink) {

	tfState, err := h.parseTFStateFromWorkflowOutputs(event.WfOutputs)
	if err != nil {
		logger.WithError(err).Error("fail to parse terraform state from workflow outputs")
	}
	logger.Infof("tf state has %d resource", len(tfState.Resources))

	var stateView service.DeploymentStateView
	if err == nil {
		stateView, err = h.convertTFStateToStateView(tfRun, tfState)
		if err != nil {
			logger.WithError(err).Error("fail to convert terraform state to state view")
		}
	}

	err = h.updateRunWithErroredResult(tfRun.ID, tfState)
	if err != nil {
		logger.WithError(err).Error("fail to update status of terraform run")
		h.stateRefreshFailedEvent(tfRun, stateView, err, sink)
	} else {
		logger.Error("execution stage failed")
		h.stateRefreshFailedEvent(tfRun, stateView, h.workflowFailedError(event), sink)
	}
}

func (h WorkflowHandler) updateRunWithErroredResult(runID common.ID, tfState deploymentcommon.TerraformState) error {
	now := h.timeSrc.Now()
	runUpdate := types.DeploymentRunUpdate{
		StateUpdatedAt: &now,
		TerraformState: &tfState,
	}
	updated, err := h.runStorage.Update(runID, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to update terraform run in storage, run %s not found", runID.String())
	}
	return nil
}

// generate an error for the failed workflow
func (h WorkflowHandler) workflowFailedError(event awmclient.WorkflowFailed) error {
	node, nodeStatus := h.findFirstFailedNode(event)
	var err error
	msg := fmt.Sprintf("execution workflow %s failed with status %s", event.WorkflowName, event.Status)
	if node != "" {
		err = fmt.Errorf("%s, node %s failed with status %s", msg, node, nodeStatus)
	} else {
		err = errors.New(msg)
	}
	return err
}

func (h WorkflowHandler) findFirstFailedNode(event awmclient.WorkflowFailed) (nodeName, nodeStatus string) {
	for node, status := range event.NodeStatus {
		if node == "" {
			continue
		}
		switch status {
		default:
		case awmclient.NodeFailed:
			return node, string(status)
		case awmclient.NodeError:
			return node, string(status)
		}
	}
	return "", ""
}
