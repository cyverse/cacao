package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// EventHandlers ...
type EventHandlers struct {
	startRunHandler           StartRunHandler
	preflightSucceededHandler PreflightSucceededHandler
	refreshStateHandler       RefreshStateHandler
	deletionHandler           DeletionStartedHandler
	wfSucceededHandler        WorkflowSucceededHandler
	wfFailedHandler           WorkflowFailedHandler
}

var _ ports.EventHandlers = (*EventHandlers)(nil)

// NewEventHandlers ...
func NewEventHandlers(portsDependency ports.Ports, whitelistCIDRs []string) EventHandlers {
	return EventHandlers{
		startRunHandler:           NewStartRunHandler(portsDependency, whitelistCIDRs),
		preflightSucceededHandler: NewPreflightSucceededHandler(portsDependency),
		refreshStateHandler:       NewRefreshStateHandler(portsDependency),
		deletionHandler:           NewDeletionHandler(portsDependency),
		wfSucceededHandler:        NewWorkflowSucceededHandler(portsDependency),
		wfFailedHandler:           NewWorkflowFailedHandler(portsDependency),
	}
}

// EventStartRunRequested ...
func (h EventHandlers) EventStartRunRequested(tid common.TransactionID, request deploymentevents.StartRunRequest, sink ports.OutgoingEventSink) {
	if h.ignoreEvent(request.Deployment.TemplateType) {
		return
	}
	result := h.startRunHandler.handle(tid, request)
	session := types.CopySessionActors(request.Session)
	if result.Error != nil {
		session.ServiceError = types.ErrorToServiceError(result.Error).GetBase()
		sink.EventRunPreflightStartFailed(deploymentevents.RunPreflightStartFailed{
			Session:      session,
			TemplateType: request.Deployment.TemplateType,
			Deployment:   request.Deployment.ID,
			Run:          request.Run.ID,
		})
	} else {
		sink.EventRunPreflightStarted(deploymentevents.RunPreflightStarted{
			Session:      session,
			TemplateType: request.Deployment.TemplateType,
			Deployment:   request.Deployment.ID,
			Run:          request.Run.ID,
			Parameters:   result.Parameters,
		})
	}
}

// EventRunPreflightSucceeded ...
func (h EventHandlers) EventRunPreflightSucceeded(tid common.TransactionID, result deploymentevents.RunPreflightResult, sink ports.OutgoingEventSink) {
	if h.ignoreEvent(result.TemplateType) {
		return
	}
	started, runExecutionStarted, runExecutionFailed := h.preflightSucceededHandler.handle(tid, result)
	if started {
		sink.EventRunExecutionStarted(runExecutionStarted)
	} else {
		sink.EventRunExecutionFailed(runExecutionFailed)
	}
}

// DeploymentDeletionStarted handles service.DeploymentDeletionStarted (emitted by metadata service), and perform resource cleanup on the deployment
func (h EventHandlers) DeploymentDeletionStarted(deletionResult service.DeploymentDeletionResult, sink ports.OutgoingEventSink) {
	if h.ignoreEvent(deletionResult.TemplateType) {
		return
	}
	result := h.deletionHandler.handle(deletionResult)
	result.WriteTo(sink)
}

// EventStartStateRefreshRequested ...
func (h EventHandlers) EventStartStateRefreshRequested(tid common.TransactionID, request deploymentevents.StartStateRefreshRequest, sink ports.OutgoingEventSink) {
	if h.ignoreEvent(request.TemplateType) {
		return
	}
	started, stateRefreshStarted, stateRefreshFailed := h.refreshStateHandler.handle(tid, request)
	if started {
		sink.EventDeploymentStateRefreshStarted(stateRefreshStarted)
	} else {
		sink.EventDeploymentStateRefreshFailed(stateRefreshFailed)
	}
}

// WorkflowSucceededEvent ...
func (h EventHandlers) WorkflowSucceededEvent(succeeded awmclient.WorkflowSucceeded, sink ports.OutgoingEventSink) {
	h.wfSucceededHandler.Handle("", succeeded, sink)
}

// WorkflowFailedEvent ...
func (h EventHandlers) WorkflowFailedEvent(failed awmclient.WorkflowFailed, sink ports.OutgoingEventSink) {
	h.wfFailedHandler.Handle("", failed, sink)
}

func (h EventHandlers) ignoreEvent(templateType service.TemplateTypeName) bool {
	// this service only handles deployments that are based on terraform template
	switch templateType {
	case types.TerraformOpenStackDeploymentType:
	case types.TerraformAWSDeploymentType:
	default:
		log.WithField("templateType", templateType).Debugf("event ignored due to template type")
		return true
	}
	return false
}
