package eventhandler

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/awmclient"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/domain/staterefresh"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/ports"
	"gitlab.com/cyverse/cacao/deployment-service/terraform_execution/types"
)

// RefreshStateHandler handles request to refresh the terraform state of deployment.
type RefreshStateHandler struct {
	runStorage ports.TFRunStorage
	wfStorage  ports.DeploymentWorkflowStorage
	awm        ports.ArgoWorkflowMediator
	credMS     ports.CredentialMicroservice
	keySrc     ports.KeySrc
}

// NewRefreshStateHandler ...
func NewRefreshStateHandler(portsDependency ports.Ports) RefreshStateHandler {
	return RefreshStateHandler{
		runStorage: portsDependency.TFRunStorage,
		wfStorage:  portsDependency.WorkflowStorage,
		awm:        portsDependency.AWM,
		credMS:     portsDependency.CredentialMS,
		keySrc:     portsDependency.KeySrc,
	}
}

func (h RefreshStateHandler) validateEvent(logger *log.Entry, event deploymentevents.StartStateRefreshRequest) error {
	if !event.Run.Validate() || event.Run.FullPrefix() != service.RunIDPrefix {
		logger.WithField("runID", event.Run).Error("incoming event has bad run ID")
		return service.NewCacaoInvalidParameterError("bad run ID")
	}
	if !event.Deployment.Validate() || event.Deployment.FullPrefix() != service.DeploymentIDPrefix {
		logger.WithField("deploymentID", event.Deployment).Error("incoming event has bad deployment ID")
		return service.NewCacaoInvalidParameterError("bad deployment ID")
	}
	return nil
}

func (h RefreshStateHandler) handle(tid common.TransactionID, event deploymentevents.StartStateRefreshRequest) (started bool, _ deploymentevents.DeploymentStateRefreshStarted, _ deploymentevents.DeploymentStateRefreshFailed) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "RefreshStateHandler.handle",
	})
	err := h.validateEvent(logger, event)
	if err != nil {
		return false, deploymentevents.DeploymentStateRefreshStarted{}, h.stateRefreshFailedEvent(event, err)
	}
	gatherer := staterefresh.NewPrerequisiteGatherer(h.runStorage, h.credMS)
	prerequisite, err := gatherer.Gather(event.Run)
	if err != nil {
		logger.WithError(err).Error("fail to gather prerequisite data for refreshing the state")
		return false, deploymentevents.DeploymentStateRefreshStarted{}, h.stateRefreshFailedEvent(event, err)
	}

	var executor staterefresh.TemplateExecutor
	awmProvider, wfName, err := executor.Execute(h.awm, h.keySrc, prerequisite)
	if err != nil {
		logger.WithError(err).Error("fail to launch workflow for refreshing the state template")
		return false, deploymentevents.DeploymentStateRefreshStarted{}, h.stateRefreshFailedEvent(event, err)
	}
	logger.WithFields(log.Fields{
		"awmProvider": awmProvider,
		"wfName":      wfName,
	}).Info("refresh workflow launched")

	err = h.saveWorkflow(prerequisite.TFRun, awmProvider, wfName)
	if err != nil {
		logger.WithError(err).Error("fail to save refresh workflow to storage")
		return false, deploymentevents.DeploymentStateRefreshStarted{}, h.stateRefreshFailedEvent(event, err)
	}
	logger.Info("state refresh started")
	return true, h.stateRefreshStartedEvent(prerequisite.TFRun), deploymentevents.DeploymentStateRefreshFailed{}
}

func (h RefreshStateHandler) saveWorkflow(tfRun types.DeploymentRun, awmProvider awmclient.AWMProvider, wfName string) error {
	err := h.wfStorage.CreateDeploymentRefreshWorkflow(tfRun.Deployment, tfRun.TemplateType, tfRun.ID, awmProvider, wfName)
	if err != nil {
		return err
	}
	return nil
}

// deploymentevents.DeploymentStateRefreshFailed
func (h RefreshStateHandler) stateRefreshFailedEvent(event deploymentevents.StartStateRefreshRequest, err error) deploymentevents.DeploymentStateRefreshFailed {
	return deploymentevents.DeploymentStateRefreshFailed{
		Session: service.Session{
			SessionActor:    event.SessionActor,
			SessionEmulator: event.SessionEmulator,
			ServiceError:    types.ErrorToServiceError(err).GetBase(),
		},
		ID:    event.Deployment,
		State: service.DeploymentStateView{},
	}
}

// deploymentevents.DeploymentStateRefreshStarted
func (h RefreshStateHandler) stateRefreshStartedEvent(tfRun types.DeploymentRun) deploymentevents.DeploymentStateRefreshStarted {
	return deploymentevents.DeploymentStateRefreshStarted{
		Session: service.Session{
			SessionActor:    tfRun.CreatedBy.User,
			SessionEmulator: tfRun.CreatedBy.Emulator,
		},
		TemplateType: tfRun.TemplateType,
		Deployment:   tfRun.Deployment,
		Run:          tfRun.ID,
	}
}
