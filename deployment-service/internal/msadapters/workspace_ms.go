package msadapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/messaging2"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
)

// WorkspaceMicroservice is a client to interact with Workspace microservice
type WorkspaceMicroservice struct {
	queryConn messaging2.QueryConnection
}

// NewWorkspaceMicroservice ...
func NewWorkspaceMicroservice(queryConn messaging2.QueryConnection) WorkspaceMicroservice {
	return WorkspaceMicroservice{
		queryConn: queryConn,
	}
}

// Get a workspace
func (w WorkspaceMicroservice) Get(actor service.Actor, id common.ID) (*service.WorkspaceModel, error) {
	logger := log.WithFields(log.Fields{
		"package":   "msadapters",
		"function":  "WorkspaceMicroservice.Get",
		"actor":     actor.Actor,
		"emulator":  actor.Emulator,
		"workspace": id,
	})
	client, err := service.NewNatsWorkspaceClientFromConn(w.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create workspace svc client")
		return nil, err
	}

	workspace, err := client.Get(context.TODO(), actor, id)
	if err != nil {
		logger.WithError(err).Error("fail to get workspace")
		return nil, err
	}
	logger.Debug("got workspace from workspace ms")
	return workspace, nil
}
