package ports

import (
	"context"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"io"
	"sync"
	"time"

	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/k8s_execution/types"
	"k8s.io/client-go/rest"
)

// EventSrc is a sink to publish events into
type EventSrc interface {
	Init(config types.Config) error
	Start(ctx context.Context, wg *sync.WaitGroup, handlers IncomingEventHandlers) error
}

// IncomingEventHandlers represents the handlers for all events that this service handles.
type IncomingEventHandlers interface {
	EventStartRunRequested(request deploymentevents.StartRunRequest, sink OutgoingEvents)    // deploymentevents.EventStartRunRequested
	DeploymentDeletionStarted(request service.DeploymentDeletionResult, sink OutgoingEvents) // service.DeploymentDeletionStarted
}

// OutgoingEvents represents all outgoing events this service will publish
type OutgoingEvents interface {
	EventRunPreflightStarted(deploymentevents.RunPreflightStarted)         // deploymentevents.EventRunPreflightStarted
	EventRunPreflightStartFailed(deploymentevents.RunPreflightStartFailed) // deploymentevents.EventRunPreflightStartFailed
	EventRunExecutionSucceeded(deploymentevents.RunExecutionSucceeded)     // deploymentevents.EventRunExecutionSucceeded
	EventRunExecutionFailed(deploymentevents.RunExecutionFailed)           // deploymentevents.EventRunExecutionFailed

	EventDeploymentDeletionCleanupSucceeded(deploymentevents.DeploymentDeletionCleanupResult) // deploymentevents.EventDeploymentDeletionCleanupSucceeded
	EventDeploymentDeletionCleanupFailed(deploymentevents.DeploymentDeletionCleanupResult)    // deploymentevents.EventDeploymentDeletionCleanupFailed
	EventDeploymentDeletionCleanupStarted(deploymentevents.DeploymentDeletionCleanupStarted)  // deploymentevents.EventDeploymentDeletionCleanupStarted
}

// K8SRunStorage is storage for Terraform Run
type K8SRunStorage interface {
	Init() error
	Get(runID common.ID) (types.DeploymentRun, error)
	Create(run types.DeploymentRun) error
	//Update(runID common.ID, update types.DeploymentRunUpdate, filter types.DeploymentRunFilter) (updated bool, err error)
	//Search(filter types.DeploymentRunFilter) ([]types.DeploymentRun, error)
}

// K8S ...
type K8S interface {
	Ping(ctx context.Context, cfg *rest.Config) error
	Apply(ctx context.Context, cfg *rest.Config, templateFiles []types.TemplateFile) error
	Delete(ctx context.Context, cfg *rest.Config, templateFiles []types.TemplateFile) error
}

// K8SConnector ...
type K8SConnector interface {
	BuildConnection(config types.K8SConnectionConfig) (*rest.Config, io.Closer, error)
}

// Git ...
type Git interface {
	// Clone clones the template from git, if template is private, gitCredential is required to clone the repo
	Clone(templateSrc service.TemplateSource, gitCredential *service.CredentialModel) (files []types.TemplateFile, commitHash string, err error)
}

// DeploymentMetadataService ...
type DeploymentMetadataService interface {
	Get(actor service.Actor, id common.ID) (service.Deployment, error)
}

// WorkspaceMicroservice is an interface to interact with Workspace service
type WorkspaceMicroservice interface {
	Get(actor service.Actor, id common.ID) (*service.WorkspaceModel, error)
}

// TemplateMicroservice is an interface to interact with Template service
type TemplateMicroservice interface {
	Get(actor service.Actor, id common.ID) (service.Template, error)
}

// ProviderMicroservice is an interface to interact with Provider service
type ProviderMicroservice interface {
	Get(actor service.Actor, id common.ID) (*service.ProviderModel, error)
	List(actor service.Actor) ([]service.ProviderModel, error)
}

// CredentialMicroservice is an interface to interact with Credential service
type CredentialMicroservice interface {
	Get(actor service.Actor, owner, id string) (*service.CredentialModel, error)
	// List return a list of credential ID
	List(actor service.Actor, owner string) ([]service.CredentialModel, error)
}

// SSHKeySrc is a way to get CACAO's private ssh key to ssh into a jump host that has access to user's k8s cluster.
type SSHKeySrc interface {
	GetPrivateSSHKey() string
}

// TimeSrc is a source of current time
type TimeSrc interface {
	Now() time.Time
}

// Ports contains a ref to most ports, this struct exist to ease passing dependency as parameter.
type Ports struct {
	K8SRunStorage K8SRunStorage
	TimeSrc       TimeSrc
	SSHKeySrc     SSHKeySrc

	MetadataSvc  DeploymentMetadataService
	TemplateMS   TemplateMicroservice
	WorkspaceMS  WorkspaceMicroservice
	CredentialMS CredentialMicroservice
	ProviderMS   ProviderMicroservice
}
