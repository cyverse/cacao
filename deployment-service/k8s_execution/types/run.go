package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DeploymentRun is the storage model (mongodb) for k8s deployment run. This store additional info that is specific to k8s.
// FIXME This is currently NOT used, since we do not persistent anything right now.
type DeploymentRun struct {
	ID           common.ID                `bson:"_id"`
	Deployment   common.ID                `bson:"deployment"`
	TemplateType service.TemplateTypeName `bson:"template_type"`
	CreatedBy    deploymentcommon.Creator `bson:",inline"`
	// transaction ID of the create run request
	CreateRunTID         common.TransactionID                        `bson:"create_run_tid"`
	PrimaryProvider      common.ID                                   `bson:"primary_provider"`
	PrerequisiteTemplate deploymentcommon.TemplateSnapshot           `bson:"prerequisite_template"`
	TemplateSnapshot     deploymentcommon.TemplateSnapshot           `bson:"template"`
	Parameters           deploymentcommon.DeploymentParameters       `bson:"parameters"`
	CloudCredentials     deploymentcommon.ProviderCredentialMappings `bson:"cloud_creds"`
	GitCredential        string                                      `bson:"git_cred"`
	Status               deploymentcommon.DeploymentRunStatus        `bson:"status"`
	ObjectMetaList       []v1.ObjectMeta                             `bson:","` // FIXME do we want to store this?
}
