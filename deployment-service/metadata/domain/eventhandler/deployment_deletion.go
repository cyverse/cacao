package eventhandler

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

type deletionResult struct {
	service.Session
	Deployment   common.ID
	Deleted      bool // if error is empty, and Deleted is false, then the deletion process has just started
	TemplateType service.TemplateTypeName
}

func (result deletionResult) publish(sink ports.OutgoingEventSink) {
	var event = service.DeploymentDeletionResult{
		Session:      result.Session,
		ID:           result.Deployment,
		TemplateType: result.TemplateType,
	}
	if result.GetServiceError() != nil {
		sink.DeploymentDeleteFailed(event)
		return
	}
	if result.Deleted {
		sink.DeploymentDeleted(event)
		return
	}
	// deletion started but not finished
	sink.DeploymentDeletionStarted(event)
}

// DeletionHandler handles deletion of deployment.
// Deployment with existing run(s) and has provisioned resource(s) (determined by the deployment state view) will have its resource cleaned up.
// The cleanup process is async, and not the responsibility of this microservice.
// Note that deployment entity is not permanently removed when deletion succeeds, but simply changed its current status to 'deleted'.
type DeletionHandler struct {
	deploymentStorage ports.DeploymentStorage
	runStorage        ports.DeploymentRunStorage
	timeSrc           ports.TimeSrc
}

// NewDeletionHandler ...
func NewDeletionHandler(portsDependency ports.Ports) DeletionHandler {
	return DeletionHandler{
		deploymentStorage: portsDependency.DeploymentStorage,
		runStorage:        portsDependency.RunStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle ...
func (h DeletionHandler) Handle(request service.DeploymentDeletionRequest, sink ports.OutgoingEventSink) {
	h.handle(request).publish(sink)
}

func (h DeletionHandler) handle(request service.DeploymentDeletionRequest) deletionResult {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "DeletionHandler.handle",
		"deployment": request.ID,
	})
	var checker deletionChecker
	err := h.initialCheck(request, &checker)
	if err != nil {
		return h.deleteFailedResult(request, err)
	}

	// atomic status update
	err = h.deploymentStatusToDeleting(request.ID)
	if err != nil {
		logger.WithError(err).Error()
		return h.deleteFailedResult(request, err)
	}

	// re-fetch and re-check after marking status
	needCleanup, err := h.refetchAndCheck(request, &checker)
	if err != nil {
		return h.deletionFailedImmediately(request, request.ID, err)
	}

	if !needCleanup {
		logger.Info("no resource to clean up, delete right away")
		return h.performSimpleDelete(request)
	}

	// notify caller and specific deployment svc of async deletion
	return h.deletionStartedResult(request, checker.deployment.TemplateType)
}

func (h DeletionHandler) initialCheck(request service.DeploymentDeletionRequest, checker *deletionChecker) error {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "DeletionHandler.initialCheck",
		"deployment": request.ID,
	})
	err := checker.SetRequest(request)
	if err != nil {
		logger.WithError(err).Error("deletion request has error in it")
		return err
	}

	deployment, err := h.deploymentStorage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch deployment from storage")
		return err
	}
	err = checker.SetDeployment(deployment)
	if err != nil {
		logger.WithError(err).Error("fetched deployment has error in it")
		return err
	}

	if deployment.HasLastRun() {
		lastRun, err2 := h.runStorage.Get(*deployment.LastRun)
		if err2 != nil {
			logger.WithField("run", deployment.LastRun).WithError(err2).Error("fail to fetch last run of deployment")
			return err2
		}
		err = checker.AddRun(lastRun)
		if err != nil {
			logger.WithError(err).Error("last run has error in it")
			return err
		}
	}
	_, err = checker.Check()
	if err != nil {
		logger.WithError(err).Error("initial check on deletion request failed")
		return err
	}
	return nil
}

func (h DeletionHandler) deploymentStatusToDeleting(deploymentID common.ID) error {
	update, filter := types.NewDeploymentStatusUpdater(h.timeSrc.Now()).PendingStatusDeleting()
	updated, err := h.deploymentStorage.Update(deploymentID, update, filter)
	if err != nil {
		return err
	}
	if !updated {
		// pending status is not updated due to no deployment matching the filter,
		// which means either deployment no longer exists in storage or its status (in storage) does not allow deletion
		return service.NewCacaoGeneralError("deployment pending status cannot be updated, either deployment has conflict status or deployment no longer exists in storage, fail to delete")
	}
	return nil
}

// the goal of re-fetching is to avoid any race condition where changes are made to deployment in storage between initial fetch and the atomic pending status update.
func (h DeletionHandler) refetchAndCheck(request service.DeploymentDeletionRequest, checker *deletionChecker) (needCleanup bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":    "eventhandler",
		"function":   "DeletionHandler.refetchAndCheck",
		"deployment": request.ID,
	})
	// re-fetch deployment
	deployment, err := h.deploymentStorage.Get(request.ID)
	if err != nil {
		logger.WithError(err).Error("fail to re-fetch deployment after pending status update")
		return false, err
	}

	err = checker.SetDeploymentAfterStatusUpdate(deployment)
	if err != nil {
		logger.WithError(err).Error("re-fetched deployment has error")
		return false, err
	}
	needCleanup, err = checker.Check()
	if err != nil {
		logger.WithError(err).Error("re-check failed")
		return false, err
	}
	return needCleanup, nil
}

// change status current_status to deleted, and clear pending_status, does NOT perform cleanup on resource.
func (h DeletionHandler) performSimpleDelete(request service.DeploymentDeletionRequest) deletionResult {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "DeletionHandler.performSimpleDelete",
	})
	update, filter := types.NewDeploymentStatusUpdater(h.timeSrc.Now()).StatusDeleted()
	updated, err := h.deploymentStorage.Update(request.ID, update, filter)
	if err != nil {
		logger.WithError(err).Error()
		return h.deleteFailedResult(request, err)
	}
	if !updated {
		err = service.NewCacaoGeneralError("deployment no longer exists in storage, fail to update status")
		logger.WithError(err).Error("fail to update status to deleted")
		return h.deleteFailedResult(request, err)
	}
	return h.deletedResult(request)
}

// This set the status of the deployment to deletion_errored, and returns a failed deletionResult.
// Note: this should be called for any terminal error (error that will stop the handler from proceeding any further) after updating pending status of the deployment to deleting.
// The deployment status is set to maintain status integrity, so that deletion CAN be retried later.
func (h DeletionHandler) deletionFailedImmediately(request service.DeploymentDeletionRequest, deployment common.ID, failure error) deletionResult {
	logger := log.WithFields(log.Fields{
		"function":   "DeletionHandler.deletionFailedImmediately",
		"deployment": deployment,
	})
	update, filter := types.NewDeploymentStatusUpdater(h.timeSrc.Now()).StatusDeletionErrored()
	updated, err := h.deploymentStorage.Update(deployment, update, filter)
	if err != nil {
		logger.WithError(err).Error("fail to update deployment status after deletion failed")
		return h.deleteFailedResult(request, fmt.Errorf("%s, %w", failure.Error(), err))
	}
	if !updated {
		err = service.NewCacaoGeneralError(fmt.Sprintf("deployment %s no longer exists in storage", deployment))
		logger.WithError(err).Error()
		return h.deleteFailedResult(request, fmt.Errorf("%s, %w", failure.Error(), err))
	}
	return h.deleteFailedResult(request, failure)
}

func (h DeletionHandler) deleteFailedResult(request service.DeploymentDeletionRequest, err error) deletionResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
		Deployment: request.ID,
		Deleted:    false,
	}
}

func (h DeletionHandler) deletedResult(request service.DeploymentDeletionRequest) deletionResult {
	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment: request.ID,
		Deleted:    true,
	}
}

// This result has 2 use cases:
// 1. notify caller (whoever sends the deletion request) that deletion is started (meaning the deletion will be async).
// 2. notify specific deployment svc that it needs to delete the resources.
func (h DeletionHandler) deletionStartedResult(request service.DeploymentDeletionRequest, templateType service.TemplateTypeName) deletionResult {
	return deletionResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment:   request.ID,
		Deleted:      false,
		TemplateType: templateType,
	}
}

func (h DeletionHandler) logResponse(logger *log.Entry, result deletionResult) {
	logger.WithFields(log.Fields{
		"actor":      result.GetSessionActor(),
		"emulator":   result.GetSessionEmulator(),
		"deployment": result.Deployment,
		"err":        result.GetServiceError(),
		"deleted":    result.Deleted,
	}).Info("response event")
}

type deletionChecker struct {
	deployment types.Deployment
	request    service.DeploymentDeletionRequest
	runs       []types.DeploymentRun
}

// SetRequest sets the deletion request and validates it.
func (dc *deletionChecker) SetRequest(request service.DeploymentDeletionRequest) error {
	if request.GetSessionActor() == "" {
		return service.NewCacaoInvalidParameterError("request actor cannot be empty")
	}
	if !request.ID.Validate() || request.ID.FullPrefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("bad deployment ID in request")
	}
	dc.request = request
	return nil
}

// SetDeployment validates the deployment object and sets it.
func (dc *deletionChecker) SetDeployment(deployment types.Deployment) error {
	if err := dc.validateDeployment(deployment); err != nil {
		return err
	}
	err := dc.checkDeploymentStatus(deployment)
	if err != nil {
		return err
	}
	dc.deployment = deployment
	return nil
}

// SetDeploymentAfterStatusUpdate validates the deployment object and sets it.
// The validation assumes that the pending status of deployment has been updated to deleting.
func (dc *deletionChecker) SetDeploymentAfterStatusUpdate(deployment types.Deployment) error {
	if err := dc.validateDeployment(deployment); err != nil {
		return err
	}
	if deployment.PendingStatus != service.DeploymentStatusDeleting {
		return service.NewCacaoInvalidParameterError("pending status did not update")
	}
	dc.deployment = deployment
	return nil
}

func (dc deletionChecker) validateDeployment(deployment types.Deployment) error {
	if !deployment.ID.Validate() || deployment.ID.FullPrefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("bad deployment ID")
	}
	if deployment.CreatedBy.User == "" {
		return service.NewCacaoInvalidParameterError("deployment has no creator")
	}
	if !deployment.Template.Validate() || deployment.Template.FullPrefix() != "template" {
		return service.NewCacaoInvalidParameterError("deployment has bad template ID")
	}
	if deployment.TemplateType == "" {
		return service.NewCacaoInvalidParameterError("deployment has no template type")
	}
	if !deployment.PrimaryCloudProvider.Validate() || deployment.PrimaryCloudProvider.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("deployment has bad provider ID")
	}
	if !deployment.CurrentStatus.Valid() {
		return service.NewCacaoInvalidParameterError("bad current status")
	}
	if !deployment.PendingStatus.Valid() {
		return service.NewCacaoInvalidParameterError("bad pending status")
	}
	return nil
}

func (dc deletionChecker) checkDeploymentStatus(deployment types.Deployment) error {
	if !deployment.CurrentStatus.AllowDeletion() {
		return service.NewCacaoInvalidParameterError("current status does not allow deletion")
	}
	if deployment.PendingStatus != service.DeploymentStatusNoPending {
		return service.NewCacaoInvalidParameterError("deployment cannot be deleted when pending status is not none")
	}
	return nil
}

// AddRun add a deployment run (run of the deployment) and validate it.
func (dc *deletionChecker) AddRun(run types.DeploymentRun) error {
	if !run.ID.Validate() || run.ID.FullPrefix() != service.RunIDPrefix {
		return service.NewCacaoInvalidParameterError("bad run ID")
	}
	if !run.Deployment.Validate() || run.Deployment.FullPrefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("bad deployment ID in run")
	}
	dc.runs = append(dc.runs, run)
	return nil
}

// Check checks for consistency, permission of the deployment deletion request. And determines whether there are resources that needs to be cleaned up separately
func (dc deletionChecker) Check() (needCleanup bool, err error) {
	if err = dc.checkPrerequisite(); err != nil {
		return false, err
	}
	if err = dc.checkInconsistency(); err != nil {
		return false, err
	}
	if err = dc.checkPermission(); err != nil {
		return false, err
	}
	needCleanup, err = dc.requireResourceCleanup()
	if err != nil {
		return false, err
	}
	return needCleanup, err
}

func (dc deletionChecker) checkPrerequisite() error {
	if dc.deployment.ID == "" {
		return service.NewCacaoInvalidParameterError("deployment not set")
	}
	if dc.request.ID == "" {
		return service.NewCacaoInvalidParameterError("request not set")
	}

	return nil
}

func (dc deletionChecker) checkPermission() error {
	// only owner/creator has permission to delete
	if dc.request.GetSessionActor() != dc.deployment.CreatedBy.User {
		return service.NewCacaoUnauthorizedError("unauthorized access to deployment")
	}
	return nil
}

func (dc deletionChecker) checkInconsistency() error {
	if dc.request.ID != dc.deployment.ID {
		return service.NewCacaoInvalidParameterError("inconsistent deployment ID between deployment and request")
	}
	if dc.deployment.HasLastRun() {
		if dc.runs == nil || len(dc.runs) == 0 {
			return service.NewCacaoInvalidParameterError("run not set when deployment has last run")
		}
	}
	if !dc.deployment.HasLastRun() && dc.runs != nil {
		return service.NewCacaoInvalidParameterError("runs set when deployment does not have any run")
	}
	if dc.runs != nil && len(dc.runs) > 0 {
		var lastRunInRuns bool
		for _, run := range dc.runs {
			if run.Deployment != dc.request.ID {
				return service.NewCacaoInvalidParameterError("inconsistent deployment ID between run and request")
			}
			if run.ID == *dc.deployment.LastRun {
				lastRunInRuns = true
			}
		}
		if !lastRunInRuns {
			return service.NewCacaoInvalidParameterError("last run of deployment is not in runs")
		}
	}
	return nil
}

func (dc deletionChecker) requireResourceCleanup() (needCleanup bool, err error) {
	if !dc.deployment.HasLastRun() {
		// if deployment has 0 runs, then it is impossible for it to provision any resources, therefore no need to clean up
		return false, nil
	}
	// checking whether deployment has resource is a little flaky right now,
	// so perform a cleanup regardless of whether deployment has resource or not.
	return true, nil
}
