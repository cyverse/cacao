package eventhandler

import (
	"fmt"
	"time"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"

	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// RunCreationHandler handles creation of deployment run
// This handler only performs barebone validation on the request, just enough
// to warrant the creation of an entity in the storage.
type RunCreationHandler struct {
	runCreationPorts
}

// runCreationPorts are ports that are needed for creating a run
type runCreationPorts struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	providerMS        ports.ProviderMicroservice
	templateMS        ports.TemplateMicroservice
	credMS            ports.CredentialMicroservice
	timeSrc           ports.TimeSrc
}

type runCreationPrerequisiteData struct {
	Deployment                   types.Deployment
	PreviousRun                  *types.DeploymentRun // nil if no previous run
	Provider                     *service.ProviderModel
	Template                     service.Template
	TemplateType                 service.TemplateType
	LatestTemplateVersion        service.TemplateVersion
	SpecifiedTemplateVersion     *service.TemplateVersion
	TemplateVersionUsedByLastRun *service.TemplateVersion // nil if no previous run
}

// NewRunCreationHandler ...
func NewRunCreationHandler(portsDependency ports.Ports) RunCreationHandler {
	return RunCreationHandler{
		runCreationPorts: runCreationPorts{
			runStorage:        portsDependency.RunStorage,
			deploymentStorage: portsDependency.DeploymentStorage,
			providerMS:        portsDependency.ProviderMS,
			templateMS:        portsDependency.TemplateMS,
			credMS:            portsDependency.CredentialMS,
			timeSrc:           portsDependency.TimeSrc,
		},
	}
}

// Handle ...
func (h RunCreationHandler) Handle(request service.DeploymentCreateRunRequest, sink ports.OutgoingEventSink) {
	startRunRequest, err := h.handle(request)
	if err != nil {
		sink.DeploymentRunCreateFailed(service.DeploymentCreateRunResult(h.createRunFailedResult(request, err)))
		return
	}
	sink.EventStartRunRequested(startRunRequest)
}

func (h RunCreationHandler) handle(request service.DeploymentCreateRunRequest) (deploymentevents.StartRunRequest, error) {
	data, err := h.dataGathering(request)
	if err != nil {
		return deploymentevents.StartRunRequest{}, err
	}

	var fac runFactory
	run, err := fac.CreateRun(common.NewID(types.RunIDPrefix), request, data, h.timeSrc.Now())
	if err != nil {
		return deploymentevents.StartRunRequest{}, err
	}
	// update deployment status, this needs to succeed before any other side effect happens to ensure integrity of deployment status
	err = h.updateDeploymentStatus(data.Deployment.ID)
	if err != nil {
		return deploymentevents.StartRunRequest{}, err
	}
	_, err = h.runStorage.Create(run)
	if err != nil {
		return deploymentevents.StartRunRequest{}, err
	}

	return h.runRequest(request, data.Template.GetMetadata().TemplateType, data.Deployment, run), nil
}

// gather data that is required for run creation from storage and external service
func (h RunCreationHandler) dataGathering(request service.DeploymentCreateRunRequest) (runCreationPrerequisiteData, error) {
	actor := service.ActorFromSession(request.Session)
	deployment, err := h.deploymentStorage.Get(request.CreateParam.Deployment)
	if err != nil {
		return runCreationPrerequisiteData{}, err
	}
	var lastDeploymentRun *types.DeploymentRun
	var templateVersionIDForLastRun common.ID
	if deployment.HasLastRun() {
		run, err := h.runStorage.Get(*deployment.LastRun)
		if err != nil {
			return runCreationPrerequisiteData{}, err
		}
		lastDeploymentRun = &run
		templateVersionIDForLastRun = lastDeploymentRun.TemplateSnapshot.TemplateVersionID
	}
	provider, err := h.providerMS.Get(actor, deployment.PrimaryCloudProvider)
	if err != nil {
		return runCreationPrerequisiteData{}, err
	}
	template, err := h.templateMS.Get(actor, deployment.Template)
	if err != nil {
		return runCreationPrerequisiteData{}, err
	}
	templateType, err := h.templateMS.GetTemplateType(actor, template.GetMetadata().TemplateType)
	if err != nil {
		return runCreationPrerequisiteData{}, err
	}
	var templateVersionSpecifiedByRequest *service.TemplateVersion
	if request.CreateParam.TemplateVersion != "" {
		// if user did not specify a template version to use, fetch the latest version
		templateVersionSpecifiedByRequest, err = h.templateMS.GetTemplateVersion(actor, request.CreateParam.TemplateVersion)
		if err != nil {
			return runCreationPrerequisiteData{}, err
		}
	}
	latestTemplateVersion, err := h.templateMS.GetTemplateVersion(actor, template.GetLatestVersionID())
	if err != nil {
		return runCreationPrerequisiteData{}, err
	}
	if latestTemplateVersion == nil {
		return runCreationPrerequisiteData{}, fmt.Errorf("last templateVersion of template is fetched as nil")
	}
	var templateVersionUsedByLastRun *service.TemplateVersion
	if templateVersionIDForLastRun != "" {
		// if there is a previous run for the template version, we need to fetch the version used by the previous run.
		templateVersionUsedByLastRun, err = h.templateMS.GetTemplateVersion(actor, templateVersionIDForLastRun)
		if err != nil {
			return runCreationPrerequisiteData{}, err
		}
	}
	return runCreationPrerequisiteData{
		Deployment:                   deployment,
		PreviousRun:                  lastDeploymentRun,
		Provider:                     provider,
		Template:                     template,
		TemplateType:                 templateType,
		LatestTemplateVersion:        *latestTemplateVersion,
		SpecifiedTemplateVersion:     templateVersionSpecifiedByRequest,
		TemplateVersionUsedByLastRun: templateVersionUsedByLastRun,
	}, nil
}

func (h RunCreationHandler) updateDeploymentStatus(deploymentID common.ID) error {
	update, filter := types.NewDeploymentStatusUpdater(h.timeSrc.Now()).PendingStatusCreating()
	updated, err := h.deploymentStorage.Update(deploymentID, update, filter)
	if err != nil {
		return err
	}
	if !updated {
		// either deployment has conflict status, or deployment no longer exists in storage
		return service.NewCacaoGeneralErrorWithOptions("conflict deployment status, current deployment status does not allow run creation")
	}
	return nil
}

func (h RunCreationHandler) createRunFailedResult(request service.DeploymentCreateRunRequest, err error) service.DeploymentCreateRunResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentCreateRunResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
	}
}

func (h RunCreationHandler) runRequest(request service.DeploymentCreateRunRequest, templateType service.TemplateTypeName, deployment types.Deployment, run types.DeploymentRun) deploymentevents.StartRunRequest {
	var cloudCreds []deploymentevents.ProviderCredentialPair
	for _, pair := range deployment.CloudCredentials {
		cloudCreds = append(cloudCreds, deploymentevents.ProviderCredentialPair{
			Provider:     pair.Provider,
			CredentialID: pair.Credential.String(),
		})
	}
	return deploymentevents.StartRunRequest{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		Deployment: deploymentevents.Deployment{
			ID: deployment.ID,
			CreatedBy: deploymentcommon.Creator{
				User:     request.GetSessionActor(),
				Emulator: request.GetSessionEmulator(),
			},
			TemplateType:     templateType,
			Workspace:        deployment.Workspace,
			PrimaryProvider:  deployment.PrimaryCloudProvider,
			CurrentStatus:    deployment.CurrentStatus,
			PendingStatus:    deployment.PendingStatus,
			CloudCredentials: cloudCreds,
			GitCredentialID:  deployment.GitCredential.String(),
		},
		Run: deploymentevents.Run{
			ID: run.ID,
			CreatedBy: deploymentcommon.Creator{
				User:     request.GetSessionActor(),
				Emulator: request.GetSessionEmulator(),
			},
			CreatedAt:         run.CreatedAt,
			TemplateID:        run.TemplateSnapshot.TemplateID,
			TemplateVersionID: run.TemplateSnapshot.TemplateVersionID,
			RequestParameters: request.CreateParam.ParamValues,
			Status:            run.Status,
		},
	}
}

// runFactory is a factory for creating a new run entity object.
// Note, this should NOT make any external call to storage or other ms.
type runFactory struct {
}

func (s *runFactory) CreateRun(
	runID common.ID,
	request service.DeploymentCreateRunRequest,
	data runCreationPrerequisiteData,
	now time.Time,
) (types.DeploymentRun, error) {
	logger := log.WithFields(log.Fields{
		"package":  "eventhandler",
		"function": "runFactory.CreateRun",
	})
	err := s.checkDeployment(request, data.Deployment)
	if err != nil {
		logger.WithError(err).Error("error while checking deployment")
		return types.DeploymentRun{}, err
	}

	// select a template version in the following order/priority:
	// version specified in request > version used by last run > latest version of the template
	var snapshot deploymentcommon.TemplateSnapshot
	if data.SpecifiedTemplateVersion != nil {
		snapshot, err = deploymentcommon.GetTemplateSnapshot(*data.SpecifiedTemplateVersion)
	} else if data.TemplateVersionUsedByLastRun != nil {
		snapshot, err = deploymentcommon.GetTemplateSnapshot(*data.TemplateVersionUsedByLastRun)
	} else {
		snapshot, err = deploymentcommon.GetTemplateSnapshot(data.LatestTemplateVersion)
	}
	if err != nil {
		return types.DeploymentRun{}, err
	}

	err = s.checkTemplate(data.Deployment, data.Template, data.LatestTemplateVersion)
	if err != nil {
		logger.WithError(err).Error("error while checking template")
		return types.DeploymentRun{}, err
	}
	err = s.checkProvider(data.Deployment, data.Provider)
	if err != nil {
		logger.WithError(err).Error("error while checking provider")
		return types.DeploymentRun{}, err
	}

	// TODO consider doing some parameter validation here, the validation should be independent of template engine or cloud provider

	run, err := s.newRun(runID, request, snapshot, now)
	if err != nil {
		logger.WithError(err).Error("fail to create deployment run in storage")
		return types.DeploymentRun{}, err
	}
	return run, nil
}

func (s runFactory) checkDeployment(request service.DeploymentCreateRunRequest, deployment types.Deployment) error {
	// check for permission
	if deployment.CreatedBy.User != request.GetSessionActor() {
		return service.NewCacaoUnauthorizedError("unauthorized access to deployment")
	}
	if !deployment.CurrentStatus.AllowRunCreation() {
		return service.NewCacaoGeneralError("current status does not allow run creation")
	}
	if deployment.PendingStatus == service.DeploymentStatusDeleting {
		// cannot create a run when the deployment is deleting, they are mutual exclusive
		return service.NewCacaoGeneralError("deployment is deleting")
	}
	if deployment.PendingStatus == service.DeploymentStatusCreating {
		// for terraform template, only 1 run is allowed at a time, no concurrent runs.
		return service.NewCacaoGeneralError("there is another on-going run creation")
	}
	if !deployment.Workspace.Validate() {
		return service.NewCacaoGeneralError("bad workspace ID")
	}
	return nil
}

func (s *runFactory) checkTemplate(deployment types.Deployment, template service.Template, templateVersion service.TemplateVersion) error {
	if deployment.Template != template.GetID() {
		return service.NewCacaoGeneralError(fmt.Sprintf("inconsistent template ID, %s, %s", deployment.Template, template.GetID()))
	}
	if template.GetSource().URI == "" {
		return service.NewCacaoGeneralError("empty template source URI")
	}
	if templateVersion.TemplateID != template.GetID() {
		return service.NewCacaoInvalidParameterError("inconsistent template ID in template version")
	}
	if template.IsDeleted() {
		return service.NewCacaoInvalidParameterError("template is deleted")
	}
	if templateVersion.Disabled {
		return service.NewCacaoInvalidParameterError("the template version used by the deployment is disabled")
	}
	var accessParam struct {
		Branch string `mapstructure:"branch"`
		Tag    string `mapstructure:"tag"`
		Path   string `mapstructure:"path"`
	}
	err := mapstructure.Decode(templateVersion.Source.AccessParameters, &accessParam)
	if err != nil {
		return err
	}
	if accessParam.Branch == "" && accessParam.Tag == "" {
		return service.NewCacaoGeneralError("template version is missing branch and tag in access parameter")
	}
	return nil
}

func (s *runFactory) checkProvider(deployment types.Deployment, provider *service.ProviderModel) error {
	if deployment.PrimaryCloudProvider != provider.ID {
		return service.NewCacaoGeneralError(fmt.Sprintf("inconsistent provider ID, %s, %s", deployment.PrimaryCloudProvider, provider.ID))
	}
	return nil
}

func (s *runFactory) newRun(runID common.ID, request service.DeploymentCreateRunRequest, snapshot deploymentcommon.TemplateSnapshot, now time.Time) (types.DeploymentRun, error) {
	run := types.DeploymentRun{
		ID:         runID,
		Deployment: request.CreateParam.Deployment,
		CreatedBy: deploymentcommon.Creator{
			User:     request.GetSessionActor(),
			Emulator: request.GetSessionEmulator(),
		},
		CreatedAt:         now,
		EndsAt:            time.Time{},
		TemplateSnapshot:  snapshot,
		RequestParameters: request.CreateParam.ParamValues,
		Status:            deploymentcommon.DeploymentRunPendingCreation,
		LastState:         service.DeploymentStateView{},
		StateUpdatedAt:    time.Time{},
	}
	return run, nil
}
