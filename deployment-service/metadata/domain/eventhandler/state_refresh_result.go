package eventhandler

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// StateRefreshSucceededHandler handles when execution stage of the run succeeded. Thus, the run has finished successfully
type StateRefreshSucceededHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	timeSrc           ports.TimeSrc
}

// NewStateRefreshSucceededHandler ...
func NewStateRefreshSucceededHandler(portsDependency ports.Ports) StateRefreshSucceededHandler {
	return StateRefreshSucceededHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		timeSrc:           portsDependency.TimeSrc,
	}
}

// Handle handles the event, this will update run state view.
func (h StateRefreshSucceededHandler) Handle(event deploymentevents.DeploymentStateRefreshSucceeded, sink ports.OutgoingEventSink) {
	logger := log.WithFields(log.Fields{
		"package":      "eventhandler",
		"function":     "StateRefreshSucceededHandler.handle",
		"deployment":   event.Deployment,
		"templateType": event.TemplateType,
		"run":          event.Run,
	})
	err := h.updateRunState(event)
	if err != nil {
		// do NOT return, just log the error, and continue
		logger.WithError(err).Errorf("fail to update run, this does not prevent emitting %s event", deploymentevents.EventDeploymentRunStatusUpdated)
	}
	sink.DeploymentStateRefreshed(service.DeploymentStateRefreshResult{
		Session: service.CopySessionActors(event.Session),
		ID:      event.Deployment,
		State:   service.DeploymentStateView(event.StateView),
	})
}

// update run StateView
func (h StateRefreshSucceededHandler) updateRunState(event deploymentevents.DeploymentStateRefreshSucceeded) error {
	now := h.timeSrc.Now()
	runUpdate := types.DeploymentRunUpdate{
		LastState:      &event.StateView,
		StateUpdatedAt: &now,
	}
	updated, err := h.runStorage.Update(event.Run, runUpdate, types.DeploymentRunFilter{})
	if err != nil {
		return err
	}
	if !updated {
		return fmt.Errorf("fail to find run %s in storage, fail to update status and state view", event.Run)
	}
	return nil
}
