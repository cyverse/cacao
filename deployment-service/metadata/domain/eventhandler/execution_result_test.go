package eventhandler

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"testing"
	"time"
)

func TestExecutionSucceededHandler_Handle(t *testing.T) {
	now := time.Now()
	type fields struct {
		runStorage        *portsmocks.DeploymentRunStorage
		deploymentStorage *portsmocks.DeploymentStorage
		timeSrc           ports.TimeSrc
	}
	type args struct {
		succeeded deploymentevents.RunExecutionSucceeded
		sink      *portsmocks.OutgoingEventSink
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "success",
			fields: fields{
				runStorage: func() *portsmocks.DeploymentRunStorage {
					runStorage := &portsmocks.DeploymentRunStorage{}
					runStorage.On("Get", common.ID("run-d8caa0598850n9abikmg")).Return(types.DeploymentRun{
						ID:                "run-d8caa0598850n9abikmg",
						Deployment:        "deployment-d600t0598850n9abikm0",
						CreatedBy:         deploymentcommon.Creator{},
						CreatedAt:         time.Time{},
						EndsAt:            time.Time{},
						TemplateSnapshot:  deploymentcommon.TemplateSnapshot{},
						RequestParameters: nil,
						Parameters:        nil,
						Status:            deploymentcommon.DeploymentRunRunning,
						StatusMsg:         "",
						LastState: service.DeploymentStateView{
							Resources: []service.DeploymentResource{
								{
									ID: "resource-1",
								},
							},
						},
						StateUpdatedAt: time.Time{},
					}, nil)
					runStorage.On("Update",
						common.ID("run-d8caa0598850n9abikmg"),
						types.DeploymentRunUpdate{
							EndsAt: func(t time.Time) *time.Time { return &t }(now),
							Status: func(s deploymentcommon.DeploymentRunStatus) *deploymentcommon.DeploymentRunStatus { return &s }(deploymentcommon.DeploymentRunActive),
							LastState: func(s service.DeploymentStateView) *service.DeploymentStateView { return &s }(service.DeploymentStateView{
								Resources: []service.DeploymentResource{
									{
										ID: "resource-1",
									},
								},
							}),
							StateUpdatedAt: func(t time.Time) *time.Time { return &t }(now),
						}, types.DeploymentRunFilter{}).Return(true, nil)
					return runStorage
				}(),
				deploymentStorage: func() *portsmocks.DeploymentStorage {
					storage := &portsmocks.DeploymentStorage{}
					storage.On("Update",
						common.ID("deployment-d600t0598850n9abikm0"),
						types.DeploymentUpdate{
							UpdatedAt:     func(t time.Time) *time.Time { return &t }(now.Add(portsmocks.TimeSrcIncrementStep)),
							CurrentStatus: func(s service.DeploymentStatus) *service.DeploymentStatus { return &s }(service.DeploymentStatusActive),
							PendingStatus: func(s service.DeploymentPendingStatus) *service.DeploymentPendingStatus { return &s }(service.DeploymentStatusNoPending),
							StatusMsg:     nil,
						}, types.DeploymentFilter{}).Return(true, nil)
					return storage
				}(),
				timeSrc: &portsmocks.IncrementingTimeSrc{Start: now},
			},
			args: args{
				succeeded: deploymentevents.RunExecutionSucceeded{
					Session:      service.Session{},
					TemplateType: "terraform_openstack",
					Deployment:   "deployment-d600t0598850n9abikm0",
					Run:          "run-d8caa0598850n9abikmg",
					StateView: service.DeploymentStateView{
						Resources: []service.DeploymentResource{
							{
								ID: "resource-1",
							},
						},
					},
				},
				sink: func() *portsmocks.OutgoingEventSink {
					sink := &portsmocks.OutgoingEventSink{}
					sink.On("EventDeploymentRunStatusUpdated", deploymentevents.DeploymentRunStatusUpdated{
						Session:       service.Session{},
						Deployment:    "deployment-d600t0598850n9abikm0",
						Run:           "run-d8caa0598850n9abikmg",
						Status:        deploymentcommon.DeploymentRunActive,
						OldStatus:     deploymentcommon.DeploymentRunRunning,
						TransactionID: "",
					})
					return sink
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h := ExecutionSucceededHandler{
				runStorage:        tt.fields.runStorage,
				deploymentStorage: tt.fields.deploymentStorage,
				timeSrc:           tt.fields.timeSrc,
			}
			h.Handle(tt.args.succeeded, tt.args.sink)
			tt.fields.runStorage.AssertExpectations(t)
			tt.fields.deploymentStorage.AssertExpectations(t)
			tt.args.sink.AssertExpectations(t)
		})
	}
}
