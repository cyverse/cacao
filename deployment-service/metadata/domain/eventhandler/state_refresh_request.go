package eventhandler

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"time"
)

// StateRefreshHandler handles refresh the state of an existing deployment.
type StateRefreshHandler struct {
	runStorage        ports.DeploymentRunStorage
	deploymentStorage ports.DeploymentStorage
	templateMS        ports.TemplateMicroservice
	credMS            ports.CredentialMicroservice
	timeSrc           ports.TimeSrc
}

// NewStateRefreshHandler ...
func NewStateRefreshHandler(portsDependency ports.Ports) StateRefreshHandler {
	return StateRefreshHandler{
		runStorage:        portsDependency.RunStorage,
		deploymentStorage: portsDependency.DeploymentStorage,
		templateMS:        portsDependency.TemplateMS,
		credMS:            portsDependency.CredentialMS,
		timeSrc:           portsDependency.TimeSrc,
	}
}

const minStateRefreshInterval = time.Minute

// Handle ...
func (h StateRefreshHandler) Handle(request service.DeploymentStateRefresh, sink ports.OutgoingEventSink) {
	actor := service.ActorFromSession(request.Session)
	deployment, err := h.deploymentStorage.Get(request.ID)
	if err != nil {
		sink.DeploymentStateRefreshFailed(h.failedResult(request, err))
		return
	}

	if !deployment.HasLastRun() {
		// no last run, then no need to refresh
		sink.DeploymentStateRefreshed(service.DeploymentStateRefreshResult{
			Session: service.CopySessionActors(request.Session),
			ID:      request.ID,
			State:   service.DeploymentStateView{}, // if no last run, then the state must be empty
		})
		return
	}

	if deployment.PendingStatus != service.DeploymentStatusNoPending {
		err = service.NewCacaoInvalidParameterError("cannot start state refresh for deployment with pending status")
		sink.DeploymentStateRefreshFailed(h.failedResult(request, err))
		return
	}

	lastDeploymentRun, err := h.runStorage.Get(*deployment.LastRun)
	if err != nil {
		sink.DeploymentStateRefreshFailed(h.failedResult(request, err))
		return
	}

	if h.timeSrc.Now().Sub(lastDeploymentRun.StateUpdatedAt) < minStateRefreshInterval {
		// prevent the state refresh from happening too frequently
		sink.DeploymentStateRefreshed(service.DeploymentStateRefreshResult{
			Session: service.CopySessionActors(request.Session),
			ID:      request.ID,
			State:   service.DeploymentStateView(lastDeploymentRun.LastState),
		})
		return
	}

	template, err := h.templateMS.Get(actor, deployment.Template)
	if err != nil {
		sink.DeploymentStateRefreshFailed(h.failedResult(request, err))
		return
	}
	templateType, err := h.templateMS.GetTemplateType(actor, template.GetMetadata().TemplateType)
	if err != nil {
		sink.DeploymentStateRefreshFailed(h.failedResult(request, err))
		return
	}

	sink.EventStartStateRefreshRequested(deploymentevents.StartStateRefreshRequest{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
		},
		TemplateType: templateType.GetName(),
		Deployment:   deployment.ID,
		Run:          lastDeploymentRun.ID,
	})
}

func (h StateRefreshHandler) failedResult(request service.DeploymentStateRefresh, err error) service.DeploymentStateRefreshResult {
	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	return service.DeploymentStateRefreshResult{
		Session: service.Session{
			SessionActor:    request.GetSessionActor(),
			SessionEmulator: request.GetSessionEmulator(),
			ServiceError:    serviceError.GetBase(),
		},
		ID:    request.ID,
		State: service.DeploymentStateView{},
	}
}
