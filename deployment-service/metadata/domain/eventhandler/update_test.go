package eventhandler

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	portsmocks "gitlab.com/cyverse/cacao/deployment-service/metadata/ports/mocks"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
	"testing"
)

func TestUpdateHandler_Handle(t *testing.T) {
	t.Run("update name", testUpdateDeploymentName)
	t.Run("update name, invalid name", testUpdateDeploymentNameInvalid)
	t.Run("update description", testUpdateDeploymentDescription)
	t.Run("update template", testUpdateDeploymentTemplate)
	t.Run("update primary provider", testUpdateDeploymentPrimaryProvider)
	t.Run("update cloud cred", testUpdateDeploymentCloudCred)
	t.Run("update git cred", testUpdateDeploymentGitCred)
	t.Run("update id (not supported)", testUpdateDeploymentIDNotSupported)
	t.Run("no field to update", testUpdateDeploymentNoField)
	t.Run("not authorized", testUpdateDeploymentNotAuthorized)
	t.Run("not exist", testUpdateNonExistDeployment)
}

func testUpdateDeploymentName(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:   deploymentID,
		Name: "old-name",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newName := "new-name"
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{Name: &newName}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "new-name",
		},
		UpdateFields: []string{"name"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentNameInvalid(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:   deploymentID,
		Name: "old-name",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newName := "_new-invalid-name"
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	//storageMock.On("Update", deploymentID, types.DeploymentUpdate{Name: &newName}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: newName,
		},
		UpdateFields: []string{"name"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdateFailed", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.NewCacaoInvalidParameterError("name of deployment must start with lower case letter").GetBase(),
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentDescription(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:          deploymentID,
		Description: "oldDescription",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newDescription := "newDescription"
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{Description: &newDescription}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:          deploymentID,
			Description: "newDescription",
		},
		UpdateFields: []string{"description"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentTemplate(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		Template: common.NewID("template"),
	}
	newTemplateID := common.NewID("template")
	newTemplate := newTemplateID
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{Template: &newTemplate}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:       deploymentID,
			Template: newTemplateID,
		},
		UpdateFields: []string{"template"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentPrimaryProvider(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		PrimaryCloudProvider: common.NewID("provider"),
	}
	newProviderID := common.NewID("provider")
	newPrimaryCloudProvider := newProviderID
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{PrimaryCloudProvider: &newPrimaryCloudProvider}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:           deploymentID,
			PrimaryCloud: newProviderID,
		},
		UpdateFields: []string{"primary_provider"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentCloudCred(t *testing.T) {
	deploymentID := common.NewID("deployment")
	primaryProviderID := common.NewID("provider")
	deployment := types.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
		PrimaryCloudProvider: primaryProviderID,
		CloudCredentials: deploymentcommon.ProviderCredentialMappings{
			{
				Credential: "old-cred",
				Provider:   primaryProviderID,
			},
		},
	}
	newCloudCred := "new-cloud-cred"
	updatedCloudCredentials := deploymentcommon.ProviderCredentialMappings{
		{
			Credential: deploymentcommon.CredentialID(newCloudCred),
			Provider:   primaryProviderID,
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{CloudCredentials: updatedCloudCredentials}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID: deploymentID,
			CloudCredentials: map[string]common.ID{
				newCloudCred: "", // provider ID may be missing from request
			},
		},
		UpdateFields: []string{"cloud_credentials"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentGitCred(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID: deploymentID,
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	newGitCred := "newGitCred"
	updatedGitCredential := deploymentcommon.CredentialID(newGitCred)
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	storageMock.On("Update", deploymentID, types.DeploymentUpdate{GitCredential: &updatedGitCredential}, types.DeploymentFilter{}).Return(true, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:            deploymentID,
			GitCredential: newGitCred,
		},
		UpdateFields: []string{"git_credential"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentIDNotSupported(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:          deploymentID,
		Description: "oldDescription",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID: deploymentID,
		},
		UpdateFields: []string{"id"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentNoField(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:   deploymentID,
		Name: "old-name",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "new-name",
		},
		UpdateFields: []string{}, // no field to update
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdated", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateDeploymentNotAuthorized(t *testing.T) {
	deploymentID := common.NewID("deployment")
	deployment := types.Deployment{
		ID:   deploymentID,
		Name: "old-name",
		CreatedBy: deploymentcommon.Creator{
			User:     "actor123",
			Emulator: "",
		},
	}
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(deployment, nil)
	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "not-authorized-user123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "new-name",
		},
		UpdateFields: []string{"name"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdateFailed", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.NewCacaoUnauthorizedError("not authorized").GetBase(),
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}

func testUpdateNonExistDeployment(t *testing.T) {
	deploymentID := common.NewID("deployment")
	storageMock := &portsmocks.DeploymentStorage{}
	storageMock.On("Get", deploymentID).Return(types.Deployment{}, service.NewCacaoNotFoundError("not found"))

	handler := NewUpdateHandler(storageMock)
	request := service.DeploymentUpdateRequest{
		Session: service.Session{
			SessionActor:    "actor123",
			SessionEmulator: "",
		},
		Deployment: service.Deployment{
			ID:   deploymentID,
			Name: "new-name",
		},
		UpdateFields: []string{"name"},
	}
	out := &portsmocks.OutgoingEventSink{}
	out.On("DeploymentUpdateFailed", service.DeploymentUpdateResult{
		Session: service.Session{
			SessionActor:    request.SessionActor,
			SessionEmulator: request.SessionEmulator,
			ServiceError:    service.NewCacaoNotFoundError("not found").GetBase(),
		},
		ID: deploymentID,
	}).Once()
	handler.Handle(request, out)
	out.AssertExpectations(t)
	storageMock.AssertExpectations(t)
}
