package eventhandler

import (
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentevents"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
)

// EventHandlers implements ports.EventHandlers, this aggregates all event
// handlers
type EventHandlers struct {
	creationHandler                 CreationHandler
	runCreationHandler              RunCreationHandler
	updateHandler                   UpdateHandler
	deletionHandler                 DeletionHandler
	runStartedHandler               RunStartedHandler
	runStartFailedHandler           RunStartFailedHandler
	preflightFailedHandler          PreflightFailedHandler
	executionStartedHandler         ExecutionStartedHandler
	executionSucceededHandler       ExecutionSucceededHandler
	executionFailedHandler          ExecutionFailedHandler
	deletionCleanupSucceededHandler DeletionCleanupSucceededHandler
	deletionCleanupFailedHandler    DeletionCleanupFailedHandler
	stateRefreshHandler             StateRefreshHandler
	stateRefreshSucceededHandler    StateRefreshSucceededHandler
}

// NewEventHandlers ...
func NewEventHandlers(portsDependency ports.Ports) EventHandlers {
	return EventHandlers{
		creationHandler:                 NewCreationHandler(portsDependency),
		runCreationHandler:              NewRunCreationHandler(portsDependency),
		updateHandler:                   NewUpdateHandler(portsDependency.DeploymentStorage),
		deletionHandler:                 NewDeletionHandler(portsDependency),
		runStartedHandler:               NewRunStartedHandler(portsDependency),
		runStartFailedHandler:           NewRunStartFailedHandler(portsDependency),
		preflightFailedHandler:          NewPreflightFailedHandler(portsDependency),
		executionStartedHandler:         NewExecutionStartedHandler(portsDependency),
		executionSucceededHandler:       NewExecutionSucceededHandler(portsDependency),
		executionFailedHandler:          NewExecutionFailedHandler(portsDependency),
		deletionCleanupSucceededHandler: NewDeletionCleanupSucceededHandler(portsDependency),
		deletionCleanupFailedHandler:    NewDeletionCleanupFailedHandler(portsDependency),
		stateRefreshHandler:             NewStateRefreshHandler(portsDependency),
		stateRefreshSucceededHandler:    NewStateRefreshSucceededHandler(portsDependency),
	}
}

var _ ports.EventHandlers = EventHandlers{}

// DeploymentCreationRequested ...
func (h EventHandlers) DeploymentCreationRequested(request service.DeploymentCreationRequest, sink ports.OutgoingEventSink) {
	h.creationHandler.Handle(request, sink)
}

// DeploymentCreateRunRequested ...
func (h EventHandlers) DeploymentCreateRunRequested(request service.DeploymentCreateRunRequest, sink ports.OutgoingEventSink) {
	h.runCreationHandler.Handle(request, sink)
}

// DeploymentDeletionRequested ...
func (h EventHandlers) DeploymentDeletionRequested(request service.DeploymentDeletionRequest, sink ports.OutgoingEventSink) {
	h.deletionHandler.Handle(request, sink)
}

// EventRunPreflightStarted ...
func (h EventHandlers) EventRunPreflightStarted(started deploymentevents.RunPreflightStarted, sink ports.OutgoingEventSink) {
	h.runStartedHandler.Handle(started, sink)
}

// EventRunPreflightStartFailed ...
func (h EventHandlers) EventRunPreflightStartFailed(failed deploymentevents.RunPreflightStartFailed, sink ports.OutgoingEventSink) {
	h.runStartFailedHandler.Handle(failed, sink)
}

// EventRunPreflightFailed ...
func (h EventHandlers) EventRunPreflightFailed(result deploymentevents.RunPreflightResult, sink ports.OutgoingEventSink) {
	h.preflightFailedHandler.Handle(result, sink)
}

// EventRunExecutionStarted ...
func (h EventHandlers) EventRunExecutionStarted(result deploymentevents.RunPreflightResult, sink ports.OutgoingEventSink) {
	h.executionStartedHandler.Handle(result, sink)
}

// EventRunExecutionSucceeded ...
func (h EventHandlers) EventRunExecutionSucceeded(succeeded deploymentevents.RunExecutionSucceeded, sink ports.OutgoingEventSink) {
	h.executionSucceededHandler.Handle(succeeded, sink)
}

// EventRunExecutionFailed ...
func (h EventHandlers) EventRunExecutionFailed(failed deploymentevents.RunExecutionFailed, sink ports.OutgoingEventSink) {
	h.executionFailedHandler.Handle(failed, sink)
}

// DeploymentUpdateRequested ...
func (h EventHandlers) DeploymentUpdateRequested(request service.DeploymentUpdateRequest, sink ports.OutgoingEventSink) {
	h.updateHandler.Handle(request, sink)
}

// EventDeploymentDeletionCleanupSucceeded ...
func (h EventHandlers) EventDeploymentDeletionCleanupSucceeded(result deploymentevents.DeploymentDeletionCleanupResult, sink ports.OutgoingEventSink) {
	h.deletionCleanupSucceededHandler.Handle(result, sink)
}

// EventDeploymentDeletionCleanupFailed ...
func (h EventHandlers) EventDeploymentDeletionCleanupFailed(result deploymentevents.DeploymentDeletionCleanupResult, sink ports.OutgoingEventSink) {
	h.deletionCleanupFailedHandler.Handle(result, sink)
}

// DeploymentStateRefreshRequested ..
func (h EventHandlers) DeploymentStateRefreshRequested(request service.DeploymentStateRefresh, sink ports.OutgoingEventSink) {
	h.stateRefreshHandler.Handle(request, sink)
}

// EventDeploymentStateRefreshSucceeded ...
func (h EventHandlers) EventDeploymentStateRefreshSucceeded(result deploymentevents.DeploymentStateRefreshSucceeded, sink ports.OutgoingEventSink) {
	h.stateRefreshSucceededHandler.Handle(result, sink)
}
