package queryhandler

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/ports"
	"gitlab.com/cyverse/cacao/deployment-service/metadata/types"
)

// GetQueryHandler is handler for get query
type GetQueryHandler struct {
	storage    ports.DeploymentStorage
	runStorage ports.DeploymentRunStorage
	HandlerCommon
}

// NewGetQueryHandler ...
func NewGetQueryHandler(
	storage ports.DeploymentStorage,
	runStorage ports.DeploymentRunStorage,
	commonDependencies HandlerCommon,
) GetQueryHandler {
	return GetQueryHandler{
		storage:       storage,
		runStorage:    runStorage,
		HandlerCommon: commonDependencies,
	}
}

// QueryOp ...
func (h GetQueryHandler) QueryOp() common.QueryOp {
	return service.DeploymentGetQueryType
}

// Handle handles the query, and returns a single reply
func (h GetQueryHandler) Handle(query types.Query) Reply {
	logger := log.WithFields(log.Fields{
		"query": "get",
	})
	var getQuery service.DeploymentGetQuery
	err := json.Unmarshal(query.CloudEvent().Data(), &getQuery)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes to DeploymentGetQuery"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply("", service.NewCacaoMarshalError(fmt.Sprintf("%s - %s", errorMessage, err.Error())))
	}
	logger = logger.WithFields(log.Fields{
		"deployment": getQuery.ID,
		"actor":      getQuery.SessionActor,
		"emulator":   getQuery.SessionEmulator,
	})

	deployment, err := h.storage.Get(getQuery.ID)
	if err != nil {
		logger.Error(err)
		return h.createErrorReply(getQuery.ID, err)
	}

	ok, err := h.Perm.DeploymentAccess(getQuery.GetSessionActor(), deployment)
	if err != nil {
		errorMessage := "permission check failed"
		logger.WithError(err).Error(errorMessage)
		return h.createErrorReply(getQuery.ID, service.NewCacaoGeneralError(errorMessage))
	} else if !ok {
		errorMessage := "not authorized"
		logger.Error(errorMessage)
		return h.createErrorReply(getQuery.ID, service.NewCacaoUnauthorizedError(errorMessage))
	}

	var lastRun types.DeploymentRun
	if deployment.HasLastRun() {
		// fetch last run
		lastRun, err = h.runStorage.Get(*deployment.LastRun)
		if err != nil {
			return h.createErrorReply(getQuery.ID, err)
		}
	}
	return h.createReply(deployment, lastRun)
}

// HandleStream handles the query
func (h GetQueryHandler) HandleStream(query types.Query) (replies []Reply) {
	return []Reply{h.Handle(query)}
}

func (h GetQueryHandler) createErrorReply(deployment common.ID, err error) service.DeploymentGetReply {
	var reply service.DeploymentGetReply

	var serviceError service.CacaoError
	if cacaoError, ok := err.(service.CacaoError); ok {
		serviceError = cacaoError
	} else {
		serviceError = service.NewCacaoGeneralError(err.Error())
	}

	reply.ServiceError = serviceError.GetBase()
	reply.DeploymentID = deployment

	return reply
}

func (h GetQueryHandler) createReply(
	deployment types.Deployment,
	lastRun types.DeploymentRun,
) service.DeploymentGetReply {
	return service.DeploymentGetReply{
		Session:      service.Session{},
		Deployment:   deploymentConvertToExternal(deployment, lastRun),
		DeploymentID: deployment.ID,
	}
}

func deploymentConvertToExternal(deployment types.Deployment, lastRun types.DeploymentRun) service.Deployment {
	// FIXME fix ConvertToExternal for deployment object
	convertedDeployment := deployment.ConvertToExternal()
	convertedLastRun := lastRun.ConvertToExternal()
	convertedDeployment.LastRun = &convertedLastRun
	return convertedDeployment
}
