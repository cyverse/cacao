package types

import (
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/deployment-service/internal/deploymentcommon"
	"go.mongodb.org/mongo-driver/bson"
	"time"
)

// DeploymentRun is the storage format of deployment run in metadata service.
// This should only store info that is template-engine/cloud-provider agnostic. Info that is specific to template-engine/cloud-provider should be stored by specific deployment service.
// This stores enough info about the run to be able to make reproducible call to specific deployment ms without access to external ms.
type DeploymentRun struct {
	// ID of the deployment run
	ID               common.ID                         `bson:"_id"`
	Deployment       common.ID                         `bson:"deployment"`
	CreatedBy        deploymentcommon.Creator          `bson:",inline"`
	CreatedAt        time.Time                         `bson:"created_at"`
	EndsAt           time.Time                         `bson:"ended_at"`
	TemplateSnapshot deploymentcommon.TemplateSnapshot `bson:"template"`
	// parameters in the run creation request
	RequestParameters service.DeploymentParameterValues     `bson:"request_parameters"`
	Parameters        deploymentcommon.DeploymentParameters `bson:"parameters"`
	Status            deploymentcommon.DeploymentRunStatus  `bson:"status"`
	StatusMsg         string                                `bson:"status_msg"`
	// most recent state
	LastState      service.DeploymentStateView `bson:"last_state"`
	StateUpdatedAt time.Time                   `bson:"state_updated_at"`
}

// HasRawState ...
// Deprecated: raw state should not be stored in metadata svc
func (r DeploymentRun) HasRawState() bool {
	panic("FIXME")
	//return r.RawState != nil
}

// HasLogs ...
// Deprecated: logs should not be stored in metadata svc
func (r DeploymentRun) HasLogs() bool {
	panic("FIXME")
	//return r.Logs != nil
}

// ConvertToExternal converts to external representation
func (r DeploymentRun) ConvertToExternal() service.DeploymentRun {
	return service.DeploymentRun{
		ID:               r.ID,
		Deployment:       r.Deployment,
		Owner:            r.CreatedBy.User,
		Start:            r.CreatedAt,
		End:              r.EndsAt,
		GitURL:           r.TemplateSnapshot.GitURL,
		CommitHash:       r.TemplateSnapshot.CommitHash,
		Parameters:       r.Parameters.ConvertToExternal(),
		CloudCredentials: nil,
		GitCredential:    "",
		Status:           r.Status.String(),
		StatusMsg:        r.StatusMsg,
		LastState:        r.LastState,
		StateUpdatedAt:   r.StateUpdatedAt,
	}
}

// DeploymentRunUpdate is used for updating deployment run, it contains fields that can potentially be updated.
// All fields should be nullable(ptr or slice), so that fields that has nil value is not updated in storage.
type DeploymentRunUpdate struct {
	Deployment       *common.ID                             `bson:"deployment"`
	EndsAt           *time.Time                             `bson:"ended_at"`
	TemplateSnapshot *deploymentcommon.TemplateSnapshot     `bson:"template"`
	Parameters       *deploymentcommon.DeploymentParameters `bson:"parameters"`
	Status           *deploymentcommon.DeploymentRunStatus  `bson:"status"`
	StatusMsg        *string                                `bson:"status_msg"`
	LastState        *service.DeploymentStateView           `bson:"last_state"`
	StateUpdatedAt   *time.Time                             `bson:"state_updated_at"`
}

// ToBSONValues ...
func (dru DeploymentRunUpdate) ToBSONValues() bson.M {
	result := bson.M{}
	if dru.Deployment != nil {
		result["deployment"] = dru.Deployment
	}
	if dru.EndsAt != nil {
		result["ended_at"] = dru.EndsAt
	}
	if dru.TemplateSnapshot != nil {
		result["template"] = dru.TemplateSnapshot
	}
	if dru.Parameters != nil {
		result["parameters"] = dru.Parameters
	}
	if dru.Status != nil {
		result["status"] = dru.Status
	}
	if dru.StatusMsg != nil {
		result["status_msg"] = dru.StatusMsg
	}
	if dru.LastState != nil {
		result["last_state"] = dru.LastState
	}
	if dru.StateUpdatedAt != nil {
		result["state_updated_at"] = dru.StateUpdatedAt
	}
	return result
}

// DeploymentRunFilter is filter for searching deployment run
type DeploymentRunFilter struct {
	ID         common.ID
	Deployment common.ID
}

// ToMongoFilters converts a deployment run filter object to filter in mongo-format
func (drf DeploymentRunFilter) ToMongoFilters() bson.M {
	mongoFilter := bson.M{}
	if len(drf.ID.String()) > 0 {
		mongoFilter["_id"] = drf.ID.String()
	}
	if len(drf.Deployment.String()) > 0 {
		mongoFilter["deployment"] = drf.Deployment.String()
	}
	return mongoFilter
}
