package types

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/service"
	"reflect"
	"testing"
	"time"
)

// This test enforces that all fields in update struct has ptr type of the same field in Deployment struct unless it is slice.
// Field1 Type1 => Field1 *Type1.
func Test_DeploymentUpdateStruct(t *testing.T) {
	deploymentStruct := reflect.TypeOf(Deployment{})
	deploymentUpdateStruct := reflect.TypeOf(DeploymentUpdate{})
	assert.Equal(t, reflect.Struct, deploymentStruct.Kind())
	assert.Equal(t, reflect.Struct, deploymentUpdateStruct.Kind())
	// update struct should not contain more fields than the deployment struct
	assert.LessOrEqual(t, deploymentUpdateStruct.NumField(), deploymentStruct.NumField())
	deploymentStructFields := map[string]reflect.StructField{}
	for i := 0; i < deploymentStruct.NumField(); i++ {
		field := deploymentStruct.Field(i)
		deploymentStructFields[field.Name] = field
	}
	for i := 0; i < deploymentUpdateStruct.NumField(); i++ {
		updateField := deploymentUpdateStruct.Field(i)
		// check if field of update struct is null-able, either ptr type or slice
		if !assert.True(t, reflect.Ptr == updateField.Type.Kind() || reflect.Slice == updateField.Type.Kind()) {
			t.Errorf("field %s(%s) is not nullable", updateField.Name, updateField.Type.Kind().String())
		}
		field, ok := deploymentStructFields[updateField.Name]
		if assert.True(t, ok) {
			if field.Type.Kind() == reflect.Slice {
				assert.Equalf(t, reflect.Slice, updateField.Type.Kind(), updateField.Name)
			} else {
				// for non-slice field, the type in update struct should be the pointer type of the field in deployment struct
				assert.Equal(t, "*"+field.Type.String(), updateField.Type.String())
			}
		}
	}
}

func TestDeploymentStatusUpdater(t *testing.T) {
	t.Run("status active", func(t *testing.T) {
		now := time.Now()
		newStatus := service.DeploymentStatusActive
		newPending := service.DeploymentStatusNoPending

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.StatusActive()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			CurrentStatus: &newStatus,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{}, filter)
	})
	t.Run("status creation errored", func(t *testing.T) {
		now := time.Now()
		newStatus := service.DeploymentStatusCreationErrored
		newPending := service.DeploymentStatusNoPending

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.StatusCreationFailed()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			CurrentStatus: &newStatus,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{}, filter)
	})
	t.Run("status deletion errored", func(t *testing.T) {
		now := time.Now()
		newStatus := service.DeploymentStatusDeletionErrored
		newPending := service.DeploymentStatusNoPending

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.StatusDeletionErrored()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			CurrentStatus: &newStatus,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{}, filter)
	})
	t.Run("status deleted", func(t *testing.T) {
		now := time.Now()
		newStatus := service.DeploymentStatusDeleted
		newPending := service.DeploymentStatusNoPending

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.StatusDeleted()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			CurrentStatus: &newStatus,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{}, filter)
	})
	t.Run("pending creation", func(t *testing.T) {
		now := time.Now()
		newPending := service.DeploymentStatusCreating

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.PendingStatusCreating()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{
			CurrentStatus: []service.DeploymentStatus{service.DeploymentStatusNone, service.DeploymentStatusActive, service.DeploymentStatusCreationErrored},
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		}, filter)
	})
	t.Run("pending deletion", func(t *testing.T) {
		now := time.Now()
		newPending := service.DeploymentStatusDeleting

		var u = DeploymentStatusUpdater{updateTimestamp: now}
		update, filter := u.PendingStatusDeleting()

		assert.Equal(t, DeploymentUpdate{
			UpdatedAt:     &now,
			PendingStatus: &newPending,
		}, update)
		assert.Equal(t, DeploymentFilter{
			CurrentStatus: []service.DeploymentStatus{service.DeploymentStatusNone, service.DeploymentStatusActive, service.DeploymentStatusCreationErrored, service.DeploymentStatusDeletionErrored},
			PendingStatus: []service.DeploymentPendingStatus{service.DeploymentStatusNoPending},
		}, filter)
	})
}

func Test_validateDeploymentName(t *testing.T) {
	type args struct {
		deploymentName string
	}
	tests := []struct {
		testCase string
		args     args
		wantErr  bool
	}{
		{
			testCase: "foobar-123",
			args: args{
				deploymentName: "foobar-123",
			},
			wantErr: false,
		},
		{
			testCase: "empty",
			args: args{
				deploymentName: "",
			},
			wantErr: true,
		},
		{
			testCase: "too long",
			args: args{
				deploymentName: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
			},
			wantErr: true,
		},
		{
			testCase: "underscore",
			args: args{
				deploymentName: "foobar_123",
			},
			wantErr: true,
		},
		{
			testCase: "start with digit",
			args: args{
				deploymentName: "1foobar-123",
			},
			wantErr: true,
		},
		{
			testCase: "start with -",
			args: args{
				deploymentName: "-foobar-123",
			},
			wantErr: true,
		},

		{
			testCase: "upper case",
			args: args{
				deploymentName: "fooBAR-123",
			},
			wantErr: true,
		},
		{
			testCase: "other punct",
			args: args{
				deploymentName: "foobar*!+=123",
			},
			wantErr: true,
		},
		{
			testCase: "non ASCII",
			args: args{
				deploymentName: "foobar\U0001f642",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.testCase, func(t *testing.T) {
			fmtStr := fmt.Sprintf("validateDeploymentName(%v)", tt.args.deploymentName)
			err := validateDeploymentName(tt.args.deploymentName)
			if tt.wantErr {
				assert.Errorf(t, err, fmtStr)
			} else {
				assert.NoErrorf(t, err, fmtStr)
			}
		})
	}
}
