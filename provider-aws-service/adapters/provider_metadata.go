package adapters

import (
	"context"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
)

// ProviderMetadataClient ...
type ProviderMetadataClient struct {
	queryConn messaging2.QueryConnection
}

// NewProviderMetadataClient ...
func NewProviderMetadataClient(queryConn messaging2.QueryConnection) *ProviderMetadataClient {
	return &ProviderMetadataClient{
		queryConn: queryConn,
	}
}

// GetMetadata fetches metadata for a provider
func (svc ProviderMetadataClient) GetMetadata(ctx context.Context, actor, emulator string, providerID common.ID) (map[string]interface{}, error) {
	// we do not need any event operations from provider metadata service, so just query connection is enough
	metadataSvcClient, err := service.NewNatsProviderClientFromConn(svc.queryConn, nil)
	if err != nil {
		return nil, err
	}
	provider, err := metadataSvcClient.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, providerID)
	if err != nil {
		return nil, err
	}
	return provider.Metadata, nil
}

// ListAWSProviders lists all AWS provider that user can access.
func (svc ProviderMetadataClient) ListAWSProviders(ctx context.Context, actor, emulator string) ([]service.ProviderModel, error) {
	// we do not need any event operations from provider metadata service, so just query connection is enough
	metadataSvcClient, err := service.NewNatsProviderClientFromConn(svc.queryConn, nil)
	if err != nil {
		return nil, err
	}
	providerList, err := metadataSvcClient.List(ctx, service.Actor{Actor: actor, Emulator: emulator})
	if err != nil {
		return nil, err
	}
	var result []service.ProviderModel
	for i := range providerList {
		if providerList[i].Type == "aws" {
			result = append(result, providerList[i])
		}
	}
	return result, nil
}
