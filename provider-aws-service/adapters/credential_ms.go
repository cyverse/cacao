package adapters

import (
	"context"
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/common"
	"gitlab.com/cyverse/cacao/provider-aws-service/types"
)

// CredentialMicroservice ...
type CredentialMicroservice struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// NewCredentialMicroservice ...
func NewCredentialMicroservice(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) *CredentialMicroservice {
	return &CredentialMicroservice{queryConn: queryConn, eventConn: eventConn}
}

// ListCredentials ...
func (svc CredentialMicroservice) ListCredentials(ctx context.Context, actor, emulator string) ([]service.CredentialModel, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentials",
		"actor":    actor,
		"emulator": emulator,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credList, err := client.List(ctx, service.Actor{Actor: actor, Emulator: emulator}, service.CredentialListFilter{
		Username: actor,
		Type:     service.AWSCredentialType,
		Disabled: common.PtrOf(false),
	})
	if err != nil {
		logger.WithError(err).Error("fail to list credentials")
		return nil, err
	}
	for i := range credList {
		// list operation does not include value (redacted), thus re-fetch each cred individually for the value.
		refetchedCred, err := client.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, credList[i].ID)
		if err != nil {
			logger.WithError(err).Error("fail to re-fetch cred for cred value")
			continue
		}
		if refetchedCred == nil {
			logger.Error("Get return nil on success")
			continue
		}
		credList[i].Value = refetchedCred.Value
	}
	return credList, nil
}

// ListCredentialsByTag ...
func (svc CredentialMicroservice) ListCredentialsByTag(ctx context.Context, actor, emulator, tagName string) ([]types.Credential, error) {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.ListCredentialsByTag",
		"actor":    actor,
		"emulator": emulator,
		"tag":      tagName,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	tagsFilter := make(map[string]string)
	if tagName != "" {
		tagsFilter[tagName] = ""
	}
	credList, err := client.List(ctx, service.Actor{Actor: actor, Emulator: emulator}, service.CredentialListFilter{
		Username: actor,
		Type:     service.AWSCredentialType,
		Disabled: common.PtrOf(false),
		Tags:     tagsFilter,
	})
	if err != nil {
		logger.WithError(err).Error("fail to list credentials")
		return nil, err
	}
	awsCredList := make([]types.Credential, 0)
	for i := range credList {
		refetchedCred, err := svc.GetCredential(ctx, actor, emulator, credList[i].ID)
		if err != nil {
			logger.WithError(err).Error("fail to re-fetch cred for cred value")
			continue
		}
		if refetchedCred == nil {
			logger.Error("GetCredential return nil on success")
			continue
		}
		awsCredList = append(awsCredList, *refetchedCred)
	}
	logger.WithFields(log.Fields{
		"listCount": len(awsCredList),
	}).Info("credential fetched")

	if len(awsCredList) == 0 {
		return []types.Credential{}, nil
	}
	return awsCredList, nil
}

// GetCredential ...
func (svc CredentialMicroservice) GetCredential(ctx context.Context, actor, emulator, ID string) (*types.Credential, error) {

	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.GetCredential",
		"actor":    actor,
		"emulator": emulator,
		"ID":       ID,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, nil)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return nil, err
	}
	credential, err := client.Get(ctx, service.Actor{Actor: actor, Emulator: emulator}, ID)
	if err != nil {
		logger.WithError(err).Error("fail to fetch credential")
		return nil, err
	}
	converted, err := svc.convertCredential(credential)
	if err != nil {
		logger.WithError(err).Error("fail to unmarshal cred value")
		return nil, err
	}
	logger.Info("credential fetched")
	return &converted, nil

}

func (svc CredentialMicroservice) convertCredential(cred *service.CredentialModel) (types.Credential, error) {
	var awsCred types.AWSAccessKeyCredential
	if err := json.Unmarshal([]byte(cred.Value), &awsCred); err != nil {
		return types.Credential{}, err
	}
	return types.Credential{
		ID:        cred.ID,
		CreatedAt: cred.CreatedAt,
		UpdatedAt: cred.UpdatedAt,
		Cred:      awsCred,
	}, nil
}

func (svc CredentialMicroservice) credentialHasTag(cred *service.CredentialModel, tagName string) bool {
	for _, tag := range cred.GetTags() {
		if tag == tagName {
			return true
		}
	}
	return false
}

// DisableCredential ...
func (svc CredentialMicroservice) DisableCredential(ctx context.Context, actor, emulator, ID string) error {
	logger := log.WithFields(log.Fields{
		"package":  "adapters",
		"function": "CredentialMicroservice.DisableCredential",
		"actor":    actor,
		"emulator": emulator,
		"ID":       ID,
	})
	client, err := service.NewNatsCredentialClientFromConn(svc.queryConn, svc.eventConn)
	if err != nil {
		logger.WithError(err).Error("fail to create credential svc client")
		return err
	}
	err = client.Update(ctx, service.Actor{Actor: actor, Emulator: emulator}, ID, service.CredentialUpdate{
		Disabled: common.PtrOf(true),
	})
	if err != nil {
		return err
	}
	return nil
}
