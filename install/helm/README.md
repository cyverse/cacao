# cacao core
- terraform provisions the hosts.
  - terraform generate/template the configs/inputs of ansible
- ansible bootstrap the k8s cluster.
  - ansible generate/template the configs/inputs of helm
- helm provisions on top of k8s cluster.

# cacao infra
- j2 template
    - cacao-logging-ns.yaml
    - mongodb-configmap.yaml
    - nats.yaml
    - vault-configmap.yaml
    - ion-configmap.yaml
    - cacao-configmap.yaml
    - globus-configmap.yaml
- nginx-ingress.yml.j2
    - j2 template
    - apply

- check tls secret exists
- create tls secret from files(cert & key file)

- template & apply mongodb.yaml
- kubectl wait mongodb deployment
- add keycloak user if dev env
    - template mongodb js script
    - run js script with mongosh
        - k3s & k3d are different

- template & apply vault-postgres.yaml
- kubectl wait for vault postgres
- template & apply vault.yaml
- kubectl wait for vault

- template & apply redis.yaml

- generate AWM key if not exists
- create k8s secret for AWM key

- template & apply nats-local-path.yaml

- ingress setup if local dev
    - context service cluster
    - get nginx ingress ip
    - get nginx ingress port
    - set NGINX_INGRESS_IP & NGINX_INGRESS_PORT
- template & apply keycloak.yaml
- kubectl wait keycloak if local keycloak pod
- keycloak_users.yaml for every KEYCLOAK_USERS if local keycloak

- kubectl wait nats pod
- import_tasks guac-lite.yml

- skaffold delete
- skaffold run
- get nginx ingress ip
- add nginx ingress ip to /etc/hosts for local dev

# k8s setup
- create dir
    - wiretap log dir
    - local path prov config dir
- helm check nginx ingress
- helm install nginx ingress
- kubectl wait nginx ingress


# dev box setup
- create dir
    - config dir
    - deploy dir
- set env_prefix

