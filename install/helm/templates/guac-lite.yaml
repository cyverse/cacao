---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    service: guac-lite
  name: guac-lite
  namespace: default
spec:
  serviceName: guac-lite
  replicas: 1
  selector:
    matchLabels:
      service: guac-lite
  template:
    metadata:
      labels:
        cacao.cyverse.org/app: cacao
        service: guac-lite
    spec:
      containers:
        - name: guac-lite
          image: registry.gitlab.com/cyverse/guac-lite-server:latest
          resources:
            requests:
              cpu: 100m
              memory: 50Mi
          env:
            - name: BASEURL
              value: /guacamole/
            - name: GUAC_LITE_AUTH_KEY
              valueFrom:
                secretKeyRef:
                  name: cacao-secrets
                  key: isession-guac-auth-key
            - name: GUAC_ENCRYPTION_KEY
              valueFrom:
                secretKeyRef:
                  name: cacao-secrets
                  key: isession-guac-encryption-key
            - name: SESSION_SECRET
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: session-secret
{{- if eq .Values.CACAO_AUTH_DRIVER "keycloak" }}
            - name: OAUTH2_CLIENT_ID
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-id
            - name: OAUTH2_CLIENT_SECRET
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-secret
            - name: OAUTH2_VALIDATE_URL
              value: {{ .Values.GUAC_LITE_OAUTH2_VALIDATE_URL | quote }}
{{- else if eq .Values.CACAO_AUTH_DRIVER "globus" }}
            - name: OAUTH2_CLIENT_ID
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-id
            - name: OAUTH2_CLIENT_SECRET
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-secret
            - name: OAUTH2_VALIDATE_URL
              value: https://auth.globus.org/v2/oauth2/token/introspect
{{- else if eq .Values.CACAO_AUTH_DRIVER "cilogon" }}
            - name: OAUTH2_CLIENT_ID
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-id
            - name: OAUTH2_CLIENT_SECRET
              valueFrom:
                configMapKeyRef:
                  name: ion-config
                  key: ion-oauth2-client-secret
            - name: OAUTH2_VALIDATE_URL
              value: https://cilogon.org/oauth2/introspect
{{- else }}
            - name: OAUTH2_CLIENT_ID # unknown CACAO_AUTH_DRIVER
              value: ""
            - name: OAUTH2_CLIENT_SECRET
              value: ""
            - name: OAUTH2_VALIDATE_URL
              value: ""
{{- end }}
            - name: KEYPATH
              value: /usr/src/app/private
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: LOG_LEVEL
              value: debug
          volumeMounts:
            - name: private # for storing ssh key pairs
              mountPath: /usr/src/app/private
        - name: guacd
          image: {{ .Values.GUACD_IMAGE | default "registry.gitlab.com/cyverse/guacamole-server/guacd:git-951cc9ea" }}
          resources:
            requests:
              cpu: 200m
              memory: 100Mi
{{ if and .Values.LOCAL_PATH_PROVISIONER_ENABLE (eq .Values.LOCAL_PATH_PROVISIONER_ENABLE true) }}
      volumes:
        - name: private
          persistentVolumeClaim:
            claimName: guac-lite-local-path-pvc
{{- else }}
      volumes:
        - name: private
          emptyDir: { }
{{- end }}
      automountServiceAccountToken: false
      securityContext:
        runAsUser: 1000
        runAsGroup: 3000
        fsGroup: 2000
---
apiVersion: v1
kind: Service
metadata:
  labels:
    service: guac-lite
  name: guac-lite
  namespace: default
spec:
  ports:
    - name: http
      port: 80
      targetPort: 3000
  selector:
    service: guac-lite
