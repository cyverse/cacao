# deployment-service

add the uuid of the external network (OpenStack) to `config.yaml`
```
EXTERNAL_NETWORK_UUID: <network-uuid-goes-here>
```

> Note: `AWM_KEY` will be generated in `config.yaml` if absent
> ```
> AWM_KEY: <key-goes-here>
> ```

run/re-run deploy script
```bash
./deploy.sh [local|ci|prod]
```

`deploy.sh local` should deploy components in `cyverse/cacao-edge-deployment` as well.
