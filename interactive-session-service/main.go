package main

import (
	"context"
	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"io"

	"gitlab.com/cyverse/cacao/interactive-session-service/adapters"
	"gitlab.com/cyverse/cacao/interactive-session-service/domain"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

func main() {
	var envConfig types.EnvConfig
	err := envconfig.Process("", &envConfig)
	if err != nil {
		log.WithError(err).Fatal(err.Error())
	}
	logLevel, err := log.ParseLevel(envConfig.LogLevel)
	if err != nil {
		log.WithError(err).Fatal(err.Error())
		return
	}
	log.SetLevel(logLevel)

	var config types.Config
	config, err = types.FromEnvConfig(envConfig)
	if err != nil {
		log.Fatal(err.Error())
	}

	// add and initialize the storage adapter
	mongoAdapter := &adapters.MongoAdapter{}

	// add and initialize the session adapter
	guacamoleAdapter := &adapters.GuacamoleAdapter{}

	natsConn, err := config.Messaging.ConnectNats()
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&natsConn)
	stanConn, err := messaging2.NewStanConnectionFromConfigWithoutEventSource(config.Messaging)
	if err != nil {
		log.WithError(err).Panic()
	}
	defer logCloserError(&stanConn)

	// add and initialize the query adapter
	queryAdapter := adapters.NewQueryAdapter(&natsConn)
	defer queryAdapter.Wait()
	eventAdapter := adapters.NewEventAdapter(&stanConn)

	var svc = domain.Domain{
		Storage:      mongoAdapter,
		QueryIn:      queryAdapter,
		EventIn:      eventAdapter,
		SessionSetup: guacamoleAdapter,
	}

	serviceCtx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()
	common.CancelWhenSignaled(cancelFunc)

	svc.Init(&config)
	err = svc.Start(serviceCtx)
	if err != nil {
		log.WithError(err).Panic()
	}
}

func logCloserError(closer io.Closer) {
	err := closer.Close()
	if err != nil {
		log.WithError(err).Error()
	}
}
