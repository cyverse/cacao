package adapters

import (
	"context"
	"encoding/json"
	"fmt"
	cloudevents "github.com/cloudevents/sdk-go/v2"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"sync"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/ports"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// QueryAdapter communicates to IncomingQueryPort
type QueryAdapter struct {
	handlers ports.IncomingQueryHandlers
	channel  chan queryWithReplier
	conn     messaging2.QueryConnection
	wg       sync.WaitGroup
}

var _ ports.IncomingQueryPort = &QueryAdapter{}

// NewQueryAdapter ...
func NewQueryAdapter(connection messaging2.QueryConnection) *QueryAdapter {
	return &QueryAdapter{
		handlers: nil,
		channel:  nil,
		conn:     connection,
		wg:       sync.WaitGroup{},
	}
}

// Wait for QueryAdapter to shut down properly
func (adapter *QueryAdapter) Wait() {
	log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.Wait",
	}).Info()
	adapter.wg.Done()
}

// SetHandlers ...
func (adapter *QueryAdapter) SetHandlers(handlers ports.IncomingQueryHandlers) {
	adapter.handlers = handlers
}

// Start starts the adapter
func (adapter *QueryAdapter) Start(ctx context.Context, wg *sync.WaitGroup) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.Start",
	})

	logger.Info("starting QueryAdapter")

	err := adapter.conn.Listen(ctx, map[cacao_common.QueryOp]messaging2.QueryHandlerFunc{
		cacao_common_service.InteractiveSessionListQueryOp:                 adapter.sentToChannel,
		cacao_common_service.InteractiveSessionGetQueryOp:                  adapter.sentToChannel,
		cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp:      adapter.sentToChannel,
		cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp: adapter.sentToChannel,
		cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp:   adapter.sentToChannel,
	}, wg)
	if err != nil {
		return err
	}
	adapter.channel = make(chan queryWithReplier, natsQueryChannelBufferSize)

	for i := 0; i < natsQueryWorkerCount; i++ {
		adapter.wg.Add(1)
		go adapter.queryWorker(&adapter.wg)
	}
	return nil
}

type queryWithReplier struct {
	Request cloudevents.Event
	Replier messaging2.ReplyWriter
}

const natsQueryWorkerCount = 4
const natsQueryChannelBufferSize = 10

func (adapter *QueryAdapter) sentToChannel(ctx context.Context, request cloudevents.Event, writer messaging2.ReplyWriter) {
	adapter.channel <- queryWithReplier{
		Request: request,
		Replier: writer,
	}
}

func (adapter *QueryAdapter) queryWorker(wg *sync.WaitGroup) {
	defer wg.Done()
	for q := range adapter.channel {
		queryOp := cacao_common.QueryOp(q.Request.Type())
		var replyData []byte
		var err error
		switch queryOp {
		case cacao_common_service.InteractiveSessionListQueryOp:
			replyData, err = adapter.handleInteractiveSessionListQuery(q.Request.Data())
			if err != nil {
				continue
			}
		case cacao_common_service.InteractiveSessionGetQueryOp:
			replyData, err = adapter.handleInteractiveSessionGetQuery(q.Request.Data())
			if err != nil {
				continue
			}
		case cacao_common_service.InteractiveSessionGetByInstanceIDQueryOp:
			replyData, err = adapter.handleInteractiveSessionGetByInstanceIDQuery(q.Request.Data())
			if err != nil {
				continue
			}
		case cacao_common_service.InteractiveSessionGetByInstanceAddressQueryOp:
			replyData, err = adapter.handleInteractiveSessionGetByInstanceAddressQuery(q.Request.Data())
			if err != nil {
				continue
			}
		case cacao_common_service.InteractiveSessionCheckPrerequisitesQueryOp:
			replyData, err = adapter.handleInteractiveSessionCheckPrerequisitesQuery(q.Request.Data())
			if err != nil {
				continue
			}
		default:
			continue
		}
		replyCe, err := messaging2.CreateCloudEvent(replyData, queryOp, messaging2.AutoPopulateCloudEventSource)
		if err != nil {
			continue
		}
		err = q.Replier.Write(replyCe)
		if err != nil {
			continue
		}
	}
}

func (adapter *QueryAdapter) handleInteractiveSessionListQuery(jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionListQuery",
	})

	var listRequest cacao_common_service.Session
	var listResponse cacao_common_service.InteractiveSessionListModel
	err := json.Unmarshal(jsonData, &listRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into Session"
		logger.WithError(err).Error(errorMessage)
		listResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(listResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionListModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	listResult, err := adapter.handlers.List(listRequest.GetSessionActor(), listRequest.GetSessionEmulator())
	if err == nil {
		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		models := []cacao_common_service.InteractiveSessionModel{}
		for _, interactiveSession := range listResult {
			model := types.ConvertToListItemModel(interactiveSession)
			models = append(models, model)
		}

		listResponse.InteractiveSessions = models
	} else {
		logger.Error(err)

		listResponse.Session = cacao_common_service.Session{
			SessionActor:    listRequest.GetSessionActor(),
			SessionEmulator: listRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			listResponse.ServiceError = cerr.GetBase()
		} else {
			listResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(listResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionListModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetQuery(jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetQuery",
	})

	var getRequest cacao_common_service.InteractiveSessionModel
	var getResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.handlers.Get(getRequest.GetSessionActor(), getRequest.GetSessionEmulator(), getRequest.ID)
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		getResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getResponse.Session = cacao_common_service.Session{
			SessionActor:    getRequest.GetSessionActor(),
			SessionEmulator: getRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getResponse.ServiceError = cerr.GetBase()
		} else {
			getResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetByInstanceIDQuery(jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetByInstanceIDQuery",
	})

	var getByInstanceIDRequest cacao_common_service.InteractiveSessionModel
	var getByInstanceIDResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getByInstanceIDRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getByInstanceIDResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getByInstanceIDResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.handlers.GetByInstanceID(getByInstanceIDRequest.GetSessionActor(), getByInstanceIDRequest.GetSessionEmulator(), getByInstanceIDRequest.InstanceID)
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getByInstanceIDRequest.GetSessionActor(),
			SessionEmulator: getByInstanceIDRequest.GetSessionEmulator(),
		}

		getByInstanceIDResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getByInstanceIDResponse.Session = cacao_common_service.Session{
			SessionActor:    getByInstanceIDRequest.GetSessionActor(),
			SessionEmulator: getByInstanceIDRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getByInstanceIDResponse.ServiceError = cerr.GetBase()
		} else {
			getByInstanceIDResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getByInstanceIDResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionGetByInstanceAddressQuery(jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionGetByInstanceAddressQuery",
	})

	var getByInstanceAddressRequest cacao_common_service.InteractiveSessionModel
	var getByInstanceAddressResponse cacao_common_service.InteractiveSessionModel
	err := json.Unmarshal(jsonData, &getByInstanceAddressRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		getByInstanceAddressResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(getByInstanceAddressResponse)
		if err2 != nil {
			errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	getResult, err := adapter.handlers.GetByInstanceAddress(getByInstanceAddressRequest.GetSessionActor(), getByInstanceAddressRequest.GetSessionEmulator(), getByInstanceAddressRequest.InstanceAddress)
	if err == nil {
		session := cacao_common_service.Session{
			SessionActor:    getByInstanceAddressRequest.GetSessionActor(),
			SessionEmulator: getByInstanceAddressRequest.GetSessionEmulator(),
		}

		getByInstanceAddressResponse = types.ConvertToModel(session, getResult)
	} else {
		logger.Error(err)

		getByInstanceAddressResponse.Session = cacao_common_service.Session{
			SessionActor:    getByInstanceAddressRequest.GetSessionActor(),
			SessionEmulator: getByInstanceAddressRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			getByInstanceAddressResponse.ServiceError = cerr.GetBase()
		} else {
			getByInstanceAddressResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(getByInstanceAddressResponse)
	if err != nil {
		errorMessage := "unable to marshal InteractiveSessionModel into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}

func (adapter *QueryAdapter) handleInteractiveSessionCheckPrerequisitesQuery(jsonData []byte) ([]byte, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "QueryAdapter.handleInteractiveSessionCheckPrerequisitesQuery",
	})

	var checkPrerequisitesRequest cacao_common_service.InteractiveSessionModel
	var checkPrerequisitesResponse cacao_common_service.Session
	err := json.Unmarshal(jsonData, &checkPrerequisitesRequest)
	if err != nil {
		errorMessage := "unable to unmarshal JSON bytes into InteractiveSessionModel"
		logger.WithError(err).Error(errorMessage)
		checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoMarshalError(errorMessage).GetBase()

		resBytes, err2 := json.Marshal(checkPrerequisitesResponse)
		if err2 != nil {
			errorMessage := "unable to marshal Session into JSON bytes"
			logger.WithError(err).Error(errorMessage)
			return nil, fmt.Errorf(errorMessage)
		}

		return resBytes, nil
	}

	checkResult, err := adapter.handlers.CheckPrerequisites(checkPrerequisitesRequest.Protocol, checkPrerequisitesRequest.InstanceAddress, checkPrerequisitesRequest.InstanceAdminUsername)
	if err == nil {
		checkPrerequisitesResponse = cacao_common_service.Session{
			SessionActor:    checkPrerequisitesRequest.GetSessionActor(),
			SessionEmulator: checkPrerequisitesRequest.GetSessionEmulator(),
		}

		if !checkResult {
			checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoGeneralError("Prerequisites are not met").GetBase()
		}
	} else {
		logger.Error(err)

		checkPrerequisitesResponse = cacao_common_service.Session{
			SessionActor:    checkPrerequisitesRequest.GetSessionActor(),
			SessionEmulator: checkPrerequisitesRequest.GetSessionEmulator(),
		}

		if cerr, ok := err.(cacao_common_service.CacaoError); ok {
			checkPrerequisitesResponse.ServiceError = cerr.GetBase()
		} else {
			checkPrerequisitesResponse.ServiceError = cacao_common_service.NewCacaoGeneralError(err.Error()).GetBase()
		}
	}

	resBytes, err := json.Marshal(checkPrerequisitesResponse)
	if err != nil {
		errorMessage := "unable to marshal Session into JSON bytes"
		logger.WithError(err).Error(errorMessage)
		return nil, fmt.Errorf(errorMessage)
	}

	return resBytes, nil
}
