package adapters

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_db "gitlab.com/cyverse/cacao-common/db"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// MongoAdapter implements PersistentStoragePort
type MongoAdapter struct {
	Config *types.Config
	Store  cacao_common_db.ObjectStore
}

// Init initialize mongodb adapter
func (adapter *MongoAdapter) Init(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.Init",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMongoDBObjectStore(&config.MongoDBConfig)
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// InitMock initialize mongodb adapter with mock_objectstore
func (adapter *MongoAdapter) InitMock(config *types.Config) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.InitMock",
	})

	logger.Info("initializing MongoAdapter")

	adapter.Config = config

	store, err := cacao_common_db.CreateMockObjectStore()
	if err != nil {
		logger.WithError(err).Fatal("unable to connect to MongoDB")
	}

	adapter.Store = store
}

// GetMock returns Mock
func (adapter *MongoAdapter) GetMock() *mock.Mock {
	if mockObjectStore, ok := adapter.Store.(*cacao_common_db.MockObjectStore); ok {
		return mockObjectStore.Mock
	}
	return nil
}

// Finalize finalizes mongodb adapter
func (adapter *MongoAdapter) Finalize() {
	err := adapter.Store.Release()
	if err != nil {
		log.Fatal(err)
	}

	adapter.Store = nil
}

// List returns active/creating interactive sessions owned by a user
func (adapter *MongoAdapter) List(user string) ([]types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.List",
	})

	results := []types.InteractiveSession{}

	// filter out inactive records
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"state": map[string]interface{}{
					"$ne": cacao_common_service.InteractiveSessionStateInactive.String(),
				},
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	err := adapter.Store.ListConditionalWithSort(adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption, &results)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to list active/creating interactive sessions for the user %s", user)
		logger.WithError(err).Error(errorMessage)
		return nil, cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return results, nil
}

// MockList sets expected results for List
func (adapter *MongoAdapter) MockList(user string, expectedInteractiveSessions []types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"state": map[string]interface{}{
					"$ne": cacao_common_service.InteractiveSessionStateInactive.String(),
				},
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	mock.On("ListConditionalWithSort", adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption).Return(expectedInteractiveSessions, expectedError)
	return nil
}

// Get returns the interactive session with the ID
func (adapter *MongoAdapter) Get(user string, interactiveSessionID cacao_common.ID) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.Get",
	})

	result := types.InteractiveSession{}

	err := adapter.Store.Get(adapter.Config.InteractiveSessionMongoDBCollectionName, interactiveSessionID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the interactive session for id %s", interactiveSessionID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the interactive session for id %s", interactiveSessionID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGet sets expected results for Get
func (adapter *MongoAdapter) MockGet(user string, interactiveSessionID cacao_common.ID, expectedInteractiveSession types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Get", adapter.Config.InteractiveSessionMongoDBCollectionName, interactiveSessionID.String()).Return(expectedInteractiveSession, expectedError)
	return nil
}

// GetByInstanceID returns the interactive session with the instance ID
func (adapter *MongoAdapter) GetByInstanceID(user string, instanceID string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.GetByInstanceID",
	})

	result := types.InteractiveSession{}

	// filter
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"instance_id": instanceID,
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	err := adapter.Store.GetConditionalWithSort(adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption, &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the interactive session for instance id %s", instanceID)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the interactive session for instance id %s", instanceID)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGetByInstanceID sets expected results for GetByInstanceID
func (adapter *MongoAdapter) MockGetByInstanceID(user string, instanceID string, expectedInteractiveSession types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	// filter
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"instance_id": instanceID,
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	mock.On("GetConditionalWithSort", adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption).Return(expectedInteractiveSession, expectedError)
	return nil
}

// GetByInstanceAddress returns the interactive session with the Instance Address
func (adapter *MongoAdapter) GetByInstanceAddress(user string, instanceAddress string) (types.InteractiveSession, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.GetByInstanceAddress",
	})

	result := types.InteractiveSession{}

	// filter
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"instance_address": instanceAddress,
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	err := adapter.Store.GetConditionalWithSort(adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption, &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the interactive session for instance address %s", instanceAddress)
		logger.WithError(err).Error(errorMessage)
		return result, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != user {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the interactive session for instance address %s", instanceAddress)
		logger.Error(errorMessage)
		return result, cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	return result, nil
}

// MockGetByInstanceAddress sets expected results for GetByInstanceAddress
func (adapter *MongoAdapter) MockGetByInstanceAddress(user string, instanceAddress string, expectedInteractiveSession types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	// filter
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"owner": user,
			},
			{
				"instance_address": instanceAddress,
			},
		},
	}

	// sort
	sortOption := []cacao_common_db.KeyValue{
		{
			Key:   "created_at",
			Value: -1, // desc
		},
	}

	mock.On("GetConditionalWithSort", adapter.Config.InteractiveSessionMongoDBCollectionName, filter, sortOption).Return(expectedInteractiveSession, expectedError)
	return nil
}

// Create inserts an interactive session
func (adapter *MongoAdapter) Create(interactiveSession types.InteractiveSession) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.Create",
	})

	err := adapter.Store.Insert(adapter.Config.InteractiveSessionMongoDBCollectionName, interactiveSession)
	if err != nil {
		if cacao_common_db.IsDuplicateError(err) {
			errorMessage := fmt.Sprintf("unable to insert an interactive session because id %s conflicts", interactiveSession.ID)
			logger.WithError(err).Error(errorMessage)
			return cacao_common_service.NewCacaoAlreadyExistError(errorMessage)
		}

		errorMessage := fmt.Sprintf("unable to insert an interactive session with id %s", interactiveSession.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockCreate sets expected results for Create
func (adapter *MongoAdapter) MockCreate(interactiveSession types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	mock.On("Insert", adapter.Config.InteractiveSessionMongoDBCollectionName).Return(expectedError)
	return nil
}

// Update updates/edits an interactive session
func (adapter *MongoAdapter) Update(interactiveSession types.InteractiveSession, updateFieldNames []string) error {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.Update",
	})

	// get and check ownership
	result := types.InteractiveSession{}

	err := adapter.Store.Get(adapter.Config.InteractiveSessionMongoDBCollectionName, interactiveSession.ID.String(), &result)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to get the interactive session for id %s", interactiveSession.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	if result.Owner != interactiveSession.Owner {
		// unauthorized
		errorMessage := fmt.Sprintf("unauthorized access to the interactive session for id %s", interactiveSession.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoUnauthorizedError(errorMessage)
	}

	// update
	updated, err := adapter.Store.Update(adapter.Config.InteractiveSessionMongoDBCollectionName, interactiveSession.ID.String(), interactiveSession, updateFieldNames)
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update the interactive session for id %s", interactiveSession.ID)
		logger.WithError(err).Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	if !updated {
		errorMessage := fmt.Sprintf("unable to update the interactive session for id %s", interactiveSession.ID)
		logger.Error(errorMessage)
		return cacao_common_service.NewCacaoGeneralError(errorMessage)
	}

	return nil
}

// MockUpdate sets expected results for Update
func (adapter *MongoAdapter) MockUpdate(existingInteractiveSession types.InteractiveSession, newInteractiveSession types.InteractiveSession, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	expectedResult := true
	if expectedError != nil {
		expectedResult = false
	}

	mock.On("Get", adapter.Config.InteractiveSessionMongoDBCollectionName, newInteractiveSession.ID.String()).Return(existingInteractiveSession, expectedError)
	mock.On("Update", adapter.Config.InteractiveSessionMongoDBCollectionName, newInteractiveSession.ID.String()).Return(expectedResult, expectedError)
	return nil
}

// DeactivateAllByInstanceID deactivates all interactive sessions by instance ID
func (adapter *MongoAdapter) DeactivateAllByInstanceID(user, instanceID string) (int64, error) {
	logger := log.WithFields(log.Fields{
		"package":  "interactive-session-service.adapters",
		"function": "MongoAdapter.DeactivateAllByInstanceID",
	})

	// filter out inactive records
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"instance_id": instanceID,
				"owner":       user,
			},
			{
				"state": map[string]interface{}{
					"$ne": cacao_common_service.InteractiveSessionStateInactive.String(),
				},
			},
		},
	}

	updateInteractiveSession := types.InteractiveSession{}
	updateInteractiveSession.RedirectURL = ""
	updateInteractiveSession.State = cacao_common_service.InteractiveSessionStateInactive
	updateInteractiveSession.UpdatedAt = time.Now().UTC()

	updated, err := adapter.Store.UpdateConditional(adapter.Config.InteractiveSessionMongoDBCollectionName, filter, updateInteractiveSession, []string{"redirect_url", "state", "updated_at"})
	if err != nil {
		errorMessage := fmt.Sprintf("unable to update interactive sessions for instance id %s", instanceID)
		logger.WithError(err).Error(errorMessage)
		return 0, cacao_common_service.NewCacaoNotFoundError(errorMessage)
	}

	return updated, nil
}

// MockDeactivateAllByInstanceID sets expected results for DeactivateAllByInstanceID
func (adapter *MongoAdapter) MockDeactivateAllByInstanceID(user, instanceID string, expectedRecords int, expectedError error) error {
	mock := adapter.GetMock()
	if mock == nil {
		return fmt.Errorf("mock is not initialized")
	}

	// filter out inactive records
	filter := map[string]interface{}{
		"$and": []map[string]interface{}{
			{
				"instance_id": instanceID,
				"owner":       user,
			},
			{
				"state": map[string]interface{}{
					"$ne": cacao_common_service.InteractiveSessionStateInactive.String(),
				},
			},
		},
	}

	mock.On("UpdateConditional", adapter.Config.InteractiveSessionMongoDBCollectionName, filter).Return(expectedRecords, expectedError)
	return nil
}
