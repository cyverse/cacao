// Code generated by mockery v2.22.1. DO NOT EDIT.

package mocks

import (
	mock "github.com/stretchr/testify/mock"
	common "gitlab.com/cyverse/cacao-common/common"

	types "gitlab.com/cyverse/cacao/interactive-session-service/types"
)

// PersistentStoragePort is an autogenerated mock type for the PersistentStoragePort type
type PersistentStoragePort struct {
	mock.Mock
}

// Create provides a mock function with given fields: interactiveSession
func (_m *PersistentStoragePort) Create(interactiveSession types.InteractiveSession) error {
	ret := _m.Called(interactiveSession)

	var r0 error
	if rf, ok := ret.Get(0).(func(types.InteractiveSession) error); ok {
		r0 = rf(interactiveSession)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// DeactivateAllByInstanceID provides a mock function with given fields: user, instanceID
func (_m *PersistentStoragePort) DeactivateAllByInstanceID(user string, instanceID string) (int64, error) {
	ret := _m.Called(user, instanceID)

	var r0 int64
	var r1 error
	if rf, ok := ret.Get(0).(func(string, string) (int64, error)); ok {
		return rf(user, instanceID)
	}
	if rf, ok := ret.Get(0).(func(string, string) int64); ok {
		r0 = rf(user, instanceID)
	} else {
		r0 = ret.Get(0).(int64)
	}

	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(user, instanceID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Get provides a mock function with given fields: user, interactiveSessionID
func (_m *PersistentStoragePort) Get(user string, interactiveSessionID common.ID) (types.InteractiveSession, error) {
	ret := _m.Called(user, interactiveSessionID)

	var r0 types.InteractiveSession
	var r1 error
	if rf, ok := ret.Get(0).(func(string, common.ID) (types.InteractiveSession, error)); ok {
		return rf(user, interactiveSessionID)
	}
	if rf, ok := ret.Get(0).(func(string, common.ID) types.InteractiveSession); ok {
		r0 = rf(user, interactiveSessionID)
	} else {
		r0 = ret.Get(0).(types.InteractiveSession)
	}

	if rf, ok := ret.Get(1).(func(string, common.ID) error); ok {
		r1 = rf(user, interactiveSessionID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByInstanceAddress provides a mock function with given fields: user, instanceAddress
func (_m *PersistentStoragePort) GetByInstanceAddress(user string, instanceAddress string) (types.InteractiveSession, error) {
	ret := _m.Called(user, instanceAddress)

	var r0 types.InteractiveSession
	var r1 error
	if rf, ok := ret.Get(0).(func(string, string) (types.InteractiveSession, error)); ok {
		return rf(user, instanceAddress)
	}
	if rf, ok := ret.Get(0).(func(string, string) types.InteractiveSession); ok {
		r0 = rf(user, instanceAddress)
	} else {
		r0 = ret.Get(0).(types.InteractiveSession)
	}

	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(user, instanceAddress)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByInstanceID provides a mock function with given fields: user, instanceID
func (_m *PersistentStoragePort) GetByInstanceID(user string, instanceID string) (types.InteractiveSession, error) {
	ret := _m.Called(user, instanceID)

	var r0 types.InteractiveSession
	var r1 error
	if rf, ok := ret.Get(0).(func(string, string) (types.InteractiveSession, error)); ok {
		return rf(user, instanceID)
	}
	if rf, ok := ret.Get(0).(func(string, string) types.InteractiveSession); ok {
		r0 = rf(user, instanceID)
	} else {
		r0 = ret.Get(0).(types.InteractiveSession)
	}

	if rf, ok := ret.Get(1).(func(string, string) error); ok {
		r1 = rf(user, instanceID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Init provides a mock function with given fields: config
func (_m *PersistentStoragePort) Init(config *types.Config) {
	_m.Called(config)
}

// List provides a mock function with given fields: user
func (_m *PersistentStoragePort) List(user string) ([]types.InteractiveSession, error) {
	ret := _m.Called(user)

	var r0 []types.InteractiveSession
	var r1 error
	if rf, ok := ret.Get(0).(func(string) ([]types.InteractiveSession, error)); ok {
		return rf(user)
	}
	if rf, ok := ret.Get(0).(func(string) []types.InteractiveSession); ok {
		r0 = rf(user)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]types.InteractiveSession)
		}
	}

	if rf, ok := ret.Get(1).(func(string) error); ok {
		r1 = rf(user)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Update provides a mock function with given fields: interactiveSession, updateFieldNames
func (_m *PersistentStoragePort) Update(interactiveSession types.InteractiveSession, updateFieldNames []string) error {
	ret := _m.Called(interactiveSession, updateFieldNames)

	var r0 error
	if rf, ok := ret.Get(0).(func(types.InteractiveSession, []string) error); ok {
		r0 = rf(interactiveSession, updateFieldNames)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewPersistentStoragePort interface {
	mock.TestingT
	Cleanup(func())
}

// NewPersistentStoragePort creates a new instance of PersistentStoragePort. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewPersistentStoragePort(t mockConstructorTestingTNewPersistentStoragePort) *PersistentStoragePort {
	mock := &PersistentStoragePort{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
