# CACAO CLI

The cacao CLI provides an interface to interact with the cacao platform and perform various tasks such as creating, deploying and managing applications and services.

The cacao CLI is written in Go and is designed to be easy to use and extend.

## Config
The CLI will use files in `~/.cacao` directory to store some configurations.
- `~/.cacao/api.json`  for base URL for api 
- `~/.cacao/config.json` for auth token
- `~/.cacao/username.json`  for username

Base URL for API and authentication token can also be specified by environment variables:
> environment variable takes priority over config files, but are temporary,
> meaning the values from env var are not persistent.
- `CACAO_API` - Base URL for the API
- `CACAO_TOKEN` - Auth token


## Usage

Build using:

```shell
$ go build -o cacao
```

To see the list of available commands:

```shell
$ cacao help
```


### Login
CACAO CLI uses auth token to authenticate with CACAO API.

There are 3 ways to set up CLI with auth token.

1. If the CACAO API you are targeting is using **Keycloak** as auth provider,
then you can login in with the CLI directly using username & password.
    ```shell
    $ cacao login
    ```

2. Authenticate using a web browser, and then enters the auth token into CLI
    ``` shell
    $ cacao login --browser
    ```
   This flag will point to a URL, which you copy to a web browser to login.
   The landing page after logged in will be a JSON response, you will copy the
   auth token from the JSON response and paste it into the CLI prompt on the
   command line.

3. Specify auth token directly on the command line
    ``` shell
    $ cacao login --auth-token <your-auth-token>
    ```

## Development
Each sub-command is on its directory/package, e.g. credential sub-command is in `credential` package(directory).

Each sub-command package should only export 1 struct that is the root of the sub-command.
e.g. credential package only exports `Cmd` struct (`credential/credential.go`).

Flags and additional sub-sub-command are added to sub-command in `init()` function inside the package.
> Note there can be multiple `init()` in a package, one in each file in the package.

`utils` package declared some shared utility functions & types that are used to
construct HTTP requests, output in various level (normal, verbose, error, etc.) and so on.
