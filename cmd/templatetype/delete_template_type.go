package templatetype

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:          "delete <name>",
	Short:        "Delete a template type",
	RunE:         templateTypeDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// Run ...
func templateTypeDelete(command *cobra.Command, args []string) error {
	client := getTemplateTypeClient()
	templateTypeName := args[0]
	req := client.NewRequest("DELETE", "/templates/types/"+neturl.PathEscape(templateTypeName), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	return client.DoRequest(req)
}
