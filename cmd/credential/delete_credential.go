package credential

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var credentialDeleteParams = struct {
	ID string
}{}
var deleteCmd = &cobra.Command{
	Use:          "delete <credential-id>",
	Short:        "Deletes a credential",
	RunE:         credentialDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// credentialDelete ...
func credentialDelete(command *cobra.Command, args []string) error {
	client := getCredentialClient()

	client.PrintDebug("Parsed credential id argument: '%s'", args[0])
	credentialDeleteParams.ID = args[0]

	req := client.NewRequest("DELETE", "/credentials/"+neturl.PathEscape(credentialDeleteParams.ID), "")
	return client.DoRequest(req)
}

func init() {

}
