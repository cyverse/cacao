package credential

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [credential-id] [flags]",
	Short:        "Get credential(s)",
	Long:         `Gets all credentials unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         credentialGet,
	SilenceUsage: true,
}

func credentialGet(command *cobra.Command, args []string) error {

	client := getCredentialClient()
	if len(args) >= 1 {
		return getOneCredential(client, args[0])
	}

	req := client.NewRequest("GET", "/credentials", "")
	return client.DoRequest(req)
}

func getOneCredential(client utils.Client, credentialID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", credentialID)
	req := client.NewRequest("GET", "/credentials/"+neturl.PathEscape(credentialID), "")
	return client.DoRequest(req)
}
