package useraction

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

type userActionUpdateFile struct {
	ID          string `json:"id"`
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	// Public needs to be null-able to marshal correctly
	Public *bool `json:"public,omitempty"`
}

type userActionUpdatePayload struct {
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
	// Public needs to be null-able to marshal correctly
	Public *bool `json:"public,omitempty"`
}

// userActionUpdateParams ...
var userActionUpdateParams = struct {
	service.UserActionModel
	Private  bool
	Filename string
}{}

var updateCmd = &cobra.Command{
	Use:                   "update (-f <filename> | <id>) [flags]",
	Short:                 "Update a user action",
	DisableFlagsInUseLine: true,
	Args:                  utils.ExactNArgsOrFileFlag(1),
	RunE:                  userActionUpdate,
	SilenceUsage:          true,
}

func init() {
	updateCmd.Flags().StringVarP(&userActionUpdateParams.Filename, "file", "f", "", "JSON file to read user action info from")
	updateCmd.Flags().StringVarP(&userActionUpdateParams.Description, "description", "d", "", "Description of the user action")
	updateCmd.Flags().StringVarP(&userActionUpdateParams.Name, "name", "n", "", "Name of the user action")
	updateCmd.Flags().BoolVar(&userActionUpdateParams.Public, "public", false, "Change visibility to public.")
	updateCmd.Flags().BoolVar(&userActionUpdateParams.Private, "private", false, "Change visibility to private.")

	updateCmd.Flags().MarkHidden("file")
	updateCmd.MarkFlagsMutuallyExclusive("file", "description")
	updateCmd.MarkFlagsMutuallyExclusive("file", "public")
	updateCmd.MarkFlagsMutuallyExclusive("file", "name")
	updateCmd.MarkFlagsMutuallyExclusive("public", "private")
}

func userActionUpdate(command *cobra.Command, args []string) error {
	client := getUserActionClient()
	var payload userActionUpdatePayload

	if command.Flags().Changed("file") {
		data, err := os.ReadFile(userActionUpdateParams.Filename)
		if err != nil {
			return err
		}
		var file userActionUpdateFile
		if err = json.Unmarshal(data, &file); err != nil {
			return err
		}
		payload = userActionUpdatePayload{
			Name:        file.Name,
			Description: file.Description,
			Public:      file.Public,
		}
		userActionUpdateParams.ID = common.ID(file.ID)
	} else {
		userActionUpdateParams.ID = common.ID(args[0])
		if command.Flags().Changed("name") {
			payload.Name = userActionUpdateParams.Name
		}
		if command.Flags().Changed("description") {
			payload.Description = userActionUpdateParams.Description
		}
		if command.Flags().Changed("public") {
			var v = true
			payload.Public = &v
		}
		if command.Flags().Changed("private") {
			var v = false
			payload.Public = &v
		}
	}

	data, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("PATCH", "/useractions/"+neturl.PathEscape(userActionUpdateParams.ID.String()), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	return client.DoRequest(req)
}
