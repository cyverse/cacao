package isession

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var isessionDeleteParams = struct {
	ID string
}{}
var deleteCmd = &cobra.Command{
	Use:          "delete <isession-id>",
	Short:        "Deletes a in interactive session",
	RunE:         isessionDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// isessionDelete ...
func isessionDelete(command *cobra.Command, args []string) error {
	client := getIsessionClient()

	client.PrintDebug("Parsed isession id argument: '%s'", args[0])
	isessionDeleteParams.ID = args[0]

	req := client.NewRequest("DELETE", "/isessions/"+neturl.PathEscape(isessionDeleteParams.ID), "")
	return client.DoRequest(req)
}
