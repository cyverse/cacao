package utils

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cyverse/cacao-common/http"
	"reflect"
	"strings"
	"testing"
)

func TestParseKeyValuePairs(t *testing.T) {
	type args struct {
		parameters string
	}
	tests := []struct {
		name    string
		args    args
		want    []http.KeyValue
		wantErr bool
	}{
		{
			name: "empty",
			args: args{
				parameters: "",
			},
			want:    []http.KeyValue{},
			wantErr: false,
		},
		{
			name: "1 pair",
			args: args{
				parameters: "key=value",
			},
			want: []http.KeyValue{
				{
					Key:   "key",
					Value: "value",
				},
			},
			wantErr: false,
		},
		{
			name: "1 pair, end with comma",
			args: args{
				parameters: "key=value,",
			},
			want: []http.KeyValue{
				{
					Key:   "key",
					Value: "value",
				},
			},
			wantErr: false,
		},
		{
			name: "2 pairs",
			args: args{
				parameters: "k1=v1,k2=v2",
			},
			want: []http.KeyValue{
				{
					Key:   "k1",
					Value: "v1",
				},
				{
					Key:   "k2",
					Value: "v2",
				},
			},
			wantErr: false,
		},
		{
			name: "2 pairs, end with comma",
			args: args{
				parameters: "k1=v1,k2=v2,",
			},
			want: []http.KeyValue{
				{
					Key:   "k1",
					Value: "v1",
				},
				{
					Key:   "k2",
					Value: "v2",
				},
			},
			wantErr: false,
		},
		{
			name: "2 pairs, separate with comma and space",
			args: args{
				parameters: "k1=v1, k2=v2",
			},
			want: []http.KeyValue{
				{
					Key:   "k1",
					Value: "v1",
				},
				{
					Key:   " k2", // the space is treated as part of key
					Value: "v2",
				},
			},
			wantErr: false,
		},
		{
			name: "empty value",
			args: args{
				parameters: "foobar=",
			},
			want: []http.KeyValue{
				{
					Key:   "foobar",
					Value: "",
				},
			},
			wantErr: false,
		},
		{
			name: "no equal character",
			args: args{
				parameters: "foobar",
			},
			want:    []http.KeyValue{},
			wantErr: true,
		},
		{
			name: "leading equal character in key",
			args: args{
				parameters: "=foobar",
			},
			want:    []http.KeyValue{},
			wantErr: true, // key cannot be empty
		},
		{
			name: "equal character in value",
			args: args{
				parameters: "foobar==",
			},
			want: []http.KeyValue{
				{
					Key:   "foobar",
					Value: "=", // 2nd '=' is treated as part of value
				},
			},
			wantErr: false,
		},
		{
			name: "3 pairs",
			args: args{
				parameters: "foo=aaaaa,bar=bbbbb,baz=ccccc",
			},
			want: []http.KeyValue{
				{
					Key:   "foo",
					Value: "aaaaa",
				},
				{
					Key:   "bar",
					Value: "bbbbb",
				},
				{
					Key:   "baz",
					Value: "ccccc",
				},
			},
			wantErr: false,
		},
		{
			name: "2 pairs, duplicate key",
			args: args{
				parameters: "foo=aaaaa,foo=bbbbb",
			},
			want: []http.KeyValue{
				{
					Key:   "foo",
					Value: "aaaaa",
				},
			},
			wantErr: false,
		},
		{
			name: "3 pairs, duplicate key",
			args: args{
				parameters: "foo=aaaaa,bar=barrrrr,foo=bbbbb",
			},
			want: []http.KeyValue{
				{
					Key:   "foo",
					Value: "aaaaa",
				},
				{
					Key:   "bar",
					Value: "barrrrr",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseKeyValuePairs(tt.args.parameters)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseKeyValuePairs(\"%s\") error = %v, wantErr %v", tt.args.parameters, err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseKeyValuePairs(\"%s\") got = %v, want %v", tt.args.parameters, got, tt.want)
			}
		})
	}
}

// 3 key-value pairs.
// perform some filtering on the input so that we can make some assertions on the output.
func FuzzParseKeyValuePairs(f *testing.F) {
	keysOrValuesHasComma := func(k1, v1, k2, v2, k3, v3 string) bool {
		for _, s := range []string{k1, v1, k2, v2, k3, v3} {
			if strings.Contains(s, ",") {
				return true
			}
		}
		return false
	}
	keysHasLeadingEqual := func(k1, v1, k2, v2, k3, v3 string) bool {
		for _, s := range []string{k1, k2, k3} {
			if len(s) > 0 && s[0] == '=' {
				return true
			}
		}
		return false
	}
	ifAnyKeyIsEmpty := func(k1, v1, k2, v2, k3, v3 string) bool {
		for _, s := range []string{k1, k2, k3} {
			if s == "" {
				return true
			}
		}
		return false
	}

	f.Add("Key1", "Value1", "Key2", "Value2", "Key3", "Value3")
	f.Fuzz(func(t *testing.T, k1, v1, k2, v2, k3, v3 string) {
		if keysOrValuesHasComma(k1, v1, k2, v2, k3, v3) {
			// skip if generated key or value has comma, since it makes filtering difficult
			return
		}
		expectError := false
		if keysHasLeadingEqual(k1, v1, k2, v2, k3, v3) {
			expectError = true
		}
		if ifAnyKeyIsEmpty(k1, v1, k2, v2, k3, v3) {
			expectError = true
		}

		input := fmt.Sprintf("%s=%s,%s=%s,%s=%s", k1, v1, k2, v2, k3, v3)
		errMsg := fmt.Sprintf("input is \"%s\"", input)
		defer func() {
			if r := recover(); r != nil {
				assert.Fail(t, "panic-ed, %s", errMsg)
			}
		}()
		pairs, err := ParseKeyValuePairs(input)
		if expectError {
			assert.Errorf(t, err, errMsg)
			assert.Equalf(t, []http.KeyValue{}, pairs, errMsg)
		} else {
			assert.NoErrorf(t, err, errMsg)
			assert.NotEmptyf(t, pairs, errMsg)
		}

	})
}
