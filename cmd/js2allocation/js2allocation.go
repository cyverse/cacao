package js2allocation

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for js2allocation sub-command
var Cmd = &cobra.Command{
	Use:          "js2allocation",
	Short:        "Manages JetStream2 allocation",
	Args:         cobra.NoArgs,
	SilenceUsage: true,
}

func init() {
	Cmd.AddCommand(getCmd)
}

func getJS2allocationClient() utils.Client {
	client := utils.NewClient()
	return client
}
