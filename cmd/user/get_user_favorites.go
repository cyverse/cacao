package user

import (
	"fmt"
	neturl "net/url"

	"github.com/spf13/cobra"
)

var getFavoriteCmd = &cobra.Command{
	Use:          "get-favorite <username>",
	Short:        "Get favorite configuration for a user",
	Args:         cobra.ExactArgs(1),
	RunE:         userGetFavorite,
	SilenceUsage: true,
}

func userGetFavorite(command *cobra.Command, args []string) error {
	client := getUserClient()
	username := args[0]
	client.PrintDebug("Parsed username argument: '%s'", username)
	path := fmt.Sprintf("/users/%s/favorites", neturl.PathEscape(username))
	req := client.NewRequest("GET", path, "")
	return client.DoRequest(req)
}
