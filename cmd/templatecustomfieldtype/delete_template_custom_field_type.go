package templatecustomfieldtype

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:          "delete <name>",
	Short:        "Delete a template custom field type",
	RunE:         templateCustomFieldTypeDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// Run ...
func templateCustomFieldTypeDelete(command *cobra.Command, args []string) error {
	client := getTemplateCustomFieldTypeClient()
	templateCustomFieldTypeName := args[0]
	req := client.NewRequest("DELETE", "/templates/customfieldtypes/"+neturl.PathEscape(templateCustomFieldTypeName), "")
	client.PrintDebug("Sending request to '%s'", req.URL)
	return client.DoRequest(req)
}
