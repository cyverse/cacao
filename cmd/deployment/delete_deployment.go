package deployment

import (
	neturl "net/url"

	"github.com/spf13/cobra"
)

var deleteCmd = &cobra.Command{
	Use:          "delete <deployment-id>",
	Short:        "Deletes a deployment",
	RunE:         deploymentDelete,
	Args:         cobra.ExactArgs(1),
	SilenceUsage: true,
}

// deploymentDelete ...
func deploymentDelete(command *cobra.Command, args []string) error {
	client := getDeploymentClient()

	deploymentID := args[0]
	client.PrintDebug("Parsed deployment id argument: '%s'", deploymentID)

	req := client.NewRequest("DELETE", "/deployments/"+neturl.PathEscape(deploymentID), "")
	return client.DoRequest(req)
}
