package token

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/service"
	"io"
	"os"
	"strconv"
	"time"
)

// tokenCreateParams
var tokenCreateParams = struct {
	service.TokenModel
	File             string
	DurationString   string
	ExpirationString string
	StartString      string
	TypeString       string
}{}

var createCmd = &cobra.Command{
	Use:               "create (-f file | [flags])",
	Short:             "Create a token",
	ValidArgs:         []string{},
	ValidArgsFunction: nil,
	Args:              cobra.NoArgs,
	RunE:              tokenCreate,
	SilenceUsage:      true,
}

func tokenCreate(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	client.PrintDebug("Parsed flags are %+v", tokenCreateParams)
	client.PrintDebug("Parsed args are %+v", args)
	if cmd.Flags().Changed("file") {
		data, err := os.ReadFile(tokenCreateParams.File)
		if err != nil {
			return err
		}
		if err = json.Unmarshal(data, &tokenCreateParams.TokenModel); err != nil {
			return err
		}
	} else {
		if len(tokenCreateParams.Name) <= 0 {
			return fmt.Errorf("name must be supplied")
		}

		err := parseTokenTimeRange(cmd)
		if err != nil {
			return err
		}

		if len(tokenCreateParams.TypeString) <= 0 {
			return fmt.Errorf("type must be supplied")
		}
		if tokenCreateParams.TypeString == string(service.PersonalTokenType) {
			tokenCreateParams.Type = service.PersonalTokenType
		} else if tokenCreateParams.TypeString == string(service.DelegatedTokenType) {
			tokenCreateParams.Type = service.DelegatedTokenType
		} else if tokenCreateParams.TypeString == string(service.InternalDeployType) {
			tokenCreateParams.Type = service.InternalDeployType
		} else {
			return fmt.Errorf("token type must be personal or delegated")
		}

		if tokenCreateParams.Type == service.DelegatedTokenType && len(tokenCreateParams.DelegatedContext) <= 0 {
			return fmt.Errorf("if type == 'delegated', delegated_context must be supplied")
		}
		if len(tokenCreateParams.Scopes) <= 0 {
			return fmt.Errorf("scopes must be supplied")
		}
	}
	data, err := json.Marshal(&tokenCreateParams.TokenModel)
	if err != nil {
		return err
	}
	client.PrintDebug("constructed request body:\n%s\n", data)
	req := client.NewRequest("POST", "/tokens", "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	client.DoRequest(req)
	return nil
}

func parseTokenTimeRange(cmd *cobra.Command) error {
	now := time.Now()
	if len(tokenCreateParams.StartString) > 0 {
		start, err := time.Parse(time.RFC3339, tokenCreateParams.StartString)
		if err != nil {
			return err
		}

		tokenCreateParams.Start = &start
	}

	var expiration time.Time
	if cmd.Flags().Changed("duration") {
		if len(tokenCreateParams.DurationString) < 2 {
			return fmt.Errorf("duration is not valid")
		}
		duration, err := parseDuration(tokenCreateParams.DurationString)
		if err != nil {
			return err
		}
		if tokenCreateParams.Start != nil {
			expiration = tokenCreateParams.Start.Add(duration)
		} else {
			expiration = now.Add(duration)
		}
	} else {
		var err error
		expiration, err = time.Parse(time.RFC3339, tokenCreateParams.ExpirationString)
		if err != nil {
			return err
		}
	}
	if expiration.IsZero() {
		return fmt.Errorf("expiration must be set")
	}
	if expiration.Before(now) {
		return fmt.Errorf("expiration must occur in the future")
	}
	tokenCreateParams.Expiration = expiration

	if tokenCreateParams.Start != nil && tokenCreateParams.Start.After(tokenCreateParams.Expiration) {
		return fmt.Errorf("start (%s) must be before expiration (%s)", tokenCreateParams.Start.String(), expiration.String())
	}
	return nil
}

func parseDuration(d string) (time.Duration, error) {
	if len(d) < 2 {
		return 0, fmt.Errorf("duration is not valid")
	}
	durationUnitStr := tokenCreateParams.DurationString[len(tokenCreateParams.DurationString)-1]
	var durationUnit time.Duration
	switch durationUnitStr {
	case 'd':
		durationUnit = time.Hour * 24
	case 'h':
		durationUnit = time.Hour
	case 'm':
		durationUnit = time.Minute
	default:
		return 0, fmt.Errorf("duration missing unit, valid units:\nday - 'd'\nhour - 'h'\nminute - 'm'")
	}
	parseInt, err := strconv.ParseInt(d[:len(tokenCreateParams.DurationString)-1], 10, 32)
	if err != nil {
		return 0, fmt.Errorf("duration does not have valid integer, %w", err)
	}
	if parseInt < 0 {
		return 0, fmt.Errorf("duration cannot be negative")
	}
	if parseInt == 0 {
		return 0, fmt.Errorf("duration cannot be zero")
	}
	return durationUnit * time.Duration(parseInt), nil
}

func init() {
	createCmd.Flags().StringVarP(&tokenCreateParams.File, "file", "f", "", "The filename of the token to create")
	createCmd.Flags().BoolVarP(&tokenCreateParams.Public, "public", "p", false, "Set the token to be public")
	createCmd.Flags().StringVarP(&tokenCreateParams.Name, "name", "n", "", "The name of the token")
	createCmd.Flags().StringVar(&tokenCreateParams.DurationString, "duration", "", "The duration of the token, token will expire after the specified duration, e.g. \"30d\" for 30 day, \"1h\" for 1 hour")
	createCmd.Flags().StringVarP(&tokenCreateParams.ExpirationString, "expiration", "e", "", "The expiration datetime of the token (RFC3339), e.g. "+time.Now().Format(time.RFC3339))
	createCmd.Flags().StringVar(&tokenCreateParams.StartString, "start", "", "The start datetime of the token (RFC3339), if not specified, the token takes effect immediately after creation")
	createCmd.Flags().StringVarP(&tokenCreateParams.TypeString, "type", "t", "", "The type of token (personal, delegated)")
	createCmd.Flags().StringVarP(&tokenCreateParams.Scopes, "scopes", "s", "", "The scope(s) of the token.")
	createCmd.Flags().StringVarP(&tokenCreateParams.DelegatedContext, "delegated_context", "d", "", "Delegated context, to be used when type = delegated")
	_ = createCmd.Flags().MarkHidden("file")
	_ = createCmd.MarkFlagRequired("name")
	_ = createCmd.MarkFlagRequired("type")
	_ = createCmd.MarkFlagRequired("scopes")
	createCmd.MarkFlagsMutuallyExclusive("file", "public")
	createCmd.MarkFlagsMutuallyExclusive("file", "name")
	createCmd.MarkFlagsMutuallyExclusive("file", "expiration")
	createCmd.MarkFlagsMutuallyExclusive("file", "start")
	createCmd.MarkFlagsMutuallyExclusive("file", "type")
	createCmd.MarkFlagsMutuallyExclusive("file", "scopes")
	createCmd.MarkFlagsMutuallyExclusive("file", "delegated_context")
	createCmd.MarkFlagsMutuallyExclusive("file", "duration")
	createCmd.MarkFlagsMutuallyExclusive("duration", "expiration")

}
