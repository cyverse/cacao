package token

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	neturl "net/url"
	"os"
)

// tokenUpdateParams
var tokenUpdateParams = struct {
	service.TokenModel
	Filename string
}{}

var updateCmd = &cobra.Command{
	Use:   "update (-f file | <token_id> [flags])",
	Short: "Update a token",
	RunE:  tokenUpdate,
}

func tokenUpdate(cmd *cobra.Command, args []string) error {
	client := getTokenClient()
	if cmd.Flags().Changed("file") && len(args) > 0 {
		return fmt.Errorf("cannot use the file flag and arguments at the same time")
	} else if cmd.Flags().Changed("file") {
		data, err := os.ReadFile(tokenUpdateParams.Filename)
		if err != nil {
			return err
		}
		client.PrintDebug("Successfully read file '%s':\n%s\n", tokenUpdateParams.Filename, string(data))
		err = json.Unmarshal(data, &tokenUpdateParams.TokenModel)
		if err != nil {
			return err
		}
	} else {
		tokenID := common.TokenID(args[0])
		tokenUpdateParams.ID = common.ID(fmt.Sprintf("token-%s", tokenID.XID().String()))
	}
	client.PrintDebug("parsed token id %s", tokenUpdateParams.ID.String())
	req := client.NewRequest("PATCH", "/tokens/"+neturl.PathEscape(tokenUpdateParams.ID.String()), "")
	client.PrintDebug("sending request to '%s'", req.URL)
	client.DoRequest(req)
	return nil
}

func init() {
	// updatable fields: "name", "description", "public", "scopes"
	updateCmd.Flags().BoolVarP(&tokenUpdateParams.Public, "public", "p", false, "The public visibility of the token")
	updateCmd.Flags().StringVarP(&tokenUpdateParams.Description, "description", "d", "", "The token description")
	updateCmd.Flags().StringVarP(&tokenUpdateParams.Name, "name", "n", "", "The token name")
	updateCmd.Flags().StringVarP(&tokenUpdateParams.Scopes, "scopes", "s", "", "The scopes of the token")
	updateCmd.Flags().StringVarP(&tokenUpdateParams.Filename, "file", "f", "", "Filename with update params")
	_ = updateCmd.Flags().MarkHidden("file")
	updateCmd.MarkFlagsMutuallyExclusive("file", "public")
	updateCmd.MarkFlagsMutuallyExclusive("file", "description")
	updateCmd.MarkFlagsMutuallyExclusive("file", "name")
	updateCmd.MarkFlagsMutuallyExclusive("file", "scopes")
}
