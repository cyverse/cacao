package template

import (
	"fmt"
	"github.com/spf13/cobra"
)

var listVersionCmd = &cobra.Command{
	Use:          "versions [template-id] [flags]",
	Short:        "List template versions",
	Long:         `List all template versions for the template ID`,
	Args:         cobra.ExactArgs(1),
	RunE:         templateVersionList,
	SilenceUsage: true,
}

func templateVersionList(command *cobra.Command, args []string) error {
	client := getTemplateClient()
	req := client.NewRequest("GET", fmt.Sprintf("/templates/%s/versions", args[0]), "")
	return client.DoRequest(req)
}
