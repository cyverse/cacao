package workspace

import (
	neturl "net/url"

	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// getCmd represents the get command
var getCmd = &cobra.Command{
	Use:          "get [workspace-id] [flags]",
	Short:        "Get workspace(s)",
	Long:         `Gets all workspaces unless specific ID is provided`,
	Args:         cobra.RangeArgs(0, 1),
	RunE:         workspaceGet,
	SilenceUsage: true,
}

// Run ...
func workspaceGet(command *cobra.Command, args []string) error {
	client := getWorkspaceClient()
	if len(args) >= 1 {
		return getOneWorkspace(client, args[0])
	}

	req := client.NewRequest("GET", "/workspaces", "")
	return client.DoRequest(req)
}

func getOneWorkspace(client utils.Client, workspaceID string) error {
	client.PrintDebug("Parsed command line flags:\n  name: '%s'", workspaceID)
	req := client.NewRequest("GET", "/workspaces/"+neturl.PathEscape(workspaceID), "")
	return client.DoRequest(req)
}
