package provider

import (
	"bytes"
	"encoding/json"
	"io"
	neturl "net/url"
	"os"

	"github.com/fatih/color"
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao-common/common"
	"gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

var providerUpdateParams = struct {
	service.ProviderModel
	Filename         string
	MetadataFilename string
	MetadataStr      string
}{}

var updateCmd = &cobra.Command{
	Use:          "update (-f <filename> | <provider-id> [flags])",
	Short:        "Update a provider",
	RunE:         providerUpdate,
	Args:         utils.ExactNArgsOrFileFlag(1),
	SilenceUsage: true,
}

// Run ...
func providerUpdate(command *cobra.Command, args []string) error {
	client := getProviderClient()
	if command.Flags().Changed("file") {
		data, err := os.ReadFile(providerUpdateParams.Filename)
		if err != nil {
			color.Set(color.FgRed)
			return err
		}
		if err = json.Unmarshal(data, &providerUpdateParams.ProviderModel); err != nil {
			color.Set(color.FgRed)
			return err
		}
	} else {
		providerUpdateParams.ID = common.ID(args[0])
	}

	if command.Flags().Changed("metadata") {
		var metadata map[string]interface{}
		if err := json.Unmarshal([]byte(providerUpdateParams.MetadataStr), &metadata); err != nil {
			color.Set(color.FgRed)
			return err
		}
		providerUpdateParams.Metadata = metadata

	} else if command.Flags().Changed("metadata-file") {
		metadata, err := os.ReadFile(providerUpdateParams.MetadataFilename)
		if err != nil {
			color.Set(color.FgRed)
			return err
		}
		if err = json.Unmarshal(metadata, &providerUpdateParams.Metadata); err != nil {
			color.Set(color.FgRed)
			return err
		}

		client.PrintDebug("read metadata from file: %s and parsed it to: %v", providerUpdateParams.MetadataFilename, providerUpdateParams.Metadata)
	}

	client.PrintDebug("updating provider with the following parameters: %v", providerUpdateParams.ProviderModel)

	data, err := json.Marshal(providerUpdateParams.ProviderModel)
	if err != nil {
		color.Set(color.FgRed)
		return err
	}

	client.PrintDebug("Constructed request body: %s", string(data))
	req := client.NewRequest("PATCH", "/providers/"+neturl.PathEscape(providerUpdateParams.ID.String()), "")
	req.Body = io.NopCloser(bytes.NewReader(data))
	client.PrintDebug("Sending request to %s", req.URL.String())
	return client.DoRequest(req)
}

func init() {
	updateCmd.Flags().StringVarP(&providerUpdateParams.Filename, "file", "f", "", "JSON file containing the provider information")
	updateCmd.Flags().StringVarP(&providerUpdateParams.MetadataFilename, "metadata-file", "", "", "The file to read the metadata from. If provider is updated from a file, this will override the metadata in the file")
	updateCmd.Flags().StringVarP(&providerCreateParams.MetadataStr, "metadata", "m", "", `
	The provider metadata as a JSON string. For example:
	'{"key1": "value1", "key2": "value2"}'. If provider is updated from a file, this will override the metadata in the file `)
	updateCmd.Flags().StringVarP(&providerUpdateParams.Name, "name", "n", "", "The name of the provider")
	updateCmd.Flags().StringVarP(&providerUpdateParams.URL, "url", "u", "", "The url of the provider")
	updateCmd.MarkFlagsMutuallyExclusive("metadata-file", "metadata")
	updateCmd.MarkFlagsMutuallyExclusive("file", "name")
	updateCmd.MarkFlagsMutuallyExclusive("file", "url")
	updateCmd.Flags().MarkHidden("file")
}
