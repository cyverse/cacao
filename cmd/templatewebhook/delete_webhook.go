package templatewebhook

import (
	"fmt"
	"github.com/spf13/cobra"
)

var deleteWebhookCmd = &cobra.Command{
	Use:     "delete [template-id]",
	Aliases: []string{"del"},
	Short:   "delete template webhook",
	Args:    cobra.ExactArgs(1),
	RunE:    deleteWebhook,
}

func deleteWebhook(cmd *cobra.Command, args []string) error {
	client := getTemplateClient()

	templateID := args[0]
	req := client.NewRequest("DELETE", fmt.Sprintf("/templates/%s/webhooks", templateID), "")
	return client.DoRequest(req)
}
