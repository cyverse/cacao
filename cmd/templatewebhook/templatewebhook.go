package templatewebhook

import (
	"github.com/spf13/cobra"
	"gitlab.com/cyverse/cacao/cmd/utils"
)

// Cmd is root for template sub-command
var Cmd = &cobra.Command{
	Use:   "template_webhook",
	Short: "Manage template webhooks",
	Args:  cobra.NoArgs,
}

func init() {
	Cmd.AddCommand(listWebhookCmd)
	Cmd.AddCommand(createWebhookCmd)
	Cmd.AddCommand(deleteWebhookCmd)
}

func getTemplateClient() utils.Client {
	client := utils.NewClient()
	return client
}
