## Description

Jira: [Feature issue title](https://cyverse.atlassian.net/browse/NAFIGOS-<FEATURE_ISSUE_ID>).

Enter a brief description of the feature being added and how to use/test it.


## Checklist before merging Merge Requests
- [ ] New test(s) included to reproduce the bug/verify the feature
- [ ] Add an entry in the changelog
- [ ] Documentation created/updated (include links)
- [ ] Any changes necessary to the Kubernetes configuration/deployment are committed and deployed automatically.
- [ ] Reviewed and approved by at least one other contributor.
