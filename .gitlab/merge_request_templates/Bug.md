## Description

Jira: [Bug issue title](https://cyverse.atlassian.net/browse/NAFIGOS-<BUG_ISSUE_ID>).

### Steps to reproduce


### What I expected to happen


### What actually happened


### Notes

## Checklist before merging Merge Requests
- [ ] New test(s) included to reproduce the bug/verify the feature
- [ ] Add an entry in the changelog
- [ ] Documentation created/updated (include links)
- [ ] Any changes necessary to the Kubernetes configuration/deployment are committed and deployed automatically.
- [ ] Reviewed and approved by at least one other contributor.
