package api

import (
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"net/http"
)

// HealthCheckAPIRouter adds a health check endpoint, the endpoint can be used to check if api service is healthy (still responding to request).
// This is used by kubernetes readinessProbe and livenessProbe for the api service.
func HealthCheckAPIRouter(router *mux.Router) {
	router.HandleFunc("/health-check", healthCheck).Methods(http.MethodGet, http.MethodHead)
}

func healthCheck(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if r.Method == http.MethodGet {
		// TODO may consider return some useful metric information here.
		// Note that this endpoint is public, no auth required, so any information published here will be open to anyone on the internet.
		_, err := w.Write([]byte("{}"))
		if err != nil {
			log.WithField("function", "healthCheck").WithError(err).Error("fail to write response body")
		}
	}
	log.Debug("/health-check")
}
