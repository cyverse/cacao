package api

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
	"gitlab.com/cyverse/cacao/api-service/clients/interactivesessions"
	"gitlab.com/cyverse/cacao/api-service/utils"
	"io"
	"net/http"
	"strings"
)

// interactiveSessions is a single instance of the interactive sessions API implementation.
type interactiveSessionsAPI struct {
	interactiveSessionsClient interactivesessions.Client
}

// InteractiveSessionAPIRouter routes for operations related to interactive sessions.
func InteractiveSessionAPIRouter(interactiveSessionsClient interactivesessions.Client, router *mux.Router) {
	isapi := &interactiveSessionsAPI{
		interactiveSessionsClient: interactiveSessionsClient,
	}

	router.HandleFunc("/isessions", isapi.listInteractiveSessions).Methods("GET")
	router.HandleFunc("/isessions", isapi.createInteractiveSession).Methods("POST")
	router.HandleFunc("/isessions/{interactive_session_id}", isapi.getInteractiveSessionByID).Methods("GET")
	router.HandleFunc("/isessions/{interactive_session_id}", isapi.deactivateInteractiveSession).Methods("DELETE")
	router.HandleFunc("/isessions/instanceid/{instance_id}", isapi.getInteractiveSessionByInstanceID).Methods("GET")
	router.HandleFunc("/isessions/instanceaddr/{instance_address}", isapi.getInteractiveSessionByInstanceAddress).Methods("GET")
	router.HandleFunc("/isessions/check/{instance_address}", isapi.checkPrerequisitesForInteractiveSession).Methods("GET")
}

// unmarshalRequest reads the request body and unmarshal it.
func (isapi *interactiveSessionsAPI) unmarshalRequest(logger *log.Entry, w http.ResponseWriter, r *http.Request, dest interface{}) error {
	// Read the request body.
	requestBody, err := io.ReadAll(r.Body)
	if err != nil {
		errorMessage := "unable to read request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoCommunicationError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	// Unmarshal the request body.
	err = json.Unmarshal(requestBody, dest)
	if err != nil {
		errorMessage := "unable to decode the request body"
		logger.WithField("error", err).Error(errorMessage)
		cerr := cacao_common_service.NewCacaoMarshalError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return err
	}

	return nil
}

// getInteractiveSessionsClientSession obtains the interactive sessions client session for a request. Writes an error response and
// returns nil if the session can't be created.
func (isapi *interactiveSessionsAPI) getInteractiveSessionsClientSession(logger *log.Entry, w http.ResponseWriter, r *http.Request) interactivesessions.Session {
	actor, emulator, isAdmin := utils.GetCacaoHeaders(r)
	session, err := isapi.interactiveSessionsClient.Session(actor, emulator, isAdmin)
	if err != nil {
		logger.WithField("error", err).Error("unable to create the interactive sessions microservice client")
		utils.JSONCacaoError(logger, w, r, err)
		return nil
	}
	return session
}

// extractID extracts an ID from the URL path and returns it, if the ID cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (isapi *interactiveSessionsAPI) extractID(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string, idType string) (cacao_common.ID, error) {
	interactiveSessionID := cacao_common.ID(mux.Vars(r)[placeholder])
	if !interactiveSessionID.Validate() {
		errorMessage := fmt.Sprintf("invalid %s ID: %s", idType, interactiveSessionID)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return cacao_common.ID(""), cerr
	}
	return interactiveSessionID, nil
}

// extractString extracts a string from the URL path and returns it, if the string cannot be extracted, an error response is sent
// to the client, and an error is returned.
func (isapi *interactiveSessionsAPI) extractString(logger *log.Entry, w http.ResponseWriter, r *http.Request, placeholder string) (string, error) {
	str := mux.Vars(r)[placeholder]
	if len(str) == 0 {
		errorMessage := fmt.Sprintf("failed to extract string for placeholder %s", placeholder)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return "", cerr
	}
	return str, nil
}

// listInteractiveSessions implements the GET /isessions endpoint.
func (isapi *interactiveSessionsAPI) listInteractiveSessions(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "listInteractiveSessions",
	})
	logger.Info("api.listInteractiveSessions(): start")
	var err error

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the list of active interactive sessions
	interactiveSessions, err := session.ListInteractiveSessions()
	if err != nil {
		errorMessage := "error listing interactive sessions"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Guard against listInteractiveSessions returning nil.
	if interactiveSessions == nil {
		interactiveSessions = make([]cacao_common_http.InteractiveSession, 0)
	}

	// Format the response body.
	utils.ReturnStatus(w, interactiveSessions, http.StatusOK)
}

// getInteractiveSessionByID implements the GET /isessions/{interactive_session_id} endpoint.
// getInteractiveSessionByID returns the interactive session with the specified ID if it exists and the authenticated user may view it.
func (isapi *interactiveSessionsAPI) getInteractiveSessionByID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getInteractiveSessionByID",
	})
	logger.Info("api.getInteractiveSessionByID(): start")
	var err error

	// Extract the interactive session ID from the request and validate it.
	interactiveSessionID, err := isapi.extractID(logger, w, r, "interactive_session_id", "interactivesession")
	if err != nil {
		return
	}

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the interactive session.
	interactiveSession, err := session.GetInteractiveSession(interactiveSessionID)
	if err != nil {
		errorMessage := "error obtaining interactive session information"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	if !interactiveSession.ID.Validate() {
		errorMessage := fmt.Sprintf("interactive session not found: %s", interactiveSessionID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, interactiveSession, http.StatusOK)
}

// getInteractiveSessionByInstanceID implements the GET /isessions/instanceid/{instance_id} endpoint.
// getInteractiveSessionByInstanceID returns the interactive session with the specified instance ID if it exists and the authenticated user may view it.
func (isapi *interactiveSessionsAPI) getInteractiveSessionByInstanceID(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getInteractiveSessionByInstanceID",
	})
	logger.Info("api.getInteractiveSessionByInstanceID(): start")
	var err error

	// Extract the instance ID from the request and validate it.
	instanceID, err := isapi.extractString(logger, w, r, "instance_id")
	if err != nil {
		return
	}

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the interactive session.
	interactiveSession, err := session.GetInteractiveSessionByInstanceID(instanceID)
	if err != nil {
		errorMessage := "error obtaining interactive session information"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	if !interactiveSession.ID.Validate() {
		errorMessage := fmt.Sprintf("interactive session not found for instance ID: %s", instanceID)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, interactiveSession, http.StatusOK)
}

// getInteractiveSessionByInstanceAddress implements the GET /isessions/instanceaddr/{instance_address} endpoint.
// getInteractiveSessionByInstanceAddress returns the interactive session with the specified instance address if it exists and the authenticated user may view it.
func (isapi *interactiveSessionsAPI) getInteractiveSessionByInstanceAddress(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "getInteractiveSessionByInstanceAddress",
	})
	logger.Info("api.getInteractiveSessionByInstanceAddress(): start")
	var err error

	// Extract the instance address from the request and validate it.
	instanceAddress, err := isapi.extractString(logger, w, r, "instance_address")
	if err != nil {
		return
	}

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Get the interactive session.
	interactiveSession, err := session.GetInteractiveSessionByInstanceAddress(instanceAddress)
	if err != nil {
		errorMessage := "error obtaining interactive session information"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	if !interactiveSession.ID.Validate() {
		errorMessage := fmt.Sprintf("interactive session not found for instance address: %s", instanceAddress)
		cerr := cacao_common_service.NewCacaoNotFoundError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, interactiveSession, http.StatusOK)
}

// checkPrerequisitesForInteractiveSession implements the GET /isessions/check/{instance_address} endpoint.
// checkPrerequisitesForInteractiveSession checks if the instance meets prerequisites for the interactive session.
func (isapi *interactiveSessionsAPI) checkPrerequisitesForInteractiveSession(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "checkPrerequisitesForInteractiveSession",
	})
	logger.Info("api.checkPrerequisitesForInteractiveSession(): start")
	var err error

	// Extract the instance address from the request and validate it.
	instanceAddress, err := isapi.extractString(logger, w, r, "instance_address")
	if err != nil {
		return
	}

	// handle all parameter
	protocol := cacao_common_service.InteractiveSessionProtocolVNC
	proto := r.URL.Query().Get("protocol")
	switch strings.ToLower(proto) {
	case "ssh":
		protocol = cacao_common_service.InteractiveSessionProtocolSSH
	case "vnc":
		protocol = cacao_common_service.InteractiveSessionProtocolVNC
	default:
		errorMessage := fmt.Sprintf("failed to extract protocol for %s", proto)
		cerr := cacao_common_service.NewCacaoInvalidParameterError(errorMessage)
		utils.JSONCacaoError(logger, w, r, cerr)
		return
	}

	instanceAdminUsername := r.URL.Query().Get("user")

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Check prerequisites for setting up the protocol.
	check, err := session.CheckPrerequisitesForInteractiveSession(protocol, instanceAddress, instanceAdminUsername)
	if err != nil {
		errorMessage := "error checking prerequisites for setting up a new interactive session"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	utils.ReturnStatus(w, check, http.StatusOK)
}

// createInteractiveSession implements the POST /isessions endpoint.
func (isapi *interactiveSessionsAPI) createInteractiveSession(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "createInteractiveSession",
	})
	logger.Info("api.createInteractiveSession(): start")
	var err error

	// Unmarshal the request body.
	var incomingRequest cacao_common_http.InteractiveSession
	err = isapi.unmarshalRequest(logger, w, r, &incomingRequest)
	if err != nil {
		return
	}

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and validate the interactive session creation request.
	err = session.ValidateInteractiveSessionCreationRequest(incomingRequest)
	if err != nil {
		errorMessage := "interactive session creation request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the creation request.
	id, err := session.CreateInteractiveSession(incomingRequest)
	if err != nil {
		errorMessage := "interactive session creation request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}

// deactivateInteractiveSession implements the DELETE /isessions/{interactive_session_id} endpoint.
func (isapi *interactiveSessionsAPI) deactivateInteractiveSession(w http.ResponseWriter, r *http.Request) {
	logger := log.WithFields(log.Fields{
		"package":  "api",
		"function": "deactivateInteractiveSession",
	})
	logger.Info("api.deactivateInteractiveSession(): start")

	// Extract the interactive session ID from the request and validate it.
	interactiveSessionID, err := isapi.extractID(logger, w, r, "interactive_session_id", "interactivesession")
	if err != nil {
		return
	}

	// Create the interactive sessions client session.
	session := isapi.getInteractiveSessionsClientSession(logger, w, r)
	if session == nil {
		return
	}

	// Build and vaidate the interactive session deletion request.
	err = session.ValidateInteractiveSessionDeactivationRequest(interactiveSessionID)
	if err != nil {
		errorMessage := "interactive session deletion request validation failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Submit the deletion request.
	id, err := session.DeactivateInteractiveSession(interactiveSessionID)
	if err != nil {
		errorMessage := "interactive session deletion request submission failed"
		logger.WithField("error", err).Error(errorMessage)
		utils.JSONCacaoError(logger, w, r, err)
		return
	}

	// Format the response body.
	body := utils.NewAcceptedResponse(id, "")
	utils.ReturnStatus(w, body, http.StatusAccepted)
}
