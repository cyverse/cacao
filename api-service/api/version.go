package api

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/cyverse/cacao/api-service/utils"
)

// VersionAPIRouter setups version api that returns the version of cacao.
func VersionAPIRouter(router *mux.Router, gitCommit string) {
	var start = time.Now()
	router.HandleFunc("/version", func(w http.ResponseWriter, req *http.Request) {
		utils.ReturnStatus(w, map[string]string{
			"hash":           gitCommit,
			"execution_time": start.String(),
		}, http.StatusOK)
	}).Methods("GET")
}
