// Code generated by mockery v0.0.0-dev. DO NOT EDIT.

package mocks

import (
	common "gitlab.com/cyverse/cacao-common/common"
	http "gitlab.com/cyverse/cacao-common/http"

	mock "github.com/stretchr/testify/mock"
)

// Session is an autogenerated mock type for the Session type
type Session struct {
	mock.Mock
}

// CreateWorkspace provides a mock function with given fields: creationRequest
func (_m *Session) CreateWorkspace(creationRequest http.Workspace) (common.ID, error) {
	ret := _m.Called(creationRequest)

	var r0 common.ID
	if rf, ok := ret.Get(0).(func(http.Workspace) common.ID); ok {
		r0 = rf(creationRequest)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(http.Workspace) error); ok {
		r1 = rf(creationRequest)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteWorkspace provides a mock function with given fields: workspaceID
func (_m *Session) DeleteWorkspace(workspaceID common.ID) (common.ID, error) {
	ret := _m.Called(workspaceID)

	var r0 common.ID
	if rf, ok := ret.Get(0).(func(common.ID) common.ID); ok {
		r0 = rf(workspaceID)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(common.ID) error); ok {
		r1 = rf(workspaceID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetWorkspace provides a mock function with given fields: workspaceID
func (_m *Session) GetWorkspace(workspaceID common.ID) (http.Workspace, error) {
	ret := _m.Called(workspaceID)

	var r0 http.Workspace
	if rf, ok := ret.Get(0).(func(common.ID) http.Workspace); ok {
		r0 = rf(workspaceID)
	} else {
		r0 = ret.Get(0).(http.Workspace)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(common.ID) error); ok {
		r1 = rf(workspaceID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListWorkspaces provides a mock function with given fields:
func (_m *Session) ListWorkspaces() ([]http.Workspace, error) {
	ret := _m.Called()

	var r0 []http.Workspace
	if rf, ok := ret.Get(0).(func() []http.Workspace); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]http.Workspace)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateWorkspace provides a mock function with given fields: workspaceID, updateRequest
func (_m *Session) UpdateWorkspace(workspaceID common.ID, updateRequest http.Workspace) (common.ID, error) {
	ret := _m.Called(workspaceID, updateRequest)

	var r0 common.ID
	if rf, ok := ret.Get(0).(func(common.ID, http.Workspace) common.ID); ok {
		r0 = rf(workspaceID, updateRequest)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(common.ID, http.Workspace) error); ok {
		r1 = rf(workspaceID, updateRequest)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateWorkspaceFields provides a mock function with given fields: workspaceID, updateRequest, updateFields
func (_m *Session) UpdateWorkspaceFields(workspaceID common.ID, updateRequest http.Workspace, updateFields []string) (common.ID, error) {
	ret := _m.Called(workspaceID, updateRequest, updateFields)

	var r0 common.ID
	if rf, ok := ret.Get(0).(func(common.ID, http.Workspace, []string) common.ID); ok {
		r0 = rf(workspaceID, updateRequest, updateFields)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(common.ID, http.Workspace, []string) error); ok {
		r1 = rf(workspaceID, updateRequest, updateFields)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ValidateWorkspaceCreationRequest provides a mock function with given fields: creationRequest
func (_m *Session) ValidateWorkspaceCreationRequest(creationRequest http.Workspace) error {
	ret := _m.Called(creationRequest)

	var r0 error
	if rf, ok := ret.Get(0).(func(http.Workspace) error); ok {
		r0 = rf(creationRequest)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateWorkspaceDeletionRequest provides a mock function with given fields: workspaceID
func (_m *Session) ValidateWorkspaceDeletionRequest(workspaceID common.ID) error {
	ret := _m.Called(workspaceID)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID) error); ok {
		r0 = rf(workspaceID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateWorkspaceUpdateFieldsRequest provides a mock function with given fields: workspaceID, updateRequest, updateFields
func (_m *Session) ValidateWorkspaceUpdateFieldsRequest(workspaceID common.ID, updateRequest http.Workspace, updateFields []string) error {
	ret := _m.Called(workspaceID, updateRequest, updateFields)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, http.Workspace, []string) error); ok {
		r0 = rf(workspaceID, updateRequest, updateFields)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateWorkspaceUpdateRequest provides a mock function with given fields: workspaceID, updateRequest
func (_m *Session) ValidateWorkspaceUpdateRequest(workspaceID common.ID, updateRequest http.Workspace) error {
	ret := _m.Called(workspaceID, updateRequest)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, http.Workspace) error); ok {
		r0 = rf(workspaceID, updateRequest)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}
