// Code generated by mockery v2.22.1. DO NOT EDIT.

package mocks

import (
	common "gitlab.com/cyverse/cacao-common/common"
	http "gitlab.com/cyverse/cacao-common/http"

	mock "github.com/stretchr/testify/mock"
)

// Session is an autogenerated mock type for the Session type
type Session struct {
	mock.Mock
}

// CreateUserAction provides a mock function with given fields: creationRequest
func (_m *Session) CreateUserAction(creationRequest http.UserAction) (common.ID, error) {
	ret := _m.Called(creationRequest)

	var r0 common.ID
	var r1 error
	if rf, ok := ret.Get(0).(func(http.UserAction) (common.ID, error)); ok {
		return rf(creationRequest)
	}
	if rf, ok := ret.Get(0).(func(http.UserAction) common.ID); ok {
		r0 = rf(creationRequest)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	if rf, ok := ret.Get(1).(func(http.UserAction) error); ok {
		r1 = rf(creationRequest)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// DeleteUserAction provides a mock function with given fields: userActionID
func (_m *Session) DeleteUserAction(userActionID common.ID) (common.ID, error) {
	ret := _m.Called(userActionID)

	var r0 common.ID
	var r1 error
	if rf, ok := ret.Get(0).(func(common.ID) (common.ID, error)); ok {
		return rf(userActionID)
	}
	if rf, ok := ret.Get(0).(func(common.ID) common.ID); ok {
		r0 = rf(userActionID)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	if rf, ok := ret.Get(1).(func(common.ID) error); ok {
		r1 = rf(userActionID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetUserAction provides a mock function with given fields: userActionID
func (_m *Session) GetUserAction(userActionID common.ID) (http.UserAction, error) {
	ret := _m.Called(userActionID)

	var r0 http.UserAction
	var r1 error
	if rf, ok := ret.Get(0).(func(common.ID) (http.UserAction, error)); ok {
		return rf(userActionID)
	}
	if rf, ok := ret.Get(0).(func(common.ID) http.UserAction); ok {
		r0 = rf(userActionID)
	} else {
		r0 = ret.Get(0).(http.UserAction)
	}

	if rf, ok := ret.Get(1).(func(common.ID) error); ok {
		r1 = rf(userActionID)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ListUserActions provides a mock function with given fields:
func (_m *Session) ListUserActions() ([]http.UserAction, error) {
	ret := _m.Called()

	var r0 []http.UserAction
	var r1 error
	if rf, ok := ret.Get(0).(func() ([]http.UserAction, error)); ok {
		return rf()
	}
	if rf, ok := ret.Get(0).(func() []http.UserAction); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]http.UserAction)
		}
	}

	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateUserAction provides a mock function with given fields: userActionID, updateRequest
func (_m *Session) UpdateUserAction(userActionID common.ID, updateRequest http.UserAction) (common.ID, error) {
	ret := _m.Called(userActionID, updateRequest)

	var r0 common.ID
	var r1 error
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction) (common.ID, error)); ok {
		return rf(userActionID, updateRequest)
	}
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction) common.ID); ok {
		r0 = rf(userActionID, updateRequest)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	if rf, ok := ret.Get(1).(func(common.ID, http.UserAction) error); ok {
		r1 = rf(userActionID, updateRequest)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// UpdateUserActionFields provides a mock function with given fields: userActionID, updateRequest, updateFields
func (_m *Session) UpdateUserActionFields(userActionID common.ID, updateRequest http.UserAction, updateFields []string) (common.ID, error) {
	ret := _m.Called(userActionID, updateRequest, updateFields)

	var r0 common.ID
	var r1 error
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction, []string) (common.ID, error)); ok {
		return rf(userActionID, updateRequest, updateFields)
	}
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction, []string) common.ID); ok {
		r0 = rf(userActionID, updateRequest, updateFields)
	} else {
		r0 = ret.Get(0).(common.ID)
	}

	if rf, ok := ret.Get(1).(func(common.ID, http.UserAction, []string) error); ok {
		r1 = rf(userActionID, updateRequest, updateFields)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// ValidateUserActionCreationRequest provides a mock function with given fields: creationRequest
func (_m *Session) ValidateUserActionCreationRequest(creationRequest http.UserAction) error {
	ret := _m.Called(creationRequest)

	var r0 error
	if rf, ok := ret.Get(0).(func(http.UserAction) error); ok {
		r0 = rf(creationRequest)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateUserActionDeletionRequest provides a mock function with given fields: userActionID
func (_m *Session) ValidateUserActionDeletionRequest(userActionID common.ID) error {
	ret := _m.Called(userActionID)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID) error); ok {
		r0 = rf(userActionID)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateUserActionUpdateFieldsRequest provides a mock function with given fields: userActionID, updateRequest, updateFields
func (_m *Session) ValidateUserActionUpdateFieldsRequest(userActionID common.ID, updateRequest http.UserAction, updateFields []string) error {
	ret := _m.Called(userActionID, updateRequest, updateFields)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction, []string) error); ok {
		r0 = rf(userActionID, updateRequest, updateFields)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

// ValidateUserActionUpdateRequest provides a mock function with given fields: userActionID, updateRequest
func (_m *Session) ValidateUserActionUpdateRequest(userActionID common.ID, updateRequest http.UserAction) error {
	ret := _m.Called(userActionID, updateRequest)

	var r0 error
	if rf, ok := ret.Get(0).(func(common.ID, http.UserAction) error); ok {
		r0 = rf(userActionID, updateRequest)
	} else {
		r0 = ret.Error(0)
	}

	return r0
}

type mockConstructorTestingTNewSession interface {
	mock.TestingT
	Cleanup(func())
}

// NewSession creates a new instance of Session. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewSession(t mockConstructorTestingTNewSession) *Session {
	mock := &Session{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}
