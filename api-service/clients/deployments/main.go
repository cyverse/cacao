package deployments

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"time"

	cacaocommon "gitlab.com/cyverse/cacao-common/common"
	hm "gitlab.com/cyverse/cacao-common/http"
	"gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the Deployments microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// ListOption is options for listing deployments
type ListOption struct {
	WorkspaceID      cacaocommon.ID
	CurrentStatus    string
	AllCurrentStatus bool
	Offset           int
	PageSize         int
	// Return full run object for the current run
	FullRun bool
}

// Session is an interface for interacting with the Deployments microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	ListDeployments(option ListOption) ([]hm.Deployment, error)
	SearchDeployments(option service.DeploymentListOption) ([]hm.Deployment, error)
	ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error
	AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error)
	GetDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error)
	ValidateDeploymentUpdateRequest(deploymentID cacaocommon.ID, deployment *hm.DeploymentUpdate) error
	UpdateDeployment(deploymentID cacaocommon.ID, deployment *hm.DeploymentUpdate) (cacaocommon.ID, error)
	ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error
	DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error)
	ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error)
	ValidateDeploymentRebuildRequest(deploymentID cacaocommon.ID, params *hm.Parameters) error
	RebuildDeployment(deploymentID cacaocommon.ID, params *hm.Parameters) (cacaocommon.ID, error)
	GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (hm.DeploymentBuild, error)
	ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error)
	ValidateDeploymentRunRequest(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) error
	RunDeployment(deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun) (cacaocommon.ID, error)
	GetDeploymentRun(deploymentID, runID cacaocommon.ID) (hm.DeploymentRun, error)
	GetDeploymentLogsLocations(deploymentID cacaocommon.ID) ([]service.LogsLocation, error)
	GetDeploymentRunLogsLocations(deploymentID, runID cacaocommon.ID) ([]service.LogsLocation, error)
	RefreshStateOfDeployment(deploymentID cacaocommon.ID) (finished bool, err error)
}

// deploymentsClient is the primary Client implementation.
type deploymentsClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// New creates a new Deployments microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &deploymentsClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// Session returns a new Deployments microservice client session.
func (c *deploymentsClient) Session(actor, emulator string, isAdmin bool) (Session, error) {

	var actor1 = service.Actor{
		Actor:    actor,
		Emulator: emulator,
	}
	// At a minimum, the actor must be specified.
	if !actor1.IsSet() {
		return nil, fmt.Errorf("no actor specified")
	}
	// Define and return the session.
	session := deploymentsSession{
		queryConn: c.queryConn,
		eventConn: c.eventConn,
		actor:     actor1,
		isAdmin:   isAdmin,
	}
	return &session, nil
}

// deploymentsSession is the primary DeploymentsSession implementation.
type deploymentsSession struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
	actor     service.Actor
	isAdmin   bool
}

// returns a context to be used by service clients
func (s *deploymentsSession) getCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.TODO(), time.Second*5)
}

func (s *deploymentsSession) getSvcClient() (service.DeploymentClient, error) {
	return service.NewDeploymentClientFromConn(s.queryConn, s.eventConn)
}

func (s *deploymentsSession) getTemplateSvcClient() (service.TemplateClient, error) {
	return service.NewNatsTemplateClientFromConn(s.queryConn, s.eventConn)
}

// ListDeployments obtains a list of deployments, optionally filtered by workspace ID.
func (s *deploymentsSession) ListDeployments(option ListOption) ([]hm.Deployment, error) {
	if option.WorkspaceID != "" {
		// check workspace ID only if it is provided
		if err := checkWorkspaceID(option.WorkspaceID); err != nil {
			return nil, err
		}
	}
	return s.SearchDeployments(service.DeploymentListOption{
		SortBy:        service.SortByID,
		SortDirection: service.DescendingSort,
		Filter: service.DeploymentFilter{
			User:             s.actor.Actor,
			Workspace:        option.WorkspaceID,
			CurrentStatus:    service.DeploymentStatus(option.CurrentStatus),
			AllCurrentStatus: option.AllCurrentStatus,
		},
		Offset:        option.Offset,
		PageSize:      option.PageSize,
		FullRunObject: option.FullRun,
	})
}

// SearchDeployments lists the deployments that match the given criteria. This function does not validate any fields
// in the list option struct. This method is used by higher-level service clients rather than by API request handlers.
func (s *deploymentsSession) SearchDeployments(option service.DeploymentListOption) ([]hm.Deployment, error) {

	// Get the lower-level client.
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}

	// Perform the search.
	result, err := client.Search(context.TODO(), s.actor, option)
	if err != nil {
		return nil, err
	}

	// Convert the result to the model used by the API.
	deploymentList := make([]hm.Deployment, 0, result.GetSize())
	for _, deployment := range result.GetDeployments() {
		deploymentList = append(deploymentList, ServiceDeploymentToHTTP(deployment))
	}

	return deploymentList, nil
}

// ValidateDeploymentCreationRequest checks a deployment creation request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentCreationRequest(creationRequest *hm.Deployment) error {
	if err := checkWorkspaceID(cacaocommon.ID(creationRequest.WorkspaceID)); err != nil {
		return err
	}
	if err := checkTemplateID(cacaocommon.ID(creationRequest.TemplateID)); err != nil {
		return err
	}
	if err := checkProviderID(cacaocommon.ID(creationRequest.PrimaryProviderID)); err != nil {
		return err
	}
	for _, cred := range creationRequest.CloudCredentials {
		if cred == "" {
			return service.NewCacaoInvalidParameterError("cloud credential ID cannot be empty")
		}
	}
	if len(creationRequest.CloudCredentials) == 0 {
		return service.NewCacaoInvalidParameterError("no cloud credential ID is provided")
	}
	return nil
}

// AddDeployment creates a new deployment.
func (s *deploymentsSession) AddDeployment(creationRequest *hm.Deployment) (cacaocommon.ID, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}
	var svcCloudCreds = make(map[string]cacaocommon.ID)
	for _, credID := range creationRequest.CloudCredentials {
		// associate all credential in creation request to the primary provider
		svcCloudCreds[credID] = cacaocommon.ID(creationRequest.PrimaryProviderID)
	}
	ctx, cancelFunc := s.getCtx()
	deploymentID, err := client.Create(ctx, s.actor, messaging2.NewTransactionID(), service.DeploymentCreateParam{
		Name:             creationRequest.Name,
		Description:      creationRequest.Description,
		Workspace:        cacaocommon.ID(creationRequest.WorkspaceID),
		Template:         cacaocommon.ID(creationRequest.TemplateID),
		PrimaryCloud:     cacaocommon.ID(creationRequest.PrimaryProviderID),
		CloudProviders:   []cacaocommon.ID{}, // no other provider other than primary at present
		CloudCredentials: svcCloudCreds,
		GitCredential:    creationRequest.GitCredentialID,
	})
	cancelFunc()
	if err != nil {
		return "", err
	}
	return deploymentID, nil
}

// GetDeployment returns the deployment with the given ID if it exists and the user has permission to view it.
func (s *deploymentsSession) GetDeployment(deploymentID cacaocommon.ID) (hm.Deployment, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return hm.Deployment{}, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return hm.Deployment{}, err
	}
	ctx, cancelFunc := s.getCtx()
	deployment, err := client.Get(ctx, s.actor, deploymentID)
	cancelFunc()
	if err != nil {
		return hm.Deployment{}, err
	}
	if deployment == nil {
		return hm.Deployment{}, service.NewCacaoNotFoundError("returned deployment is nil")
	}
	httpDeployment := ServiceDeploymentToHTTP(*deployment)
	return httpDeployment, nil
}

// ValidateDeploymentUpdateRequest checks a deployment parameter update request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentUpdateRequest(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	if updateRequest.Name != nil && len(*updateRequest.Name) > 100 {
		return service.NewCacaoInvalidParameterError("deployment name too long")
	}
	if updateRequest.Description != nil && len(*updateRequest.Description) > 255 {
		return service.NewCacaoInvalidParameterError("deployment description too long")
	}
	if updateRequest.WorkspaceID != nil && !cacaocommon.ID(*updateRequest.WorkspaceID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad workspace id")
	}
	if updateRequest.TemplateID != nil && !cacaocommon.ID(*updateRequest.TemplateID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad template id")
	}
	if updateRequest.PrimaryProviderID != nil && !cacaocommon.ID(*updateRequest.PrimaryProviderID).Validate() {
		return service.NewCacaoInvalidParameterError("deployment has bad primary provider id")
	}
	if len(updateRequest.Parameters) > 0 {
		for _, keyValPair := range updateRequest.Parameters {
			if keyValPair.Key == "" {
				return service.NewCacaoInvalidParameterError("parameter name cannot be empty")
			}
		}
	}
	return nil
}

// UpdateDeployment updates parameters for an existing deployment.
func (s *deploymentsSession) UpdateDeployment(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) (cacaocommon.ID, error) {
	// TODO: deployment microservices have not implemented this
	//return "", service.NewCacaoNotImplementedError("not implemented")
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}
	deployment, updateFields := s.deploymentUpdateFields(deploymentID, updateRequest)
	ctx, cancelFunc := s.getCtx()
	runID, err := client.Update(ctx, s.actor, messaging2.NewTransactionID(), deployment, updateFields)
	cancelFunc()
	if err != nil {
		return "", err
	}
	return runID, nil
}

func (s *deploymentsSession) deploymentUpdateFields(
	deploymentID cacaocommon.ID, updateRequest *hm.DeploymentUpdate,
) (deployment service.Deployment, fields []string) {
	deployment.ID = deploymentID
	fields = make([]string, 0)
	if updateRequest.Owner != nil {
		deployment.Owner = *updateRequest.Owner
		fields = append(fields, "owner")
	}
	if updateRequest.Name != nil {
		deployment.Name = *updateRequest.Name
		fields = append(fields, "name")
	}
	if updateRequest.Description != nil {
		deployment.Description = *updateRequest.Description
		fields = append(fields, "description")
	}
	if updateRequest.WorkspaceID != nil {
		deployment.Workspace = cacaocommon.ID(*updateRequest.WorkspaceID)
		fields = append(fields, "workspace_id")
	}
	if updateRequest.TemplateID != nil {
		deployment.Template = cacaocommon.ID(*updateRequest.TemplateID)
		fields = append(fields, "template_id")
	}
	if updateRequest.PrimaryProviderID != nil {
		deployment.PrimaryCloud = cacaocommon.ID(*updateRequest.PrimaryProviderID)
		fields = append(fields, "primary_provider_id")
	}
	//if updateRequest.Parameters != nil {
	// TODO update parameter should be done via this call
	//}
	if updateRequest.CloudCredentials != nil {
		for _, credID := range updateRequest.CloudCredentials {
			// note, provider ID is left as blank
			deployment.CloudCredentials[credID] = ""
		}
		fields = append(fields, "cloud_credentials")
	}
	if updateRequest.GitCredentialID != nil {
		deployment.GitCredential = *updateRequest.GitCredentialID
		fields = append(fields, "git_credential_id")
	}
	return
}

// ValidateDeploymentDeletionRequest checks a deployment deletion request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentDeletionRequest(deploymentID cacaocommon.ID) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	return nil
}

// DeleteDeployment deletes an existing deployment.
func (s *deploymentsSession) DeleteDeployment(deploymentID cacaocommon.ID) (cacaocommon.ID, bool, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return "", false, err
	}
	ctx, cancelFunc := s.getCtx()
	deleted, err := client.Delete(ctx, s.actor, messaging2.NewTransactionID(), deploymentID)
	cancelFunc()
	if err != nil {
		return "", false, err
	}
	return deploymentID, deleted, nil
}

// ListDeploymentBuilds lists the builds for the given deployment.
// TODO: implement me
func (s *deploymentsSession) ListDeploymentBuilds(deploymentID cacaocommon.ID) ([]hm.DeploymentBuild, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return nil, err
	}
	return nil, service.NewCacaoNotImplementedError("not implemented")
}

// ValidateDeploymentRebuildRequest checks a deployment rebuild request to ensure that it's valid.
// TODO: implement me
func (s *deploymentsSession) ValidateDeploymentRebuildRequest(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	return service.NewCacaoNotImplementedError("not implemented")
}

// RebuildDeployment rebuilds an existing deployment.
// TODO: implement me
func (s *deploymentsSession) RebuildDeployment(
	deploymentID cacaocommon.ID, params *hm.Parameters,
) (cacaocommon.ID, error) {
	return "", service.NewCacaoNotImplementedError("not implemented")
}

// GetDeploymentBuild returns information about a single deployment build.
// TODO: implement me
func (s *deploymentsSession) GetDeploymentBuild(deploymentID, buildID cacaocommon.ID) (hm.DeploymentBuild, error) {
	err := checkDeploymentID(deploymentID)
	if err != nil {
		return hm.DeploymentBuild{}, err
	}
	// TODO check build ID
	return hm.DeploymentBuild{}, service.NewCacaoNotImplementedError("not implemented")
}

// ListDeploymentRuns lists the runs for the given deployment.
func (s *deploymentsSession) ListDeploymentRuns(deploymentID cacaocommon.ID) ([]hm.DeploymentRun, error) {
	err := checkDeploymentID(deploymentID)
	if err != nil {
		return nil, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}
	ctx, cancelFunc := s.getCtx()
	runList, err := client.SearchRun(ctx, s.actor, service.DeploymentRunListOption{
		Deployment: deploymentID,
		Offset:     0,
		PageSize:   -1,
	})
	cancelFunc()
	if err != nil {
		return nil, err
	}
	if runList.GetSize() == 0 {
		return []hm.DeploymentRun{}, nil
	}
	var httpRunList = make([]hm.DeploymentRun, 0, runList.GetSize())
	for _, run := range runList.GetRuns() {
		httpRunList = append(httpRunList, ServiceRunToHTTP(run))
	}
	return httpRunList, nil
}

// ValidateDeploymentRunRequest checks a deployment run request to ensure that it's valid.
func (s *deploymentsSession) ValidateDeploymentRunRequest(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) error {
	if err := checkDeploymentID(deploymentID); err != nil {
		return err
	}
	if err := checkDeploymentID(cacaocommon.ID(runRequest.DeploymentID)); err != nil {
		return err
	}
	return nil
}

// RunDeployment launches a new deployment run.
func (s *deploymentsSession) RunDeployment(
	deploymentID cacaocommon.ID, runRequest *hm.DeploymentRun,
) (cacaocommon.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "deployments",
		"function": "deploymentsSession.RunDeployment",
	})
	client, err := s.getSvcClient()
	if err != nil {
		return "", err
	}

	templateClient, err := s.getTemplateSvcClient()
	if err != nil {
		return "", err
	}
	logger.Trace("get deployment & template svc client")

	ctx, cancelFunc := s.getCtx()
	deployment, err := client.Get(ctx, s.actor, deploymentID)
	cancelFunc()
	if err != nil {
		return "", err
	}
	logger.Trace("fetched deployment")

	if !deployment.Template.Validate() {
		return "", service.NewCacaoInvalidParameterError("templateID is not valid")
	}

	ctx, cancelFunc = s.getCtx()
	template, err := templateClient.Get(ctx, s.actor, deployment.Template)
	cancelFunc()
	if err != nil {
		return "", err
	}
	logger.Trace("fetched template")

	meta := template.GetMetadata()

	parameterValues, err := HTTPParametersToServiceParameterValues(meta, runRequest.Parameters)
	if err != nil {
		return "", err
	}
	logger.Trace("converted parameters")

	ctx, cancelFunc = s.getCtx()
	run, err := client.CreateRun(ctx, s.actor, messaging2.NewTransactionID(), service.DeploymentRunCreateParam{
		Deployment:      deploymentID,
		TemplateVersion: cacaocommon.ID(runRequest.TemplateVersion),
		ParamValues:     parameterValues,
	})
	cancelFunc()
	if err != nil {
		logger.WithError(err).Trace("fail to create run")
		return "", err
	}
	logger.Trace("run created")
	return run.ID, nil
}

// GetDeploymentRun gets information about an existing deployment run.
func (s *deploymentsSession) GetDeploymentRun(deploymentID, runID cacaocommon.ID) (hm.DeploymentRun, error) {
	if err := checkDeploymentID(deploymentID); err != nil {
		return hm.DeploymentRun{}, err
	}
	if err := checkRunID(runID); err != nil {
		return hm.DeploymentRun{}, err
	}
	client, err := s.getSvcClient()
	if err != nil {
		return hm.DeploymentRun{}, err
	}
	ctx, cancelFunc := s.getCtx()
	run, err := client.GetRun(ctx, s.actor, deploymentID, runID)
	cancelFunc()
	if err != nil {
		return hm.DeploymentRun{}, err
	}
	httpRun := ServiceRunToHTTP(*run)
	return httpRun, nil
}

// GetDeploymentLogsLocations ...
func (s *deploymentsSession) GetDeploymentLogsLocations(deploymentID cacaocommon.ID) ([]service.LogsLocation, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}
	ctx, cancelFunc := s.getCtx()
	locations, err := client.ListDeploymentLogsLocations(ctx, s.actor, deploymentID)
	cancelFunc()
	return locations, err
}

// GetDeploymentRunLogsLocations ...
func (s *deploymentsSession) GetDeploymentRunLogsLocations(deploymentID, runID cacaocommon.ID) ([]service.LogsLocation, error) {
	client, err := s.getSvcClient()
	if err != nil {
		return nil, err
	}
	ctx, cancelFunc := s.getCtx()
	locations, err := client.ListRunLogsLocations(ctx, s.actor, deploymentID, runID)
	cancelFunc()
	return locations, err
}

// RefreshStateOfDeployment refresh the last state view of the deployment.
func (s *deploymentsSession) RefreshStateOfDeployment(deploymentID cacaocommon.ID) (finished bool, err error) {
	logger := log.WithFields(log.Fields{
		"package":    "deployments",
		"function":   "deploymentsSession.RunDeployment",
		"deployment": deploymentID,
	})
	client, err := s.getSvcClient()
	if err != nil {
		return false, err
	}
	ctx, cancelFunc := s.getCtx()
	state, err := client.RefreshState(ctx, s.actor, messaging2.NewTransactionID(), deploymentID)
	cancelFunc()
	if err != nil {
		logger.WithError(err).Trace("fail to refresh state")
		return false, err
	}
	if state == nil {
		logger.Debug("state refresh started")
		return false, nil
	}
	logger.Trace("state refreshed")
	return true, nil
}

func checkDeploymentID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("deployment ID is not valid")
	}
	if id.FullPrefix() != service.DeploymentIDPrefix {
		return service.NewCacaoInvalidParameterError("deployment ID is not valid")
	}
	return nil
}

func checkRunID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("run ID is not valid")
	}
	if id.FullPrefix() != service.RunIDPrefix {
		return service.NewCacaoInvalidParameterError("run ID is not valid")
	}
	return nil
}

func checkWorkspaceID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	if id.FullPrefix() != "workspace" {
		return service.NewCacaoInvalidParameterError("workspace ID is not valid")
	}
	return nil
}

func checkTemplateID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("template ID is not valid")
	}
	if id.FullPrefix() != "template" {
		return service.NewCacaoInvalidParameterError("template ID is not valid")
	}
	return nil
}

func checkProviderID(id cacaocommon.ID) error {
	if !id.Validate() {
		return service.NewCacaoInvalidParameterError("provider ID is not valid")
	}
	if id.PrimaryType() != "provider" {
		return service.NewCacaoInvalidParameterError("provider ID is not valid")
	}
	return nil
}
