package templates

import (
	"context"
	"fmt"
	"gitlab.com/cyverse/cacao-common/messaging2"
	"gitlab.com/cyverse/cacao/common"
	"time"

	log "github.com/sirupsen/logrus"
	cacao_common "gitlab.com/cyverse/cacao-common/common"
	cacao_common_http "gitlab.com/cyverse/cacao-common/http"
	cacao_common_service "gitlab.com/cyverse/cacao-common/service"
)

// Client is an interface for interacting with the Template microservice. Implementations of this interface
// should encapsulate information necessary to interact with the microservice, such as Nats and Stan connection
// information.
type Client interface {
	Session(actor, emulator string, isAdmin bool) (Session, error)
}

// Session is an interface for interacting with the Template microservice on behalf of a user. The purpose of
// having a session is to consolidate parameters that are common in all or most requests, but are not known at
// configuration time.
type Session interface {
	// TemplateType
	ListTemplateTypes() ([]cacao_common_http.TemplateType, error)
	ListTemplateTypesForProvideType(providerType cacao_common_service.TemplateProviderType) ([]cacao_common_http.TemplateType, error)
	GetTemplateType(templateTypeName cacao_common_service.TemplateTypeName) (cacao_common_http.TemplateType, error)

	CreateTemplateType(creationRequest cacao_common_http.TemplateType) (cacao_common_service.TemplateTypeName, error)
	ValidateTemplateTypeCreationRequest(creationRequest cacao_common_http.TemplateType) error
	UpdateTemplateType(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType) (cacao_common_service.TemplateTypeName, error)
	ValidateTemplateTypeUpdateRequest(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType) error
	UpdateTemplateTypeFields(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType, updateFields []string) (cacao_common_service.TemplateTypeName, error)
	ValidateTemplateTypeUpdateFieldsRequest(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType, updateFields []string) error
	DeleteTemplateType(templateTypeName cacao_common_service.TemplateTypeName) (cacao_common_service.TemplateTypeName, error)
	ValidateTemplateTypeDeletionRequest(templateTypeName cacao_common_service.TemplateTypeName) error

	// TemplateSourceType
	ListTemplateSourceTypes() ([]cacao_common_service.TemplateSourceType, error)

	// TemplateCustomFieldType
	ListTemplateCustomFieldTypes() ([]cacao_common_http.TemplateCustomFieldType, error)
	GetTemplateCustomFieldType(templateCustomFieldTypeName string) (cacao_common_http.TemplateCustomFieldType, error)
	QueryTemplateCustomFieldType(templateCustomFieldTypeName string, queryParams map[string]string) (cacao_common_http.TemplateCustomFieldTypeQueryResult, error)

	CreateTemplateCustomFieldType(creationRequest cacao_common_http.TemplateCustomFieldType) (string, error)
	ValidateTemplateCustomFieldTypeCreationRequest(creationRequest cacao_common_http.TemplateCustomFieldType) error
	UpdateTemplateCustomFieldType(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType) (string, error)
	ValidateTemplateCustomFieldTypeUpdateRequest(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType) error
	UpdateTemplateCustomFieldTypeFields(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType, updateFields []string) (string, error)
	ValidateTemplateCustomFieldTypeUpdateFieldsRequest(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType, updateFields []string) error
	DeleteTemplateCustomFieldType(templateCustomFieldTypeName string) (string, error)
	ValidateTemplateCustomFieldTypeDeletionRequest(templateCustomFieldTypeName string) error

	// Template
	ListPublicTemplates() ([]PublicTemplate, error)
	ListTemplates(includeCacaoReservedPurposes bool) ([]cacao_common_http.Template, error)
	GetTemplate(templateID cacao_common.ID) (cacao_common_http.Template, error)
	ListTemplateVersions(templateID cacao_common.ID) ([]cacao_common_http.TemplateVersion, error)
	GetTemplateVersion(templateVersionID cacao_common.ID) (cacao_common_http.TemplateVersion, error)

	ImportTemplate(importRequest cacao_common_http.Template, credentialID string) (cacao_common.ID, error)
	ValidateTemplateImportRequest(importRequest cacao_common_http.Template, credentialID string) error
	UpdateTemplate(templateID cacao_common.ID, updateRequest cacao_common_http.Template) (cacao_common.ID, error)
	ValidateTemplateUpdateRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template) error
	UpdateTemplateFields(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) (cacao_common.ID, error)
	ValidateTemplateUpdateFieldsRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) error
	DeleteTemplate(templateID cacao_common.ID) (cacao_common.ID, error)
	ValidateTemplateDeletionRequest(templateID cacao_common.ID) error
	SyncTemplate(templateID cacao_common.ID, credentialID string) (cacao_common.ID, error)
	ValidateTemplateSyncRequest(templateID cacao_common.ID, credentialID string) error

	// TemplateWebhook
	ListTemplateWebhooks() ([]cacao_common_service.TemplateWebhook, error)
	CreateTemplateWebhook(request cacao_common_service.TemplateWebhook) (TemplateWebhook, error)
	DeleteTemplateWebhook(templateID cacao_common.ID) error
}

// templatesClient is the primary Client implementation.
type templatesClient struct {
	queryConn messaging2.QueryConnection
	eventConn messaging2.EventConnection
}

// New creates a new Templates microservice client.
func New(queryConn messaging2.QueryConnection, eventConn messaging2.EventConnection) Client {
	return &templatesClient{
		queryConn: queryConn,
		eventConn: eventConn,
	}
}

// Session returns a new Templates microservice client session.
func (c *templatesClient) Session(actor string, emulator string, isAdmin bool) (Session, error) {
	// At a minimum, the actor must be specified.
	if actor == "" {
		return nil, cacao_common_service.NewCacaoInvalidParameterError("no actor specified")
	}
	serviceClient, err := cacao_common_service.NewNatsTemplateClientFromConn(c.queryConn, c.eventConn)
	if err != nil {
		return nil, err
	}
	webhookServiceClient, err := cacao_common_service.NewNatsTemplateWebhookClientFromConn(c.queryConn, c.eventConn)
	if err != nil {
		return nil, err
	}
	dependencyMediatorClient, err := cacao_common_service.NewDependencyMediatorClientFromConn(c.queryConn, c.eventConn)
	if err != nil {
		return nil, err
	}
	// Define and return the session.
	session := templatesSession{
		serviceClient:            serviceClient,
		webhookServiceClient:     webhookServiceClient,
		dependencyMediatorClient: dependencyMediatorClient,
		actor: cacao_common_service.Actor{
			Actor:    actor,
			Emulator: emulator,
		},
		isAdmin: isAdmin,
		context: context.Background(),
	}
	return &session, nil
}

// templatesSession is the primary TemplatesSession implementation.
type templatesSession struct {
	serviceClient            cacao_common_service.TemplateClient
	webhookServiceClient     cacao_common_service.TemplateWebhookClient
	dependencyMediatorClient cacao_common_service.DependencyMediatorClient
	actor                    cacao_common_service.Actor
	isAdmin                  bool
	context                  context.Context
}

func (s *templatesSession) convertTemplateTypeToHTTPObject(obj cacao_common_service.TemplateType) cacao_common_http.TemplateType {
	return cacao_common_http.TemplateType{
		Name:          obj.GetName(),
		Formats:       obj.GetFormats(),
		Engine:        obj.GetEngine(),
		ProviderTypes: obj.GetProviderTypes(),
	}
}

func (s *templatesSession) convertTemplateTypeToServiceObject(obj cacao_common_http.TemplateType) cacao_common_service.TemplateType {
	return &cacao_common_service.TemplateTypeModel{
		Name:          obj.Name,
		Formats:       obj.Formats,
		Engine:        obj.Engine,
		ProviderTypes: obj.ProviderTypes,
	}
}

func (s *templatesSession) convertTemplateCustomFieldTypeToHTTPObject(obj cacao_common_service.TemplateCustomFieldType) cacao_common_http.TemplateCustomFieldType {
	return cacao_common_http.TemplateCustomFieldType{
		Name:                      obj.GetName(),
		Description:               obj.GetDescription(),
		QueryMethod:               obj.GetQueryMethod(),
		QueryTarget:               obj.GetQueryTarget(),
		QueryData:                 obj.GetQueryData(),
		QueryResultJSONPathFilter: obj.GetQueryResultJSONPathFilter(),
	}
}

func (s *templatesSession) convertTemplateCustomFieldTypeToQueryResultHTTPObject(obj cacao_common_service.TemplateCustomFieldTypeQueryResult) cacao_common_http.TemplateCustomFieldTypeQueryResult {
	return cacao_common_http.TemplateCustomFieldTypeQueryResult{
		Name:     obj.GetName(),
		DataType: obj.GetDataType(),
		Value:    obj.GetValue(),
	}
}

func (s *templatesSession) convertTemplateCustomFieldTypeToServiceObject(obj cacao_common_http.TemplateCustomFieldType) cacao_common_service.TemplateCustomFieldType {
	return &cacao_common_service.TemplateCustomFieldTypeModel{
		Name:                      obj.Name,
		Description:               obj.Description,
		QueryMethod:               obj.QueryMethod,
		QueryTarget:               obj.QueryTarget,
		QueryData:                 obj.QueryData,
		QueryResultJSONPathFilter: obj.QueryResultJSONPathFilter,
	}
}

func (s *templatesSession) convertTemplateToHTTPObject(obj cacao_common_service.Template) cacao_common_http.Template {
	return cacao_common_http.Template{
		ID:              obj.GetID(),
		Owner:           obj.GetOwner(),
		Name:            obj.GetName(),
		Description:     obj.GetDescription(),
		Public:          obj.IsPublic(),
		Deleted:         obj.IsDeleted(),
		Source:          obj.GetSource(),
		LatestVersionID: obj.GetLatestVersionID(),
		Metadata:        obj.GetMetadata(),
		UIMetadata:      obj.GetUIMetadata(),
		CreatedAt:       obj.GetCreatedAt(),
		UpdatedAt:       obj.GetUpdatedAt(),
	}
}

func (s *templatesSession) convertTemplateToServiceObject(obj cacao_common_http.Template) cacao_common_service.Template {
	return &cacao_common_service.TemplateModel{
		ID:          obj.ID,
		Owner:       obj.Owner,
		Name:        obj.Name,
		Description: obj.Description,
		Public:      obj.Public,
		Source:      obj.Source,
	}
}

func (s *templatesSession) convertTemplateVersionToHTTPObject(obj cacao_common_service.TemplateVersion) cacao_common_http.TemplateVersion {
	return cacao_common_http.TemplateVersion{
		ID:         obj.ID,
		TemplateID: obj.TemplateID,
		Source:     obj.Source,
		Disabled:   obj.Disabled,
		DisabledAt: obj.DisabledAt,
		Metadata:   obj.Metadata,
		UIMetadata: obj.UIMetadata,
		CreatedAt:  obj.CreatedAt,
	}
}

func (s *templatesSession) getCtx() context.Context {
	ctx, _ := context.WithTimeout(s.context, time.Second*5)
	return ctx
}

// ListTemplateTypes obtains a list of template types.
func (s *templatesSession) ListTemplateTypes() ([]cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateTypes",
	})

	templateTypes, err := s.serviceClient.ListTypes(s.getCtx(), s.actor)
	if err != nil {
		msg := "failed to list template types"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateType, 0, len(templateTypes))
	for _, templateType := range templateTypes {
		httpObject := s.convertTemplateTypeToHTTPObject(templateType)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// ListTemplateTypesForProvideType obtains a list of template types that support the given provider type.
func (s *templatesSession) ListTemplateTypesForProvideType(providerType cacao_common_service.TemplateProviderType) ([]cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateTypesForProvideType",
	})

	templateTypes, err := s.serviceClient.ListTypesForProviderType(s.getCtx(), s.actor, providerType)
	if err != nil {
		msg := fmt.Sprintf("failed to list template types for provider type %s", providerType)
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateType, 0, len(templateTypes))
	for _, templateType := range templateTypes {
		httpObject := s.convertTemplateTypeToHTTPObject(templateType)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplateType returns the template type with the given name if it exists.
func (s *templatesSession) GetTemplateType(templateTypeName cacao_common_service.TemplateTypeName) (cacao_common_http.TemplateType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplateType",
	})

	templateType, err := s.serviceClient.GetType(s.getCtx(), s.actor, templateTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateType{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateTypeToHTTPObject(templateType)
	return httpObject, nil
}

// CreateTemplateType creates a new template type.
func (s *templatesSession) CreateTemplateType(creationRequest cacao_common_http.TemplateType) (cacao_common_service.TemplateTypeName, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateTemplateType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to create template type")
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(creationRequest)

	err := s.serviceClient.CreateType(s.getCtx(), s.actor, serviceObject)
	if err != nil {
		msg := "failed to create a template type"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return creationRequest.Name, nil
}

// ValidateTemplateTypeCreationRequest checks a template type creation request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeCreationRequest(creationRequest cacao_common_http.TemplateType) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	if len(creationRequest.Formats) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
	}

	if len(creationRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
	}

	if len(creationRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
	}

	return nil
}

// UpdateTemplateType updates the template type with the given name.
func (s *templatesSession) UpdateTemplateType(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType) (cacao_common_service.TemplateTypeName, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to update template type")
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateTypeName)

	err := s.serviceClient.UpdateType(s.getCtx(), s.actor, serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeUpdateRequest checks a template type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeUpdateRequest(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType) error {
	if len(templateTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	if len(updateRequest.Formats) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
	}

	if len(updateRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
	}

	if len(updateRequest.Engine) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
	}

	return nil
}

// UpdateTemplateTypeFields updates fields of the template type with the given name.
func (s *templatesSession) UpdateTemplateTypeFields(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType, updateFields []string) (cacao_common_service.TemplateTypeName, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateTypeFields",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to update template type")
	}

	// convert to service object
	serviceObject := s.convertTemplateTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateTypeName)

	err := s.serviceClient.UpdateTypeFields(s.getCtx(), s.actor, serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeUpdateFieldsRequest checks a template type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeUpdateFieldsRequest(templateTypeName cacao_common_service.TemplateTypeName, updateRequest cacao_common_http.TemplateType, updateFields []string) error {
	if len(templateTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template type name is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "formats":
			if len(updateRequest.Formats) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("formats are not valid")
			}
		case "engine":
			if len(updateRequest.Engine) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("engine is not valid")
			}
		case "provider_types":
			if len(updateRequest.ProviderTypes) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("provider types are not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTemplateType deletes an existing template type.
func (s *templatesSession) DeleteTemplateType(templateTypeName cacao_common_service.TemplateTypeName) (cacao_common_service.TemplateTypeName, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTemplateType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to delete template type")
	}

	err := s.serviceClient.DeleteType(s.getCtx(), s.actor, templateTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the template type for %s", templateTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateTypeName, nil
}

// ValidateTemplateTypeDeletionRequest checks a template type delete request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateTypeDeletionRequest(templateTypeName cacao_common_service.TemplateTypeName) error {
	if len(templateTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("type name is not valid")
	}

	return nil
}

// ListTemplateSourceTypes obtains a list of template source types.
func (s *templatesSession) ListTemplateSourceTypes() ([]cacao_common_service.TemplateSourceType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateSourceTypes",
	})

	templateSourceTypes, err := s.serviceClient.ListSourceTypes(s.getCtx(), s.actor)
	if err != nil {
		msg := "failed to list template source types"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	return templateSourceTypes, nil
}

// ListTemplateCustomFieldTypes obtains a list of template custom field types.
func (s *templatesSession) ListTemplateCustomFieldTypes() ([]cacao_common_http.TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateCustomFieldTypes",
	})

	templateCustomFieldTypes, err := s.serviceClient.ListCustomFieldTypes(s.getCtx(), s.actor)
	if err != nil {
		msg := "failed to list template custom field types"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateCustomFieldType, 0, len(templateCustomFieldTypes))
	for _, templateCustomFieldType := range templateCustomFieldTypes {
		httpObject := s.convertTemplateCustomFieldTypeToHTTPObject(templateCustomFieldType)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplateCustomFieldType returns the template custom field type with the name if it exists.
func (s *templatesSession) GetTemplateCustomFieldType(templateCustomFieldTypeName string) (cacao_common_http.TemplateCustomFieldType, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplateCustomFieldType",
	})

	templateCustomFieldType, err := s.serviceClient.GetCustomFieldType(s.getCtx(), s.actor, templateCustomFieldTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateCustomFieldType{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateCustomFieldTypeToHTTPObject(templateCustomFieldType)
	return httpObject, nil
}

// QueryTemplateCustomFieldType returns the query result with the template custom field type if it exists.
func (s *templatesSession) QueryTemplateCustomFieldType(templateCustomFieldTypeName string, queryParams map[string]string) (cacao_common_http.TemplateCustomFieldTypeQueryResult, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "QueryTemplateCustomFieldType",
	})

	queryResult, err := s.serviceClient.QueryCustomFieldType(s.getCtx(), s.actor, templateCustomFieldTypeName, queryParams)
	if err != nil {
		msg := fmt.Sprintf("failed to query to the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateCustomFieldTypeQueryResult{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateCustomFieldTypeToQueryResultHTTPObject(queryResult)
	return httpObject, nil
}

// CreateTemplateCustomFieldType creates a new template custom field type.
func (s *templatesSession) CreateTemplateCustomFieldType(creationRequest cacao_common_http.TemplateCustomFieldType) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "CreateTemplateCustomFieldType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to create template custom field type")
	}

	// convert to service object
	serviceObject := s.convertTemplateCustomFieldTypeToServiceObject(creationRequest)

	err := s.serviceClient.CreateCustomFieldType(s.getCtx(), s.actor, serviceObject)
	if err != nil {
		msg := "failed to create a template custom field type"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return creationRequest.Name, nil
}

// ValidateTemplateCustomFieldTypeCreationRequest checks a template custom field type creation request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateCustomFieldTypeCreationRequest(creationRequest cacao_common_http.TemplateCustomFieldType) error {
	if len(creationRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type name is not valid")
	}

	if len(creationRequest.QueryMethod) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query method is not valid")
	}

	if len(creationRequest.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query target is not valid")
	}

	return nil
}

// UpdateTemplateCustomFieldType updates the template custom field type with the given name.
func (s *templatesSession) UpdateTemplateCustomFieldType(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateCustomFieldType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to update template custom field type")
	}

	// convert to service object
	serviceObject := s.convertTemplateCustomFieldTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateCustomFieldTypeName)

	err := s.serviceClient.UpdateCustomFieldType(s.getCtx(), s.actor, serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateCustomFieldTypeName, nil
}

// ValidateTemplateCustomFieldTypeUpdateRequest checks a template custom field type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateCustomFieldTypeUpdateRequest(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType) error {
	if len(templateCustomFieldTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type name is not valid")
	}

	if len(updateRequest.QueryMethod) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query method is not valid")
	}

	if len(updateRequest.QueryTarget) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query target is not valid")
	}

	return nil
}

// UpdateTemplateCustomFieldTypeFields updates fields of the template custom field type with the given name.
func (s *templatesSession) UpdateTemplateCustomFieldTypeFields(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType, updateFields []string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateCustomFieldTypeFields",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to update template custom field type")
	}

	// convert to service object
	serviceObject := s.convertTemplateCustomFieldTypeToServiceObject(updateRequest)
	serviceObject.SetName(templateCustomFieldTypeName)

	err := s.serviceClient.UpdateCustomFieldTypeFields(s.getCtx(), s.actor, serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateCustomFieldTypeName, nil
}

// ValidateTemplateCustomFieldTypeUpdateFieldsRequest checks a template custom field type update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateCustomFieldTypeUpdateFieldsRequest(templateCustomFieldTypeName string, updateRequest cacao_common_http.TemplateCustomFieldType, updateFields []string) error {
	if len(templateCustomFieldTypeName) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type name is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "query_method":
			if len(updateRequest.QueryMethod) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query method is not valid")
			}
		case "query_target":
			if len(updateRequest.QueryTarget) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template custom field type query target is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTemplateCustomFieldType deletes an existing template custom field type.
func (s *templatesSession) DeleteTemplateCustomFieldType(templateCustomFieldTypeName string) (string, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTemplateCustomFieldType",
	})
	if !s.isAdmin {
		return "", cacao_common_service.NewCacaoUnauthorizedError("user is not authorized to delete template custom field type")
	}

	err := s.serviceClient.DeleteCustomFieldType(s.getCtx(), s.actor, templateCustomFieldTypeName)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the template custom field type for name %s", templateCustomFieldTypeName)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateCustomFieldTypeName, nil
}

// ValidateTemplateCustomFieldTypeDeletionRequest checks a template custom field type deletion request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateCustomFieldTypeDeletionRequest(name string) error {
	if len(name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template custom field type name is not valid")
	}
	return nil
}

// ListPublicTemplates list only the public template, the template object in the list is cut-down version of the normal http template.
func (s *templatesSession) ListPublicTemplates() ([]PublicTemplate, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": " ListPublicTemplates",
	})

	const includeCacaoReservedPurposes = false

	templates, err := s.serviceClient.List(s.getCtx(), s.actor, includeCacaoReservedPurposes)
	if err != nil {
		msg := "failed to list templates"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to public http object
	publicObjects := make([]PublicTemplate, 0, len(templates))
	for _, template := range templates {
		httpObject := s.convertTemplateToPublicObject(template)
		publicObjects = append(publicObjects, httpObject)
	}

	return publicObjects, nil
}

// PublicTemplate is a cut-down version from cacao_common_http.Template, because this is used for public api endpoint.
type PublicTemplate struct {
	ID          cacao_common.ID                     `json:"id"`
	Name        string                              `json:"name"`
	Description string                              `json:"description,omitempty"`
	Public      bool                                `json:"public"`
	Source      cacao_common_service.TemplateSource `json:"source"`
}

func (s *templatesSession) convertTemplateToPublicObject(template cacao_common_service.Template) PublicTemplate {
	return PublicTemplate{
		ID:          template.GetID(),
		Name:        template.GetName(),
		Description: template.GetDescription(),
		Public:      template.IsPublic(),
		Source:      template.GetSource(),
	}
}

// ListTemplates obtains a list of templates.
func (s *templatesSession) ListTemplates(includeCacaoReservedPurposes bool) ([]cacao_common_http.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplates",
	})

	templates, err := s.serviceClient.List(s.getCtx(), s.actor, includeCacaoReservedPurposes)
	if err != nil {
		msg := "failed to list templates"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.Template, 0, len(templates))
	for _, template := range templates {
		httpObject := s.convertTemplateToHTTPObject(template)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplate returns the template with the given name if it exists.
func (s *templatesSession) GetTemplate(templateID cacao_common.ID) (cacao_common_http.Template, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplate",
	})

	template, err := s.serviceClient.Get(s.getCtx(), s.actor, templateID)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.Template{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateToHTTPObject(template)
	return httpObject, nil
}

// ListTemplateVersions return a list of template version of a template.
func (s *templatesSession) ListTemplateVersions(templateID cacao_common.ID) ([]cacao_common_http.TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ListTemplateVersions",
	})

	templateVersions, err := s.serviceClient.ListTemplateVersions(s.getCtx(), s.actor, templateID)
	if err != nil {
		msg := "failed to list template versions"
		logger.WithField("error", err).Error(msg)
		return nil, err
	}

	// convert to http object
	httpObjects := make([]cacao_common_http.TemplateVersion, 0, len(templateVersions))
	for _, version := range templateVersions {
		httpObject := s.convertTemplateVersionToHTTPObject(version)
		httpObjects = append(httpObjects, httpObject)
	}

	return httpObjects, nil
}

// GetTemplateVersion returns the template version specified by its ID.
func (s *templatesSession) GetTemplateVersion(templateVersionID cacao_common.ID) (cacao_common_http.TemplateVersion, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "GetTemplateVersion",
	})
	templateVersion, err := s.serviceClient.GetTemplateVersion(s.getCtx(), s.actor, templateVersionID)
	if err != nil {
		msg := fmt.Sprintf("failed to get the template version for ID %s", templateVersionID)
		logger.WithField("error", err).Error(msg)
		return cacao_common_http.TemplateVersion{}, err
	}

	// convert to http object
	httpObject := s.convertTemplateVersionToHTTPObject(*templateVersion)
	return httpObject, nil
}

// ImportTemplate imports a template.
func (s *templatesSession) ImportTemplate(importRequest cacao_common_http.Template, credentialID string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "ImportTemplate",
	})

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(importRequest)

	tid, err := s.serviceClient.Import(s.getCtx(), s.actor, serviceObject, credentialID)
	if err != nil {
		msg := "failed to import a template"
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return tid, nil
}

// ValidateTemplateImportRequest checks a template import request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateImportRequest(importRequest cacao_common_http.Template, credentialID string) error {
	if len(importRequest.Source.URI) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
	}

	if len(importRequest.Source.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
	}

	// requires credential ID if source visibility is private
	if importRequest.Source.Visibility == cacao_common_service.TemplateSourceVisibilityPrivate {
		if len(credentialID) == 0 {
			return cacao_common_service.NewCacaoInvalidParameterError("credential ID is not valid")
		}
	}

	return nil
}

// UpdateTemplate updates the template with the given name.
func (s *templatesSession) UpdateTemplate(templateID cacao_common.ID, updateRequest cacao_common_http.Template) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplate",
	})

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(updateRequest)
	serviceObject.SetID(templateID)

	err := s.serviceClient.Update(s.getCtx(), s.actor, serviceObject)
	if err != nil {
		msg := fmt.Sprintf("failed to update the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateUpdateRequest checks a template update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateUpdateRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	if len(updateRequest.Name) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template name is not valid")
	}

	if len(updateRequest.Source.URI) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
	}

	if len(updateRequest.Source.Type) == 0 {
		return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
	}

	return nil
}

// UpdateTemplateFields updates fields of the template with the given name.
func (s *templatesSession) UpdateTemplateFields(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "UpdateTemplateFields",
	})

	// convert to service object
	serviceObject := s.convertTemplateToServiceObject(updateRequest)
	serviceObject.SetID(templateID)

	err := s.serviceClient.UpdateFields(s.getCtx(), s.actor, serviceObject, updateFields)
	if err != nil {
		msg := fmt.Sprintf("failed to update fields of the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateUpdateFieldsRequest checks a template update request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateUpdateFieldsRequest(templateID cacao_common.ID, updateRequest cacao_common_http.Template, updateFields []string) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	for _, field := range updateFields {
		switch field {
		case "name":
			if len(updateRequest.Name) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template name is not valid")
			}
		case "source":
			if len(updateRequest.Source.URI) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template source URI is not valid")
			}

			if len(updateRequest.Source.Type) == 0 {
				return cacao_common_service.NewCacaoInvalidParameterError("template source type is not valid")
			}
		default:
			// pass
		}
	}

	return nil
}

// DeleteTemplate deletes an existing template with dependency check.
func (s *templatesSession) DeleteTemplate(templateID cacao_common.ID) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "DeleteTemplate",
	})
	deletionResult, err := s.dependencyMediatorClient.DeleteTemplate(s.getCtx(), s.actor.Session(), templateID)
	if err != nil {
		msg := fmt.Sprintf("failed to delete the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}
	logger.WithField("deletionResult", deletionResult).Info("deletion result with dependency check")
	if err := deletionResult.ToError("template", templateID.String()); err != nil {
		return "", err
	}
	return templateID, nil
}

// ValidateTemplateDeletionRequest checks a template delete request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateDeletionRequest(templateID cacao_common.ID) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	return nil
}

// SyncTemplate syncs the template with the given name.
func (s *templatesSession) SyncTemplate(templateID cacao_common.ID, credentialID string) (cacao_common.ID, error) {
	logger := log.WithFields(log.Fields{
		"package":  "clients",
		"function": "SyncTemplate",
	})

	err := s.serviceClient.Sync(s.getCtx(), s.actor, templateID, credentialID)
	if err != nil {
		msg := fmt.Sprintf("failed to sync the template for ID %s", templateID)
		logger.WithField("error", err).Error(msg)
		return "", err
	}

	return templateID, nil
}

// ValidateTemplateSyncRequest checks a template sync request to ensure that it's valid.
func (s *templatesSession) ValidateTemplateSyncRequest(templateID cacao_common.ID, credentialID string) error {
	if !templateID.Validate() {
		return cacao_common_service.NewCacaoInvalidParameterError("template ID is not valid")
	}

	// credentialID may be empty if the source repo is public

	return nil
}

// ListTemplateWebhooks ...
func (s *templatesSession) ListTemplateWebhooks() ([]cacao_common_service.TemplateWebhook, error) {
	return s.webhookServiceClient.List(s.getCtx(), s.actor, s.actor.Actor, "")
}

// TemplateWebhook is the response body of webhook creation
type TemplateWebhook struct {
	cacao_common_service.TemplateWebhook `json:",inline"`
	// path of the webhook URL
	Path string `json:"path"`
}

// CreateTemplateWebhook ...
func (s *templatesSession) CreateTemplateWebhook(request cacao_common_service.TemplateWebhook) (TemplateWebhook, error) {
	webhook, err := s.webhookServiceClient.Create(s.getCtx(), s.actor, cacao_common_service.TemplateWebhook{
		TemplateID: request.TemplateID,
		Platform:   request.Platform,
	})
	if err != nil {
		return TemplateWebhook{}, err
	}
	return TemplateWebhook{
		TemplateWebhook: webhook,
		Path:            fmt.Sprintf(common.WebhookURLPathFormat, webhook.TemplateID, webhook.Platform),
	}, nil
}

// DeleteTemplateWebhook ...
func (s *templatesSession) DeleteTemplateWebhook(templateID cacao_common.ID) error {
	return s.webhookServiceClient.Delete(s.getCtx(), s.actor, templateID)
}
